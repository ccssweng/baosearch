/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package edu.miami.ccs.bao;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class to fetch substance summary for response endpoints
 * 
 * @author Sreeharsha Venkatapuram, UM Center for Computational Science
 * @version 1.0
 */
public class SubstanceSummary2 extends HttpServlet
{
	/**
     * Constant serialized ID used for compatibility.
     */
    private static final long serialVersionUID = 6969637801354420730L;
	Logger log = Logger.getLogger(this.getClass().getName());
	SessionFactory sessions;
	Session ses;

	/**
	 * Overrides the method in GenericServlet class to create an active hibernate session  
	 */
	public void init(ServletConfig config) throws ServletException
	{

		sessions = connect();
		ses = sessions.openSession();

	} 

	
	/* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
	    this.doGet(req, resp);
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		JSONObject jsonObj = new JSONObject();
		
		String input = request.getParameter("input");
		
		out.print("{\"data\": [");

		String QueryStr = "select distinct v.assay_id, pa.assay_name, ad.UniProt_ID, ep.ep_name, v.epval_num, ep.ep_unit," +
				"if(ad.TargetNucleicAcid2<>\"\",ad.TargetNucleicAcid2,if(ad.TargetNucleicAcid1<>\"\",ad.TargetNucleicAcid1," +
						"if(ad.TargetProtein2<>\"\",ad.TargetProtein2,if(ad.TargetProtein1<>\"\",ad.TargetProtein1," +
								"if(ad.BiologicalProcessTarget<>\"\",ad.BiologicalProcessTarget,ad.SignalingPathwayTarget))))) target," +
				"if(ad.`Format 5`<>\"\",ad.`Format 5`,if(ad.`Format 4`<>\"\",ad.`Format 4`,if(ad.`Format 3`<>\"\",ad.`Format 3`," +
						"if(ad.`Format 2`<>\"\",ad.`Format 2`,if(ad.`Format 1 MainClassification`<>\"\",ad.`Format 1 MainClassification`,\"\"))))) bao_format," +
				"ad.DesignMainConcept, ad.DetectionTechnology, ad.EndpointModeOfAction" +
				" from filter_redesign.epval v, annotation_data ad, pcassay pa, filter_redesign.ep_details ep " +
				" where v.ASSAY_ID=ad.aid and v.ASSAY_ID=ep.assay_id and v.tid=ep.tid and v.ASSAY_ID=pa.assay_aid and v.EPT_ID=ep.ept_id and ep.EPT_ID in(2,4,7,11,12,16) and v.sid="+input.trim()+" order by v.EPVAL_NUM desc";
		
		SQLQuery query = ses.createSQLQuery(QueryStr).addScalar("assay_id", Hibernate.STRING).addScalar("assay_name",  Hibernate.STRING).addScalar("UniProt_ID", Hibernate.STRING).addScalar("ep_name", Hibernate.STRING).addScalar("epval_num", Hibernate.STRING).addScalar("ep_unit", Hibernate.STRING).addScalar("target", Hibernate.STRING).addScalar("bao_format", Hibernate.STRING).addScalar("DesignMainConcept", Hibernate.STRING).addScalar("DetectionTechnology", Hibernate.STRING).addScalar("EndpointModeOfAction", Hibernate.STRING);
		
		Iterator<?> objects = null; 
		try
		{
			objects = query.list().iterator();
		}
		catch(HibernateException he)
		{
			log.info(he.getMessage());
		}

		if(objects.hasNext())	
		{
			while(objects.hasNext())
			{
				Object[] tuple = (Object[]) objects.next();
				try
	            {
		            jsonObj.put("assay_id", (String)tuple[0]);
		            jsonObj.put("name", (String)tuple[1]);
		            jsonObj.put("UniProt_ID", (String)tuple[2]);
		            jsonObj.put("ep_name", (String)tuple[3]);
		            jsonObj.put("epval_num", (String)tuple[4]);
		            jsonObj.put("ep_unit", (String)tuple[5]);
		            jsonObj.put("target", (String)tuple[6]);
		            jsonObj.put("bao_format", (String)tuple[7]);
		            jsonObj.put("DesignMainConcept", (String)tuple[8]);
		            jsonObj.put("DetectionTechnology", (String)tuple[9]);
		            jsonObj.put("EndpointModeOfAction", (String)tuple[10]);
		            out.print(jsonObj.toString());
		            if(objects.hasNext())
		            	out.print(",");
	            }
	            catch (JSONException e)
	            {
		            e.printStackTrace();
	            }
			}
			out.print("]}");
		}

	}

	/**
	 * Called by the init(...) method to generate hibernate Session object
	 * 
	 * @return Hibernate SessionFactory object
	 * 
	 */
	public static SessionFactory connect()
	{

		Configuration cfg = new Configuration();
		SessionFactory sessions = cfg.configure().buildSessionFactory();

		return sessions;
	}

}

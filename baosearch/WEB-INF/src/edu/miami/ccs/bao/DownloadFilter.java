/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package edu.miami.ccs.bao;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Servlet implementation class to query database for filter results and write to a CSV file
 * 
 * @author Sreeharsha Venkatapuram, Center for Computational Science
 * @version 1.0
 * 
 */
public class DownloadFilter extends HttpServlet
{	
	/**
     * Constant serialized ID used for compatibility.
     */
    private static final long serialVersionUID = 1820820982131503008L;
	/**
	 * Maximum length of the file to write
	 */
	private static final int FILE_LENGTH = Integer.MAX_VALUE;
	/**
	 * file name prefix for the file to be generated
	 */
	private static final String FILE_NAME = "filter_results";

	SessionFactory sessions;
	Session ses;
	SQLQuery filterQuery = null;

	private ServletConfig config;  
	Logger log = Logger.getLogger(getClass().getName());

	/**
	 * Overrides the method in GenericServlet class to create an active hibernate session  
	 */
	public void init(ServletConfig config) throws ServletException
	{
		this.config=config;  
		sessions = connect();
		ses = sessions.openSession();
	} 

	/* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
	    this.doGet(req, resp);
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String extension = ".csv";
		String delim = ",";
		String filename = FILE_NAME;
		Calendar cal = Calendar.getInstance();
		cal.getTimeInMillis();
		filename = filename + cal.getTimeInMillis() + extension;
		String[] columns = {"sid", "aid", "target", "ep_name", "ep_val", "ep_unit", "screening_conc", "qualifier"};
		makeFile(columns, filename, delim, request, response);
		
	}

	/**
	 * queries results and writes results to the file 
	 * @param columns - Array of column name headers
	 * @param filename - filename of the file to be generated (Default Value: "filter_results<current_time_in_millisecons>.csv")
	 * @param delimiter - Comma by default
	 * @param request - HttpServletRequest with parameters
	 * @param response - HttpServletResponse to write output to
	 * @throws ServletException 
	 * @throws IOException - Thrown when there was an error writing to file
	 */
	private void makeFile(String[] columns, String filename, String delimiter, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
		PrintWriter fwriter = new PrintWriter(filename);
		fwriter.print("There was an error in downloading file. Please try downloading again.");
		fwriter.close();
		ServletOutputStream sos = response.getOutputStream();
        ServletContext context = config.getServletContext();
        String mimetype = context.getMimeType( filename );
        
        response.setContentType( (mimetype != null) ? mimetype : "application/octet-stream" );
        response.setContentLength(FILE_LENGTH) ;
        response.setHeader( "Content-Disposition", "attachment; filename=\"" + filename + "\"" );

        int numColumns = columns.length;
        for(int k = 0; k < numColumns ; k++)
	    {
        	String str = columns[k] + delimiter ;
			sos.print(str);
	    }
        sos.println();
        filter(sos, request, response);
		sos.flush();

	    return;
    }

	/**
	 * Called by the init(...) method to generate hibernate Session object
	 * 
	 * @return Hibernate SessionFactory object
	 * 
	 */
	public static SessionFactory connect()
	{

		Configuration cfg = new Configuration();
		SessionFactory sessions = cfg.configure().buildSessionFactory();

		return sessions;
	}


	/**
	 * Prepares the input and output elements for the filter
	 * @param sos - ServletOutputStream object to output file
	 * @param request - HttpServletRequest
	 * @param response - HttpServletResponse
	 * @throws ServletException
	 * @throws IOException - Thrown when there was an error writing to sos ServletOutputStream object
	 */
	protected void filter(ServletOutputStream sos, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String aids = request.getParameter("aids");
//		String[] things = request.getParameterValues("concepts");
		String[] eptids = request.getParameterValues("ep"); 
		String[] epMins = request.getParameterValues("epMin"); 
		String[] epMaxs = request.getParameterValues("epMax");
		String sliceId = request.getParameter("sliceId");
		filterEndPoints(sos, aids, eptids, epMins, epMaxs, sliceId);
        
	}

	/**
	 * @param out - OutputStream to write to
	 * @param aids - List of comma separated assay IDs to query for
	 * @param eptids - Array of endpoint ids to query for
	 * @param epMins - Array of the minimum for endpoint values to query for, indexed corresponding to the endpoints from eptids parameter 
	 * @param epMaxs - Array of the maximum for endpoint values to query for, indexed corresponding to the endpoints from eptids parameter 
	 * @param sliceId - slice of the pie (concept ID) that was selected
	 * @throws IOException
	 */
	private void filterEndPoints(ServletOutputStream out, String aids, String[] eptids, String[] epMins, String[] epMaxs, String sliceId) throws IOException
    {
		String query = new String();
		if(sliceId == null)
		{
			query = "select distinct v.assay_id, v.sid, v.epval_num, ep.ep_name, ep.ep_unit, v.scr_conc, v.epval_qualifier, t.thing_name from filter_redesign.epval v, filter_redesign.ep_details ep, filter_redesign.epval_thing et, filter_redesign.thing t where v.assay_id=ep.assay_id and v.tid=ep.tid and v.assay_id in ("+aids+") and v.assay_id = et.assay_id and v.tid=ep.tid and et.thing_id=t.thing_id and v.tid=et.tid and v.ept_id = ep.ept_id and t.thing_type='meta target' and (";
			for(int i=0; i < eptids.length; i++)
			{
				query += "(v.ept_id = " + eptids[i] + " and v.epval_num > " + epMins[i] + " and v.epval_num < " + epMaxs[i] +"  ) ";
				if(i != (eptids.length-1))
				{
					query += " or ";
				}
			}
			

			query += ") order by v.sid, v.assay_id, ep.ep_name";
		}
		else
		{
			query = "select distinct v.assay_id, v.sid, v.epval_num, ep.ep_name, ep.ep_unit, v.scr_conc, v.epval_qualifier, t.thing_name from filter_redesign.epval v, filter_redesign.ep_details ep, filter_redesign.epval_thing et, filter_redesign.thing t where v.assay_id=ep.assay_id and v.assay_id in ("+aids+")  and v.assay_id = et.assay_id and v.tid=ep.tid and et.thing_id=t.thing_id and v.tid=et.tid and v.ept_id = ep.ept_id and et.thing_id = "+sliceId+" and t.thing_type='meta target' and (";
			for(int i=0; i < eptids.length; i++)
			{
				query += "(v.ept_id = " + eptids[i] + " and v.epval_num > " + epMins[i] + " and v.epval_num < " + epMaxs[i] +"  ) ";
				if(i != (eptids.length-1))
				{
					query += " or ";
				}
			}
			

			query += ") order by v.sid, v.assay_id, ep.ep_name";
		}
		
		SQLQuery q = ses.createSQLQuery(query).addScalar("assay_id", Hibernate.STRING).addScalar("sid", Hibernate.INTEGER).addScalar("epval_num", Hibernate.STRING).addScalar("ep_name", Hibernate.STRING).addScalar("ep_unit", Hibernate.STRING).addScalar("scr_conc", Hibernate.STRING).addScalar("epval_qualifier", Hibernate.STRING).addScalar("thing_name", Hibernate.STRING);
		ScrollableResults results = null;
		
		results = q.scroll(ScrollMode.FORWARD_ONLY);
		
		int loopCount = 0;
		String aid = new String(); 
		Integer sid = null; 
		String epName = null; 
		String relmirVal = null; 
		String epUnit = null; 
		String screeningConc = null;
		String qualifier = null;
		String thingName = new String();
		if(results != null)
		{
			while(results.next())
			{
				
				loopCount++;
				Object[] row = results.get();
				aid = (String)row[0]; //Fetch assay id
				sid = (Integer)row[1]; // Fetch substance id
				epName = (String)row[3]; //Fetch endpoint name
				relmirVal = (String)row[2]; //Fetch endpoint value
				epUnit = (String)row[4]; //Fetch endpoint unit
				screeningConc = (String)row[5]; // Fetch screening concentration
				qualifier = (String) row[6]; //Fetch qualifier
				thingName = (String) row[7]; // Fetch target
				out.print(sid+","+aid+","+thingName+","+epName+","+relmirVal+","+epUnit+","+screeningConc+","+qualifier);
				out.println();
			}	
		} // end if(results != null)

		
    }

}

/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package edu.miami.ccs.bao;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MultiHashMap;
import org.apache.commons.collections.MultiMap;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Servlet implementation class to search for all substances for the given smile strings
 * 
 * @author Sreeharsha Venkatapuram, akoleti - Center for Computational Science
 * @version 1.0
 */
public class SmilesSearch extends HttpServlet
{
	/**
     * Constant serialized ID used for compatibility.
     */
    private static final long serialVersionUID = -4438641931358238450L;
	Logger log = Logger.getLogger(this.getClass().getName());
	SessionFactory sessions;
	Session ses;

	/**
	 * Overrides the method in GenericServlet class to create an active hibernate session  
	 */
	public void init(ServletConfig config) throws ServletException
	{

		sessions = connect();
		ses = sessions.openSession();

	} 

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String input = request.getParameter("input");
		String[] tokens = input.split(",");
		String modifiedInput = new String();
		for(int i = 0; i < tokens.length; i++)
		{
			modifiedInput += "'"+tokens[i].trim()+"'";
			if(i != tokens.length-1)
				modifiedInput+=",";
		}
		
		String pageSizeStr = request.getParameter("pagesize");
		String pageNumStr = request.getParameter("pagenum");
		int pageSize=-1,pageNum=-1;
		try
		{
			pageSize = Integer.parseInt(pageSizeStr);
			pageNum = Integer.parseInt(pageNumStr);
		}
		catch(NumberFormatException nfe)
		{
			log.info("Difficulty in parsing pagenum details :: "+pageNumStr+"::"+pageSizeStr);
		}

		// sidCount 
		String countQry = "select count(distinct pr.sid) as sidcount from pccompound pc inner join pcassay_result pr on (pr.cid = pc.cid) where pc.CD_SMILES in ("+modifiedInput.trim()+")";
		SQLQuery countQuery = ses.createSQLQuery(countQry).addScalar("sidcount", Hibernate.INTEGER);
		Iterator countObject = null; 
		try
		{
			countObject = countQuery.list().iterator();
		}
		catch(HibernateException he)
		{
			log.info("Possibly a communications link failure, check database connectivity");
		}
		
		int sidCount = 0;

		if(countObject != null)	
		{
			sidCount = (Integer) countObject.next();
		}
		
		out.print("{\"sidCount\":\""+sidCount+"\",");


		out.print("\"data\": [");

		String QueryStr = "select distinct pr.sid as sid,pc.CID as cid,pc.IUPAC_NAME as name,pc.CD_SMILES as smiles,pc.CD_FORMULA as formula,pc.CD_MOLWEIGHT as molecularweight from pccompound pc inner join pcassay_result pr on (pr.cid = pc.cid) where pc.CD_SMILES in ("+modifiedInput.trim()+")";
		
		SQLQuery query = ses.createSQLQuery(QueryStr).addScalar("sid", Hibernate.STRING).addScalar("cid",  Hibernate.STRING).addScalar("name", Hibernate.STRING).addScalar("smiles", Hibernate.STRING).addScalar("formula", Hibernate.STRING).addScalar("molecularweight", Hibernate.STRING);
	
		if(pageSize == -1 || pageNum == -1)
		{
			pageSize = 100;
			pageNum = 0;
		}
		if(pageNum == 0)
			query.setFirstResult(pageNum);
		else
			query.setFirstResult((pageSize * (pageNum-1)));
		query.setMaxResults(pageSize);
		
		Iterator objects = null; 
		try
		{
			objects = query.list().iterator();
		}
		catch(HibernateException he)
		{
			log.info("Possibly a communications link failure, check database connectivity");
		}
		String[] arr = new String[6];
		if(objects!=null)	
		{
			while(objects.hasNext())
			{
				Object[] tuple = (Object[]) objects.next();
				
				arr[0] = (String)tuple[0]; //Fetch sid
				arr[1] = (String)tuple[1]; //Fetch cid
				arr[2] = (String)tuple[2]; //Fetch name
				arr[3] = (String)tuple[3]; //Fetch smiles
				arr[4] = (String)tuple[4]; //Fetch formula
				arr[5] = (String)tuple[5]; //Fetch molecularweight
				out.println("{" + "\"SubstanceId\":\"" + arr[0] + "\"," + "\"CompoundId\":\"" + arr[1] + "\"," + "\"Name\":\""
				        + arr[2] + "\"," + "\"smiles\":\"" + arr[3] + "\"," + "\"MolecularWeight\":\"" + arr[5] + "\"}");
				
				if (objects.hasNext())
					out.print(",");

			}

		}
		out.print("] }");
	}

	/**
	 * Called by the init(...) method to generate hibernate Session object
	 * 
	 * @return Hibernate SessionFactory object
	 * 
	 */
	public static SessionFactory connect()
	{

		Configuration cfg = new Configuration();
		SessionFactory sessions = cfg.configure().buildSessionFactory();

		return sessions;
	}

}

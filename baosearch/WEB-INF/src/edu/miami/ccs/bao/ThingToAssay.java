/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package edu.miami.ccs.bao;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Servlet implementation class to return assays related to a given concept
 * 
 * @author Sreeharsha Venkatapuram, Center for Computational Science
 * @version 1.0
 */
public class ThingToAssay extends HttpServlet
{
	/**
     * Constant serialized ID used for compatibility.
     */
    private static final long serialVersionUID = 5277824106059001883L;
	Logger log = Logger.getLogger(this.getClass().getName());
	SessionFactory sessions;
	Session ses;

	/**
	 * Overrides the method in GenericServlet class to create an active hibernate session  
	 */
	public void init(ServletConfig config) throws ServletException
	{
		sessions = connect();
		ses = sessions.openSession();
	} 

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String input = request.getParameter("sliceId");
		String thingId = request.getParameter("thingId");
		String[] things = request.getParameterValues("concepts");
		String aids = request.getParameter("aids");

		out.print("{\"assays\": [");
	
		String queryStr = "select distinct e.assay_id from filter_redesign.thing t, filter_redesign.epval_thing e, ";
		
		for(int j=0; j<things.length; j++)
		{
			queryStr += " filter_redesign.epval_thing e"+(j+1)+", filter_redesign.thing_bag tb"+(j+1);
			if ( j != (things.length - 1))
				queryStr += ", ";
		}
		queryStr += " where t.thing_id = e.thing_id and ";
		for(int j=0; j<things.length; j++)
		{
			queryStr += " e"+(j+1)+".thing_id = tb"+(j+1)+".child_thing_id ";
			if(j+1<things.length)
				queryStr += " and e"+(j+1)+".assay_id = e"+(j+2)+".assay_id ";
		}
		queryStr += " and e.assay_id in("+aids+") and t.thing_id="+input;
		
		SQLQuery query = ses.createSQLQuery(queryStr).addScalar("assay_id", Hibernate.STRING);;

		Iterator objects = null; 
		try
		{
			objects = query.list().iterator();
		}
		catch(HibernateException he)
		{
			log.info("Possibly a communications link failure, check database connectivity");
		}
		if(objects.hasNext())	
		{
			while(objects.hasNext())
			{
				String aid = (String) objects.next();
					
				out.println (aid);
				if (objects.hasNext())
					out.println(",");

			}
		}

		out.print("] }");
	}

	/**
	 * Called by the init(...) method to generate hibernate Session object
	 * 
	 * @return Hibernate SessionFactory object
	 * 
	 */
	public static SessionFactory connect()
	{

		Configuration cfg = new Configuration();
		SessionFactory sessions = cfg.configure().buildSessionFactory();

		return sessions;
	}

}

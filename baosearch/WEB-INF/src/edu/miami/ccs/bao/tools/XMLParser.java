/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package edu.miami.ccs.bao.tools;

import java.io.IOException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.log4j.BasicConfigurator;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * This class takes URL string which parses an XML to return a Document object
 * 
 * @author Sreeharsha Venkatapuram, Center for Computational Science
 * @version 1.0
 *
 */
public class XMLParser
{	
	/**
	 * Creates a Document object of the XML in the URL parameter
	 * @param url
	 * @return Document object which can be parsed further
	 * @throws XPathExpressionException
	 */
	public static Document parseXml(String url) throws XPathExpressionException
	{
		BasicConfigurator.configure();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		Document doc = null;

		try
		{
			dbf.setNamespaceAware(true);
			db = dbf.newDocumentBuilder();
		}
		catch (ParserConfigurationException pce)
		{

			pce.printStackTrace();
		}
		try
		{
			doc = db.parse(new URL(url).openStream());
		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
		}
		catch (SAXException se)
		{
			se.printStackTrace();
		}
		return doc;

	}

}

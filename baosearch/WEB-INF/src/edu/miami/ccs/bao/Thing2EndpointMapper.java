/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package edu.miami.ccs.bao;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Servlet implementation class to return EndPoints related to the selected concepts 
 * 
 * @author Sreeharsha Venkatapuram, akoleti - Center for Computational Science
 * @version 1.0
 */
public class Thing2EndpointMapper extends HttpServlet
{
	/**
     * Constant serialized ID used for compatibility.
     */
	private static final long serialVersionUID = 6516876365491739260L;
	SessionFactory sessions;
	Session ses;

	/**
	 * Overrides the method in GenericServlet class to create an active hibernate session  
	 */
	public void init(ServletConfig config) throws ServletException
	{
		sessions = connect();
		ses = sessions.openSession();
	} 

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String aids = request.getParameter("aids");
		String[] concepts = request.getParameterValues("concepts");
		
		out.print("{\"eptids\": [");
		SortedSet<String> epIdSet = new TreeSet<String>();
		String query = null;
		if(aids == null)
		{
			query = " select distinct ae.ept_id from filter_redesign.ep_details ae where ae.assay_id in (select assay_id from filter_redesign.epval_thing where thing_id in(select child_thing_id from filter_redesign.thing_bag where thing_id in(";
			for(int i = 0; i < concepts.length; i++)
			{
				query += concepts[i];
				if(i != concepts.length-1)
					query += ",";
				else
					query += "))) ";
			} 
		}
		else
		{
			String[] aidArr = aids.split(",");
			query = " select distinct ae.ept_id from filter_redesign.ep_details ae where ae.assay_id in (";
			for(int i = 0; i < aidArr.length; i++)
			{
				query += aidArr[i];
				if(i != aidArr.length-1)
					query += ",";
				else
					query += ")";
			}
		}	
		Query q = ses.createSQLQuery(query).addScalar("ept_id",Hibernate.STRING);       
		
		epIdSet.clear();

		try
		{
			Iterator objects = null;
			try
			{
				objects = q.list().iterator();
			}
			catch(HibernateException he)
			{
				sessions = connect();
				ses = sessions.openSession();
				objects = q.list().iterator();
			}
			if(objects != null)	
			{
				int k = 0;
				while(objects.hasNext())
				{
					String eptid = (String) objects.next();
					if(eptid != null)
						epIdSet.add(eptid);
				}
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		Iterator it = epIdSet.iterator();
		for(int i = 0; i < epIdSet.size(); i++)
		{
			out.print(it.next());
			if(i != epIdSet.size()-1)
				out.print(",");
		}
		out.print("]}");
	}

	/**
	 * Called by the init(...) method to generate hibernate Session object
	 * 
	 * @return Hibernate SessionFactory object
	 * 
	 */
	public static SessionFactory connect()
	{
		Configuration cfg = new Configuration();
		SessionFactory sessions = cfg.configure().buildSessionFactory();
		return sessions;
	}

}

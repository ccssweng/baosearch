/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package edu.miami.ccs.bao;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Feeds data from workset into the pie chart screen of endpoint filter's interface
 * 
 * @author Sreeharsha Venkatapuram, akoleti - Center for Computational Science
 * @version 1.0
 * 
 */
public class Workset2Chart extends HttpServlet
{
	/**
     * Constant serialized ID used for compatibility.
     */
    private static final long serialVersionUID = -7600607423094211669L;
	Logger log = Logger.getLogger(this.getClass().getName());
	SessionFactory sessions;
	Session ses;

	/**
	 * Overrides the method in GenericServlet class to create an active hibernate session  
	 */
	public void init(ServletConfig config) throws ServletException
	{

		sessions = connect();
		ses = sessions.openSession();

	} 

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String[] things = request.getParameterValues("concepts");
		String[] eptids = request.getParameterValues("ep"); 
		log.info("epLength::**************************************************"+eptids.length);
		String[] epMins = request.getParameterValues("epMin"); 
		String[] epMaxs = request.getParameterValues("epMax"); 
		String workset = request.getParameter("ws");
		String aids = new String();
		if(workset.equals("0"))
			aids = request.getParameter("aids"); 
		String sids = new String();
		if(workset.equals("1"))
			sids = request.getParameter("sids"); 
		
		String queryStr = new String();
		SortedSet<String> assaySet = new TreeSet<String>();
		out.print("{\"data\": [");

		for(int i=0; i < eptids.length; i++)
		{
			if(workset.equals("0"))
				queryStr = "select t.thing_id, t.thing_type, t.thing_name, count(distinct v.assay_id, v.sid, v.tid) as countof from filter_redesign.epval_thing e, filter_redesign.epval v, filter_redesign.thing t where  e.assay_id = v.assay_id and e.tid=v.tid and e.thing_id = t.thing_id and v.EPT_ID = "+eptids[i]+" and v.epval_num > "+epMins[i]+" and v.epval_num < "+epMaxs[i]+" and e.assay_id in ("+aids+")  group by t.thing_type, t.thing_name";
			else
			{
				queryStr = "select t.thing_id, t.thing_type, t.thing_name, count(distinct v.assay_id, v.sid, v.tid) as countof from filter_redesign.epval_thing e, filter_redesign.epval v, filter_redesign.thing t where  e.assay_id = v.assay_id and e.tid=v.tid and e.thing_id = t.thing_id and v.EPT_ID = "+eptids[i]+" and v.epval_num > "+epMins[i]+" and v.epval_num < "+epMaxs[i]+" and v.sid in ("+sids+")  group by t.thing_type, t.thing_name";
				String assaySetQryStr = "select distinct v.assay_id from filter_redesign.epval v where v.sid in ("+sids+") and v.EPT_ID = "+eptids[i]+" and v.epval_num > "+epMins[i]+" and v.epval_num < "+epMaxs[i];
			
				SQLQuery assaySetQuery = ses.createSQLQuery(assaySetQryStr).addScalar("assay_id", Hibernate.STRING);

				try
				{
					Iterator objects = null;
					try
					{
						objects = assaySetQuery.list().iterator();
					}
					catch(HibernateException he)
					{
						sessions = connect();
						ses = sessions.openSession();
						objects = assaySetQuery.list().iterator();
					}
					if(objects != null)	
					{
						int k = 0;
						while(objects.hasNext())
						{
							String aid = (String) objects.next();
							assaySet.add(aid);
						}
					}
					
				}
				catch(Exception e)
				{

				}
			}
			SQLQuery query = ses.createSQLQuery(queryStr).addScalar("thing_id", Hibernate.STRING).addScalar("thing_type", Hibernate.STRING).addScalar("thing_name",  Hibernate.STRING).addScalar("countof", Hibernate.STRING);
			
			Iterator objects = null; 
			try
			{
				objects = query.list().iterator();
			}
			catch(HibernateException he)
			{
				log.info("Possibly a communications link failure, check database connectivity");
			}
			
			String[] arr = new String[4];
			out.print("{\"EndpointName\"" +":" + "\""+eptids[i]+"\","  );
			out.print("\"Pies\" : [");
			boolean flag = false;
			if(objects != null)	
			{
				String pieName = "";
				String previousPieName = "";
				flag = true;
				int count = 0;
				while(objects.hasNext())
				{
					Object[] tuple = (Object[]) objects.next();
					flag = false;
					arr[0] = (String)tuple[1]; //Fetch thing_type
					arr[1] = (String)tuple[2]; //Fetch thing_name
					arr[2] = (String)tuple[3]; //Fetch countof
					arr[3] = (String)tuple[0]; //Fetch thing_id
					
					pieName = arr[0];
					if(count != 0)
					{	
						if(!(previousPieName.equalsIgnoreCase(pieName)))
						{
							
						}
						else
							out.print(",");
					}
							
					
					if(!(previousPieName.equalsIgnoreCase(pieName)))	
					{
						if(count != 0)
						{
							out.print("] },");

						}
						out.print("{" + "\"PieName\":\"" + arr[0] + "\"," );
						out.print("\"Slices\" : [");
					}
					out.print("{\"SliceName\":\"" + arr[1] + "\", \"Count\" : " + arr[2] + ", \"SliceId\":\""+arr[3]+"\"}"  );
					count++;
					previousPieName = pieName;
				}
				if(!flag)
					out.print("] }");
			}
			out.print("]}");
			if(i != (eptids.length-1))
				out.print(",");
		}
		if(workset.equals("0"))
			out.print("],\"aids\":\""+aids+"\"}"); 
		else
		{
			out.print("],\"aids\":\"");
			Iterator it = assaySet.iterator();
			for(int i = 0; i < assaySet.size(); i++)
			{
				out.print(it.next());
				if(i != assaySet.size()-1)
					out.print(",");
				
			}
			out.print("\"}");
		}
	}

	/**
	 * Called by the init(...) method to generate hibernate Session object
	 * 
	 * @return Hibernate SessionFactory object
	 * 
	 */
	public static SessionFactory connect()
	{
		Configuration cfg = new Configuration();
		SessionFactory sessions = cfg.configure().buildSessionFactory();
		return sessions;
	}

}




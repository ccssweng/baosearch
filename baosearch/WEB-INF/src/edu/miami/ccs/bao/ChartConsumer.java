/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package edu.miami.ccs.bao;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Consumes data from the pie chart screen of endpoint filter and gives out the
 *         details for the selected pie slice
 * @author Sreeharsha Venkatapuram, UM Center for Computational Science 
 * @version 1.0
 * 
 */

public class ChartConsumer extends HttpServlet
{
	/**
     * Constant serialized ID used for compatibility.
     */
    private static final long serialVersionUID = 1535337797821167906L;
	private static final Logger log = Logger.getLogger("ChartConsumer");
	SessionFactory sessions;
	Session ses;

	/**
	 * Overrides the method in GenericServlet class to create an active hibernate session  
	 */
	public void init(ServletConfig config) throws ServletException
	{

		sessions = connect();
		ses = sessions.openSession();

	} 

	//Handle posts by passing request to get
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doGet(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		Set<String> assaySet = new HashSet<String>();

		String pageSizeStr = request.getParameter("pagesize");
		String pageNumStr = request.getParameter("pagenum");
		int pageSize = -1, pageNum = -1;
		try
		{
			pageSize = Integer.parseInt(pageSizeStr);
			pageNum = Integer.parseInt(pageNumStr);
		}
		catch (NumberFormatException nfe)
		{
			log.info("Difficulty in parsing pagenum details :: " + pageNumStr + "::" + pageSizeStr);
		}
		// get all parameters sent along with request
		String thing_id = request.getParameter("sliceId");
		String sortElement = request.getParameter("sort");
		String eptid = request.getParameter("ep");
		String epMin = request.getParameter("epMin");
		String epMax = request.getParameter("epMax");
		String assayids = "";
		String sids = "";
		String[] things = request.getParameterValues("concepts[]");

		String mode = request.getParameter("mode");
		if (mode.equalsIgnoreCase("assayWs"))
			assayids = request.getParameter("aids");
		else if (mode.equalsIgnoreCase("cpdWs"))
			sids = request.getParameter("sids");
		else
		{
			// fetch aids

			String queryStrAids = "select v.assay_id from filter_redesign.thing t, filter_redesign.epval_thing e, filter_redesign.epval v,";
			for (int j = 0; j < things.length; j++)
			{
				queryStrAids += " filter_redesign.epval_thing e" + (j + 1) + ", filter_redesign.thing_bag tb" + (j + 1);
				if (j != (things.length - 1))
					queryStrAids += ", ";
			}

			queryStrAids += " where t.thing_id = e.thing_id AND v.assay_id = e.assay_id AND e.tid = v.tid ";
			for (int j = 0; j < things.length; j++)
			{
				queryStrAids += " and e" + (j + 1) + ".thing_id = tb" + (j + 1) + ".child_thing_id and ";
				queryStrAids += " tb" + (j + 1) + ".thing_id = " + things[j] + " and v.tid=e" + (j + 1) + ".tid ";
				if (j + 1 < things.length)
				{
					queryStrAids += " and e" + (j + 1) + ".assay_id = e" + (j + 2) + ".assay_id ";
				}

			}

			queryStrAids += " and v.assay_id =e1.assay_id  and v.EPT_ID = " + eptid + " and v.epval_num > " + epMin + " and v.epval_num < " + epMax
			        + " group by v.assay_id";

			SQLQuery queryAids = ses.createSQLQuery(queryStrAids).addScalar("assay_id", Hibernate.STRING);
			Iterator objectsAids = null;
			try
			{
				objectsAids = queryAids.list().iterator();
			}
			catch (HibernateException he)
			{
				log.info("Possibly a communications link failure, check database connectivity");
			}
			if (objectsAids != null)
			{
				while (objectsAids.hasNext())
				{
					String aid = (String) objectsAids.next(); // Fetch assay_id
					assaySet.add(aid);
				}
			}
			Object[] objArr = assaySet.toArray();
			String[] assayArr = new String[objArr.length];

			for (int z = 0; z < assayArr.length; z++)
			{
				assayArr[z] = objArr[z].toString();
				assayids += assayArr[z];
				if (z != assayArr.length - 1)
					assayids += ",";
			}

		}

		String queryStr = null;
		out.println("{\"data\": [");
		String countQry = "";
		if (mode.equalsIgnoreCase("cpdWs"))
			countQry = "select count(distinct v.sid) as sidcount from filter_redesign.epval_thing e, filter_redesign.epval v where e.assay_id = v.assay_id and e.tid = v.tid and v.EPT_ID = "
			        + eptid
			        + " and v.epval_num > "
			        + epMin
			        + " and v.epval_num < "
			        + epMax
			        + " and e.THING_ID in("
			        + thing_id
			        + ") and v.sid in ( "
			        + sids + ")";
		else
			countQry = "select count(distinct v.sid) as sidcount from filter_redesign.epval_thing e, filter_redesign.epval v where e.assay_id = v.assay_id and e.tid = v.tid and v.EPT_ID = "
			        + eptid
			        + " and v.epval_num > "
			        + epMin
			        + " and v.epval_num < "
			        + epMax
			        + " and e.THING_ID in("
			        + thing_id
			        + ") and e.assay_id in ( " + assayids + ")";
		SQLQuery countQuery = ses.createSQLQuery(countQry).addScalar("sidcount", Hibernate.INTEGER);
		Iterator countObject = null;
		try
		{
			countObject = countQuery.list().iterator();
		}
		catch (HibernateException he)
		{
			log.info("Possibly a communications link failure, check database connectivity");
		}

		int sidCount = 0;

		if (countObject != null)
		{
			sidCount = (Integer) countObject.next();
		}
		String sortElt = null;
		if (sortElement.equals("min"))
			sortElt = "rangeMin asc";
		else
			sortElt = "rangeMax desc";

		SQLQuery query = null;
		queryStr = "select v.sid, count(distinct v.assay_id, v.sid, v.tid) as epcount, min(v.epval_num) as rangeMin, max(v.epval_num) as rangeMax, v.ASSAY_ID  from filter_redesign.thing t, filter_redesign.epval_thing e, filter_redesign.epval v, ";

		if (!mode.equalsIgnoreCase("assayWs"))
		{
			for (int j = 0; j < things.length; j++)
			{
				queryStr += " filter_redesign.epval_thing e" + (j + 1) + ", filter_redesign.thing_bag tb" + (j + 1);
				if (j != (things.length - 1))
					queryStr += ", ";
			}
			queryStr += " where t.thing_id = e.thing_id AND v.assay_id = e.assay_id AND e.tid = v.tid  ";
			for (int j = 0; j < things.length; j++)
			{
				queryStr += " and e" + (j + 1) + ".thing_id = tb" + (j + 1) + ".child_thing_id and ";
				queryStr += " tb" + (j + 1) + ".thing_id = " + thing_id + " and v.tid=e" + (j + 1) + ".tid ";
				if (j + 1 < things.length)
				{
					queryStr += " and e" + (j + 1) + ".assay_id = e" + (j + 2) + ".assay_id ";
				}
			}

			//queryStr for assays
			if (mode.equalsIgnoreCase("cpdWs"))
			{
				queryStr += " and v.sid in (" + sids + ") ";
			}
			else
			{
				queryStr += " and v.assay_id in (" + assayids + ")";
			}
			queryStr += "and v.assay_id =e1.assay_id and v.EPT_ID = " + eptid + " and v.epval_num > " + epMin + " and v.epval_num < " + epMax
			        + " group by v.sid order by " + sortElt;
			query = ses.createSQLQuery(queryStr).addScalar("sid", Hibernate.INTEGER).addScalar("epcount", Hibernate.INTEGER)
			        .addScalar("rangeMin", Hibernate.FLOAT).addScalar("rangeMax", Hibernate.FLOAT).addScalar("assay_id", Hibernate.INTEGER);

		}
		else
		{

			queryStr += " filter_redesign.epval_thing e1 ";
			queryStr += " where t.thing_id = e.thing_id AND v.assay_id = e.assay_id AND e.tid = v.tid and ";

			queryStr += " e.thing_id = " + thing_id;

			//queryStr for assays
			if (mode.equalsIgnoreCase("cpdWs"))
			{
				queryStr += " and v.sid in (" + sids + ") ";
			}
			else
			{
				queryStr += " and v.assay_id in (" + assayids + ")";
			}
			queryStr += "and v.assay_id =e1.assay_id and v.EPT_ID = " + eptid + " and v.epval_num > " + epMin + " and v.epval_num < " + epMax
			        + " group by v.sid order by " + sortElt;
			query = ses.createSQLQuery(queryStr).addScalar("sid", Hibernate.INTEGER).addScalar("epcount", Hibernate.INTEGER)
			        .addScalar("rangeMin", Hibernate.FLOAT).addScalar("rangeMax", Hibernate.FLOAT).addScalar("assay_id", Hibernate.INTEGER);

		}

		if (pageSize == -1 || pageNum == -1)
		{
			pageSize = 100;
			pageNum = 0;
		}
		if (pageNum == 0)
			query.setFirstResult(pageNum);
		else
			query.setFirstResult((pageSize * (pageNum - 1)));
		query.setMaxResults(pageSize);

		Iterator objects = null;
		try
		{
			objects = query.list().iterator();
		}
		catch (HibernateException he)
		{
			log.info("Possibly a communications link failure, check database connectivity");
		}

		int aid, sid, count;
		float rangeMin, rangeMax;

		if (objects != null)
		{

			while (objects.hasNext())
			{
				Object[] tuple = (Object[]) objects.next();

				sid = (Integer) tuple[0]; //Fetch sid
				count = (Integer) tuple[1]; //Fetch epCount
				rangeMin = (Float) tuple[2]; //Fetch min
				rangeMax = (Float) tuple[3]; //Fetch max
				aid = (Integer) tuple[4]; //Fetch aid
				if (rangeMin != rangeMax)
					out.println("{\"sid\":" + sid + ", \"count\" : " + count + ", \"range\" : \"" + rangeMin + " - " + rangeMax + "\", \"aid\" : "
					        + aid + "}");
				else
					out.println("{\"sid\":" + sid + ", \"count\" : " + count + ", \"range\" : \"" + rangeMin + "\", \"aid\" : " + aid + "}");
				if (objects.hasNext())
					out.println(",");
			}

		}

		out.println("],\"sidCount\":\"" + sidCount + "\", \"selectedEpMin\":\"" + epMin + "\", \"selectedEpMax\":\"" + epMax + "\"}");

	}

	/**
	 * Called by the init(...) method to generate hibernate Session object
	 * 
	 * @return Hibernate SessionFactory object
	 * 
	 */
	public static SessionFactory connect()
	{

		Configuration cfg = new Configuration();
		SessionFactory sessions = cfg.configure().buildSessionFactory();

		return sessions;
	}

}

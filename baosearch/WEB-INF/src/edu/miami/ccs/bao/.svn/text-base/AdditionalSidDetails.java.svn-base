/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package edu.miami.ccs.bao;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Provide activity on other assays for the given substance and given endpoint
 * 
 * @author Sreeharsha Venkatapuram, UM Center for Computational Science 
 * @version 1.0 
 * 
 */
public class AdditionalSidDetails extends HttpServlet
{

    private static final long serialVersionUID = 3658939572299350376L;
	private static final Logger log = Logger.getLogger("AdditionalSidDetails");
	SessionFactory sessions;
	Session ses;

	/**
	 * Overrides the method in GenericServlet class to create an active hibernate session  
	 */
	public void init(ServletConfig config) throws ServletException
	{

		sessions = connect();
		ses = sessions.openSession();

	} 

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doGet(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("{\"data\": [");

		String selectedEp = request.getParameter("selectedEp");
		String epMins[] = request.getParameterValues("epMin");
		String epMaxs[] = request.getParameterValues("epMax");
		String eptids[] = request.getParameterValues("ep");
		String epMin = null;
		String epMax = null;
		String eptid = null;
		//list of non-concentration endpoint ids from ep_list (Not supposed to be hard coded), subject to change
		String[] nonConcEpArr = { "1", "3", "5", "6", "8", "9", "10", "13", "14", "15", "17", "18", "19", "20" };
		List<String> nonConcEpList = Arrays.asList(nonConcEpArr);
		String flag = null;
		if (nonConcEpList.contains(selectedEp))
			flag = "response";
		else
			flag = "conc";
		for (int i = 0; i < eptids.length; i++)
		{
			if (eptids[i].equalsIgnoreCase(selectedEp))
			{
				eptid = eptids[i];
				epMin = epMins[i];
				epMax = epMaxs[i];
			}
		}

		// fetch aids
		Set<String> assaySet = new HashSet<String>();
		String assayids = "";
		String sids = "";
		String mode = request.getParameter("mode");
		if (mode.equalsIgnoreCase("assayWs"))
			assayids = request.getParameter("aids");
		else if (mode.equalsIgnoreCase("cpdWs"))
			sids = request.getParameter("sids");
		else
		{
			// fetch aids
			String[] things = request.getParameterValues("concepts[]");
			String queryStrAids = "select v.assay_id from filter_redesign.thing t, filter_redesign.epval_thing e, filter_redesign.epval v,";
			for (int j = 0; j < things.length; j++)
			{
				queryStrAids += " filter_redesign.epval_thing e" + (j + 1) + ", filter_redesign.thing_bag tb" + (j + 1);
				if (j != (things.length - 1))
					queryStrAids += ", ";
			}

			queryStrAids += " where t.thing_id = e.thing_id AND v.assay_id = e.assay_id AND e.tid = v.tid and ";
			for (int j = 0; j < things.length; j++)
			{
				queryStrAids += " e" + (j + 1) + ".thing_id = tb" + (j + 1) + ".child_thing_id and ";
				queryStrAids += " tb" + (j + 1) + ".thing_id = " + things[j];
				if (j + 1 < things.length)
					queryStrAids += " e" + (j + 1) + ".assay_id = e" + (j + 2) + ".assay_id ";

			}

			queryStrAids += " and v.assay_id =e1.assay_id  and v.EPT_ID = " + eptid + " and v.epval_num > " + epMin + " and v.epval_num < " + epMax
			        + " group by v.assay_id";

			SQLQuery queryAids = ses.createSQLQuery(queryStrAids).addScalar("assay_id", Hibernate.STRING);
			Iterator objectsAids = null;
			try
			{
				objectsAids = queryAids.list().iterator();
			}
			catch (HibernateException he)
			{
				log.info("Possibly a communications link failure, check database connectivity");
			}
			if (objectsAids != null)
			{
				while (objectsAids.hasNext())
				{
					String aid = (String) objectsAids.next(); // Fetch assay_id
					assaySet.add(aid);
				}
			}
			Object[] objArr = assaySet.toArray();
			String[] assayArr = new String[objArr.length];

			for (int z = 0; z < assayArr.length; z++)
			{
				assayArr[z] = objArr[z].toString();
				assayids += assayArr[z];
				if (z != assayArr.length - 1)
					assayids += ",";
			}
		}
		String QueryStr = "";
		if (mode.equalsIgnoreCase("cpdWs"))
		{
			String filteredAssayQueryStr = "select distinct e.ASSAY_ID as assayid" + " from filter_redesign.epval e"
			        + " inner join filter_redesign.epval_thing et on (et.ASSAY_ID = e.ASSAY_ID )"
			        + " inner join filter_redesign.epval_thing et2 on (et2.ASSAY_ID = e.ASSAY_ID )"
			        + " inner join filter_redesign.thing t on (t.thing_id = et.THING_ID)"
			        + " inner join filter_redesign.thing t2 on (t2.thing_id = et2.THING_ID)" + " where t.thing_id = "
			        + request.getParameter("sliceId") + " and e.sid in (" + sids + ")  and EPT_ID = " + selectedEp + "  and SID = "
			        + request.getParameter("sid") + " and EPVAL_NUM > " + epMin + " and EPVAL_NUM < " + epMax + " and t2.THING_TYPE = 'meta target'";
			SQLQuery filteredAssayQuery = ses.createSQLQuery(filteredAssayQueryStr).addScalar("assayid", Hibernate.STRING);

			Iterator objects = null;
			try
			{
				objects = filteredAssayQuery.list().iterator();
			}
			catch (HibernateException he)
			{
				log.info("Possibly a communications link failure, check database connectivity");
			}
			String filteredAids = "";

			if (objects != null)
			{

				while (objects.hasNext())
				{
					filteredAids += (String) objects.next();
					if (objects.hasNext())
						filteredAids += ",";
				}
			}
			QueryStr = "select distinct e.EPVAL_NUM as value, e.ASSAY_ID as assayid, t2.thing_name as target, e.scr_conc, e.scr_conc_unit "
			        + " from filter_redesign.epval e" + " inner join filter_redesign.epval_thing et on (et.ASSAY_ID = e.ASSAY_ID )"
			        + " inner join filter_redesign.epval_thing et2 on (et2.ASSAY_ID = e.ASSAY_ID )"
			        + " inner join filter_redesign.thing t on (t.thing_id = et.THING_ID)"
			        + " inner join filter_redesign.thing t2 on (t2.thing_id = et2.THING_ID)" + " where t.thing_id = "
			        + request.getParameter("sliceId") + " and e.assay_id not in (" + filteredAids + ")  and EPT_ID = " + selectedEp + "  and SID = "
			        + request.getParameter("sid") + " and t2.THING_TYPE = 'meta target'";
		}
		else
			QueryStr = "select distinct e.EPVAL_NUM as value, e.ASSAY_ID as assayid, t2.thing_name as target, e.scr_conc, e.scr_conc_unit "
			        + " from filter_redesign.epval e" + " inner join filter_redesign.epval_thing et on (et.ASSAY_ID = e.ASSAY_ID )"
			        + " inner join filter_redesign.epval_thing et2 on (et2.ASSAY_ID = e.ASSAY_ID )"
			        + " inner join filter_redesign.thing t on (t.thing_id = et.THING_ID)"
			        + " inner join filter_redesign.thing t2 on (t2.thing_id = et2.THING_ID)" + " where t.thing_id = "
			        + request.getParameter("sliceId") + " and e.ASSAY_ID not in (" + assayids + ")  and EPT_ID = " + selectedEp + "  and SID = "
			        + request.getParameter("sid") + " and t2.THING_TYPE = 'meta target'";

		SQLQuery query = ses.createSQLQuery(QueryStr).addScalar("value", Hibernate.FLOAT).addScalar("assayid", Hibernate.INTEGER)
		        .addScalar("target", Hibernate.STRING).addScalar("scr_conc", Hibernate.STRING).addScalar("scr_conc_unit", Hibernate.STRING);

		Iterator objects = null;
		try
		{
			objects = query.list().iterator();
		}
		catch (HibernateException he)
		{
			log.info("Possibly a communications link failure, check database connectivity");
		}
		String target, concUnit, scrConc;
		int aid = -1;
		float val = Float.NaN;

		if (objects != null)
		{

			while (objects.hasNext())
			{
				Object[] tuple = (Object[]) objects.next();

				try
				{
					val = (Float) tuple[0]; //Fetch value
				}
				catch (NullPointerException npe)
				{
					val = Float.NaN;
					log.info(npe.getMessage());
				}
				aid = (Integer) tuple[1]; //Fetch assayid
				target = (String) tuple[2]; //Fetch target
				scrConc = (String) tuple[3]; //Fetch scr_conc
				concUnit = (String) tuple[4];//Fetch concentration unit

				out.println("{\"value\" : \"" + val + "\"," + "\"aid\":\"" + aid + "\", \"target\" : \"" + target + "\", \"conc\":\"" + scrConc
				        + "\", \"concUnit\":\"" + concUnit + "\"}");
				if (objects.hasNext())
					out.println(",");
			}
		}
		out.println("],\"epType\":\"" + flag + "\" }");
	}

	/**
	 * Called by the init(...) method to generate hibernate Session object
	 * 
	 * @return Hibernate SessionFactory object
	 * 
	 */
	public static SessionFactory connect()
	{

		Configuration cfg = new Configuration();
		SessionFactory sessions = cfg.configure().buildSessionFactory();

		return sessions;
	}

}

package edu.miami.ccs.bao;
/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Feeds data into the pie chart screen of endpoint concept filter
 * 
 * @author Sreeharsha Venkatapuram, akoleti - UM Center for Computational Science
 * @version 1.0
 */
public class ChartFeeder extends HttpServlet
{
	/**
     * Constant serialized ID used for compatibility.
     */
    private static final long serialVersionUID = 2370850639392148131L;
	private static final Logger log = Logger.getLogger("ChartFeeder");
	SessionFactory sessions;
	Session ses;

	/**
	 * Overrides the method in GenericServlet class to create an active hibernate session  
	 */
	public void init(ServletConfig config) throws ServletException
	{
		sessions = connect();
		ses = sessions.openSession();
	} 

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		Set<String> assaySet = new HashSet<String>();
		
		String[] things = request.getParameterValues("concepts");
		String[] eptids = request.getParameterValues("ep"); 
		String[] epMins = request.getParameterValues("epMin"); 
		String[] epMaxs = request.getParameterValues("epMax"); 
		String queryStr = null;
		out.println("{\"data\": [");
		for(int i=0; i < eptids.length; i++)
		{
			
			//query for fetching aidSet
			String queryStrAids = "select v.assay_id from filter_redesign.thing t, filter_redesign.epval_thing e, filter_redesign.epval v,"; 
			for(int j=0; j<things.length; j++)
			{
				queryStrAids += " filter_redesign.epval_thing e"+(j+1)+", filter_redesign.thing_bag tb"+(j+1);
				if ( j != (things.length - 1))
					queryStrAids += ", ";
			}
		
			queryStrAids += " where t.thing_id = e.thing_id AND v.assay_id = e.assay_id AND e.tid = v.tid ";
			for(int j=0; j<things.length; j++)
			{
				queryStrAids += " and e"+(j+1)+".thing_id = tb"+(j+1)+".child_thing_id and ";
				queryStrAids += " tb"+(j+1)+".thing_id = "+things[j] +" and v.tid=e"+(j+1)+".tid ";
				if(j+1<things.length)
				{
					queryStrAids += " and e"+(j+1)+".assay_id = e"+(j+2)+".assay_id ";
				}

			}

			if(request.getParameter("chartMode").equals("assay"))
				queryStrAids += " and v.assay_id =e1.assay_id group by v.assay_id";
			else
				queryStrAids += " and v.assay_id =e1.assay_id  and v.EPT_ID = "+eptids[i]+" and v.epval_num > "+epMins[i]+" and v.epval_num < "+epMaxs[i]+" group by v.assay_id";
			SQLQuery queryAids = ses.createSQLQuery(queryStrAids).addScalar("assay_id", Hibernate.STRING);
			Iterator objectsAids = null; 
			try
			{
				objectsAids = queryAids.list().iterator();
			}
			catch(HibernateException he)
			{
				log.info("Possibly a communications link failure, check database connectivity");
			}
			if(objectsAids != null)	
			{
				while(objectsAids.hasNext())
				{
					String aid =  (String) objectsAids.next(); // Fetch assay_id
					assaySet.add(aid);
				}
			}
			Object[] objArr = assaySet.toArray();
			String[] assayArr = new String[objArr.length];
			String assayids = null;
			for(int z = 0; z < assayArr.length; z++)
			{
				assayArr[z] = objArr[z].toString();
				assayids += assayArr[z];
				if(z != assayArr.length-1)
					assayids += ",";
			}
			
			//queryStr for assays
			if(request.getParameter("chartMode").equals("assay"))
				queryStr = "select t.thing_id, t.thing_type, t.thing_name, count(distinct e.assay_id) as countof from filter_redesign.thing t, filter_redesign.epval_thing e, ";
			else
				queryStr = "select t.thing_id, t.thing_type, t.thing_name, count(*) as countof from filter_redesign.thing t, filter_redesign.epval_thing e, filter_redesign.epval v, ";
			
			for(int j=0; j<things.length; j++)
			{
				queryStr += " filter_redesign.epval_thing e"+(j+1)+", filter_redesign.thing_bag tb"+(j+1);
				if ( j != (things.length - 1))
					queryStr += ", ";
			}
			if(request.getParameter("chartMode").equals("assay"))
				queryStr += " where t.thing_id = e.thing_id and t.thing_id = e1.thing_id ";
			else
				queryStr += " where t.thing_id = e.thing_id AND v.assay_id = e.assay_id AND e.tid = v.tid  ";
			for(int j=0; j<things.length; j++)
			{
				queryStr += " and e"+(j+1)+".thing_id = tb"+(j+1)+".child_thing_id and ";

				if(!(request.getParameter("chartMode").equals("assay")))
				{
					queryStr += " tb"+(j+1)+".thing_id = "+things[j] ;
					queryStr += " and v.tid=e"+(j+1)+".tid ";
				}

					
				if(j+1<things.length)
				{
					queryStr += " and e"+(j+1)+".assay_id = e"+(j+2)+".assay_id ";
				}
					
			}
		
			SQLQuery query;
			
			//queryStr for assays
			if(request.getParameter("chartMode").equals("assay"))
			{
				queryStr += " e.ASSAY_ID in("+assayids+") group by t.thing_type, t.thing_name";
				query = ses.createSQLQuery(queryStr).addScalar("thing_id", Hibernate.STRING).addScalar("thing_type", Hibernate.STRING).addScalar("thing_name",  Hibernate.STRING).addScalar("countof", Hibernate.STRING);
			}
			else
			{
				queryStr += " and v.assay_id =e1.assay_id and v.EPT_ID = "+eptids[i]+" and v.epval_num > "+epMins[i]+" and v.epval_num < "+epMaxs[i]+" group by t.thing_type, t.thing_name";
				query = ses.createSQLQuery(queryStr).addScalar("thing_id", Hibernate.STRING).addScalar("thing_type", Hibernate.STRING).addScalar("thing_name",  Hibernate.STRING).addScalar("countof", Hibernate.STRING);
			}
			
			Iterator objects = null; 
			try
			{
				objects = query.list().iterator();
			}
			catch(HibernateException he)
			{
				log.info("Possibly a communications link failure, check database connectivity");
			}
			
			String[] arr = new String[4];
			out.println("{");
			if(request.getParameter("chartMode").equals("endpoint"))
				out.println("\"EndpointName\"" +":" + "\""+eptids[i]+"\","  );
			out.println("\"Pies\" : [");
			boolean flag = false;
			if(objects != null)	
			{
				String pieName = "";
				String previousPieName = "";
				flag = true;
				int count = 0;
				while(objects.hasNext())
				{
					Object[] tuple = (Object[]) objects.next();
					flag = false;
					arr[0] = (String)tuple[1]; //Fetch thing_type
					arr[1] = (String)tuple[2]; //Fetch thing_name
					arr[2] = (String)tuple[3]; //Fetch countof
					arr[3] = (String)tuple[0]; //Fetch thing_id
	
					pieName = arr[0];
					if(count != 0)
					{	
						if(!(previousPieName.equalsIgnoreCase(pieName)))
						{
							
						}
						else
							out.println(",");
					}
							
					
					if(!(previousPieName.equalsIgnoreCase(pieName)))	
					{
						if(count != 0)
						{
							out.println("] },");

						}

							
						out.println("{" + "\"PieName\":\"" + arr[0] + "\"," );
						out.println("\"Slices\" : [");
					}
					
					
					out.println("{\"SliceName\":\"" + arr[1] + "\", \"Count\" : " + arr[2] + ", \"SliceId\":\""+arr[3]+"\"}"  );


					count++;

					previousPieName = pieName;

				}
				if(!flag)
					out.println("] }");
			}
			out.println("]}");
			if(i != (eptids.length-1))
				out.println(",");
		}
		Object[] objArr = assaySet.toArray();
		String[] assayArr = new String[objArr.length];
		out.print("],\"aids\":\"");
		for(int z = 0; z < assayArr.length; z++)
		{
			assayArr[z] = objArr[z].toString();
			out.print(assayArr[z]);
			if(z != assayArr.length-1)
				out.print(",");
		}
		out.print("\"}"); 

	}

	/**
	 * Called by the init(...) method to generate hibernate Session object
	 * 
	 * @return Hibernate SessionFactory object
	 * 
	 */
	public static SessionFactory connect()
	{

		Configuration cfg = new Configuration();
		SessionFactory sessions = cfg.configure().buildSessionFactory();

		return sessions;
	}

}




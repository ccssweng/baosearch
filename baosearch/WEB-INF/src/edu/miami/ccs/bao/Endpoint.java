/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package edu.miami.ccs.bao;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Servlet implementation class to return EndPoints for assays
 * 
 * @author Sreeharsha Venkatapuram, akoleti - Center for Computational Science
 * @version 1.0
 */
public class Endpoint extends HttpServlet
{
	/**
     * Constant serialized ID used for compatibility.
     */
    private static final long serialVersionUID = -7886622636912371777L;
	SessionFactory sessions;
	Session ses;

	/**
	 * Overrides the method in GenericServlet class to create an active hibernate session  
	 */
	public void init(ServletConfig config) throws ServletException
	{
		sessions = connect();
		ses = sessions.openSession();
	} 

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		Logger log = Logger.getLogger(this.getClass().getName());
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String input = request.getParameter("input");
		String nonSummaryAssayQryStr = "select pa.assay_aid from pcassay pa where pa.assay_aid in("+input+") and pa.assay_activity_outcome_method not like '%summary%'";
		Query nonSummaryAssayQry = ses.createSQLQuery(nonSummaryAssayQryStr).addScalar("assay_aid", Hibernate.STRING);
		Set<String> nonSummaryAssaySet = new HashSet<String>();
		try
		{
			Iterator objects = null;
			try
			{
				objects = nonSummaryAssayQry.list().iterator();
			}
			catch(HibernateException he)
			{
				sessions = connect();
				ses = sessions.openSession();
				objects = nonSummaryAssayQry.list().iterator();
			}
			if(objects != null)	
			{
				while(objects.hasNext())
				{
					String aid = (String) objects.next();
					nonSummaryAssaySet.add(aid);
				}
			}
		}
		catch(Exception e)
		{
			log.warning(e.getMessage());
		}
		Object[] t = nonSummaryAssaySet.toArray();
		String[] nonSummaryAssayArr = new String[t.length];
		for(int i = 0; i < t.length; i++)
		{
			nonSummaryAssayArr[i] = t[i].toString();
		}
		String tokens[] = input.split(",");
		int numTokens = nonSummaryAssayArr.length;
		
		SortedSet<String> globalEpNameSet = new TreeSet<String>();
		

		out.print("{\"assays\": [");
		for (int i = 0; i < numTokens; i++)
		{
			Set<String> epNameSet = new HashSet<String>();
			Query q = ses.createSQLQuery("select distinct pa.assay_name, ep.ep_name from pcassay pa, filter_redesign.ep_details ep where pa.assay_aid = ep.assay_id and ep.assay_id = "
			        + nonSummaryAssayArr[i]).addScalar("assay_name", Hibernate.STRING).addScalar("ep_name", Hibernate.STRING);
			epNameSet.clear();
			if(i!=0)
				out.print(",");
			try
			{
				Iterator objects = null;
				try
				{
					objects = q.list().iterator();
				}
				catch(HibernateException he)
				{
					sessions = connect();
					ses = sessions.openSession();
					objects = q.list().iterator();
				}
				String assayName = new String();
				String eptid = new String();
				if(objects != null)	
				{
					while(objects.hasNext())
					{
						Object[] tuple = (Object[]) objects.next();
						assayName = (String)tuple[0];
						eptid = (String)tuple[1];
						if(eptid != null && !eptid.equals("null"))
						{
							epNameSet.add(eptid);
							globalEpNameSet.add(eptid);
						}
					}
					out.print("{" + "\"AssayId\":\"" + nonSummaryAssayArr[i] + "\"," + "\"AssayName\":\"" + assayName + "\" ");
					if(!objects.hasNext())
					{
						out.print(", \"epNameSet\":[");
						Iterator it = epNameSet.iterator();
						if(it.hasNext())
						{
							for(int j = 0; j < epNameSet.size(); j++)
							{
								String epname = (String)it.next();
								out.print("\""+epname+"\"");
								if(j != (epNameSet.size()-1))
									out.print(",");
							}
						}
						out.print("]}");
					}
				}
			}
			catch(Exception e)
			{

			}
		}
		out.print("],");
		out.print("\"globalEpNameSet\":[");
		Iterator it = globalEpNameSet.iterator();
		if(it.hasNext())
		{
			for(int j = 0; j < globalEpNameSet.size(); j++)
			{
				String epname = (String)it.next();
				out.print("\""+epname+"\"");
				if(j != (globalEpNameSet.size()-1))
					out.print(",");
			}
		}
		out.print("] }");
	}

	/**
	 * Called by the init(...) method to generate hibernate Session object
	 * 
	 * @return Hibernate SessionFactory object
	 * 
	 */
	public static SessionFactory connect()
	{

		Configuration cfg = new Configuration();
		SessionFactory sessions = cfg.configure().buildSessionFactory();

		return sessions;
	}

}

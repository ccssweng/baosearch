/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package edu.miami.ccs.bao;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class to fetch substance summary
 * 
 * @author Sreeharsha Venkatapuram, UM Center for Computational Science
 * @version 1.0
 */
public class SubstanceSummary1 extends HttpServlet
{
	/**
     * Constant serialized ID used for compatibility.
     */
    private static final long serialVersionUID = 1289131897014606851L;
	Logger log = Logger.getLogger(this.getClass().getName());
	SessionFactory sessions;
	Session ses;

	/**
	 * Overrides the method in GenericServlet class to create an active hibernate session  
	 */
	public void init(ServletConfig config) throws ServletException
	{

		sessions = connect();
		ses = sessions.openSession();

	} //end of method

	
	/* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
	    this.doGet(req, resp);
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		JSONObject jsonObj = new JSONObject();
		
		String input = request.getParameter("input");

		String json = "";
		String QueryStr = "SELECT distinct ss.cd_id, ss.cid, ss.cd_smiles, ss.iupac_name, ss.mol_wt, ss.mol_formula, ss.h_bond_acceptors, ss.h_bond_donors, ss.tpsa, ss.logp, ss.logd, ss.rotatable_bonds, ss.lipinski_r5, ss.lipinski_r3, ss.lead_likeness, ss.bioavailability FROM substance_summary ss, pcassay_result pr where ss.cid=pr.cid and pr.sid="+input.trim();
		
		SQLQuery query = ses.createSQLQuery(QueryStr).addScalar("cd_id", Hibernate.STRING).addScalar("cid",  Hibernate.STRING).addScalar("cd_smiles", Hibernate.STRING).addScalar("iupac_name", Hibernate.STRING).addScalar("mol_wt", Hibernate.STRING).addScalar("mol_formula", Hibernate.STRING).addScalar("h_bond_acceptors", Hibernate.STRING).addScalar("h_bond_donors", Hibernate.STRING).addScalar("tpsa", Hibernate.STRING).addScalar("logp", Hibernate.STRING).addScalar("logd", Hibernate.STRING).addScalar("rotatable_bonds", Hibernate.STRING).addScalar("lipinski_r5", Hibernate.STRING).addScalar("lipinski_r3", Hibernate.STRING).addScalar("lead_likeness", Hibernate.STRING).addScalar("bioavailability", Hibernate.STRING);
		
		Iterator objects = null; 
		try
		{
			objects = query.list().iterator();
		}
		catch(HibernateException he)
		{
			log.info("Possibly a communications link failure, check database connectivity");
		}
		String[] arr = new String[6];
		if(objects.hasNext())	
		{

			Object[] tuple = (Object[]) objects.next();
			try
            {
	            jsonObj.put("cd_id", (String)tuple[0]);
	            jsonObj.put("cid", (String)tuple[1]);
	            jsonObj.put("smiles", (String)tuple[2]); //needed if smile is being displayed  
	            jsonObj.put("iupac", (String)tuple[3]); //needed if iupac is being displayed
	            jsonObj.put("mol_wt", (String)tuple[4]);
	            jsonObj.put("mol_formula", (String)tuple[5]);
	            jsonObj.put("h_bond_acceptors", (String)tuple[6]);
	            jsonObj.put("h_bond_donors", (String)tuple[7]);
	            jsonObj.put("tpsa", (String)tuple[8]);
	            jsonObj.put("logp", (String)tuple[9]);
	            jsonObj.put("logd", (String)tuple[10]);
	            jsonObj.put("rotatable_bonds", (String)tuple[11]);
	            jsonObj.put("lipinski_r5", ((String)tuple[12]=="0")?"fail":"pass");
	            jsonObj.put("lipinski_r3", ((String)tuple[13]=="0")?"fail":"pass");
	            jsonObj.put("lead_likeness", ((String)tuple[14]=="0")?"fail":"pass");
	            jsonObj.put("bioavailability", ((String)tuple[15]=="0")?"fail":"pass");
	            out.print(jsonObj.toString());
            }
            catch (JSONException e)
            {
	            e.printStackTrace();
            }
		
		}

	}

	/**
	 * Called by the init(...) method to generate hibernate Session object
	 * 
	 * @return Hibernate SessionFactory object
	 * 
	 */
	public static SessionFactory connect()
	{

		Configuration cfg = new Configuration();
		SessionFactory sessions = cfg.configure().buildSessionFactory();

		return sessions;
	}

}

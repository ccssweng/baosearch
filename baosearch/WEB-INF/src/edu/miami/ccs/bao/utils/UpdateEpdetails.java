/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package edu.miami.ccs.bao.utils;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.logging.Logger;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Update ep_list and ep_details table from the annotation
 * 
 * @author Sreeharsha Venkatapuram, UM Center for Computational Science
 * @version 1.0
 */
public class UpdateEpdetails
{

	
	static SessionFactory sessions;
	static Session ses;

	/**
	 * Overrides the method in GenericServlet class to create an active hibernate session  
	 */
	private static void init() 
	{
		sessions = connect();
		ses = sessions.openSession();
	} 

	public static void main(String[] args) throws SQLException
	{
		init();
		Logger log = Logger.getLogger(UpdateEpdetails.class.getName());
		String query = "select aid, tid, endpointunit, endpointstandardized from annotation_data order by aid";
		SQLQuery selectQuery = ses.createSQLQuery(query).addScalar("aid", Hibernate.STRING).addScalar("tid", Hibernate.STRING).addScalar("endpointunit", Hibernate.STRING).addScalar("endpointstandardized", Hibernate.STRING);
		
		Iterator objects = null; 
		try
		{
			objects = selectQuery.list().iterator();
			
		}
		catch(HibernateException he)
		{
			he.printStackTrace();
		}
		String[] arr = new String[4];
		if(objects != null)	
		{
			while(objects.hasNext())
			{
				Object[] tuple = (Object[]) objects.next();
				
				arr[0] = (String)tuple[0]; //Fetch aid
				arr[1] = (String)tuple[1]; //Fetch tid
				arr[2] = (String)tuple[2]; //Fetch epunit
				arr[3] = (String)tuple[3]; //Fetch epname
				String[] tids = arr[1].split(";");
				String[] epUnits = arr[2].split(";");
				String[] epNames = arr[3].split(";");
				for(int i=0; i<tids.length; i++)
				{
					String tid, epunit, epname;
					//considering case with just an orphan ; in any column
					if(tids.length == 0)
						tid = null;
					else if(tids[i].equals(""))
						tid = null;
					else
						tid = tids[i];
					if(epUnits.length == 0)
						epunit = null;
					else if(epUnits.length < tids.length )
						epunit = null;
					else
						epunit = epUnits[i];
					if(epNames.length == 0)
						epname = null;
					else if(epNames[i].equals(""))
						epname = null;
					else
						epname = epNames[i];

					String insertQueryStr = "insert into filter_redesign.ep_details(assay_id,tid,ep_unit,ep_name) values("+arr[0]+","+tid+",\""+epunit+"\",\""+epname+"\")";
					SQLQuery insertQuery = ses.createSQLQuery(insertQueryStr);
					insertQuery.executeUpdate();

					System.out.println(arr[0]+","+tid+","+epunit+","+epname);
				}
			}
		}
    }

	public static SessionFactory connect()
	{

		Configuration cfg = new Configuration();
		SessionFactory sessions = cfg.configure().buildSessionFactory();

		return sessions;
	}
	

}

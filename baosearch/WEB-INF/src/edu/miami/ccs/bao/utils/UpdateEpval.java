package edu.miami.ccs.bao.utils;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

import com.csvreader.CsvReader;
import java.sql.*;
import java.io.FileWriter;
import java.io.File;
import com.csvreader.CsvWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;

/**
 * This class reads csv files from pubchem and takes tids from
 *  epval_thing and creates Evpal csv file which can be loaded to epval
 *  table .
 *  
 * @author akoleti, Center for Computational Science, University of Miami
 * @version 1.0
 */
public class UpdateEpval {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Connection conn = null;
		String url;
		String userName;
		String password;
		String outputFile;
		String pathOfAnnotationFile;
		String pathOfPubchemfiles;

		System.out
				.println("Enter in databse uri,username,password,pathOfAnnotationFile,pathOfPubchemfiles,outputfilePath and separate each with a return.");
		url = in.next();
		userName = in.next();
		password = in.next();
		pathOfAnnotationFile = in.next();
		pathOfPubchemfiles = in.next();
		outputFile = in.next();
		boolean alreadyExists = new File(outputFile).exists();
		Set<Integer> aidSet = new HashSet<Integer>();

		try {
			CsvReader annotation = new CsvReader(pathOfAnnotationFile);

			annotation.readHeaders();
			while (annotation.readRecord()) {

				String assay = annotation.get("aid");

				int aid = Integer.parseInt(assay);
				aidSet.add(aid);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			CsvWriter csvOutput = new CsvWriter(
					new FileWriter(outputFile, true), ',');

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			Iterator it = aidSet.iterator();

			List<Integer> filesToRead = new ArrayList<Integer>(aidSet);

			for (int i = 0; i < filesToRead.size(); i++) {
				int inputFile = filesToRead.get(i);

				Statement st = conn.createStatement();
				String Query = "select * from filter_redesign.ep_details where ASSAY_ID =";
				ResultSet res = st.executeQuery(Query + inputFile);
				String pathOfFile = "/Users/../0446001_0447000/";

				String typeOfFile = ".csv";

				while (res.next()) {
					int tids = res.getInt("tid");
					int assayId = res.getInt("ASSAY_ID");

					String tid = Integer.toString(tids);
					String aid = Integer.toString(assayId);

					CsvReader products = new CsvReader(pathOfFile + inputFile
							+ typeOfFile);

					products.readHeaders();

					while (products.readRecord()) {

						String productID = products.get("PUBCHEM_SID");
						String productName = products.get("PUBCHEM_CID");
						String value = products.get(tid);

						csvOutput.write(aid);
						csvOutput.write(productID);
						csvOutput.write(productName);
						csvOutput.write(tid);
						csvOutput.write(value);
						csvOutput.endRecord();

					}

					products.close();
				}

			}
			conn.close();
			csvOutput.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.print("done");
	}
}

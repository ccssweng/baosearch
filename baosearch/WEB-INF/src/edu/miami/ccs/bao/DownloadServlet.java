/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package edu.miami.ccs.bao;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Servlet implementation class DownloadServlet downloads the selected columns from Workset
 * to a file
 * 
 * @author Sreeharsha Venkatapuram, UM Center for Computational Science
 * @version 1.0		   
 * 
 */
public class DownloadServlet extends HttpServlet
{
	/**
     * Constant serialized ID used for compatibility.
     */
    private static final long serialVersionUID = 3058440283680208159L;
	/**
	 * Maximum length of the file to write
	 */
	private static final int FILE_LENGTH = Integer.MAX_VALUE;
	private static final String tableName1 = "pccompound";
	private static final String tableName2 = "pcassay_result";
	/**
	 * File with mapping of column names from UI to column names in the database
	 */
	private static final String colMapFile = "column_mapping.properties";
	SessionFactory sessions;
	Session ses;

	private ServletConfig config;

	/**
	 * Overrides the method in GenericServlet class to create an active hibernate session  
	 */
	public void init(ServletConfig config) throws ServletException
	{
		this.config = config;
		sessions = connect();
		ses = sessions.openSession();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doGet(req, resp);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		String input = request.getParameter("input");
		String sids[] = input.split(",");
		String extension = "";
		String delim = request.getParameter("delimiter");
		if (delim.equals("comma"))
			extension = ".csv";
		else if (delim.equals("tab"))
			extension = ".tsv";
		String filename = request.getParameter("filename") + extension;
		String commaSeparatedColumns = request.getParameter("columns") + ",smiles";
		String columns[] = commaSeparatedColumns.split(",");

		Class myClass = getClass();
		ClassLoader loader = myClass.getClassLoader();
		URL myURL = loader.getResource(colMapFile);
		String path = myURL.getPath();
		path = path.replaceAll("%20", " ");

		FileReader freader = new FileReader(path);
		BufferedReader dis = new BufferedReader(freader);
		String line = "";
		HashMap<String, String> map = new HashMap<String, String>();
		while ((line = dis.readLine()) != null)
		{
			String mockColumns[] = line.split("::");
			map.put(mockColumns[0], mockColumns[1]);
		}
		for (int i = 0; i < columns.length; i++)
			columns[i] = map.get(columns[i]);

		makeAndProcessQuery(columns, sids, filename, delim, request, response);

	}
	
	/**
	 * Query Database and write results to the file
	 * 
	 * @param columns - Array of column names to download
	 * @param sids - Array of substance IDs for which the details have to be downloaded
	 * @param filename - Output filename as provided by the user
	 * @param delimiter - comma or tab
	 * @param request - HttpServletRequest object to retrieve input parameters
	 * @param response - HttpServletResponse object to write the output to
	 * @throws ServletException
	 * @throws IOException
	 */
	private void makeAndProcessQuery(String[] columns, String[] sids, String filename, String delimiter, HttpServletRequest request,
	        HttpServletResponse response) throws ServletException, IOException
	{

		if (delimiter.equals("comma"))
			delimiter = ",";
		else if (delimiter.equals("tab"))
			delimiter = "\t";

		PrintWriter fwriter = new PrintWriter(filename);
		fwriter.print("There was an error in downloading file. Please try downloading again.");
		fwriter.close();
		ServletOutputStream sos = response.getOutputStream();
		ServletContext context = config.getServletContext();
		String mimetype = context.getMimeType(filename);

		response.setContentType((mimetype != null) ? mimetype : "application/octet-stream");
		response.setContentLength(FILE_LENGTH);
		response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

		int numSids = sids.length;
		int numColumns = columns.length;
		for (int k = 0; k < numColumns; k++)
		{
			String str = columns[k] + delimiter;
			sos.print(str);
		}
		sos.println();
		for (int i = 0; i < numSids; i++)
		{

			String query = "";
			query += "select distinct ";
			for (int j = 0; j < numColumns; j++)
			{
				query += columns[j];
				if (j != (numColumns - 1))
					query += ",";
			}
			query += " from " + tableName1 + " as c, " + tableName2 + " as pr where c.cid = pr.cid and pr.sid = " + sids[i];

			SQLQuery q = ses.createSQLQuery(query);
			for (int f = 0; f < numColumns; f++)
			{
				q.addScalar(columns[f], Hibernate.STRING);
			}

			String str = new String();
			try
			{
				Iterator row = null;
				try
				{
					row = q.list().iterator();
				}
				catch (HibernateException he)
				{
					sessions = connect();
					ses = sessions.openSession();
					row = q.list().iterator();
				}
				if (row != null)
				{
					while (row.hasNext())
					{
						Object[] tuple = (Object[]) row.next();
						for (int l = 0; l < numColumns; l++)
						{
							str = (String) tuple[l]; //Fetch a column value
							if (str == null)
								str = "";
							if (str.contains(","))
								str = "\"" + str + "\"" + delimiter;
							else
								str = str + delimiter;
							sos.print(str);
						}

						sos.println();
					}
				}

			}
			catch (Exception e)
			{

			}

		}
		sos.flush();
		return;
	}

	/**
	 * Called by the init(...) method to generate hibernate Session object
	 * 
	 * @return Hibernate SessionFactory object
	 * 
	 */
	public static SessionFactory connect()
	{

		Configuration cfg = new Configuration();
		SessionFactory sessions = cfg.configure().buildSessionFactory();

		return sessions;
	}

}

/**
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package edu.miami.ccs.bao;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.miami.ccs.bao.tools.XMLParser;

/**
 * Servlet class to return assay counts for the search criteria on Pubchem
 * 
 * @author Sreeharsha Venkatapuram, Center for Computational Science
 * @version 1.0
 *
 */
public class PubchemRedirector extends HttpServlet
{
	/**
     * Constant serialized ID used for compatibility.
     */
    private static final long serialVersionUID = -3942608519270696984L;
	static Document doc = null;
	static XPathFactory factory = XPathFactory.newInstance();
	static XPath xpath = factory.newXPath();
	
	/* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
	    this.doPost(req, resp);
    }

	/* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
	    String keyword = req.getParameter("keyword");
	    String count = new String();
	    resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		
	    String url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pcassay&term="+keyword+"&datetype=edat&retmax=100&usehistory=y&retmode=xml&version=2.0";
	    try
        {
	        doc = XMLParser.parseXml(url);
        }
        catch (XPathExpressionException e)
        {
	        e.printStackTrace();
        }
	    if(doc != null)
		{
			try
            {
	            count = extractText("//eSearchResult/Count/text()");
            }
            catch (XPathExpressionException e)
            {
	            e.printStackTrace();
            }
		}
	    out.print("{\"count\":\""+count+"\"}");
    }
    
	/**
	 * @param string - Path of the tag from XML
	 * @return Value of the tag from XML after extracting
	 * @throws XPathExpressionException
	 */
	private static String extractText(String string) throws XPathExpressionException
    {
		XPathExpression expr = xpath.compile(string); 
		NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		Node node = nodes.item(0);
		if (node != null)
			return (node.getNodeValue());
		else
			return null;
    }
}

{
"2":{"epName":"AC50","epMin":"0","epMax":"1000000","sort":"min"},
"11":{"epName":"CC50","epMin":"0","epMax":"100","sort":"min"},
"7":{"epName":"EC50","epMin":"0","epMax":"722200","sort":"min"},
"4":{"epName":"IC50","epMin":"0","epMax":"6356750","sort":"min"},
"17":{"epName":"Perturbagen Concentration","epMin":"0","epMax":"10629","sort":"min"},
"5":{"epName":"Percent Activation","epMin":"-44490","epMax":"29142","sort":"max"},
"1":{"epName":"Percent Inhibition","epMin":"-14017.3","epMax":"5052.57","sort":"max"},
"10":{"epName":"Percent growth inhibition","epMin":"-52.08","epMax":"238.03","sort":"max"},
"8":{"epName":"Percent Response","epMin":"-1753980","epMax":"7527830","sort":"max"},
"19":{"epName":"Percent Viability","epMin":"-9.59","epMax":"366.87","sort":"max"},
"3":{"epName":"Fold Activation","epMin":"0","epMax":"27.64","sort":"max"},
"20":{"epName":"Fold Inhibition","epMin":"0","epMax":"5.1","sort":"max"},
"13":{"epName":"Maximal Response","epMin":"-153","epMax":"74075.8","sort":"max"},
"15":{"epName":"Bmax","epMin":"273","epMax":"1160","sort":"max"},
"14":{"epName":"Kd","epMin":"0","epMax":"4001","sort":"min"},
"12":{"epName":"Ki","epMin":"0.077","epMax":"4.63","sort":"min"},
"16":{"epName":"Ki'","epMin":"0.809","epMax":"141","sort":"min"},
"18":{"epName":"Tm","epMax":"12.2","sort":"min"},
"6":{"epName":"Raw Activity Reported","epMin":"-372.756","epMax":"3287840","sort":"min"},
"9":{"epName":"Response","epMin":"-100","epMax":"1000","sort":"min"}

}

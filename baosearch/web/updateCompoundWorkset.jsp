<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%
	List<String>  worksetArr = new ArrayList();

	//There is no selected mode
	if(request.getParameter("mode") == null)
	{
		out.print("ERROR: No Mode Selected");
	}
	//We don't have a workset in the request and we don't want to clear the existing session
        else if(request.getParameter("compoundWorksetList") == null && !request.getParameter("mode").equals("clear"))
        {
		out.print("ERROR: No compounds were selected");
	}
	//We don't have a workset in the request but we want to clear the existing session
	else if(request.getParameter("mode").equals("clear"))
	{
			session.setAttribute("cpdWorksetArr",null);
			out.print("Workset Cleared");
	}
	//We have a workset in the request
	else if(!request.getParameter("mode").equals("clear"))
	{
		String[] cpdList = request.getParameterValues("compoundWorksetList");
		for (String s : cpdList) {
			worksetArr.add(s);
    		}
		//We have a current workset, just need to update it
        	if(session.getAttribute("cpdWorksetArr") != null)
		{

			List<String> cpdArr = new ArrayList();
			cpdArr = (List)session.getAttribute("cpdWorksetArr");

			//We have additions to the workset
			if(request.getParameter("mode").equals("add"))
			{
				for(int i = 0; i < worksetArr.size(); i++)
				{
					if(cpdArr.indexOf(worksetArr.get(i)) == -1)
						cpdArr.add(worksetArr.get(i));
				}
				session.setAttribute("cpdWorksetArr", cpdArr);
				out.print("Workset Updated");
			}
			//We need to remove elements from the workset
			else if(request.getParameter("mode").equals("delete"))
			{
                                for(int i = 0; i < worksetArr.size(); i++)
				{
					int delIndex = cpdArr.indexOf(worksetArr.get(i));
                                        if(delIndex != -1)
						cpdArr.remove(delIndex);
				}
                                session.setAttribute("cpdWorksetArr", cpdArr);
                                out.print("Workset Updated");
			}
			else
				out.print("ERROR: Invalid Mode");
		}
		//We don't have a current workset, create a new one
		else
		{
			session.setAttribute("cpdWorksetArr", worksetArr);
			out.print("New Workset Created");
		}
	}	
%>

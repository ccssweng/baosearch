<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ include file = "header.jsp" %>

        <title>BAOSearch - Terms of Use</title>

<%@ include file = "topmenu.jsp" %>

        <div class = "title">
                <h1>Terms Of Use</h1>
                <hr>
                <br>
        </div>


        <div class = "content">
<p>Welcome to the BAOSearch website, hosted by the Center for 
Computational Science (hereinafter 'we', 'us', 'our'). If you continue to browse and use this website you are agreeing to comply with and be bound by the following 
terms and conditions of use, which together with our privacy policy govern the Center for Computational Science's relationship with you in relation to this website.  
</p>
                        <p>

                        Cite: Mader C, Abeyruwan S, Datar N, Chung C, Koleti A, Venkatapuram S, Puram D, Vempati U, Sakurai K, Przydzial M, Lemmon V, Visser U, Sch�rer S. BAOSearch: <a 
href="http://baosearch.ccs.miami.edu">http://baosearch.ccs.miami.edu</a> 
                        </p>
                        <p>
                        A list of articles that describe BAO can be found in the <a href="http://bioassayontology.org/index.php?option=com_content&amp;view=article&amp;id=98&amp;Itemid=141">BAO 
Publications
                        section</a>  of the <a href="http://bioassayontology.org">BAO website</a>.
                        </p><p>
The use of this website is subject to the terms of use on the <a href = "http://www.ccs.miami.edu/terms-of-use.html">CCS website</a>

</p>
        </div>


        <br>

<%@ include file = "footer.jsp" %>

</body>



</html>


<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ include file = "header.jsp" %>

        <title>BAOSearch - Endpoint Filter  - Substance Details</title>

        <link rel = "stylesheet" href = "/baosearch/jquery-ui/css/jquery-ui-1.8.6.custom.css" type = "text/css"/>

	<style type = "text/css">
		.dataTables_wrapper { min-height: 130px; }
		#dynamicPlus table.display thead th { background: #dce6e7; color: #145c61;}
		#dynamicPlus table tr.odd td, tr.even td { border-bottom: 1px dashed #a0a7a7; }
	</style>

	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="jquery-ui/jquery-ui-1.8.6.custom.min.js"></script>

        <script type="text/javascript">

			var epCount = 0;
			var epArray = new Array();
			var concepts = new Array();
			var querystring = "<%=request.getQueryString()%>";
			var selectedEp = <%=request.getParameter("selectedEp")%>;
			var sliceName = "<%=request.getParameter("sliceName")%>";
			var sid = "<%=request.getParameter("sid")%>";
			var sliceId = "<%=request.getParameter("sliceId")%>";
			var epMax = "<%=request.getParameter("epMax")%>";
			var epMin = "<%=request.getParameter("epMin")%>";
			concepts = [<%=request.getParameter("concepts")%>];
			var mode = "<%=request.getParameter("mode")%>";
			var sids = "";
<% if(request.getParameter("mode").equals("cpdWs"))
        {
%>
			sids = "<%=request.getParameter("sids")%>";
<%
        }
%>

			var aids = "<%=request.getParameter("aids")%>";

			$(document).ready(function(){                
				$("#message").html("<img src = '/baosearch/images/loader2.gif'>");
				processQuery();
				$.getJSON("data/epList.js", getEpList);
			});





        </script>


<%@ include file = "topmenu.jsp" %>

        <div class = "title">
                <h1>Endpoint Filter - Substance Details</h1>
		<div class = "epHeader" id = "epHeader"><span id = "selEpName"></span><span id = "selEpRange"></span><span id = "selConcept"></span></div>
                <hr>
        </div>


        <div class = "content">
		<form id = "cpdWorksetForm">
		<div>
			<img src = "/chemfile?type=sid&format=jpeg:w120,h120&id=<%=request.getParameter("sid")%>" style = "float: left;">
			<span id = "subDetails"></span>			
		</div>
		<div id = "dynamic"></div>
		<input type = "hidden" id = "mode" name = "mode"/>
		<h3>Activity on Other Assays for this Endpoint</h3>
		<div id = "dynamicPlus"></div>
		</form>
	</div>


<%@ include file = "footer.jsp" %>

</body>



</html>


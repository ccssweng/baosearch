<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%
	List<String>  worksetArr = new ArrayList();

	//There is no selected mode
	if(request.getParameter("mode") == null)
	{
		out.print("ERROR: No Mode Selected");
	}
	//We don't have a workset in the request and we don't want to clear the existing session
        else if(request.getParameter("assayWorksetList") == null && !request.getParameter("mode").equals("clear"))
        {
		out.print("ERROR: No assays were selected");
	}
	//We don't have a workset in the request but we want to clear the existing session
	else if(request.getParameter("mode").equals("clear"))
	{
			session.setAttribute("assayWorksetCount",0);
			session.setAttribute("assayWorksetArr",null);
			out.print("Workset Cleared");
	}
	//We have a workset in the request
	else if(!request.getParameter("mode").equals("clear"))
	{
		if(session.getAttribute("assayWorksetCount") == null)
			session.setAttribute("assayWorksetCount", 0);
		int count = Integer.parseInt(session.getAttribute("assayWorksetCount").toString());
		String[] assayList = request.getParameterValues("assayWorksetList");

		for (String s : assayList) {
			worksetArr.add(s);
    		}
		//We have a current workset, just need to update it
        	if(session.getAttribute("assayWorksetArr") != null)
		{

			List<String> assayArr = new ArrayList();
			assayArr = (List)session.getAttribute("assayWorksetArr");

			//We have additions to the workset
			if(request.getParameter("mode").equals("add"))
			{
				if(count > 200)
				{
					out.print("Workset Limit Reached. Please remove some assays or clear workset.");
				}
				else
				{	
					int i = 0;
					for(i = 0; i < worksetArr.size(); i++)
					{
						if(assayArr.indexOf(worksetArr.get(i)) == -1)
						{
							if(count < 200)
							{
								assayArr.add(worksetArr.get(i));
								count++;
							}
							else
								break;
						}
					}
					session.setAttribute("assayWorksetArr", assayArr);
					if(count >= 200)
						out.print("Workset Updated with "+(i-1)+" assays - Workset Limit Reached");
					else
						out.print("Workset Updated - "+count+ " Assays");
				}
			}
			//We need to remove elements from the workset
			else if(request.getParameter("mode").equals("delete"))
			{
                                for(int i = 0; i < worksetArr.size(); i++)
				{
					int delIndex = assayArr.indexOf(worksetArr.get(i));
                                        if(delIndex != -1)
					{
						assayArr.remove(delIndex);
						if(count >= 0 )
							count--;
					}
				}
                                session.setAttribute("assayWorksetArr", assayArr);
                                out.print("Workset Updated - "+count+" Assays");
			}
			else
				out.print("ERROR: Invalid Mode");
			session.setAttribute("assayWorksetCount", count);
		}
		//We don't have a current workset, create a new one
		else
		{
			session.setAttribute("assayWorksetCount", 0);
			session.setAttribute("assayWorksetArr", worksetArr);
			out.print("New Workset Created");
		}
	}	
%>

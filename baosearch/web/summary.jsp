<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ include file = "header.jsp" %>

        <title>BAOSearch - Endpoint Filter Summary</title>

	<link rel = "stylesheet" href = "/baosearch/jquery-ui/css/jquery-ui-1.8.6.custom.css" type = "text/css"/>


	<script type="text/javascript" language="javascript" src="jquery-ui/jquery-ui-1.8.6.custom.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/summary.js"></script>


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18726103-2']);
  _gaq.push(['_trackPageview']);

</script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

		var epCount = 0;
		var epArray = new Array();
		var conceptArray = new Array();

		conceptArray = [<%=request.getParameter("concepts")%>];
        var querystring = "<%=request.getQueryString()%>";
		var sliceName = "<%=request.getParameter("sliceName")%>";
		var sliceId = "<%=request.getParameter("sliceId")%>";
		var epMax = "<%=request.getParameter("epMax")%>";
		var epMin = "<%=request.getParameter("epMin")%>";
		var aids = "<%=request.getParameter("aids")%>";

		var mode = "<%=request.getParameter("mode")%>";
		var sids = "";
<% if(request.getParameter("mode").equals("cpdWs"))
        {
%>
	        sids = "<%=request.getParameter("sids")%>";
<%
        }
%>

		var dlQueryStr = "sliceId="+sliceId;

		dlQueryStr += "&aids="+aids;
				
		var epList = "";
		var epHeaderStr = "";
		var toggleStructs = false;
		var selectedEp = <%=request.getParameter("selectedEp")%>;
		
</script>


<%@ include file = "topmenu.jsp" %>

        <div class = "title">
                <h1>Endpoint Filter - Summary</h1>
		<div class = "epHeader" id = "epHeader"><span id = "selEpName"></span><span id = "selEpRange"></span><span id = "selConcept"></span></div>
                <hr>
        </div>
	<form id = "detailsForm" action = "details" method = "POST">
		<input type = "hidden" name = "aids" id = "aids"/>
		<input type = "hidden" name = "sids" id = "sids"/>
		<input type = "hidden" name = "concepts" id = "concepts"/>
		<input type = "hidden" name = "sid" id = "sid"/>
		<input type = "hidden" name = "sliceId" id = "sliceId"/>
		<input type = "hidden" name = "selectedEp" id = "selectedEp"/>
		<input type = "hidden" name = "epMin" id = "epMin"/>
		<input type = "hidden" name = "epMax" id = "epMax"/>
		<input type = "hidden" name = "sliceName" id = "sliceName"/>
		<input type = "hidden" name = "mode" id = "mode"/>
	</form>

	<form id = "cpdWorksetForm">
        <div class = "content">

	Show 
	<select name = "pagesize" id = "pagesize" onChange = "showLoader();processQuery();">
		<option value = "25">25</option>
		<option value = "50">50</option>
		<option value = "100">100</option>
	</select>
	 per page OR Jump to Page
	<input type = "text" id = "jumpPage" style = "width: 20px;"/>
	<input type = "button" value = "Go" onClick = "processQuery();">

		<div id = "pagingTop"></div>
		<div id = "dynamic"></div>
		<div id = "pagingBottom"></div>
	</div>
	<input type = "hidden" id = "wsMode" name = "mode"/>
	</form>

<%@ include file = "footer.jsp" %>

</body>



</html>


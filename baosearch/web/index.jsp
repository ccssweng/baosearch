<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<title>BAOSearch - Home</title>
	<link rel = "stylesheet" href="css/stylesheet.css" type="text/css" title="main" media="screen"/>
	<link rel="alternate stylesheet" href = "css/star-trek.css" title = "alt1" type = "text/css"/>

	<script src = "js/jquery-1.4.2.min.js" type = "text/JavaScript"></script>
        <script src = "js/jquery.marquee.min.js" type = "text/JavaScript"></script>


        <script src = "js/baosearch.js" type = "text/JavaScript"></script> 


	<script type="text/javascript">
	$(function() {
		$("#marquee").marquee();
                $("#go").click(function()
                {
			$("#searchform").submit();
		});
	});

	$(document).ready(function() { 

			//Check what the current style is and change it
			$("#lightswitch").click(function()
           	{
				var current = readCookie('style');
	            if (current == "css/star-trek.css")
					switchStyle("css/stylesheet.css");
				else
					switchStyle("css/star-trek.css");
				return false;
			});

		if (readCookie('style'))
                        switchStyle(readCookie('style'));
                else
               		switchStyle("css/stylesheet.css");


	})

//Function to switch to the alternate stylesheet
function switchStyle(styleName)
{
	   $("link").attr("href",styleName);
           createCookie('style', styleName, 365);
           return false;
}


// cookie functions http://www.quirksmode.org/js/cookies.html
function createCookie(name,value,days)
{
	if (days)
	{
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name)
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++)
	{
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
function eraseCookie(name)
{
	createCookie(name,"",-1);
}
// /cookie functions

	</script>

</head>

<body class = "home">



	<div class = "splash" style = "position: relative">
		<!-- Easter egg control -->
		<div id = "lightswitch"></div>



		<div class = "splash-search">
			<div id = "go"></div>
			<div class = "bao-logo"></div>
	                <form id="searchform" action="query" onSubmit = "return validateSearch()">
        	                <input class="query blurText" value = "Type Search Term and Press Enter" id = "searchTerm" name="searchTerm" type="text"/>
				<span class = "sample-query"></span>	
				<ul id="marquee" class="marquee"> 
					<li><a href = "/baosearch/query?searchTerm=breast cancer">breast cancer</a></li>
					<li><a href = "/baosearch/query?searchTerm=parkinson -p97">parkinson -p97</a></li>
					<li><a href = "/baosearch/query?searchTerm=dopamine -Inhibition">dopamine -Inhibition</a></li>
					<li><a href = "/baosearch/query?searchTerm=hexokinase %2Bic50">hexokinase +ic50</a></li>
					<li><a href = "/baosearch/query?searchTerm=cancer+%2B'estrogen+receptor'+%2Bic50">cancer +"estrogen receptor" +ic50</a></li>
				<ul>
                	</form>
		</div>		
		<br>
		<div class = "left-menu-bar">
			<a class = "left-menu substance" href = "#" onClick = "$('#substanceOpts').toggle('slow');">Substance Search</a>
			<div id = "substanceOpts" style = "display: none;">
				<a class = "left-menu" href = "substance">- Exact Term</a>
				<a class = "left-menu" href = "substance-search">- Structure</a>
			</div>
                        <a class = "left-menu filter" href = "#" onClick = "$('#conceptOpts').toggle('slow');">Concept Search</a>
			<div id = "conceptOpts" style = "display: none;">
				<a class = "left-menu" href = "filter">- Endpoints</a>
				<a class = "left-menu" href = "assays-by-concept">- Assays</a>
			</div>
			<!--<a class = "left-menu browse" href = "browse">Browse</a>-->
			<a class = "left-menu visualize" target = "blank" href = "http://bioportal.bioontology.org/ontologies/46364?p=terms " title = "Visualize">Visualize</a>
			<a class = "left-menu help" title = "help" href = "http://www.bioassayontology.org/wiki/index.php/BAOSearch_User_Guide">Help</a>
			<a class = "left-menu download" title "Download Ontology" href = "http://bioassayontology.org/index.php?option=com_content&view=article&id=96&Itemid=137">Download Ontology</a>
			<a class = "left-menu contact" title = "Contact" href = "http://bioassayontology.org/index.php?option=com_contact&view=contact&id=3&Itemid=74">Contact</a>
			<a class = "left-menu technology" title = "Technology" href = "technology">Technology</a>
		</div>
		<div class = "blurb">

			<h4>Welcome to the public beta of BAOSearch</h4>
                        <p>
                        BAOSearch is a web-based application for querying, browsing and downloading small molecule biological screening results including high-throughput screening
                        (HTS) data. 
			</p><p>
BAOSearch content was curated from <a href = "http://pubchem.ncbi.nlm.nih.gov" target = "blank">PubChem</a> by Ph.D. biologists. The current release (BAOContent Q2 
2011) includes 944 assays with over 50 million results (screening endpoints) characterizing the <a href = "http://mlsmr.glpg.com/MLSMR_HomePage" target = "blank">NIH 
MLSMR</a> library and others. All assays and their results have been annotated with standardized terms from the BioAssay Ontology (BAO). BAO categorizes biological 
assays by several hierarchies that correspond to the assay format, design, detection technology, target, and the result endpoints, which are relevant to interpret 
and analyze HTS data. BAO is analysis-centric with the purpose to facilitate conclusions about the molecular mechanisms of action of small molecules based on a large number of result sets. To learn 
more about the BAO project, visit the <a href = "http://bioassayontology.org" target = "blank">BioAssay Ontology</a> Website.
                        </p>
                        <p>
Leveraging the ontology, BAOSearch incorporates semantic search capabilities to identify relevant assays. It is a research and demonstration project and is 
under active development.                         
			</p>
                        <p>
			Cite: Mader C, Datar N, Abeyruwan S, Koleti A, Venkatapuram S, Chung C, Puram D, Vempati U, Sakurai K, Przydzial M, Lemmon V, Visser U, Sch�rer S. BAOSearch: <a href = 
"http://baosearch.ccs.miami.edu">http://baosearch.ccs.miami.edu</a> 
                        </p>
                        <p>
                        A list of articles that describe BAO can be found in the <a href = "http://bioassayontology.org/index.php?option=com_content&view=article&id=98&Itemid=141" target = 
"blank">BAO Publications
                        section</a>  of the <a href = "http://bioassayontology.org" target = "blank">BAO website</a>.
                        </p>
                        <p>
                        We are very interested in your feedback (positive or critical).  Please explore BAOSearch and let us know what you think - send us a 
smile or report a bug by using the links available at the bottom of every page.
                        </p>
		
		</div>
		<!-- Easter egg logo -->
		<div class = "boldly-search"></div>

		<div class = "clearBoth">
			<br><br>
			<a href = "http://ccs.miami.edu"><img src = "/baosearch/images/um-logo.png"/></a>	
			<br><br>
			<a class = "footer-link" href = "terms-of-use">Terms of Use</a>
			<a class = "footer-link" href = "privacy-policy">Privacy Policy</a>
			<a class = "footer-link" href = "funding">Funding Information</a>
		</div>

	</div>
	
	<br>

	
</body>



</html>


<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ include file = "header.jsp" %>

        <title>BAOSearch - Endpoint Filter</title>

        <link rel="stylesheet" href="css/suggest.css" type="text/css" media="screen" charset="utf-8" />

        <script type="text/javascript" language="javascript" src="js/jquery.fcbkcomplete.js"></script>
        <script type="text/javascript">

		var epCount = 0;
		var epArray = new Array();
		var epData = "";

		$(document).ready(function(){                
			$("#concepts").fcbkcomplete({
				json_url: "data/suggest-data.txt",
				addontab: true,          
				onselect: fetchEndpoints,          
				onremove: fetchEndpointsWithDelay,
				height: 10                    
			});
			
			$.getJSON("data/epList.js", getEndpointList );
		
			$("input.maininput").focus();
			
			$("#goMiniExtra").click(function()
			{
				$("#searchformExtra").submit();
			});
        	
			$('#searchTermExtra').click(function() 
			{
				if (this.value == this.defaultValue)
				{
						this.value = '';
						$('#searchTermExtra').removeClass('blurText').addClass('focusText');
				}
			});

			$('#searchTerm').blur(function() 
			{
				if (this.value == '')
				{
						this.value = this.defaultValue;
						$('#searchTermExtra').removeClass('focusText').addClass('blurText');
				}
			});
		});

        </script>


<%@ include file = "topmenu.jsp" %>

        <div class = "title">
                <h1>Filter Endpoints</h1>
                <hr>
        </div>

        <div class = "content">
		<div class = "sub-content">
			<h3>Filter By Concepts</h3>
                        Select one or more concepts from the suggested search box below. The concepts
                        will be logically 'AND'ed together to get the result. Add the endpoints you want to filter for
                        from the dropdown list and select upper and lower bounds for each endpoint. Click the "Filter" button to view results.
			<h4>Select Concept</h4>
        		<div id="text">
        		</div>
        		<form action="chart" id = "filterForm" onsubmit = "return validateFilter(2);" method="GET" accept-charset="utf-8">
				<input type = "hidden" name = "chartMode" id = "chartMode" value = "endpoint"/>
            			<select id="concepts" name="concepts">
            			</select>
	   			<br/>
	   			<div id = "epListContParent" style = "display: none;">
					<h4>Select Endpoint</h4>
					<div id = "epListContainer"></div>
			 		<br><br>
	   				<input name = "mode" value = "suggest" type = "hidden"/>	
	   			</div>
				<br>
      				<input type="submit" value="Filter" id = "filterBtn">

        		</form>
        	        <br>
		</div>
	</div>

<%@ include file = "footer.jsp" %>

</body>



</html>


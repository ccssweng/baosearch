<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ include file = "header.jsp" %>

        <title>BAOSearch - Substance Search</title>

<script type="text/javascript" src="/marvin/marvin.js"></script>
    <script type="text/javascript" src="/marvin/js2java.js"></script>
<%@ include file = "topmenu.jsp" %>

        <div class = "title">
                <h1>Substance Search - Structure</h1>
                <hr>
                <br>
        </div>

        <div class = "content">

    <form name="molform" method="post" action="/baosearch/substance-results" >
    
    
    
    <script type="text/javascript">
<!--
var isJs2Java = isLiveConnect(); // Is JavaScript - Java communication available?
setPath("/marvin/"); // Sets the relative path of the Marvin directory.

/* Sets form variable and submits the form.*/
function submitMethod() {
	document.molform.searchTerm.value = getResult();
	document.molform.submit();
}

/* Retrieves the molecule (in the specified format) from the sketcher and
calls 'submitMethod()'.
The 'runMethod' method should be the last statement in this function.*/
function getMol(format) {
	if((document.MSketch != null) && isJs2Java) {
		var s = document.MSketch.getMol(format);
		s = unix2local(s); // Convert "\n" to local line separator
		document.molform.searchTerm.value = s;
		document.molform.submit();
	} else if(!isJs2Java) {
		setMethod("MSketch.getMol","java.lang.String");
		addMethodParam(format);
		setPostJsMethod("parent.submitMethod()");
		runMethod();
	} else {
		alert("Cannot import molecule:\n" +
		"no JavaScript to Java communication in your browser.\n");
	}
}

msketch_name="MSketch";
msketch_begin("/marvin/",450, 490);
msketch_end();
//--></script>    
    
    
          
          <div align="left">  
            <input type="button" value="Search"
                    onClick="getMol('smiles')">
            <input type="hidden" name="searchTerm">
	    <input type = "hidden" name = "mode" value = "structure"/>
            
            <select name="searchtype" style="width: 150px;">
				<option value="2">Substructure</option>
				<option value="3">Similarity</option>
				<option value="5">Duplicate</option>
				<option value="6">Superstructure</option>
				<option value="7">Full fragment</option>
				<option value="4">Full structure</option>
			</select>
            
            
            </div>
    </form>
	<a href = "http://www.chemaxon.com" target = "blank"><img src = "http://www.chemaxon.com/images/powered_100px.gif"></a>

        </div>


        <br>

<%@ include file = "footer.jsp" %>

</body>



</html>


<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ include file = "header.jsp" %>

	<title>BAOSearch - Compound Search Results</title>

	
    <link rel = "stylesheet" href = "/baosearch/jquery-ui/css/jquery-ui-1.8.6.custom.css" type = "text/css"/>


	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
	<script src="js/jquery.colorbox-min.js"></script>
    <script type="text/javascript" language="javascript" src="jquery-ui/jquery-ui-1.8.6.custom.min.js"></script>
	<script src="/baosearch/js/advQuery.js"></script>


	<script type="text/javascript">

	$(function() {
                $("#message").html("<img src = '/baosearch/images/loader2.gif'>");
		processAdvancedQuery();
	});
	
	var newClassGroup = "";
	var worksetArr = new Array();

    var searchTerm = "<%= request.getParameter("searchTerm") %>";
    var mode = "<%= request.getParameter("adSearchOption") %>";


	</script>
	

<%@ include file = "topmenu.jsp" %>

	<br>
        <div class = "title">
                <h1>Compound Search Results</h1>
                <hr>
                <br>
        </div>

	<form name = "cpdWorksetForm" id = "cpdWorksetForm">
		<div class="content">
			<div id="tabs">
				<div id = "dynamic"></div>
			</div>
			<input type = "hidden" id = "mode" name = "mode"/>
			<br><br>
		</div>
		<br>
	</form>

	<br><br><br>

<%@ include file = "footer.jsp" %>


</body>

</html>

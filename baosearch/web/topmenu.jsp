<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
</head>

<body>

      	<div class = "header">
            <div id = "navigation">
			
				<ul id = "nav" class="dropdown dropdown-horizontal">
						<li><a class = "home" href = "/baosearch">Home</a></li>
						<li class = "dir"><a class = "substance" href = "#">Substance Search</a>
								<ul>
										<li><a href = "substance">Exact Term</a></li>
										<!-- This is the link to the substructure search powered by ChemAxon, uncomment if you have a license installed -->
										<!--<li><a href = "structure-search">Structure</a></li>-->
								</ul>

						</li>
						<li class = "dir"><a class = "filter" href = "#">Concept Search</a>
								<ul>
										<li><a href = "filter">Endpoints</a></li>
										<li><a href = "assays-by-concept">Assays</a></li>
								</ul>
						</li>
						<!-- This is the TreeMap functionality, uncomment to include in menu -->
						<!-- <li><a class = "home" href = "browse">Browse</a></li> -->
						<li><a class = "visualize" target = "blank" href = "http://bioportal.bioontology.org/ontologies/46364?p=terms">Visualize</a></li>
						<li><a href = "http://www.bioassayontology.org/wiki/index.php/BAOSearch_User_Guide" class = "help">Help</a></li>
						<li class = "dir"><a class = "workset" href = "#">Worksets</a>
								<ul>
										<li><a href = "assay-workset">Assay Workset</a></li>
										<li><a href = "compound-workset">Compound Workset</a></li>
								</ul>
						</li>
				</ul>

				<span class = "separator">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
				<span id = "toolbox"></span>
			</div>
        </div>
	<div id = "searchHeader" style = "clear: both; margin: 5px auto; width: 90%; padding: 10px 0px 0px 0px; position: relative;">	
                <a href = "/baosearch"><img src = "/baosearch/images/bao-search-mini.png" style = "float: left;"></a>
		<div id = "go-mini"></div>
		<div class = "searchbox-mini" style = "float: left; padding-left: 30px;">
        	        <form id="searchform" action="query" onSubmit = "return validateSearch()">
       	        	        <input class="query-mini blurText" value = "Type Search Term and Press Enter" id = "searchTerm" name="searchTerm" type="text">
               		</form>
		</div>	
		<span id = "message"></span>	
	</div>


	<div id = "container" style = "clear: both; margin: 5px auto; width: 90%;">


	<div id = "main">

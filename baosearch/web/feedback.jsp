<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */

/** 
 * Process the feedback from the bug or smile pages by checking the CAPTCHA 
 * and sending a mail with the text of the form field if correct
 */

%>

<%@ page import="com.octo.captcha.module.servlet.image.SimpleImageCaptchaServlet" %>
<%@ page import="java.util.*" %>
<%@ page import="javax.mail.*" %>
<%@ page import="javax.mail.internet.*" %>
<%@ page import="javax.activation.*" %>

<%

String userCaptchaResponse = request.getParameter("jcaptcha");
boolean captchaPassed = SimpleImageCaptchaServlet.validateResponse(request, userCaptchaResponse);
if(captchaPassed){
%>
	<font color = "green">Thank you for the feedback.</font>
<%

     boolean debug = false;

     //Set the host smtp address
     Properties props = new Properties();
     props.put("mail.smtp.host", "smtp.med.miami.edu");

    // create some properties and get the default Session
    Session s = Session.getDefaultInstance(props, null);
    s.setDebug(debug);

    // create a message
    Message msg = new MimeMessage(s);

    // set the from and to address
    InternetAddress addressFrom = new InternetAddress("baosearch@ccs.miami.edu");
    msg.setFrom(addressFrom);

    InternetAddress addressTo = new InternetAddress("ndatar@med.miami.edu");
    //InternetAddress[] addressTo = new InternetAddress[recipients.length]; 
    //for (int i = 0; i < recipients.length; i++)
    //{
    //    addressTo[i] = new InternetAddress(recipients[i]);
    //}
    
    msg.setRecipient(Message.RecipientType.TO, addressTo);

    String msgBody =  "Submitted By: "+request.getParameter("fbName")+"\nEmail: "+request.getParameter("fbEmail")+"\nMessage: "+request.getParameter("body");

    // Setting the Subject and Content Type
    if(request.getParameter("mode").equals("smile"))
	    msg.setSubject("BAOSearch Feedback: Smile");
    else
	    msg.setSubject("BAOSearch Feedback: Bug");

    msg.setContent( msgBody, "text/plain");
    Transport.send(msg);
}
else{
// return error to user
%>

<font color = "red">Sorry, the CAPTCHA text did not match, please refresh the page and try again</font>
<%
}



%>


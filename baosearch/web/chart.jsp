<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ include file = "header.jsp" %>

        <title>BAOSearch - Endpoint Filter Charts</title>

    	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript" src="/baosearch/js/chart.js"></script>
    	<script type="text/javascript">

		var mode = "<%=request.getParameter("mode")%>";
		var chartMode = "<%=request.getParameter("chartMode")%>";

<% if(request.getParameter("mode").equals("cpdWs"))
	{
%>
		var sids = "<%=request.getParameter("sids")%>";
<%
	}
%>
	var querystring = "<%=request.getQueryString()%>";


<% if(request.getParameter("mode").equals("suggest"))
	{
%>
	var concepts = "<%
        String[] things = request.getParameterValues("concepts");
        for(int i = 0; i < things.length; i++)
        {
                //if(i != 0)
                //        out.print("&concepts=");
                out.print(things[i]);
                if(i < (things.length - 1))
                        out.print(",");
        }
	%>";

        var thingId = "<%
        for(int i = 0; i < things.length; i++)
        {
                out.print(things[i]);
                if(i < (things.length - 1))
                        out.print(",");
        }
        %>";


<%
	}

	String[] endpoints = request.getParameterValues("ep");		
%>
	var numEndpoints = <%=endpoints.length%>;
	var epMax = new Array();
	epMax = [<%
	String[] epMax = request.getParameterValues("epMax");
	for(int i = 0; i < epMax.length; i++)
	{
		out.print(epMax[i]);
		if(i < (epMax.length - 1))
			out.print(",");
	}
%>];

        var epMin = new Array();
        epMin = [<%
        String[] epMin = request.getParameterValues("epMin");
        for(int i = 0; i < epMin.length; i++)
        {
                out.print(epMin[i]);
                if(i < (epMin.length - 1))
                        out.print(",");
        }
%>];

	var epList = "";
	var aids = "";
	var optionList = new Array();
	optionList[0] = new Array();
	optionList[1] = new Array();
	optionList[2] = new Array();
	optionList[3] = new Array();

	google.load("visualization", "1", {packages:["corechart", "table"]});
	google.setOnLoadCallback(getChartData);


       	$(document).ready(function(){                
			$("select").val(-1);
			$("#message").html("<img src = '/baosearch/images/loader2.gif'/>");
			$.getJSON("data/epList.js", function(response) { epList = response; } );
                     	if(mode == "assayWs")
				echoQuery("ASSAYS FROM WORKSET");
                        else if(mode == "cpdWs")
                                echoQuery("COMPOUNDS FROM WORKSET");
			else
				$.getJSON("get-concepts?"+ querystring, loadQuerySubsumption );
            	});

				

        </script>


<%@ include file = "topmenu.jsp" %>

        <div class = "title">
                <h1>Filter Results - <%=request.getParameter("chartMode")%> distribution</h1>
		<span id = "searchstring">Search Terms: </span>
                <hr>
        </div>


        <div class = "content">
	<div id = "status"></div>
	<form id = "summaryForm" action = "summary" method = "POST">
		<input type = "hidden" name = "concepts" id = "concepts"/>
		<input type = "hidden" name = "aids" id = "aids"/>
		<input type = "hidden" name = "sids" id = "sids"/>
		<input type = "hidden" name = "sliceId" id = "sliceId"/>
		<input type = "hidden" name = "selectedEp" id = "selectedEp"/>
		<input type = "hidden" name = "epMin" id = "epMin"/>
		<input type = "hidden" name = "epMax" id = "epMax"/>
		<input type = "hidden" name = "sliceName" id = "sliceName"/>
		<input type = "hidden" name = "mode" id = "mode"/>
	</form>
	<div id = "dynamic" style = "display: none;">
<%
	out.print("<table>");
	for(int i = 0; i < endpoints.length; i++)
	{
		if(request.getParameter("chartMode").equals("endpoint"))
		{
			out.print("<tr><td><span id = \"epHeader"+i+"\" class = \"epHeader\"></span><input type = \"hidden\" id = \"selectedEp"+i+"\"></td>");
		}
		else
			out.print("<td>Select Type & Concept to View Assays: </td>");
		out.print("<td colspan = \"3\"><div id = \"optionHolder"+i+"\"><select id = \"epType"+i+"\" onChange = \"populateSlices(this, 'selectedSlice"+i+"',"+i+")\"><option value = \"default\">-- Select Type --</option><option value = \"0\">Design</option><option value = \"1\">Detection Technology</option><option value = \"2\">Format</option><option value = \"3\">Meta Target</option></select>");
		out.print("<select id = \"selectedSlice"+i+"\" name = \"SliceId\"></select><input type = \"button\" value = \"Submit\" onClick = \"loadSummary("+i+",'"+request.getParameter("chartMode")+"')\"></div></td><tr>");

		for(int j = 0; j < 4; j++)
		{
			out.print("<td><div id = \"chart");
			out.print(i);
			out.print(j);
			out.print("\"></div></td>");
		}
		out.print("</tr>");
	}
	out.print("</table>");
%>
	</div>
	</div>
	
<%@ include file = "footer.jsp" %>

</body>



</html>


<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ include file = "header.jsp" %>

	<title>BAOSearch - Query</title>
	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/query.js"></script>

	<script type="text/javascript">

	</script>
	

<%@ include file = "topmenu.jsp" %>

	<div class = "title">
		<span id = "queryText"><h1>Query Results</h1></span>
		<hr>
	</div>

	<form name = "worksetForm" id = "worksetForm">
	<div class="content">
		<div id="tabs">
			<div id="tabTitles">
				<a class = "category currCategory" href = "#" onClick = "setClassGroup('http://www.bioassayontology.org/bao#BAO_0000015');changeTabs(this);">Bioassay</a>	
				<a class = "category" href = "#" onClick = "setClassGroup('http://localhost:8080/vivo/individual/vitroClassGroupBAO_0000020');changeTabs(this);">Target</a>	
				<a class = "category" href = "#" onClick = "setClassGroup('http://localhost:8080/vivo/individual/vitroClassGroupBAO_0000023');changeTabs(this);">Design</a>	
				<a class = "category" href = "#" onClick = "setClassGroup('http://localhost:8080/vivo/individual/vitroClassGroupBAO_0000035');changeTabs(this);">Detection</a>	
				<a class = "category" href = "#" onClick = "setClassGroup('http://localhost:8080/vivo/individual/vitroClassGroupBAO_0000019');changeTabs(this);">Format</a>	
<!--				<a class = "category fancyTip" href = "#" onClick = "setClassGroup('all');changeTabs(this);" title = "View all semantic triples from the ontology matching the current search">All URI</a>	-->	
			</div>
			
			<div id="lrgResultMsg">
			</div>
			<span id = "pagination" style = "font-weight: normal; padding-top: 3px;"></span>
			<div id = "dynamic"></div>
		</div>
		<input type = "hidden" id = "mode" name = "mode"/>
		<br><br>
		<span id = "worksetResponse"></span>


	</div>
	</form>
	<div id = "check"></div>
	<br><br><br>

<%@ include file = "footer.jsp" %>


</body>

</html>

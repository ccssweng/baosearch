<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ include file = "header.jsp" %>

<%
	String mode = "";
        if(request.getParameter("mode") == null)
		mode = "format";
	else
		mode = request.getParameter("mode");

	String ucMode = mode.substring(0,1).toUpperCase() + mode.substring(1);
%>

	<title>BAOSearch - Browse</title>

	<!-- CSS Files -->
	<link type="text/css" href="/baosearch/css/treemap.css" rel="stylesheet" />

	<!--[if IE]><script language="javascript" type="text/javascript" src="/baosearch/js/excanvas.js"></script><![endif]-->

	<!-- JIT Library File -->
	<script language="javascript" type="text/javascript" src="/baosearch/jit/jit-yc.js"></script>

	<!-- JIT Treemap File -->
	<script language="javascript" type="text/javascript" src="/baosearch/jit/treemap.js"></script>

	<!-- Top Level JSON Data File -->
	<script language="javascript" type="text/javascript" src="/baosearch/jit/<%=mode%>.js"></script>

        <script type="text/javascript">
        $(function() {
		init();
		$("#toolbox").append('<a href = "#" class = "tool clearWs"  onClick = "$(\'#mode\').val(\'clear\');return processWorkset(\'assay\');">Clear Workset</a>');
        });

        function zoomTree(id)
        {
                $.getJSON("update-treemap?mode=<%=mode%>&input="+id, updateTreemap);
                return false;
        }

	function changeMode()
	{
		if($("#mode").val != "<%=mode%>" && $("#mode").val != "default")
			$("#browseForm").submit();
	}

        </script>

<%@ include file = "topmenu.jsp" %>

        <div class = "title">
                <h1 style = "display: inline;">Browsing By <%=ucMode%> | Change Category: </h1> 
		
		<form id = "browseForm"  style = "display: inline;">
			<select id = "mode" name = "mode" onChange = "changeMode();">
				<option value = "default">--Select--</option>
				<option value = "target">Target</option>
				<option value = "detection">Detection</option>
				<option value = "design">Design</option>
				<option value = "format">Format</option>
			</select>
		</form>
                <hr>
                <br>
        </div>
	
	
	<div class = "content">

        	    <b>Left click</b> to set an element as the new parent node for the visualization. 
        	    <b>Right click</b> to add an assay to the workset. 
		    Click <b><a id = "back" href = "#" style = "margin: 0px;">Here</a></b> to go back to the top level. 
		    </br></br>
	        <input type="radio" id="r-sq" name="layout" checked="checked" value="left" style = "display: none;"/>
        	<input type="radio" id="r-st" name="layout" value="top"  style = "display: none;"/>
            	<input type="radio" id="r-sd" name="layout" value="bottom"  style = "display: none;"/>


    		<div id="infovis"></div>    

		<div id="log"></div>

	</div>
<%@ include file = "footer.jsp" %>

</body>



</html>

<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ include file = "header.jsp" %>

        <title>BAOSearch -  Privacy Policy</title>

<%@ include file = "topmenu.jsp" %>

        <div class = "title">
                <h1>Privacy Policy</h1>
                <hr>
                <br>
        </div>


        <div class = "content">
	<p>Thank you for visiting the BAOSearch website, hosted by 
University of Miami Center for Computational Science (CCS).
</p>
<p>
This site conforms to the <a href = 
"http://www.ccs.miami.edu/privacy.html">privacy 
practices</a> of 
the 
Center for Computational Science. Our 
policy is to collect no personally identifiable information about you 
when you visit our website unless you actively choose to make such 
information available to us. This site, however, contains links to other 
non-University of Miami sites. The University of Miami and the Center 
for Computational Science are not responsible for the privacy practices 
and/or the content of such websites. Please be sure to check the 
individual privacy policies of those sites.
</p>

        </div>


        <br>

<%@ include file = "footer.jsp" %>

</body>



</html>


<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ include file = "header.jsp" %>

        <title>BAOSearch - Technology</title>

<%@ include file = "topmenu.jsp" %>

        <div class = "title">
                <h1>Technology</h1>
                <hr>
                <br>
        </div>


        <div class = "content">

BAOSearch is a multi-tier, web-based, AJAX-enabled, application 
written primarily in Java and built following a ReSTful web services 
paradigm.  The service-based aspect of the architecture allows the UI to 
be separated from the storage and manipulation of the data, and provides 
well defined interfaces for UI components to use to access and manipulate 
application data.  This separation of concerns creates the potential of 
developing multiple UIs that access the same service, but which render 
the data differently, or run on different platforms (e.g., browsers, 
mobile apps).  This architecture also creates an opportunity for other 
software applications (not only User Interfaces) to access the system to 
query and retrieve data.  The application middle-tier was written in 
Java using Jena<sup>1</sup> for accessing and manipulating semantic 
data, and also using components from the open source VIVO<sup>2</sup> 
project.  The browser-based UI was built using JSP and Javascript, with components 
from several Javascript libraries including jQuery<sup>3</sup>. The chemical search component is powered by Chemaxon<sup>4</sup>. 
All data is stored in MySQL.  SDB<sup>5</sup> is used for the triple-store and other 
data required by the application is stored in a relational schema and 
accessed using Hibernate<sup>6</sup>.  BAOSearch is still undergoing active 
development with new features being added and possible changes being 
made to the underlying architecture and enabling technologies.  In the 
near future we will release all of the source code 
for BAOSearch as an open source offering.  This code will be available 
for download here and also posted to other sites, the complete list of 
which will be published on this site once the code is released.
<hr/>
[1] <a href = "http://jena.sourceforge.net" target = "blank">Jena</a>
<br> 
[2] <a href = "http://www.vivoweb.org" target = "blank">VIVO</a> 
<br>
[3] <a href = "http://jquery.com" target = "blank">jQuery</a>
<br>
[4] <a href = "http://www.chemaxon.com/" target = "blank">Chemaxon</a>
<br>
[5] <a href = "http://openjena.org/SDB" target = "blank">SDB</a>
<br>
[6] <a href = "http://www.hibernate.org" target = "blank">Hibernate</a> 
 	</div>

        <br>

<%@ include file = "footer.jsp" %>

</body>



</html>


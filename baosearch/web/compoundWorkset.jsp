<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>

<%@ include file = "header.jsp" %>

	<title>BAOSearch - Compound Workset</title>

	<!-- Required for the form to allow the user to pick what columns to download, uncomment to enable -->
	<!--<link rel = "stylesheet" href = "/baosearch/css/colorbox.css" type = "text/css"/>-->
	<link rel = "stylesheet" href = "/baosearch/jquery-ui/css/jquery-ui-1.8.6.custom.css" type = "text/css"/>


	<script type="text/javascript" language="javascript" src="jquery-ui/jquery-ui-1.8.6.custom.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/compoundWorkset.js"></script>
	<!-- Required for the form to allow the user to pick what columns to download, uncomment to enable -->
	<!--<script src="js/jquery.colorbox-min.js"></script>-->

	<script type="text/javascript">

	<%
		if(session.getAttribute("cpdWorksetArr") != null)
		{
			out.print("var cpdWorkset = \"");
					List cpdArr = (List)session.getAttribute("cpdWorksetArr");

			for(int i = 0; i < cpdArr.size(); i++)
					{
						out.print(cpdArr.get(i));
				if(i != cpdArr.size()-1)
					out.print(",");
					}
			out.print("\";");
		}
		else if(request.getParameter("reset") != null && request.getParameter("reset").equals("true"))
			out.print("var cpdWorkset = \"RESET\"");
		else
			out.print("var cpdWorkset = \"ERROR\"");
	%>


	$(function() {
		processQuery();
		
		//Required for the form to allow the user to pick what columns to download, uncomment to enable
		//$(".dlPopup").colorbox({width:"50%", inline:true, href:"#dlForm"});
	});

	</script>
	
<%@ include file = "topmenu.jsp" %>

        <div class = "title">
                <h1>Compound Workset</h1>
                <hr>
        </div>


	<span style = "font-weight: bold;">Click the substance ID to display the assays in which the compound was tested. The assays can be added to an assay workset. <br></span>
        <div  id = "filterPanel" style = "display: none; text-align: left; margin: auto; padding: 10px;">
		<div id = "wsForm">
			<form id = "filterForm" name = "filter" action = "chart" onsubmit = "return filterEndpoints();">
				<h4>Endpoint Filtering Options - Select Endpoint</h4>
	   			<div id = "epListContainer"></div>

				<div style = "clear: both;"></div>
				<br>
				<input type = "submit" value = "Filter" class = "stylish-button">

				<input name = "sids" id = "sids" type = "hidden"/>
				<input name = "mode" type = "hidden" value = "cpdWs"/>
				<input type = "hidden" name = "chartMode" value = "endpoint"/>
			</form>
		</div>
	</div>


	<form name = "cpdWorksetForm" id = "cpdWorksetForm" action = "compound-workset" method = "GET">

	<div class = "content">

		<div id="tabs">
			<ul id="tabTitles" style = "padding: 5px; margin: 0px;">
				<li class ="tabLink ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#" onClick="showList();$('#tabTitles li').toggleClass('ui-tabs-selected ui-state-active');">List</a></li>
				<li class ="tabLink ui-state-default ui-corner-top"><a href="#" onClick="showMatrix(); $('#tabTitles li').toggleClass('ui-tabs-selected ui-state-active');">Matrix</a></li>
			</ul>
				
			<div id="tabContainer" style = "clear: both;">
			</div>

		
			<div id = "dynamic" style = "overflow: auto;"></div>
			<input type = "hidden" id = "reset" name = "reset"/>
			<input type = "hidden" id = "mode" name = "mode"/>
		</div>
		<br>
	</div>
        <br>
	</form>


	<div style = "display: none;">
		<div id = "dlForm" style = "background: #ffffff;">
			<form id = "downloadForm" name = "download" class = "baoForm" onSubmit = "downloadWorkset(); return false;">
				<h2>Workset Download Options</h2>
				<div style = "padding-left: 50px; background: url('/baosearch/images/1.png') no-repeat;">
				<h3>Select the Columns:</h3>
				<input type = "checkbox" checked = "yes" name = "columns" value = "cpd_id"/>Substance ID<br>
				<input type = "checkbox" checked = "yes" name = "columns" value = "iupac"/>IUPAC Name<br>
				<input type = "checkbox" checked = "yes" name = "columns" value = "mol_wt"/>Molecular Weight<br>
				<input type = "checkbox" checked = "yes" name = "columns" value = "mol_formula"/>Molecular Formula
				</div>
				<div style = "padding-left: 50px; background: url('/baosearch/images/2.png') no-repeat;">
				<h3>Select Delimiter:</h3>
				<select name = "delimiter" id = "delimiter">
					<option selected value = "comma">Comma-Separated</option>
					<option value = "tab">Tab-Separated</option>
				</select>
				</div>
				<div style = "padding-left: 50px; background: url('/baosearch/images/3.png') no-repeat;">
				<h3>Enter File Name:</h3>
				<input type = "text" name = "filename" id = "filename" value = "cpdWorkset"/>
				<br>
				<input type = "button" class = "stylish-button" value = "Download" onClick = "downloadWorkset();"/>
				</div>
			</form>
		</div>
	</div>
	
	<br><br><br>

<%@ include file = "footer.jsp" %>

</body>



</html>

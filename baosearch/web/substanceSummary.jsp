<%@ include file = "header.jsp" %>

        <title>BAOSearch - Substance Summary</title>

        <link rel = "stylesheet" href = "/baosearch/jquery-ui/css/jquery-ui-1.8.6.custom.css" type = "text/css"/>

	<style type = "text/css">
		.dataTables_wrapper { min-height: 130px; }
		#dynamicPlus table.display thead th { background: #dce6e7; color: #145c61;}
		#dynamicPlus table tr.odd td, tr.even td { border-bottom: 1px dashed #a0a7a7; }
	</style>

	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="jquery-ui/jquery-ui-1.8.6.custom.min.js"></script>

        <script type="text/javascript">

                var sid = "<%=request.getParameter("sid")%>";

            	$(document).ready(function(){                
                        $("#message").html("<img src = '/baosearch/images/loader2.gif'>");
			processQuery();
            	});


		function processQuery()
		{
			$.getJSON("compound-by-id?input="+<%=request.getParameter("sid")%>, processDetails);
			$.getJSON("substance-summary?input="+<%=request.getParameter("sid")%>, processComputed);
			$.getJSON("conc-summary?input="+<%=request.getParameter("sid")%>, processConcentration);
		}

		function processDetails(response)
		{

			var subDetails = "<strong>SID:</strong> "+response.data[0].SubstanceId
					+"<br>"
					+"<br><strong>IUPAC Name:</strong> "+response.data[0].Name
					+"<table class = 'sub-summary'><tr>"
					+"<td><strong>MW:</strong> "+response.data[0].MolecularWeight+"</td>"
					+"<td><strong>MF:</strong> "+response.data[0].Formula+"</td></tr></table>"
					+"<input type = 'checkbox' name = 'cpdWorksetList' value = '<%=request.getParameter("sid")%>'"
					+" checked = 'true' style = 'display: none;'>";
			$("#subDetails").html(subDetails);

			$('a.detailsPopout').click(popOut);	
		}

		function processComputed(response)
		{
			var computed = "<table class = 'sub-summary'><tr>"
					+"<td><strong>TPSA:</strong> "+response.tpsa+"</td>"
					+"<td><strong>LogP:</strong> "+parseFloat(response.logp).toFixed(2)+"</td>"
					+"<td><strong>LogD:</strong> "+parseFloat(response.logd).toFixed(2)+"</td></tr><tr>"
					+"<td><strong>RB:</strong> "+response.rotatable_bonds+"</td>"
					+"<td><strong>HBA:</strong> "+response.h_bond_acceptors+"</td>"
					+"<td><strong>HBD:</strong> "+response.h_bond_donors+"</td></tr><tr>"
					+"<td><strong>Lipinski Rule of 5 (4 of 4):</strong> "+response.lipinski_r5+"</td>"
					+"<td><strong>Lipinski Rule of 3:</strong> "+response.lipinski_r3+"</td></tr><tr>"
					+"<td><strong>Lead Likeness:</strong> "+response.lead_likeness+"</td>"
					+"<td><strong>Bioavailability:</strong> "+response.bioavailability+"</td></tr></table>";

			$("#computedDetails").html(computed);
			$('#toolbox').append('<a href = "#" class = "tool addWs" onClick = "$(\'#mode\').val(\'add\');return processWorkset(\'assay\',\'assayWorksetForm\');">Add To Workset</a><a href = "#" class = "tool clearWs"  onClick = "$(\'#mode\').val(\'clear\');return processWorkset(\'assay\',\'assayWorksetForm\');">Clear Workset</a>');
			$('#message').html("");
		}

                function processConcentration(response)
                {
			var data = response.data;
			var conc, epVal;

			if(data.length == 0)
			{
				$('#dynamic').html("No results were returned.");
				return;
			}
			$("#assayCount").html("<b>Tested Concentration Response Assays: </b>"+data.length);

                        var aDataSet = "[ ";
                        for(var i = 0; i < data.length; i++)
                        {	
				
				if(data[i].epval_num)
					epVal = parseFloat(data[i].epval_num);
				else
					epVal = "Inactive";
				aDataSet += "["+
						"'<input type = \"checkbox\" name = \"assayWorksetList\" onClick = \"highlightRow(this);\" value = \""+data[i].assay_id+"\">" + "','" +
						"<a href = \"/vivo/bao-individual?uri="+$.URLEncode("http://www.bioassayontology.org/bao/BAO_0000015_")+data[i].assay_id+"\">"+data[i].assay_id+"</a>','"+
						data[i].name.replace(/'/gi, "`") +"','" +
						data[i].UniProt_ID +"','" +
						data[i].target.replace(/'/gi, "`") + "','" +
						data[i].ep_name  +"','" +
						epVal  +"','" +
                                                data[i].ep_unit +"','" +
						data[i].bao_format+"','"+
						data[i].DesignMainConcept + "','" +
					 	data[i].DetectionTechnology + "','" + 
                                                data[i].EndpointModeOfAction +
						"']";
                                if(i != (data.length - 1))
                                        aDataSet += ",";
                        }
                        aDataSet += " ]";
                        var colSet;

                        colSet = "[ { \"sTitle\": \"<input type='checkbox' value = '0' name='selectAllB' id='selectAllB' onClick='jqCheckAll(this.name)'>\"}, { \"sTitle\": \"Assay ID\" }, { \"sTitle\": \"Assay Name\" }, { \"sTitle\": \"UniProt ID\" }, { \"sTitle\": \"Target\" }, { \"sTitle\": \"Endpoint\" }, { \"sTitle\": \"Value\" }, { \"sTitle\": \"Unit\" }, { \"sTitle\": \"Format\" }, { \"sTitle\": \"Design\" }, { \"sTitle\": \"Detection Technology\" }, { \"sTitle\": \"Mode of Action\" }, ]";

                        $('#dynamic').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="conc"></table>' );
                        oTable = $('#conc').dataTable( {
				"bJQueryUI": false,
                                "aaData": eval(aDataSet),
				"bSort": true,
				"aaSorting": [],
                                "aoColumns": eval(colSet),
				"sDom": '<"top"<"clear">>rt<"bottom"<"clear">>',
				"iDisplayLength": -1,
				"aLengthMenu": [[25, 50, -1], [25, 50, "All"]]
                        } );		
		}


		function popOut() 
		{
            		var url = this.href;
	    		var pTitle;
	    		var pWidth = 350;
			var linkTitle = this.title;
	    		if(this.title == "Click to Zoom")
				pTitle = "Hi-Res Structure - "+this.id;
	    		else if(this.title ==  "View Details")
	    		{
		    		pTitle = "Assays Related to This Substance";
		    		pWidth = 650;
	    		}
	    		else
		    		pTitle = this.id;	
            		var dialog = $('<div id = "p'+this.id+'" style="display:hidden"></div>').appendTo('body');
            		// load remote content
            		$.get(
            			url,
                		function (responseText, textStatus, XMLHttpRequest) {
		    			if(textStatus == "success")
					{
						if(linkTitle == "Click to Zoom")
							dialog.html(responseText);	
						else
							printAssayList(responseText, dialog);
	                    			dialog.dialog({ minWidth: pWidth, title: pTitle });
					}
		    			else
		    			{	    
						dialog.html("The requested substance was not found");
			    			dialog.dialog({ minWidth: pWidth, title: pTitle });
		    			}
                		}
            		);
            		//prevent the browser from following the link
            		return false;
        	}
		
		function processWorkset(type, formName)
		{
			
			$.get("update-"+type+"-workset?"+$("#"+formName+"").serialize(), worksetResult);
			return false;
		}

		function worksetResult(response)
		{
			if($.trim(response).substr(0,5) == "ERROR")
				$('#message').removeClass().addClass('errorHighlight');
			else
				$('#message').removeClass().addClass('successHighlight');

			$('#message').show();
			setTimeout("$('#message').fadeOut('slow')", 4000);

	                setTimeout("$('#message').hide()", 15000);
			$('#message').html(response);

		}



        </script>


<%@ include file = "topmenu.jsp" %>

        <div class = "title">
                <h1>Substance Summary</h1>
                <hr>
        </div>


        <div class = "content">
		<form id = "assayWorksetForm">
		<div style = "margin: auto; width: 680px;">
			<img src = "/chemfile?type=sid&format=jpeg:w220,h220&id=<%=request.getParameter("sid")%>" style = "float: left;">
			<span id = "subDetails"></span>
		</div>
		<div id = "computedDetails"></div>			
		<span id = "assayCount"></span>
		<div id = "dynamic"></div>
		<div id = "dynamicPlus"></div>
		<input type = "hidden" id = "mode" name = "mode"/>
		</form>
	</div>


<%@ include file = "footer.jsp" %>

</body>



</html>


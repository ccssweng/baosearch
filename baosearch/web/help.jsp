<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<%@ include file = "header.jsp" %>

        <title>BAOSearch - Help</title>

<%@ include file = "topmenu.jsp" %>

        <div class = "title">
                <h1>Help</h1>
                <hr>
                <br>
        </div>


        <div class = "content">
		BAOSearch is a semantic web application for querying, browsing and downloading biological screening data from the BAO Repository, a database of screening data relevant for drug 
		discovery that has been annotated using the BioAssay Ontology.

		<h3>How to search assays?</h3>
		The Standard Search is simple, type biological assay relevant terms in the search box, hit the Enter (Return) key and BAOSearch will search the BAO repository for the assays relevant 
		to your search. 
		<br>
		Keep in mind:
		<ul>
			<li style = "font-size: 13px;">Every word matters</li>
			<li style = "font-size: 13px;">Search is case insensitive</li>
		</ul>
		
		<h3>How to search compounds?</h3>
		The Compound Search page provides features for directly querying compounds that are associated with the assays annotated in the BAO Repository. Results are returned in a sortable, 
		gridded display that supports many of the same features as the grid use for viewing standard search results.  
		<br>
		To search for compounds by Substance ID select the "Substance ID" radio button and type or paste a list of SIDs into the text box. Press the Search button to retrieve the list of 
		matching compounds.
		<br>
		To search for compounds using an exact SMILES string match, select the "Exact SMILES" radio button and type or paste a list of SMILES strings into the text box. Press the Search button 
		to retrieve the list of matching compounds.
		<br>
		<h3>How to manipulate worksets?</h3>
		BAOSearch allows the user to manipulate two types of worksets:  assay and compound. 
		<br>
 		Worksets are sets of Assays or Compounds that have been selected for further exploration or export. Worksets are persistent only for the length of the session, which will timeout 
		after approximately 25 minutes of inactivity.
		<br>
		The Assay Workset allows the user to filter endpoints from a set of assays of interest.To filter endpoints select the assays of interest using the checkbox present in each row and click 
		the Filter Workset button. The Endpoint Filter Options will then be displayed.Select the endpoint, select the operator, enter the value and click filter. Additional filtering 
		conditions may be added or removed using the Add and Undo Endpoint controls.
		<br><br>
		Please refer to the <a href = "/baosearch/web/help.pdf">BAOSearch User Manual</a> for more information.
        </div>


        <br>

<%@ include file = "footer.jsp" %>

</body>



</html>


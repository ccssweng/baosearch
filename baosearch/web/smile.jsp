<%/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */


%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<title>BAOSearch - Send A Smile</title>
	<link rel = "stylesheet" href="css/stylesheet.css" type="text/css" title="main" media="screen"/>
	<link rel="alternate stylesheet" href = "css/star-trek.css" title = "alt1" type = "text/css"/>

	<script src = "js/jquery-1.4.2.min.js" type = "text/JavaScript"></script>

        <script src = "js/baosearch.js" type = "text/JavaScript"></script> 


	<script type="text/javascript">
	$(function() {

	});

	function validateFeedback()
	{
                if($("#fbName").val().length == 0)
                        $("#message").html("<font color = \"red\">Please enter a valid name</font>");
                else if($("#fbEmail").val().length == 0 || $("#fbEmail").val().indexOf('@') == -1)
                        $("#message").html("<font color = \"red\">Please enter a valid email address</font>");
		else if($("#msgBody").val().length == 0)
			$("#message").html("<font color = \"red\">Please enter a valid comment</font>");
		else			
		{
			$.post("feedback", $("#feedbackForm").serialize(), function(response) { $("#message").html(response)});
                        $("#message").html("<img src = '/baosearch/images/loader2.gif'/>");
		}
	
		return false;
	}

	</script>

</head>

<body class = "home">



	<div class = "splash" style = "position: relative">

		<div style = "background: url('/baosearch/images/smile-large.png') no-repeat top right; height: 130px; width: 90%;">
			<div class = "bao-logo"></div>
		</div>		

		<div class = "left-menu-bar">
			<a class = "left-menu substance" href = "#" onClick = "$('#substanceOpts').toggle('slow');">Substance Search</a>
			<div id = "substanceOpts" style = "display: none;">

				<a class = "left-menu" href = "substance">- Exact Term</a>
				<a class = "left-menu" href = "substance-search">- Structure</a>
			</div>
                        <a class = "left-menu filter" href = "#" onClick = "$('#conceptOpts').toggle('slow');">Concept Search</a>
			<div id = "conceptOpts" style = "display: none;">
				<a class = "left-menu" href = "filter">- Endpoints</a>
				<a class = "left-menu" href = "assays-by-concept">- Assays</a>

			</div>
			<!--<a class = "left-menu browse" href = "browse">Browse</a>-->
                        <a class = "left-menu visualize" target = "blank" href = "http://bioportal.bioontology.org/ontologies/45410?p=terms#visualization">Visualize</a>
			<a class = "left-menu help" href = "http://www.bioassayontology.org/wiki/index.php/BAOSearch_User_Guide">Help</a>
			<a class = "left-menu download" href = "http://bioassayontology.org/wiki/index.php/Development:Versions">Download Ontology</a>
			<a class = "left-menu contact" href = "http://bioassayontology.org/index.php?option=com_contact&view=contact&id=3&Itemid=74">Contact</a>
			<a class = "left-menu technology" href = "technology">Technology</a>

		</div>

		<div class = "blurb">
			<h4>BAOSearch Made Me Happy Because...</h4>
			We would love to hear how BAOSearch made your searching experience better. Whether you found 
something you were looking for easily and quickly or learned a surprising fact or if you just liked the colors, please 
send us note by filling in your message in the box below. Remember to enter the text in the image correctly in the box 
next to it - it helps us weed out spam and get to your comments quicker. Thanks!
			<form id = "feedbackForm" method = "post" onSubmit = "return validateFeedback();">
				<b>Name:</b> <input type = "text" name = "fbName" id = "fbName" style = "margin: 2px 0px;"><br>
				<b>Email:</b> <input type = "text" name = "fbEmail" id = "fbEmail" style = "margin: 2px 0px;">
				<textarea rows = "4" cols = "60" name = "body" id = "msgBody"></textarea>
				<input type = "hidden" name = "mode" value = "smile"/>
				<p><img src="jcaptcha.jpg" style = "float: left; padding: 0px 5px;"/>
				<input type="text" name="jcaptcha" value="" style = "height: 25px; font-size: 18px;"/></p>
				<input type = "submit" value = "Submit"/>
				<br>
				<span id = "message"></span>
			</form>

		</div>



	</div>
	
	<br>

	

</body>



</html>


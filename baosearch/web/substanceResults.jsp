<%@ include file = "header.jsp" %>

	<title>BAOSearch - Substance Search Results</title>

	
        <link rel = "stylesheet" href = "/baosearch/jquery-ui/css/jquery-ui-1.8.6.custom.css" type = "text/css"/>


	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
	<script src="js/jquery.colorbox-min.js"></script>
      <script type="text/javascript" language="javascript" src="jquery-ui/jquery-ui-1.8.6.custom.min.js"></script>


	<script type="text/javascript">

	$(function() {
                $("#message").html("<img src = '/baosearch/images/loader2.gif'>");
		processQuery();
	});
	
	var newClassGroup = "";
	var worksetArr = new Array();
	var totalReults = 0;
	var pagesize = 25;
	var pagenum = 1;

        var searchTerm = "<%= request.getParameter("searchTerm") %>";
        var mode = "<%= request.getParameter("mode") %>";
<% 
if(request.getParameter("mode").equals("structure"))
{
		
%>
	var searchtype = <%= request.getParameter("searchtype") %>;
<%
}
%>
                function renderTable(response) 
		{
                        $('#toolbox').html("");
                        var data = response.data;

			if(data.length < 1)
			{
				$("#dynamic").html("Sorry, no results were found for your query. Please ensure you have selected the right mode (SMILES or Substance ID or IUPAC name) and that the input is in the correct format");
				$("#message").html("");
				return false;
			}	
			totalResults = parseInt(response.sidCount);

			$("#pagination").html("Showing page "+pagenum+" of "+(Math.ceil(totalResults/pagesize)));
			$("#pagination").show();	

                        var aDataSet = "[ ";
                        for(var i = 0; i < data.length; i++)
                        {
                                aDataSet += '["<input type = \'checkbox\' name = \'cpdWorksetList\' onClick = \'highlightRow(this);\' value = \''+data[i].SubstanceId+'\'>'+
					'","<a href = \'fetch-assays?input='+data[i].SubstanceId+'\' '+
					'class = \'actionLink detailsPopout\' title = \'View Assays connected to this substance\'>View Assays</a>'+
					'<br><a href = \'sub-summary?sid='+data[i].SubstanceId+'\' '+
					'class = \'actionLink\' title = \'View summary for this substance\'>View Summary</a>","'+
					'<a href = \'zoom-structure?sid='+data[i].SubstanceId+'\' class = \'zoom\' title = \'Click to Zoom\' id = \''+data[i].SubstanceId+'\'><img class = \'bgLoad\' src=\'/chemfile?type=sid&id='+data[i].SubstanceId+'&format=jpeg:w100,h100\'/></a>'+
					'","'+data[i].SubstanceId+'","'+data[i].Name+'","'+data[i].MolecularWeight+'"]';
                                if(i != (data.length - 1))
                                        aDataSet += ",";
                        }
                        aDataSet += " ]";
                        var colSet;

                        colSet = "[ { \"sTitle\": \"<input type='checkbox' value = '0' name='selectAllB' id='selectAllB' onClick='jqCheckAll(this.id)'>\"}, { \"sTitle\": \"Actions\" }, { \"sTitle\": \"Structure\" }, { \"sTitle\": \"Substance ID\" }, { \"sTitle\": \"IUPAC Name(s)\" }, { \"sTitle\": \"Mol. Wt.\" } ]";

//                        alert(aDataSet);
//                        alert(colSet);
                        $('#dynamic').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>' );
                        oTable = $('#example').dataTable( {
                                "aaData": eval(aDataSet),
                                "aoColumns": eval(colSet),
				"sDom": '<"top"flp<"clear">>rt<"bottom"flp<"clear">>',
				"iDisplayLength": pagesize,
				"aLengthMenu": [[25, 50, 100], [25, 50, 100]]
                        } );



			$('#example_next').click(function() {
					if(pagenum != (Math.ceil(totalResults/pagesize)))
					{
						pagenum++;
				                $("#pagination").show();
                				$("#pagination").html("Showing page "+pagenum+" of "+(Math.ceil(totalResults/pagesize)));
						processQuery("plus");					
						$('#message').html("<img src = '/baosearch/images/loader2.gif'/>");
					}
				});
			$('#example_previous').click(function() {
					if(pagenum != 1)
					{
						pagenum--;
                                                $("#pagination").show();
                                                $("#pagination").html("Showing page "+pagenum+" of "+(Math.ceil(totalResults/pagesize)));
	                                        processQuery("minus");
						$('#message').html("<img src = '/baosearch/images/loader2.gif'/>");
					}
				});

			$('select[name=example_length]').change( function() { pagesize = $('select[name=example_length]').val(); processQuery('noop'); $('#message').html("<img src = '/baosearch/images/loader2.gif'/>"); });
		
			$('#toolbox').append('<a href = "#" class = "tool addWs" onClick = "$(\'#mode\').val(\'add\');return processWorkset(\'compound\',\'cpdWorksetForm\');">Add To Workset</a>');
			$('#toolbox').append('<a href = "#" class = "tool clearWs" onClick = "$(\'#mode\').val(\'clear\');return processWorkset(\'compound\',\'cpdWorksetForm\');">Clear Workset</a>');


        		$('a.zoom').click(popOut);
			$('a.detailsPopout').tipsy({gravity: 's'})
	                $('a.actionLink').tipsy({gravity: 's'})

			$('a.detailsPopout').click(popOut);	
	                $("#message").html("");


			//$(".detailsPopout").colorbox({width: "70%"});
                }

		function fnShowHide( iCol )
		{
			var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
			oTable.fnSetColumnVis( iCol, bVis ? false : true );
		}

                function processQuery(oper)
                {
			if(oper == "plus")
			{
				pagenum++;
			}
			else if(oper == "minus" && pagenum > 1)
			{
				pagenum--;
			}
			
                        if(searchTerm && mode && searchTerm != "null")
                        {
                                if(mode == "compound")
				{
                                        $.ajax( {type:"GET",url:"compound-by-id",dataType:"json",data: {input: searchTerm, pagesize: pagesize, pagenum: pagenum}, success: processResponse, error: handleError });
                                }
				else if(mode == "smiles")
				{
                                        $.ajax( {type:"GET",url:"compound-by-smiles",dataType:"json",data: {input: searchTerm, pagesize: pagesize, pagenum: pagenum}, success: processResponse, error: handleError });
				}
				else if(mode == "iupac")
				{
                                        $.ajax( {type:"GET",url:"substance-by-name",dataType:"json",data: {input: searchTerm, pagesize: pagesize, pagenum: pagenum}, success: processResponse, error: handleError });
				}
				else if(mode == "structure")
				{
                                        $.ajax( {type:"POST",url:"marvin-results",dataType:"json",data: {molecule: searchTerm, pagesize: pagesize, pagenum: pagenum, searchtype: searchtype}, success: processResponse, error: handleError });
				}

                        }
                        return false;
                }

		function handleError()
		{
			$("#dynamic").html("<span class = 'errorHighlight' style = 'height: 30px;'>An error occurred while connecting to the server. Please try again after some time.</span>");
		}

                function processResponse(response,status,xhr)
                {
			renderTable(response);
                }


		function processWorkset(type, formName)
		{
			$.get("update-"+type+"-workset?"+$("#"+formName+"").serialize(), worksetResult);
			return false;
		}

		function worksetResult(response)
		{
			if($.trim(response).substr(0,5) == "ERROR")
				$('#message').removeClass().addClass('errorHighlight');
			else
				$('#message').removeClass().addClass('successHighlight');

			$('#message').show();
			setTimeout("$('#message').fadeOut('slow')", 4000);

	                setTimeout("$('#message').hide()", 15000);
			$('#message').html(response);

		}


		function popOut() 
		{
            		var url = this.href;
	    		var pTitle;
	    		var pWidth = 400;
			var linkTitle = this.title;
	    		if(this.title == "Click to Zoom")
				pTitle = "Hi-Res Structure - "+this.id;
	    		else if(this.title ==  "View Assays connected to this substance")
	    		{
		    		pTitle = "Assays Related to This Substance";
		    		pWidth = 650;
	    		}
	    		else
		    		pTitle = this.id;	
            		var dialog = $('<div id = "p'+this.id+'" style="display:hidden"></div>').appendTo('body');
            		// load remote content
            		$.get(
            			url,
                		function (responseText, textStatus, XMLHttpRequest) {
		    			if(textStatus == "success")
					{
						if(linkTitle == "Click to Zoom")
							dialog.html(responseText);	
						else
							printAssayList(responseText, dialog);
	                    			dialog.dialog({ minWidth: pWidth, title: pTitle });
					}
		    			else
		    			{	    
						dialog.html("The requested substance was not found");
			    			dialog.dialog({ minWidth: pWidth, title: pTitle });
		    			}
                		}
            		);
            		//prevent the browser from following the link
            		return false;
        	}

		function printAssayList(response, dialog)
		{
			var assays = eval("("+response+")").assayId;
			//alert(eval("("+response+")"));
			var assayString = "<h3>IsPerturbagenOf:</h3><form id = \"assayWorksetForm\"><input type = \"hidden\" name = \"mode\" id = \"assayWsMode\" value = \"add\">";
			assayString += "<input type=\"checkbox\" onclick=\"jqCheckAllSpans(this.id, 'assayWorksetList')\" id=\"selectAllAssays\" name=\"selectAllAssays\" value=\"0\">";
			assayString += "Select All <br><input class = \"stylish-button\" type = \"button\" value = \"Add to Assay Workset\" onClick = \"$('#assayWsMode').val('add');processWorkset('assay','assayWorksetForm');\">";			
			assayString += "&nbsp;<input class = \"stylish-button\" type = \"button\" value = \"Clear Assay Workset\" onClick = \"$('#assayWsMode').val('clear');processWorkset('assay','assayWorksetForm');\"><br>";

                        if(assays.length == 0)
                                assayString = "Sorry, no annotated assays have been linked to this substance in the BAO Repository. However, this substance may be linked to PubChem assays that have not yet been added to the BAO Repository";
                        else
                        {
                                for(var i = 0; i < assays.length; i++)
                                        assayString += "<span class = \"wsAssay\" style = \"width: 60px; float: left;\"><input type = \"checkbox\" name = \"assayWorksetList\" onClick = \"highlightSpan(this);\" value = \""+assays[i]+"\"><a href = \"/vivo/individual?uri="+$.URLEncode("http://www.bioassayontology.org/bao#individual_BAO_0000015_")+ assays[i] + "\">"+assays[i]+"</a></span>";
                        }
			assayString += "</form>";
			dialog.html(assayString);
		}

	        function highlightSpan(cbox)
        	{
                	if(cbox.checked)
                	        cbox.parentNode.setAttribute('style', 'background: #ebffad; float: left; width: 60px;');
                	else
                        	cbox.parentNode.setAttribute('style', 'background: #FFFFFF;float: left; width: 60px;');
        	}


        	function jqCheckAllSpans( id, name )
        	{
                	$("INPUT[name='" + name + "'][type='checkbox']").attr('checked', $('#' + id).is(':checked'));
                	if($('#' + id).is(':checked'))
                	{
                        	$("span.wsAssay").css("background", "#ebffad");

                        	$('#message').show();
                        	setTimeout("$('#message').fadeOut('slow')", 4000);
                        	$('#message').removeClass().addClass('successHighlight');
                        	$('#message').html("Selected all records on this page");
                	}
                	else
                	{
                        	$("span.wsAssay").css("background", "#ffffff");
                	}
        	}


	</script>
	

<%@ include file = "topmenu.jsp" %>

	<br>
        <div class = "title">
                <h1>Substance Search Results</h1>
                <hr>
                <br>
        </div>

	<form name = "cpdWorksetForm" id = "cpdWorksetForm">
		<div class="content">
			<div id="tabs">
				<div id = "pagination"></div>
				<div id = "dynamic"></div>
			</div>
			<input type = "hidden" id = "mode" name = "mode"/>
			<br><br>
			<span id = "worksetResponse"></span>
		</div>
		<br>
	</form>

	<br><br><br>

<%@ include file = "footer.jsp" %>


</body>

</html>

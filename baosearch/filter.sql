insert into filter_redesign.ep_list(ep_name) select distinct ep_name from ep_details;

update filter_redesign.ep_details epd, filter_redesign.ep_list ep set epd.ept_id=ep.ept_id where epd.ep_name=ep.ep_name;

update filter_redesign.epval v, filter_redesign.ep_details epd set v.ept_id=epd.ept_id where v.assay_id=epd.assay_id and v.tid=epd.tid;

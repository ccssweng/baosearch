/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */
 
/**
 * This is a collection of functions that are used across multiple pages in the BAOSearch application
 */


/**
 * Register the search input field to show an explanatory text and then hide it on focus
 * Change the style of the text accordingly
 * 
 */
$(document).ready(function() {

	$('#searchTerm').click
	(
		function() {
			if (this.value == this.defaultValue) 
			{
				this.value = '';
				$('#searchTerm').removeClass('blurText').addClass('focusText');
			}
		}
	);

	$('#searchTerm').blur(
		function() {
			if (this.value == '') 
			{
				this.value = this.defaultValue;
				$('#searchTerm').removeClass('focusText').addClass('blurText');
			}
		}
	);

});

/**
 * Provide feedback to the user if the AJAX operation failed due to a connection issue
 *
 * 
 */
function handleError()
{
		$("#dynamic").html("<span class = 'errorHighlight' style = 'height: 30px;'>An error occurred while connecting to the server. Please try again after some time.</span>");
}

/**
 * Validate the input of the substance exact term search form before submission
 * Replace bad characters like nextlines in textareas
 * 
 */
function validate()
{
	$("#adv-query").val($("#adv-query").val().trim());
	if($("#adv-query").val().length < 1)
	{
		$("#message").text("Error: Please enter at least one search term");
		$('#message').removeClass().addClass('errorHighlight');
		$("#message").show();
		setTimeout("$('#message').hide()", 15000);
			return false;
	}
	if($('input[name=adSearchOption]:checked').val() == "iupac" && $("#adv-query").val().length < 7)
	{
		$("#message").text("Error: Search string is too short. Please enter a search string 8 characters or greater in length.");
		$('#message').removeClass().addClass('errorHighlight');
		$("#message").show();
		setTimeout("$('#message').hide()", 15000);
		return false;
	}

	var searchString = $("#adv-query").val();
	searchString = (searchString.replace(/ /g,''));
	if($('input[name=adSearchOption]:checked').val() == "compound" && isNaN(searchString.replace(/,/g,'')))
	{
		$("#status").text("Error: Please enter only numerical values for Substance ID search");
		$("#status").show();
		setTimeout("$('#status').hide()", 15000);
		return false;
	}

	return true;
}

/**
 * Validate the contents of the search box on each page before submission
 * Preface quotes with escape characters
 * 
 */
function validateSearch()
{
	if($("#searchTerm").val().length < 1)
	{
		alert("No search term selected. Please type in a search term and press enter");
		return false;
	}
	$("#searchTerm").val($("#searchTerm").val().replace(/"/g,'\\"'));
	return true;
}

/**
 * Validate the feedback form for either bug or smile reporting before submission
 *
 * 
 */
function validateFeedback()
{
		if($("#fbName").val().length == 0)
				$("#message").html("<font color = \"red\">Please enter a valid name</font>");
		else if($("#fbEmail").val().length == 0 || $("#fbEmail").val().indexOf('@') == -1)
				$("#message").html("<font color = \"red\">Please enter a valid email address</font>");
		else if($("#msgBody").val().length == 0)
				$("#message").html("<font color = \"red\">Please enter a valid comment</font>");
		else
{	
	$("#message").html("<img src = '/baosearch/images/loader2.gif'/>");
				$.post("feedback", $("#feedbackForm").serialize(), function(response) { $("#message").html(response)});
}
		return false;
}

/**
 * Checks if at least one checkbox from the group is selected
 *
 * @return (boolean) false if no checkboxes are checked, true otherwise
 */	
function validateCheckboxes(name)
{
	if($("input[@name='"+name+"[]']:checked").length < 1)
			{
				$('#message').show();
				setTimeout("$('#message').hide()", 5000);
					$('#message').removeClass().addClass('errorHighlight');
					$('#message').html("Please select at least one item");
		return false;
	}
	return true;
}

/**
* Checks to see if the at least one concept and one endpoint has been selected for filtering and shows a message
* Check concepts: Mode = 0
* Check endpoints: Mode = 1
* Check both: Mode = 2
* 
* @return (boolean) The validity of the filter selection
*/	
function validateFilter(mode) 
{
	if((mode == 1 || mode == 2) && epCount == 0)
	{
		$('#message').show();
		setTimeout("$('#message').hide()", 5000);
		$('#message').removeClass().addClass('errorHighlight');
		$('#message').html("Please select at least one endpoint");
		return false;
	}	

	if((mode == 0 || mode == 2) && $("#concepts").val() == null)
	{
		$('#message').show();
		setTimeout("$('#message').hide()", 5000);
		$('#message').removeClass().addClass('errorHighlight');
		$('#message').html("Please select at least one concept");
		return false;
	}
	return true;
}

/**
 * Toggle the state of all checkboxes in a form based on the state of the master checkbox
 * Change the highlights in the rows
 *
 * @param (String) The id of the master checkbox
 * @param (String) The name of the array of checkboxes controlled by the master
 */
function jqCheckAll( id, name )
{
	$("INPUT[@name=" + name + "][type='checkbox']").attr('checked', $('#' + id).is(':checked'));
	if($('#' + id).is(':checked'))
	{
		$("tr.odd").css("background", "#ebffad");
		$("tr.even").css("background", "#ebffad");			

		$('#message').show();
		setTimeout("$('#message').fadeOut('slow')", 4000);
			$('#message').removeClass().addClass('successHighlight');
		$('#message').html("Selected all records on this page");
	}
	else
	{
			$("tr.odd").css("background", "#ffffff");
			$("tr.even").css("background", "#ffffff");
	}
}

/**
 * Toggle highlight the background of a row in the table once its corresponding checkbox is checked
 *
 * @param (HTMLElement) The checkbox that was clicked
 */
function highlightRow(cbox)
{
	if(cbox.checked)
			cbox.parentNode.parentNode.setAttribute('style', 'background: #ebffad');
	else
			cbox.parentNode.parentNode.setAttribute('style', 'background: #FFFFFF');
}

/**
 * Toggle highlight the background of a span in the popup once its corresponding checkbox is checked
 *
 * @param (HTMLElement) The checkbox that was clicked
 */
function highlightSpan(cbox)
{
	if(cbox.checked)
			cbox.parentNode.setAttribute('style', 'background: #ebffad; float: left; width: 60px;');
	else
			cbox.parentNode.setAttribute('style', 'background: #FFFFFF;float: left; width: 60px;');
}

/**
 * Toggle the state of all checkboxes in a form based on the state of the master checkbox
 * Change the highlights in the spans
 *
 * @param (String) The id of the master checkbox
 * @param (String) The name of the array of checkboxes controlled by the master
 */
function jqCheckAllSpans( id, name )
{
	$("INPUT[name='" + name + "'][type='checkbox']").attr('checked', $('#' + id).is(':checked'));
	if($('#' + id).is(':checked'))
	{
			$("span.wsAssay").css("background", "#ebffad");

			$('#message').show();
			setTimeout("$('#message').fadeOut('slow')", 4000);
			$('#message').removeClass().addClass('successHighlight');
			$('#message').html("Selected all records on this page");
	}
	else
	{
			$("span.wsAssay").css("background", "#ffffff");
	}
 }


/**
 * Display a jQuery dialog containing the required HTML content.
 * The content can be of 3 different types:
 *		1. Hi-res image of a substance structure
 * 		2. List of assays connected to a substance/concept in a form
 *		3. The details of the selected item from Vivo
 */
function popOut()
{
	var url = this.href;
	var pTitle;
	var pWidth = 400;
	pTitle = this.id;
	var linkTitle = this.title;
	
	var dialog = $('<div id = "p'+this.id+'" style="display:hidden"></div>').appendTo('body');

	// load remote content
	dialog.load(
		url,
		{},
		function (responseText, textStatus, XMLHttpRequest) {
			if(textStatus == "success")
			{
				if(linkTitle == "View Assays connected to this substance")
					printAssayList(responseText, dialog);
				dialog.dialog({ minWidth: pWidth, title: pTitle });
			}
			else
			{       dialog.html("The requested substance was not found");
					dialog.dialog({ minWidth: pWidth, title: pTitle });
			}
		}
	);
	//prevent the browser from following the link
	return false;
}

/**
 * Print a list of assays connected to the substance in a form with options to add to the workset
 *
 * @param (HTTPResponse) The list of assays 
 * @param (HTMLElement) The dialog that will be displayed to the user 
 */
function printAssayList(response, dialog)
{
	var assays = eval("("+response+")").assayId;
	var assayString = "<h3>IsPerturbagenOf:</h3><form id = \"assayWorksetForm\"><input type = \"hidden\" name = \"mode\" id = \"assayWsMode\" value = \"add\">";
	assayString += "<input type=\"checkbox\" onclick=\"jqCheckAllSpans(this.id, 'assayWorksetList')\" id=\"selectAllAssays\" name=\"selectAllAssays\" value=\"0\">";
	assayString += "Select All <br><input class = \"stylish-button\" type = \"button\" value = \"Add to Assay Workset\" onClick = \"$('#assayWsMode').val('add');processWorkset('assay','assayWorksetForm');\">";			
	assayString += "&nbsp;<input class = \"stylish-button\" type = \"button\" value = \"Clear Assay Workset\" onClick = \"$('#assayWsMode').val('clear');processWorkset('assay','assayWorksetForm');\"><br>";

	if(assays.length == 0)
		assayString = "Sorry, no annotated assays have been linked to this substance in the BAO Repository. However, this substance may be linked to PubChem assays that have not yet been added to the BAO Repository";
	else
	{
		for(var i = 0; i < assays.length; i++)
			assayString += "<span class = \"wsAssay\" style = \"width: 60px; float: left;\"><input type = \"checkbox\" name = \"assayWorksetList\" onClick = \"highlightSpan(this);\" value = \""+assays[i]+"\"><a href = \"/vivo/individual?uri="+$.URLEncode("http://www.bioassayontology.org/bao#individual_BAO_0000015_")+ assays[i] + "\">"+assays[i]+"</a></span>";
	}
	assayString += "</form>";
	dialog.html(assayString);
}

/**
 * Perform the requested operations on the workset
 *
 * @param (String) type
 * @param (String) formName
 */
function processWorkset(type, formName)
{
	if(validateCheckboxes(type+"WorksetList") || $('#mode').val() == "clear")
		$.get("update-"+type+"-workset?"+$("#"+formName+"").serialize(), worksetResult);
	else
		return false;
}

/**
 * Function to receive the HTTP request response after workset operations and show error or success message
 *
 * @param (HTTPResponse) The object containing the endpoints list and details
 */
function worksetResult(response)
{
	$('#message').html(response);	
	if($.trim(response).substr(0,5) == "ERROR")
		$('#message').removeClass().addClass('errorHighlight');
	else
		$('#message').removeClass().addClass('successHighlight');

	$('#message').show();
	setTimeout("$('#message').fadeOut('slow')", 4000);
}
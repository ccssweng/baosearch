/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */
 
/**
 * Handle the response forwarded from processResponse() and render it in the form of a table containing columns for each substance
 * 
 * @param (HTTPResponse) response
 */
function renderTable(response) 
{
	$('#toolbox').html("");
	var data = response.data;
	if(data.length < 1)
	{
		$("#dynamic").html("Sorry, no results were found for your query. Please ensure you have selected the right mode (SMILES or Substance ID or IUPAC name) and that the input is in the correct format");
		$("#message").html("");
		return false;
	}	
	//alert(data);
	var aDataSet = "[ ";
	for(var i = 0; i < data.length; i++)
	{
		aDataSet += '["<input type = \'checkbox\' name = \'compoundWorksetList\' onClick = \'highlightRow(this);\' value = \''+data[i].SubstanceId+'\'>'+
					'","<a href = \'fetch-assays?input='+data[i].SubstanceId+'\' '+
					'class = \'detailsPopout\' title = \'View Assays connected to this substance\'>View Assays</a>","'+
					'<a href = \'zoom-structure?sid='+data[i].SubstanceId+'\' class = \'zoom\' title = \'Click to Zoom\' id = \''+data[i].SubstanceId+'\'><img class = \'bgLoad\' src=\'/chemfile?type=sid&id='+data[i].SubstanceId+'&format=jpeg:w100,h100\'/></a>'+
					'","'+data[i].SubstanceId+'","'+data[i].Name+'","'+data[i].MolecularWeight+'"]';
		if(i != (data.length - 1))
					aDataSet += ",";
	}
	aDataSet += " ]";
	var colSet;

	colSet = "[ { \"sTitle\": \"<input type='checkbox' value = '0' name='selectAllB' id='selectAllB' onClick='jqCheckAll(this.name)'>\"}, { \"sTitle\": \"Select\" }, { \"sTitle\": \"Structure\" }, { \"sTitle\": \"Substance ID\" }, { \"sTitle\": \"IUPAC Name(s)\" }, { \"sTitle\": \"Mol. Wt.\" } ]";

	//alert(aDataSet);
	//alert(colSet);
	$('#dynamic').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>' );
	oTable = $('#example').dataTable( {
						"aaData": eval(aDataSet),
						"aoColumns": eval(colSet),
						"sDom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
						"iDisplayLength": 25,
						"aLengthMenu": [[25, 50, -1], [25, 50, "All"]]
				} );

	$('#toolbox').append('<a href = "#" class = "tool addWs" onClick = "$(\'#mode\').val(\'add\');return processWorkset(\'compound\', \'cpdWorksetForm\');">Add To Workset</a>');
	$('#toolbox').append('<a href = "#" class = "tool clearWs" onClick = "$(\'#mode\').val(\'clear\');return processWorkset(\'compound\', \'cpdWorksetForm\');">Clear Workset</a>');

	$('a.zoom').click(popOut);
	$('a.detailsPopout').tipsy({gravity: 's'})

	$('a.detailsPopout').click(popOut);	
	$("#message").html("");
}

/**
 * Utility function to show and hide columns from datatable using column id
 * 
 * @param (number) iCol
 */
function fnShowHide( iCol )
{
	var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
	oTable.fnSetColumnVis( iCol, bVis ? false : true );
}

/**
 * Send a search query to the appropriate servlet depending on user input
 * 
 * 
 */
function processAdvancedQuery()
{
	if(searchTerm && mode && searchTerm != "null")
	{
		if(mode == "compound")
			$.ajax( {type:"GET",url:"compound-by-id",dataType:"json",data: {input: searchTerm}, success: processResponse, error: handleError });
		else if(mode == "smiles")
			$.ajax( {type:"GET",url:"compound-by-smiles",dataType:"json",data: {input: searchTerm}, success: processResponse, error: handleError });
		else if(mode == "iupac")
			$.ajax( {type:"GET",url:"substance-by-name",dataType:"json",data: {input: searchTerm}, success: processResponse, error: handleError });
	}
	return false;
}

/**
 * Handle the error response and display a message
 * 
 *
 */
function handleError()
{
	$("#dynamic").html("<span class = 'errorHighlight' style = 'height: 30px;'>An error occurred while connecting to the server. Please try again after some time.</span>");
}

/**
 * Handle the response from processAdvancedQuery() and forward it to the rendering function
 * 
 * @param (HTTPResponse) response
 * @param (number) status code for the response
 * @param (XMLHTTPResponse) xhr
 */
function processResponse(response,status,xhr)
{
	renderTable(response);
}


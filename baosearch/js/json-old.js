var json = {
    "id": "Thing",
    "name": "Thing",
    "data": {
        "definition": "undefined"
    },
    "children": [
        {
            "id": "bioassay component",
            "name": "bioassay component",
            "data": {
                "definition": "It refers to the various constituents of an experiment which is carried out for the purpose of screening a perturbing agent in a biological system."
            },
            "children": [
                {
                    "id": "endpoint",
                    "name": "endpoint",
                    "data": {
                        "definition": "An abstract concept representing a measurement or parameter quantifying or qualifying a perturbation. An endpoint consists of a series of data points, one for each perturbing agent employed by the assay.  Another way to think of an endpoint is as a column in a spreadsheet with \"compound ID\" on the vertical axis.  PubChem specifies two generic endpoints, \"Outcome\" and \"Score\", for every assay."
                    },
                    "children": [
                        {
                            "id": "temperature",
                            "name": "temperature",
                            "data": {
                                "definition": "null "
                            },
                            "children": [
                                {
                                    "id": "tm",
                                    "name": "tm",
                                    "data": {
                                        "definition": "Tm is defined as the midpoint temperatuere of the protein unfolding transition. It is determined by generating a thermal denaturation curve for the target molecule and taking the midpoint temperature between folding and unfolding state of the protein"
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "response",
                            "name": "response",
                            "data": {
                                "definition": "Reporting a response induced by the perturbagen such as percent inhibition. A response type of end point would typically be obtained from a single concentration perturbagen measurement."
                            },
                            "children": [
                                {
                                    "id": "raw activity reported",
                                    "name": "raw activity reported",
                                    "data": {
                                        "definition": "Measurement of fluorescence intensity"
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "fold response",
                                    "name": "fold response",
                                    "data": {
                                        "definition": "null "
                                    },
                                    "children": [
                                        {
                                            "id": "fold inhibition",
                                            "name": "fold inhibition",
                                            "data": {
                                                "definition": "The ratio of biological activity in the absence of an exogenous substance to that in its presence. It is the median of the same plateâs Min wells divided by test perturbagenâs observed response (raw data value). This result type is used exclusively with single point assays."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "fold activation",
                                            "name": "fold activation",
                                            "data": {
                                                "definition": "The ratio of biological activity in the presence of an exogenous substance to that in its absence. It is the test perturbagenâs observed response (raw data value) divided by the median of the same plateâs Min wells. This result type is used exclusively with single point assays. If the value is greater than 1, the test compound is likely an agonist. If the calculated value is less than 1, the test compound could be an inverse agonist."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "percent response",
                                    "name": "percent response",
                                    "data": {
                                        "definition": "null "
                                    },
                                    "children": [
                                        {
                                            "id": "percent inhibition",
                                            "name": "percent inhibition",
                                            "data": {
                                                "definition": "% reduction of a predefined stimulus. Unit of Measure is always % when normalized to the dynamic range of the assay. % Inhibition = 100*(1-((signal-blank mean)/(control mean-blank mean))) "
                                            },
                                            "children": [
                                                {
                                                    "id": "80 percent inhibition",
                                                    "name": "80 percent inhibition",
                                                    "data": {
                                                        "definition": "80% reduction of a predefined stimulus. Unit of Measure is always % when normalized to the dynamic range of the assay."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "50 percent inhibition",
                                                    "name": "50 percent inhibition",
                                                    "data": {
                                                        "definition": "50% reduction of a predefined stimulus. Unit of Measure is always % when normalized to the dynamic range of the assay."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "percent growth inhibition",
                                                    "name": "percent growth inhibition",
                                                    "data": {
                                                        "definition": "% growth reduction of a predefined stimulus. Unit of Measure is always % when normalized to the dynamic range of the assay."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "percent activation",
                                            "name": "percent activation",
                                            "data": {
                                                "definition": "% increase of a predefined stimulus. Unit of Measure is always % when normalized to the dynamic range of the assay."
                                            },
                                            "children": [
                                                {
                                                    "id": "50 percent activation",
                                                    "name": "50 percent activation",
                                                    "data": {
                                                        "definition": "50% increase of a predefined stimulus. Unit of Measure is always % when normalized to the dynamic range of the assay."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "percent cytotoxicity",
                                    "name": "percent cytotoxicity",
                                    "data": {
                                        "definition": "% reduction of the number of viable cells. Unit of Measure is always % when normalized to the dynamic range of the assay. Cytotoxicity (%) = (exp. value - low control)/(high control - low control)Ã 100 Compound cytotoxicity is an important parameter to measure when developing potential human therapeutics. Cytotoxicity is determined as a measure of radioisotope (3H thymidine or 51Cr) release, lactate dehydrogenase release from damaged cells, tetrazolium salt and alamar blue reduction, fluorescent dyes that selectively stain live or dead cells, and decrease in ATP content. ATP levels are detected using a luminescence based assay kit such as CellTiter-Glo (Promega). ATP values higher than controls indicate proliferation and cultures with ATP concentrations lower than controls indicate cytotoxicity."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "perturbagen concentration",
                            "name": "perturbagen concentration",
                            "data": {
                                "definition": "Reporting a concentration value of the perturbagen that mediated an effect as the endpoint."
                            },
                            "children": [
                                {
                                    "id": "perturbagen protein affinity",
                                    "name": "perturbagen protein affinity",
                                    "data": {
                                        "definition": "null "
                                    },
                                    "children": [
                                        {
                                            "id": "ki'",
                                            "name": "ki'",
                                            "data": {
                                                "definition": "The affinity of the inhibitor for the enzyme-substrate complex."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "ki",
                                            "name": "ki",
                                            "data": {
                                                "definition": "Result from the Cheng-Prusoff equation or from a slightly modified derivation. This label is used primarily with binding assays and represents the affinity of a compound for a receptor. Documentation of the formula and any changes to the Cheng-Prusoff should be noted in the assay protocol."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "tgi",
                                    "name": "tgi",
                                    "data": {
                                        "definition": "TGI is total growth inhibition, which is the effective concentration of a growth inhibitor, which produces 0% relative growth for that inhibitor. Thus TGI signifies a cytostatic effect. tgi is calculated from [(Ti-Tz)/(C-Tz)] x 100 =0, where Ti = Tz and Tz is absorbance at time zero, Ti is absorbance in the presence of test drug, and C is absorbance in the control (untreated cells).  As a measure of viable cells, different researchers measure different parameters, namely, protein content (by sulforhodamine B staining followed by absorbance measurement), mitochondrial dehydrogenase activity (by 3-(4,5-dimethylthiazol-2-yl)-2,5-diphenyl tetrasodium bromide, known as MTT staining followed by absorbance measurement), and ATP content (by using CellTiter-Glo reagent (Promega) followed by luminescence measurement)."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "ic80",
                                    "name": "ic80",
                                    "data": {
                                        "definition": "The effective concentration of an inhibitor, which produces 80% of the maximum possible response for that inhibitor."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "ic50",
                                    "name": "ic50",
                                    "data": {
                                        "definition": "The effective concentration of an inhibitor, which produces 50% of the maximum possible response for that inhibitor."
                                    },
                                    "children": [
                                        {
                                            "id": "pic50",
                                            "name": "pic50",
                                            "data": {
                                                "definition": "pIC50 is -logIC50, where IC50 is the effective concentration of an inhibitor, which produces 50% of the maximum possible response for that inhibitor."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "ic50 relative",
                                            "name": "ic50 relative",
                                            "data": {
                                                "definition": "The concentration of a test substance that produces an inhibitory effect corresponding to 50% of the activity of that particular substance (i.e. 50% of the difference between top and bottom of fitted curve). It can also be described as the concentration corresponding to the inflection point of the curve."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "ic50 absolute",
                                            "name": "ic50 absolute",
                                            "data": {
                                                "definition": "Absolute IC50 (calculated from the curve-fit function) is the concentration corresponding to exactly 50 percent inhibition. It is obtained from the curve fit function (back-calculated IC50) and can also be extrapolated. Commonly the relative IC50 is reported; however in cell proliferation assays absolute IC50 is often used. The absolute IC50 can be more reproducible."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "gi50",
                                            "name": "gi50",
                                            "data": {
                                                "definition": "The effective concentration of a growth inhibitor, which produces 50% of the maximum possible response for that inhibitor. GI50 is calculated from [(Ti-Tz)/(C-Tz)] x 100 = 50, where Tz is absorbance at time zero, Ti is absorbance in the presence of test drug, and C is absorbance in the control (untreated cells).  As a measure of viable or proliferating cells, different researchers measure different parameters, namely, protein content (by sulforhodamine B staining followed by absorbance measurement), mitochondrial dehydrogenase activity (by 3-(4,5-dimethylthiazol-2-yl)-2,5-diphenyl tetrasodium bromide, known as MTT staining followed by absorbance measurement), expression of proliferation associated antigens (by immunostaining for Ki-67), and ATP content (by using CellTiter-Glo reagent (Promega) followed by luminescence measurement)."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "ec50",
                                    "name": "ec50",
                                    "data": {
                                        "definition": "The effective concentration of an agonist, which produces 50% of the maximum possible response for that agonist."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "cc50",
                                    "name": "cc50",
                                    "data": {
                                        "definition": "The effective concentration of a cytotoxic compound, which produces 50% of the maximum possible cell death for that compound. Compound cytotoxicity is an important parameter to measure when developing potential human therapeutics. Cytotoxicity is determined as a measure of radioisotope (3H thymidine or 51Cr) release, lactate dehydrogenase release from damaged cells, tetrazolium salt and alamar blue reduction, fluorescent dyes that selectively stain live or dead cells, and decrease in ATP content. ATP levels are detected using a luminescence based assay kit such as CellTiter-Glo (Promega). ATP values higher than controls (untreated cells) indicate proliferation and cultures with ATP concentrations lower than controls indicate cytotoxicity."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "ac50",
                                    "name": "ac50",
                                    "data": {
                                        "definition": "The effective concentration of a perturbagen, which produces 50% of the maximal possible response, which could mean either activation (EC50) or inhibition (IC50) for that perturbagen."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "protein substrate and ligand constants",
                            "name": "protein substrate and ligand constants",
                            "data": {
                                "definition": "They are used to express the binding interactions between labeled or unlabeled ligands with protein receptors and include Bmax, Kd, etc."
                            },
                            "children": [
                                {
                                    "id": "binding constants",
                                    "name": "binding constants",
                                    "data": {
                                        "definition": "null "
                                    },
                                    "children": [
                                        {
                                            "id": "kd",
                                            "name": "kd",
                                            "data": {
                                                "definition": "Kd is the equilibrium dissociation constant for radioligand. It is the concentration of the radioligand at which binding reaches half the Bmax."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "bmax",
                                            "name": "bmax",
                                            "data": {
                                                "definition": "Bmax is the maximum number of binding sites on the receptor protein and is expressed as moles per microgram of protein sample."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "enzyme kinetic constants",
                                    "name": "enzyme kinetic constants",
                                    "data": {
                                        "definition": "Describe kinetics of enzyme-catalyzed reactions. It includes the enzyme kinetic constants namely, Km and Vmax, which help to model the time course of disappearance of substrate and the production of product."
                                    },
                                    "children": [
                                        {
                                            "id": "vmax",
                                            "name": "vmax",
                                            "data": {
                                                "definition": "null "
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "kcat",
                                            "name": "kcat",
                                            "data": {
                                                "definition": "Turnover number representing the maximum number of substrate molecules converted to products per active site per unit time."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "kon",
                                            "name": "kon",
                                            "data": {
                                                "definition": "The on-rate associated with the formation of an enzyme-inhibitor complex."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "koff",
                                            "name": "koff",
                                            "data": {
                                                "definition": "The off-rate associated with the release of inhibitor from an enzyme-inhibitor complex."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "km",
                                            "name": "km",
                                            "data": {
                                                "definition": "The concentration of substrate at ½ Vmax, according to the Henri-Michaelis-Menten kinetic model."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    "id": "detection technology",
                    "name": "detection technology",
                    "data": {
                        "definition": "The physical method (technology) used to record / detect the effect caused by the perturbagen in the assay environment."
                    },
                    "children": [
                        {
                            "id": "microscopy",
                            "name": "microscopy",
                            "data": {
                                "definition": "The field of using microscopes to see objects that cannot be seen with the unaided eye."
                            },
                            "children": [
                                {
                                    "id": "scanning probe microscopy",
                                    "name": "scanning probe microscopy",
                                    "data": {
                                        "definition": "It forms images of surfaces using a physical probe that scans the specimen and measuring on a fine scale, down to the level of molecules and groups of atoms. The resolution is not limited by the diffraction, but by the size of probe-sample interaction volume. Atomic force microscopy (AFM), scanning tunneling microscopy (STM), and near-field scanning optical microscopy (NSOM) are the three common scanning probe techniques."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "optical microscopy",
                                    "name": "optical microscopy",
                                    "data": {
                                        "definition": "Optical or light microscopy involves passing visible light transmitted through or reflected from the sample through a single or multiple lenses to allow a magnified view of the sample"
                                    },
                                    "children": [
                                        {
                                            "id": "phase contrast microscopy",
                                            "name": "phase contrast microscopy",
                                            "data": {
                                                "definition": "Phase contrast microscopy shows differences in refractive index as difference in contrast. Regions with higher contrast, e.g. nuclei, appear darker than the surrounding cytoplasm."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "brightfield microscopy",
                                            "name": "brightfield microscopy",
                                            "data": {
                                                "definition": "In brightfield microscopy, sample is illuminated with transmitted white light from below and observed from above."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "fluorescence microscopy",
                                            "name": "fluorescence microscopy",
                                            "data": {
                                                "definition": "Fluorescence microscopy is used to image the distribution of molecules based on the properties of fluorescence emission.  Excitation of fluorescent molecules with a specific wavelength results in the production of an image by the secondary fluorescence emission at longer wavelengths."
                                            },
                                            "children": [
                                                {
                                                    "id": "confocal microscopy",
                                                    "name": "confocal microscopy",
                                                    "data": {
                                                        "definition": "Confocal microscopy has advantages over widefield optical microscopy, including the ability to eliminate or reduce background information away from the focal plane and collect serial optical sections from thick specimens. It uses point illumination and a spatial pinhole to eliminate out-of-focus light in specimens that are thicker than the focal plane."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "total internal reflection fluorescence microscopy ",
                                                    "name": "total internal reflection fluorescence microscopy ",
                                                    "data": {
                                                        "definition": "This technique probes the surface of fluorescently-labeled living cells with an evanescent wave generated by a light beam traveling between two media of differing refractive indices. An incident laser beam is reflected at a critical angle (total internal reflection) when it encounters the interface between a microscope glass slide and the aqueous medium containing the cells.  Only fluorophores which are within a few nanometers of the surface are excited, while those farther away are not, making possible the study the interactions at the cell surface."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "electron microscopy",
                                    "name": "electron microscopy",
                                    "data": {
                                        "definition": "Electron microscopy involve the diffraction, reflection, or refraction of electron beams interacting with the subject of study, and the subsequent collection of this scattered radiation in order to build up an image."
                                    },
                                    "children": [
                                        {
                                            "id": "transmission electron microscopy",
                                            "name": "transmission electron microscopy",
                                            "data": {
                                                "definition": "Transmission electron microscopy (TEM) is quite similar to the compound light microscope, by sending an electron beam through a very thin slice of the specimen. The resolution limit in 2005 was approximately 0.05 nanometer and has not increased appreciably since that time."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "scanning electron microscopy",
                                            "name": "scanning electron microscopy",
                                            "data": {
                                                "definition": "Scanning electron microscopy (SEM) visualizes details on the surfaces of cells and particles and gives a very nice 3D view. It gives results much like those of the stereo light microscope, and, akin to that, its most useful magnification is in the lower range than that of the transmission electron microscope."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "label free technology",
                            "name": "label free technology",
                            "data": {
                                "definition": "It allows the detection of binding interactions and cell-based reactions without the need to use conventional labels, e.g., fluorescent probes. The advantages include the ability to measure a) functional activity without modifying the binding partners with labels, b) binding interactions independent of functional activity, and c) cell-Based GPCR assays without the need for engineering cell-lines to over-express given receptors."
                            },
                            "children": [
                                {
                                    "id": "quartz crystal microbalance",
                                    "name": "quartz crystal microbalance",
                                    "data": {
                                        "definition": "Quartz crystal microbalance (QCM) technology is based on measuring the resonant frequency of piezoelectric quartz crystals. It allows measurement of important cellular parameters such as adhesion, proliferation and cytotoxicity. It is also used for the identification of protein ligands by immobilized receptors."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "isothermal titration calorimetry",
                                    "name": "isothermal titration calorimetry",
                                    "data": {
                                        "definition": "ITC is a thermodynamic technique that can directly measure the heat released or absorbed during a biomolecular binding event. It can measure the binding affinity (Ka), reaction stoichiometry (n), enthalpy (âH) and entropy (ÎS) of the interaction between two or more molecules in solution."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "x ray crystallography",
                                    "name": "x ray crystallography",
                                    "data": {
                                        "definition": "X-ray crystallography is used to analyze protein-small molecule interactions. This binding interaction is monitored by measuring the difference of diffraction between protein-compound binding complex and protein alone.  With this technique, both the binding interaction and bound compounds can be detected, which allows pools of 5-100 compounds to be tested for binding."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "electrical sensor",
                                    "name": "electrical sensor",
                                    "data": {
                                        "definition": "Electrical sensors are used to measure the electrical impedance, or the current-voltage relationships of these ion channels."
                                    },
                                    "children": [
                                        {
                                            "id": "carbon nanotube based sensor",
                                            "name": "carbon nanotube based sensor",
                                            "data": {
                                                "definition": "Carbon nanotubes (CNT) are rolled up seamless cylinders of graphene sheets, exhibiting many unique physical, mechanical and chemical properties. CNT based biosensors are used to detect protein interactions, e.g. protein-DNA, antibody-aptamer interaction, etc."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "microelectrode measurement",
                                            "name": "microelectrode measurement",
                                            "data": {
                                                "definition": "Electrically active cells, such as neurons, muscle and endocrine cells undergo changes in membrane potential due to depolarization and hyperpolarization as a result of opening and closing of ion channels. The electrical recordings from these cells are obtained by the insertion or attaching microelectrodes to these cells. Patch clamp, voltage clamp and current clamp are three ways of studying the current-voltage relationships of these ion channels."
                                            },
                                            "children": [
                                                {
                                                    "id": "voltage clamp",
                                                    "name": "voltage clamp",
                                                    "data": {
                                                        "definition": "The voltage clamp technique is used to measure the ion currents across an excitable cell membrane, such as neuronal membrane, while holding the membrane voltage at a constant level. The neuronal membrane contains different kinds of ion channels including voltage gated ion channels. Manipulation of the membrane voltage using voltage clamping allows the current-voltage relationships of these ion channels to be studied."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "patch clamp",
                                                    "name": "patch clamp",
                                                    "data": {
                                                        "definition": "The patch clamping technique is refined from the voltage clamp technique, wherein a blunt electrode is placed on the surface of the cell membrane. It is a common intracellular recording technique in electrophysiology that allows either the measurement of current passing through individual ion channels in cells or through whole cells. Suction is applied via the blunt electrode (glass pipette in classical patch clamp technique) to create a high resistance (gigaohm) between the electrode and cell surface. This seal isolates the ion channels within the patch from the rest of the membrane and allows the neurophysiologist to record only from these ion channels."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "current clamp",
                                                    "name": "current clamp",
                                                    "data": {
                                                        "definition": "The current clamp technique is used to measure biological voltages such as the action potential of an excitable cell with a microelectrode while keeping electrical current through the recording electrode constant. It can be used for both extracellular and intracellular recordings."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "impedance",
                                            "name": "impedance",
                                            "data": {
                                                "definition": "Impedance biosensors measure the electrical opposition of an interface in AC steady state with constant DC bias conditions. This is accomplished by imposing a small sinusoidal voltage at a particular frequency and measuring the resulting current; the process can be repeated at different frequencies. The current-voltage ratio gives the impedance."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "circular dichroism",
                                    "name": "circular dichroism",
                                    "data": {
                                        "definition": "Circular dichroism (CD) is a technology for assessing interactions of chiral molecules with circularly polarized electromagnetic rays. Secondary and tertiary structure of proteins will impart a distinct CD to that molecule allowing the use of CD to examine the protein folding under different conditions. Information about conformational properties of nucleic acids can be obtained by CD spectroscopy."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "nuclear magnetic resonance",
                                    "name": "nuclear magnetic resonance",
                                    "data": {
                                        "definition": "Nuclear magnetic resonance (NMR) is used to analyze protein-small molecule interactions. This binding interaction is commonly monitored by the chemical shift of the compound.  With this technique, both the binding interaction and bound compounds can be detected, which allows pools of 5-100 compounds to be tested for binding."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "optical based",
                                    "name": "optical based",
                                    "data": {
                                        "definition": "These biosensors employ evanescent waves (surface plasmon resonance, resonant waveguide grating) or interference of light waves to characterize the cellular processes that are taking place at or near the sensor surface: e.g.  to study the affinity of a sample to the biological receptors immobilized on the sensor surface."
                                    },
                                    "children": [
                                        {
                                            "id": "resonant waveguide grating",
                                            "name": "resonant waveguide grating",
                                            "data": {
                                                "definition": "Cells are directly cultured onto the surface of a resonant waveguide grating (RWG) biosensor. The mass redistribution within the bottom portion of cells, mediated by stimulus is directly measured with the biosensor. RWG biosensor utilizes an optical beam with an appropriate angular content to illuminate a waveguide film in which a grating structure is embedded. When this beam is reflected by the sensor surface, the resonant angle dominates in the output beam. The mass redistribution within the sensing volume alters the incident angle. Because of its ability for multiparameter measurements, the biosensor has potential to provide high information content for cell sensing. These parameters include the angular shift (one of the most common outputs), the intensity, the peak width at half-maximum (PWHM), and area of the resonant peaks."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "optical waveguide grating",
                                            "name": "optical waveguide grating",
                                            "data": {
                                                "definition": "The optical grating coupler sensor responds to the change of optical refractive index of the liquid or gas cover medium and to the adsorption or binding of molecules on the surface. The optical grating coupler sensor chip is based on a fine optical grating prepared on a thin waveguide layer carried by a glass substrate. The optical grating couples the light of a He-Ne laser at a given resonance angle into the waveguide layer. This incoupling angle is very sensitive to the presence of adsorbed molecules and to any change of refractive index of the medium covering the surface of the chip. By precise measurement of the incoupling angle, the amount of the adsorbed material can be determined with ultra high sensitivity."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "fiber optic waveguide",
                                            "name": "fiber optic waveguide",
                                            "data": {
                                                "definition": "It is based on the ability of a transparent substance, such as glass to conduct some of the visible or near-visible light to its interior by means of internal reflection."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "bio layer interferometry",
                                            "name": "bio layer interferometry",
                                            "data": {
                                                "definition": "Bio-Layer Interferometry is a used for measuring biomolecular interactions. In this technique, the interference pattern of white light reflected from two surfaces: a layer of immobilized protein on the biosensor tip, and an internal reference layer are analyzed. Changes in the number of molecules bound to the biosensor tip cause a shift in the interference pattern, which can be measured in real-time."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "surface plasmon resonance",
                                            "name": "surface plasmon resonance",
                                            "data": {
                                                "definition": "For surface plasmon resonance measurements, target protein is immobilized onto a solid surface via covalent or noncovalent interaction. Binding of small molecule compounds changes the refractive index of the immobilized target, which can be detected by optical sensing. Shift in wavelength of light is proportional to mass changes near the sensor surface, with sensitivity to detect mass change being ~300 Da."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "mass spectrometry",
                                    "name": "mass spectrometry",
                                    "data": {
                                        "definition": "This method is used to detect compounds that are bound to the protein targets. The unbound compounds are separated by affinity based, size exclusion, or ultrafiltration methods. The bound compounds are eluted from the protein and detected by mass spectrometry. Based on the separation method, the throughput varies: between 200-2700 compounds can be loaded in each run for separation. Mass spectrometry is used to determine the chemical structure of molecules. The samples are vaporized, followed by ionization and then the mass to charge ratio of the particles is analyzed."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "scintillation counting",
                            "name": "scintillation counting",
                            "data": {
                                "definition": "It involves the incorporation of radiolabeled precursors into uniform distribution with a liquid medium capable of converting the kinetic energy of nuclear emissions into light energy. A scintillation counter is used to measure ionizing radiation. The scintillation counter consists of a sensor, called a scintillator, made of transparent crystal or an organic liquid that fluoresces when struck by ionizing radiation. A photomultiplier is then used to measure the light emitted by the sensor and is attached to an electronic apparatus that amplifies and quantifies the radiation signal."
                            },
                            "children": [
                                {
                                    "id": "lysate based",
                                    "name": "lysate based",
                                    "data": {
                                        "definition": "This assay is performed to study interaction between a radio-labeled ligand and the receptor in presence of cold ligand. The cells exposed to the radioligand and cold ligand are washed to remove the unbound ligands. They are then lysed and the bound radioactivity is counted in a liquid scintillation counter."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "filter assay",
                                    "name": "filter assay",
                                    "data": {
                                        "definition": "This assay is performed to study interaction between a radio-labeled ligand and the receptor. The reaction mixture containing the receptor and radioligand is transferred to a filter plate, washed to remove the unbound radioligand. The bound complex remains on the filter, which can be eluted into the scintillation fluid and the radioactivity measured. Filter assays differ from SPA because a separation of free radioligand and radioligand bound to the receptor is required for measurement."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "scintillation proximity assay",
                                    "name": "scintillation proximity assay",
                                    "data": {
                                        "definition": "Scintillation proximity assay (SPA) eliminates the separation step of unbound radioisotopes and is performed using low energy radioisotopes (3H and 125I) as labels, which have short range electron emission. SPA involves the use of microscopic beads which are first coated with a receptor protein of interest and contain a scintillant. When stimulated by the binding of radio-labeled ligand molecules, they emit light, which is detected by the photomultiplier tube. SPA is suitable for high throughput since the protocol has no wash or separation steps."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "spectrophotometry",
                            "name": "spectrophotometry",
                            "data": {
                                "definition": "Spectrophotometry involves the use of a spectrophotometer to measure the amount of light that a sample absorbs. The instrument operates by passing a beam of light through a sample and measuring the intensity of light reaching a detector."
                            },
                            "children": [
                                {
                                    "id": "transmittance",
                                    "name": "transmittance",
                                    "data": {
                                        "definition": "The transmittance of a sample is the ratio of the intensity of the light that has passed through the sample to the intensity of the light when it entered the sample (T = Iout / Iin ).  To convert between the absorbance and transmittance scales, the following equation is used: A = -log T = - log (I / Io)."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "absorbance",
                                    "name": "absorbance",
                                    "data": {
                                        "definition": "The absorbance of a beam of collimated monochromatic radiation in a homogeneous isotropic medium is proportional to the absorption path length and to the concentration of the absorbing species. Absorbance is a logarithmic measure of the amount of light (at a specific wavelength) that is absorbed when passing through a sample.  A= -log10(I/I0), where A is absorbance, I is the intensity of incident light and I0 is the intensity of light transmitted after passing through the sample."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "fluorescence",
                            "name": "fluorescence",
                            "data": {
                                "definition": "Detection techniques that use the principles of fluorescence, whereby incident light excites a fluorophore which then emits light at lower energy (higher wavelength). The emitted light is typically from the visible portion of the UV-Visible spectrum."
                            },
                            "children": [
                                {
                                    "id": "fluorescence intensity ",
                                    "name": "fluorescence intensity ",
                                    "data": {
                                        "definition": "Fluorescence intensity is the measure of fluorescence from a sample."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "thermal shift",
                                    "name": "thermal shift",
                                    "data": {
                                        "definition": "Upon heating, a protein unfolds and loses the native conformation. Binding of a small molecule can often stabilize the protein conformation, resulting in a higher unfolding temperature. The temperature shift in unfolding can be detected using a fluorescent dye, which is sensitive to the protein environment. "
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "flow cytometry ",
                                    "name": "flow cytometry ",
                                    "data": {
                                        "definition": "Flow cytometry uses the principles of light scattering and fluorescence from fluorochrome molecules to generate specific multi-parameter data from particles and cells in the size range of 0.5um to 40um diameter. "
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "fluorescence polarization",
                                    "name": "fluorescence polarization",
                                    "data": {
                                        "definition": "Fluorescence polarization (FP) measurements are based on the assessment of the rotational motions of species. It is used to measure binding interactions. When linear polarized light is used to excite fluorophores, only those aligned with the plane of polarization will be excited. For fluorophores attached to small, rapidly rotating molecules, the initially photoselected orientational distribution becomes randomized prior to emission, resulting in low fluorescence polarization. But binding of the low molecular weight tracer to a large, slowly rotating molecule results in high fluorescence polarization."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "fluorescence resonance energy transfer ",
                                    "name": "fluorescence resonance energy transfer ",
                                    "data": {
                                        "definition": "Fluorescence resonance energy transfer (FRET) is based on the transfer of energy between two fluorophores, a donor and an acceptor, when in close proximity. The energy transfer is directed from higher-energy donor fluorophore to lower-energy acceptor fluorophore of labeled protein pairs. Target protein pairs are likely to exhibit FRET if they are no more than 10 nm apart."
                                    },
                                    "children": [
                                        {
                                            "id": "time resolved fluorescence resonance energy transfer",
                                            "name": "time resolved fluorescence resonance energy transfer",
                                            "data": {
                                                "definition": "Time resolved-fluorescence resonance energy transfer (TR-FRET) unites TRF (Time-Resolved Fluorescence) and FRET (Fluorescence Resonance Energy Transfer) principles. This combination brings together the low background benefits of TRF with the homogeneous assay format of FRET. Introducing a time delay (50-150 micro seconds) between the system excitation and fluorescence measurement allows the signal to be cleared of all non-specific short lived emissions. TR-FRET uses lanthanide donors which are fluorophores with long emission half-lives.      "
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "homogenous time resolved fluorescence",
                                            "name": "homogenous time resolved fluorescence",
                                            "data": {
                                                "definition": "Homogenous time resolved fluorescence (HTRF) is a technology which combines standard FRET technology with the time-resolved measurement of fluorescence. Through this, HTRF eliminates the short-lived background fluorescence. Introducing a time delay (50-150 micro seconds) between the system excitation and fluorescence measurement allows the signal to be cleared of all non-specific short lived emissions."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "luminescence",
                            "name": "luminescence",
                            "data": {
                                "definition": "The emission of light from a substance that occurs from an electronically excited state which is reached either by a physical, mechanical, or chemical mechanism."
                            },
                            "children": [
                                {
                                    "id": "alphascreen",
                                    "name": "alphascreen",
                                    "data": {
                                        "definition": "This technology measures the interaction of two molecules bioconjugated to donor and acceptor beads. Laser excitation leads to light emission which is directly proportional to the amount of binding, provided molecules are in proximity to cause a chemical reaction (interact). Users have modified this and coupled it to chemiluminescence emission. This technology has been adapted to study protein-protein, protein-DNA and receptor-ligand, immunoassay, phosphorylation and protease reactions."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "chemiluminescence",
                                    "name": "chemiluminescence",
                                    "data": {
                                        "definition": "Chemiluminescence is the generation of light as a result of a chemical reaction using synthetic compounds and usually involving a highly oxidized species such as peroxide."
                                    },
                                    "children": [
                                        {
                                            "id": "chemiluminescence resonance energy transfer ",
                                            "name": "chemiluminescence resonance energy transfer ",
                                            "data": {
                                                "definition": "Chemiluminescence resonance energy transfer (CRET) is a resonance energy transfer between chemiluminescent donors and fluorescent acceptors. CRET occurs by the oxidation of a luminescent substrate, such as luminol/hydrogen peroxide reaction catalyzed by horseradish peroxidase. CRET eliminates the disadvantages associated with donor fluorophore activation."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "bioluminescence",
                                    "name": "bioluminescence",
                                    "data": {
                                        "definition": "Bioluminescence refers to the production and emission of light by a living organism. Bioluminescence is a naturally occurring form of chemiluminescence whereby energy is released by a chemical reaction in the form of light emission. The chemical reaction can occur either inside or outside the cell, and can be detected using a plate reader or a charge-coupled device (CCD) camera."
                                    },
                                    "children": [
                                        {
                                            "id": "bioluminescence resonance energy transfer ",
                                            "name": "bioluminescence resonance energy transfer ",
                                            "data": {
                                                "definition": "BRET is similar to FRET but has a bioluminescent protein such as aequorin as the donor and a fluorescent acceptor, e.g. GFP. The donor emits blue light in the absence of GFP, but when GFP and aequorin are associated as they are in vivo, GFP accepts the energy from aequorin and emits green light. Hence BRET eliminates the disadvantages associated with donor fluorophore activation."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    "id": "design",
                    "name": "design",
                    "data": {
                        "definition": "The underlying method (technology) and assay design used to determine the action of the perturbagen in the assay system."
                    },
                    "children": [
                        {
                            "id": "morphology reporter",
                            "name": "morphology reporter",
                            "data": {
                                "definition": "The technologies used to measure the size and shape of cells and organelles (phenotype). These are detected by microscopy."
                            },
                            "children": [
                                {
                                    "id": "cell movement",
                                    "name": "cell movement",
                                    "data": {
                                        "definition": "The technologies used to measure the movement of cells and organelles (phenotype). These are detected by microscopy."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "cell phenotype",
                                    "name": "cell phenotype",
                                    "data": {
                                        "definition": "The technologies used to quantitate the cell and organelle size and shape. This is detected by microscopy."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "membrane potential reporter",
                            "name": "membrane potential reporter",
                            "data": {
                                "definition": "All living cells maintain a resting membrane potential, which is the difference in voltage between the interior and exterior of the cell. Electrically active cells, such as neurons, muscle and endocrine cells undergo changes in membrane potential due to depolarization and hyperpolarization as a result of opening and closing of ion channels. Action potentials are large and rapid changes in membrane potential which are generated as a result of a large depolarization event. In neurons, action potentials are used to transmit signals across neurons, while in muscle and endocrine cells, they lead to intracellular events, such as muscle contraction and hormone release, respectively. In addition, mitochondria possess a trans-membrane potential as a result of their respiratory function. Membrane potential can be measured either by using electrophysiological techniques or by staining with dyes whose binding is dependent on the membrane potential."
                            },
                            "children": [
                                {
                                    "id": "dye binding",
                                    "name": "dye binding",
                                    "data": {
                                        "definition": "Most often, the mitochondrial membrane potential which is called delta-psi is quantitated by staining with specific dyes whose binding is dependent on the potential."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "electrophysiological measurements",
                                    "name": "electrophysiological measurements",
                                    "data": {
                                        "definition": "Electrically active cells, such as neurons, muscle and endocrine cells undergo changes in membrane potential due to depolarization and hyperpolarization as a result of opening and closing of ion channels. The electrical recordings from these cells are obtained by the insertion or attaching microelectrodes to these cells. Patch clamp, voltage clamp and current clamp are three ways of studying the current-voltage relationships of these ion channels."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "viability reporter",
                            "name": "viability reporter",
                            "data": {
                                "definition": "Compound cytotoxicity is an important parameter to measure when developing potential human therapeutics. Cytotoxicity is determined as a measure of radioisotope (3H thymidine or 51Cr) release, lactate dehydrogenase release from damaged cells, tetrazolium salt and alamar blue reduction, fluorescent dyes that selectively stain live or dead cells, and decrease in ATP content. The viability reporter technologies measure cell life or death by quantitating ATP content, caspase activity, membrane potential, and so on."
                            },
                            "children": [
                                {
                                    "id": "dehydrogenase activity",
                                    "name": "dehydrogenase activity",
                                    "data": {
                                        "definition": "An expansion in the number of viable cells results in an increase in the overall activity of mitochondrial dehydrogenases in the sample. The internal environment of the proliferating cell is more reduced than that of non-proliferating cells. Specifically, the ratios of NADPH/NADP, FADH/FAD, FMNH/FMN, and NADH/NAD, increase during proliferation. Compounds such as tetrazolium salts and Alamar Blue, which can be reduced by these metabolic intermediates, can be used to monitor cell proliferation. Their reduction is accompanied by a measurable shift in wavelength with a corresponding change in color."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "esterase activity",
                                    "name": "esterase activity",
                                    "data": {
                                        "definition": "Cell viability can be assessed by the presence of cytoplasmic esterases that cleave a lipid-soluble nonfluorescent probe to yield a fluorescent product. The product is charged and is thus retained within the cell if the membrane is intact. Typical probes include fluorescein diacetate (FDA), carboxyfluorescein, and calcein."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "phospholipid redistribution",
                                    "name": "phospholipid redistribution",
                                    "data": {
                                        "definition": "In a viable cell, the phospholipid, phosphatidyl serine (PS) is localized on the inner leaflet of the cell membrane facing the cytosol. When the cell is undergoing apoptosis, the cell membrane loses its phospholipid asymmetry and exposes PS on the outer layer of the membrane. Annexin V specifically binds PS. Hence, apoptotic cells can be detected by staining with FITC conjugated Annexin V followed by flow cytometry. "
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "mitochondrial membrane potential",
                                    "name": "mitochondrial membrane potential",
                                    "data": {
                                        "definition": "Activity of the mitochondrial respiratory chain results in the generation of a proton gradient which consists of two components: a membrane potential and a pH gradient. Disruption of delta-psi has been shown to be one of the first intracellular changes following the onset of apoptosis. Delta-psi is generally quantitated by staining with specific dyes whose binding is dependent on the potential and less commonly, by electrophysiological techniques."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "chromatin condensation",
                                    "name": "chromatin condensation",
                                    "data": {
                                        "definition": "Chromatin condensation is one of the morphological changes that occur during apoptosis. It is also known as pyknosis and can be visualized by staining with dyes followed my microscopy."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "caspase activity",
                                    "name": "caspase activity",
                                    "data": {
                                        "definition": "Caspases are cysteine proteases which play an important role in apoptosis. They are synthesized and stored in cells as inactive procaspases. Once apoptosis is initiated in a cell, the procaspases get activated by proteolytic cleavage. This sets in motion a caspase cascade, which leads to the activation of proteins involved in the chromatin fragmentation and finally culminates in cell death."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "protease activity",
                                    "name": "protease activity",
                                    "data": {
                                        "definition": "Live cells have a conserved and constitutive protease activity which serves as a biomarker of cell viability. The live-cell protease activity is measured using a fluorogenic, cell-permeant, peptide substrate (Gly-Phe-AFC). The substrate enters intact cells, where it is cleaved by the live-cell protease activity to generate a fluorescent signal proportional to the number of living cells. The live-cell protease becomes inactive upon loss of membrane integrity and leakage into the surrounding culture medium."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "cell number",
                                    "name": "cell number",
                                    "data": {
                                        "definition": "Cells are counted to determine their viability. Cultured animal cells are counted either manually using hemocytometer, or by an automated cell counter, e.g. Coulter counter or Vi-CELL. The latter can distinguish live and dead cells by the trypan blue cell exclusion method.  Yeast and bacterial cell growth is determined by measuring absorbance in a spectrophotometer, or by viable plate count."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "membrane permeability",
                                    "name": "membrane permeability",
                                    "data": {
                                        "definition": "Live cells with intact membranes have the ability to exclude dyes that easily penetrate dead or damaged cells. Examples of the dyes include propidium iodide, 7-amino actinomycin D, and trypan blue. Cells stained with the former two can be quantified by flow cytometry, while the latter is by manual counting using hemocytometer.   "
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "atp content",
                                    "name": "atp content",
                                    "data": {
                                        "definition": "ATP levels are an indicator of viable cell number. ATP values higher than controls (untreated cells) indicate proliferation and cultures with ATP concentrations lower than controls indicate cytotoxicity. ATP levels can be measured using different commercial kits."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "dna content",
                                    "name": "dna content",
                                    "data": {
                                        "definition": "Cellular nuclei can be by stained with Hoechst, DAPI, etc, which is a measure of cell viability and proliferation."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "redistribution reporter",
                            "name": "redistribution reporter",
                            "data": {
                                "definition": "The technologies used to measure the distribution of organelles and molecules within the cell (proteins, second messengers, metal ions, etc). This is generally visualized by microscopy."
                            },
                            "children": [
                                {
                                    "id": "second messenger",
                                    "name": "second messenger",
                                    "data": {
                                        "definition": "Second messengers relay signals from the receptors to the cell interior as part of a signaling event. In the process, they amplify the signal. Examples include cAMP, cGMP, calcium, nitric oxide, diacylglycerol, etc, which play important roles in different physiological processes."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "protein redistribution",
                                    "name": "protein redistribution",
                                    "data": {
                                        "definition": "Cellular signaling can bring about the movement of proteins in cells. Upon receiving a cue, proteins undergo posttranslational modifications, e.g. phosphorylation, dephosphorylation, etc, which results in their activation and migration to different compartments, often to the nucleus, where they regulate gene expression."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "metal ion redistribution",
                                    "name": "metal ion redistribution",
                                    "data": {
                                        "definition": "Metal ions such as copper, iron, magnesium and zinc are transported between different cellular compartments to bring about specific functions. This is regulated by their influx and efflux across membranes, which could occur either by passive diffusion or active transport."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "inducible reporter",
                            "name": "inducible reporter",
                            "data": {
                                "definition": "Inducible reporter technologies involve the use of a reporter gene to study the effect of perturbagens on gene expression. Some reporter genes can be easily detected, e.g. green fluorescence protein, and others function as selectable markers, e.g. antibiotic resistance."
                            },
                            "children": [
                                {
                                    "id": "chloramphenicol acetyltransferase induction",
                                    "name": "chloramphenicol acetyltransferase induction",
                                    "data": {
                                        "definition": "Chloramphenicol acetyltransferase (CAT) gene is attached to the regulatory sequence of a gene of interest. When induced to express, CAT breaks down the antibiotic chloramphenicol resulting in a resistance against that antibiotic. The effect of a perturbagen on the expression of the gene of interest could easily be monitored by the gain of antibiotic resistance by the cells."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "Fluorescent protein induction",
                                    "name": "Fluorescent protein induction",
                                    "data": {
                                        "definition": "Fluorescent proteins are commonly used as reporter genes to study the effect of perturbagens on gene expression. Examples include green fluorescent protein (GFP) from the jellyfish Aequorea victoria and the various modifications of it created by amino acid substitutions to either optimize the codon usage in mammalian cells or to change the spectral properties of the protein."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "luciferase induction",
                                    "name": "luciferase induction",
                                    "data": {
                                        "definition": "Luciferase gene is attached to the regulatory sequence of a gene of interest. The effect of a perturbagen on the expression of the gene of interest could be easily measured by the detection of light emitted as a product the luciferase reaction."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "beta lactamase induction",
                                    "name": "beta lactamase induction",
                                    "data": {
                                        "definition": "Beta lactamase gene is attached to the regulatory sequence of a gene of interest. The effect of a perturbagen on the expression of the gene of interest could be easily monitored by the detection of the product of beta lactamase enzyme."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "beta galactosidase induction",
                                    "name": "beta galactosidase induction",
                                    "data": {
                                        "definition": "Beta galactosidase gene is attached to the regulatory sequence of a gene of interest. The effect of a perturbagen on the expression of the gene of interest could be easily monitored by the detection of the product of beta galactosidase enzyme."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "enzyme reporter",
                            "name": "enzyme reporter",
                            "data": {
                                "definition": "This technology is used to measure the effect of a perturbagen on the enzyme activity, which can be measured either directly or coupled to a secondary signal."
                            },
                            "children": [
                                {
                                    "id": "coupled enzyme",
                                    "name": "coupled enzyme",
                                    "data": {
                                        "definition": "This technology is used to measure the effect of a perturbagen on an enzyme activity, which is coupled to a secondary signal. E.g., the activity of a kinase determines the residual pool of ATP, with ATP being its substrate, which in turn determines the rate of a coupled enzyme luciferase, since ATP is one of its substrates too."
                                    },
                                    "children": [
                                        {
                                            "id": "protease coupled enzyme",
                                            "name": "protease coupled enzyme",
                                            "data": {
                                                "definition": "null "
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "substrate coupled enzyme",
                                            "name": "substrate coupled enzyme",
                                            "data": {
                                                "definition": "The effect of a perturbagen on an enzyme activity is coupled to a secondary signal. The activity of an enzyme, e.g. luciferase is limited by the availability of one of its substrates (ATP or luciferin), which is coupled to the activity of another enzyme, e.g. a kinase or Cytochrome P450."
                                            },
                                            "children": [
                                                {
                                                    "id": "luciferin coupled enzyme",
                                                    "name": "luciferin coupled enzyme",
                                                    "data": {
                                                        "definition": "The effect of a perturbagen on an enzyme activity is coupled to a secondary signal. The activity of an enzyme, e.g. luciferase is limited by the availability of one of its substrates, namely, luciferin, which is coupled to the activity of another enzyme, e.g. a Cytochrome P450, protease, etc."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "atp coupled enzyme",
                                                    "name": "atp coupled enzyme",
                                                    "data": {
                                                        "definition": "The effect of a perturbagen on an enzyme activity is coupled to a secondary signal. The activity of an enzyme, e.g. luciferase is limited by the availability of one of its substrates, namely, ATP, which is coupled to the activity of another enzyme, e.g. a kinase, ATPase, etc."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "chaperone coupled enzyme",
                                            "name": "chaperone coupled enzyme",
                                            "data": {
                                                "definition": "The effect of a perturbagen on an enzyme activity is coupled to a secondary signal. The effect of a perturbagen upon the activity of a chaperone, e.g. Hsp90 is coupled to the activity of an enzyme, e.g. luciferase, as denatured  luciferase could serve as a substrate for Hsp90 mediated refolding."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "enzyme activity",
                                    "name": "enzyme activity",
                                    "data": {
                                        "definition": "An enzyme is a protein that acts as a catalyst for specific biochemical reaction, converting specific substrates into chemically distinct products. Enzyme activity assays quantify the kinetics of the product formation or substrate depletion in the reaction, which result in fluorescence, luminescence or colorimetric outputs."
                                    },
                                    "children": [
                                        {
                                            "id": "enzyme activation",
                                            "name": "enzyme activation",
                                            "data": {
                                                "definition": "The increase in the activity of an enzyme by binding to a perturbagen. "
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "enzyme inhibition",
                                            "name": "enzyme inhibition",
                                            "data": {
                                                "definition": "The decrease in the activity of an enzyme by binding to a perturbagen. "
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "binding reporter",
                            "name": "binding reporter",
                            "data": {
                                "definition": "Binding reporter technology is used to quantitate the interactions between two molecules, e.g. perturbagen-protein, protein-protein, protein-DNA, etc. The technologies used are radioligand binding, resonance energy transfer, fluorescence polarization, protein fragment complementation assay, and several label-free methods, including surface plasmon resonance, impedance, nuclear magnetic resonance, X-ray diffraction, isothermal titration calorimetry, mass spectrometry, etc. "
                            },
                            "children": [
                                {
                                    "id": "protein fragment complementation assay",
                                    "name": "protein fragment complementation assay",
                                    "data": {
                                        "definition": "This is used to study the interaction of two proteins. An enzyme or fluorescent protein is rationally dissected into two fragments and fused to two test proteins, whose interaction is being studied. Binding of the two test proteins results in the reconstitution of the enzyme or fluorescent protein from the two fragments, which can be measured."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "radioligand binding",
                                    "name": "radioligand binding",
                                    "data": {
                                        "definition": "It is used for analysis of receptor-ligand interactions in screening applications. It utilizes a radiolabeled ligand and a source of receptor (membranes, soluble/purified)."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "immunoassay",
                                    "name": "immunoassay",
                                    "data": {
                                        "definition": "It measures the concentration of a substance in a solution based on the specific binding of an antibody to that substance, which produces a measurable signal. The signal measured depends on the label that is associated with this interaction, namely radioactive element (in radioimmunoassays), fluorescent, phosphorescent and chemiluminescent dyes, etc."
                                    },
                                    "children": [
                                        {
                                            "id": "ex situ immunoassay",
                                            "name": "ex situ immunoassay",
                                            "data": {
                                                "definition": "Ex situ refers to out of place or position, with respect to the antigen that is being tested in the immunoassay. Usually the antigen is present in a tissue or cell lysate, or as a purified protein in the assay."
                                            },
                                            "children": [
                                                {
                                                    "id": "western blot",
                                                    "name": "western blot",
                                                    "data": {
                                                        "definition": "Western blot or protein immunoblot allows one to determine the size of proteins and quantitate the amount of specific protein present in the samples. The protein samples are loaded and electrophoresed on a gel, followed by transfer onto a membrane and hybridization with a specific antibody. Subsequently, a secondary antibody that is conjugated to an enzyme needed for the detection of signal is hybridized to the primary antibody. Signal is generally detected by chemiluminescence or at other times by a colored reaction. The signals are quantitated by densitometric scanning."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "elisa",
                                                    "name": "elisa",
                                                    "data": {
                                                        "definition": "Enzyme linked immunosorbent assay or ELISA is used to detect the presence of a particular protein, either an antigen or antibody in the sample such as a body fluid."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "antigen down assay",
                                                            "name": "antigen down assay",
                                                            "data": {
                                                                "definition": "An antigen-down immunoassay or immunometric assay involves binding the antigen to a solid surface instead of an antibody. Antigen-down immunoassays are used to bind antibodies found in a sample. When the sample is added (such as human serum), the antibodies (IgE for example) from the sample bind to the antigen coated on the plate."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "sandwich elisa",
                                                            "name": "sandwich elisa",
                                                            "data": {
                                                                "definition": "A Sandwich Immunoassay is a method using two antibodies, which bind to different sites on the antigen or ligand. The primary antibody, which is highly specific for the antigen, is attached to a solid surface. The antigen is then added followed by addition of a second antibody referred to as the detection antibody. The detection antibody binds the antigen to a different epitope than the primary antibody. As a result the antigen is âsandwichedâ between the two antibodies."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "competitive immunoassay",
                                                            "name": "competitive immunoassay",
                                                            "data": {
                                                                "definition": "A competitive binding assay is based upon the competition of labeled and unlabeled ligand for a limited number of antibody binding sites. Competitive inhibition assays are often used to measure small analytes. Only one antibody is used in a competitive binding ELISA."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "in situ immunoassay",
                                            "name": "in situ immunoassay",
                                            "data": {
                                                "definition": "In situ refers to in place or position; undisturbed with respect to the antigen that is being tested in the immunoassay. The protein detection and localization is achieved by visualization under a microscope."
                                            },
                                            "children": [
                                                {
                                                    "id": "immunocytochemistry",
                                                    "name": "immunocytochemistry",
                                                    "data": {
                                                        "definition": "This technique is used to detect proteins and peptides in a cell by binding to specific antibodies. Subsequently, a secondary antibody that is conjugated to a fluorophore or an enzyme needed for the detection of a signal is hybridized to the primary antibody. Signal is generally detected by immunofluorescence or less commonly,  by a colored reaction. This allows the detection and subcellular localization of the protein of interest by visualization under a microscope."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "immunohistochemistry",
                                                    "name": "immunohistochemistry",
                                                    "data": {
                                                        "definition": "This technique is used to detect proteins and peptides in a tissue section by binding to specific antibodies. Subsequently, a secondary antibody that is conjugated to a fluorophore or an enzyme needed for the detection of a signal is hybridized to the primary antibody. Signal is generally detected by immunofluorescence or less commonly, by a colored reaction. This allows the detection and subcellular localization of the protein of interest by visualization under a microscope."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "conformation reporter",
                            "name": "conformation reporter",
                            "data": {
                                "definition": "This technology relates to measurement of protein and nucleic acid conformational properties. Proteins undergo changes to their tertiary structure upon heating, binding to perturbagens, other proteins and substrates. Nucleic acids have characteristic secondary structures, which include the B-family of structures, A-form, Z-form, guanine quadruplexes, cytosine quadruplexes, triplexes, etc. Information about conformational properties of protein and nucleic acids can be obtained by circular dichroism spectroscopy, thermal shift, fluorescence resonance energy transfer (FRET), nuclear magnetic resonance (NMR), X-ray crystallography, etc."
                            },
                            "children": [
                                {
                                    "id": "protein conformation",
                                    "name": "protein conformation",
                                    "data": {
                                        "definition": "This technology relates to measurement of protein conformational properties using one of the following methods: circular dichroism spectroscopy, thermal shift, fluorescence resonance energy transfer (FRET), nuclear magnetic resonance (NMR), X-ray crystallography, etc."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "nucleic acid conformation",
                                    "name": "nucleic acid conformation",
                                    "data": {
                                        "definition": "This technology relates to measurement of nucleic acid conformational properties using one of the following methods: circular dichroism spectroscopy, thermal shift, fluorescence resonance energy transfer (FRET), nuclear magnetic resonance (NMR), X-ray crystallography, etc."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    "id": "perturbagen",
                    "name": "perturbagen",
                    "data": {
                        "definition": "The agents used to alter the activity of the target in a bioassay."
                    },
                    "children": [
                        {
                            "id": "small molecule",
                            "name": "small molecule",
                            "data": {
                                "definition": "Small molecule is an organic compound with a low molecular weight (<800 Da) which is used to screen against a biological target, e.g. protein, nucleic acid, cell or organism  to test its ability to bind it and bring about a change in activity. It is either isolated from a natural source or is synthesized."
                            },
                            "children": [
                                
                            ]
                        },
                        {
                            "id": "protein",
                            "name": "protein",
                            "data": {
                                "definition": "A protein or polypeptide is a long polymer of amino acids joined together by peptide bonds. The sequence of the amino acids is defined by the genetic code. Here, a protein is a perturbagen whereby its ability to modulate a physiological process or a disease pathology is assessed."
                            },
                            "children": [
                                {
                                    "id": "peptide",
                                    "name": "peptide",
                                    "data": {
                                        "definition": "Peptide is a short polymer of amino acids, which is generally <50 amino acids long."
                                    },
                                    "children": [
                                        {
                                            "id": "synthetic peptide",
                                            "name": "synthetic peptide",
                                            "data": {
                                                "definition": "These peptides are synthesized chemically which starts at the C-terminal end of the peptide and ends at the N-terminus, which is opposite to that observed in protein synthesis."
                                            },
                                            "children": [
                                                {
                                                    "id": "peptide aptamer",
                                                    "name": "peptide aptamer",
                                                    "data": {
                                                        "definition": "Aptamers are synthetic affinity probes. Peptide aptamers consist of a variable peptide loop attached at both ends of a protein scaffold, such as thioredoxin-A. They bind to target proteins with high affinity, similar to that of an antibody. They are designed to inhibit the function of other protein inside the cell."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "natural peptide",
                                            "name": "natural peptide",
                                            "data": {
                                                "definition": "These peptides are available in natural sources in vivo, e.g. glutathione, Neuropeptide Y, glucagon, enkephalins, etc."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "peptoid",
                            "name": "peptoid",
                            "data": {
                                "definition": "Peptoids are a class of biomimetic polymers based on oligo-N-substituted glycine backbones, designed to mimic peptides and proteins. Peptoids are resistant to proteolysis and not typically denatured by solvent, temperature, or chemical denaturants."
                            },
                            "children": [
                                
                            ]
                        },
                        {
                            "id": "nucleic acid perturbagen",
                            "name": "nucleic acid perturbagen",
                            "data": {
                                "definition": "The perturbagen is a nucleic acid. A nucleic acid is a polymer of nucleotides, which carry genetic information."
                            },
                            "children": [
                                {
                                    "id": "rna perturbagen",
                                    "name": "rna perturbagen",
                                    "data": {
                                        "definition": "The perturbagen is RNA or ribonucleic acid. RNA is a polymer of nucleotides with a backbone made of ribose sugar and phosphate groups linked together by ester bonds. RNA has the base uracil instead of thymine, which is present in DNA."
                                    },
                                    "children": [
                                        {
                                            "id": "rna aptamer",
                                            "name": "rna aptamer",
                                            "data": {
                                                "definition": "Aptamers are synthetic affinity probes. RNA aptamers are oligonucleotides that bind to a specific target molecule such as small molecules, proteins, nucleic acids and cells. They are created by selecting them from a large random sequence pool, but natural aptamers also exist in riboswitches."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "trna perturbagen",
                                            "name": "trna perturbagen",
                                            "data": {
                                                "definition": "tRNA or transfer RNA is a small RNA molecule that transfers a specific active amino acid to a growing polypeptide chain at the ribosomal site of protein synthesis."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "rrna perturbagen",
                                            "name": "rrna perturbagen",
                                            "data": {
                                                "definition": "rRNA or ribosomal RNAs are present in ribosomes in a cell. Ribosomes are protein translation machinery which can read messenger RNAs and translate that information into proteins."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "rna interference",
                                            "name": "rna interference",
                                            "data": {
                                                "definition": "RNA interference (RNAi) includes the gene silencing induced by endogenous miRNAs as well as silencing triggered by foreign dsRNA. Short hairpin RNA (shRNA) or small interfering RNA (si RNA) molecules can be introduced into cells to knockdown gene expression."
                                            },
                                            "children": [
                                                {
                                                    "id": "sirna",
                                                    "name": "sirna",
                                                    "data": {
                                                        "definition": "In the RNAi pathway,  the double stranded RNAs first get processed into 20-25 nucleotide small interfering RNAs (siRNAs) by an RNase III-like enzyme called Dicer. The siRNAs enter into endoribonuclease-containing complexes known as RNA-induced silencing complexes (RISCs), and bring about the cleavage of the target mRNAs to achieve gene silencing."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "shrna",
                                                    "name": "shrna",
                                                    "data": {
                                                        "definition": "Short hairpin RNA (shRNA) can be used to achieve stable knockdown of gene expression. shRNA molecules are cloned into lentiviral or other vectors and expressed by a pol III type promoter."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "mirna",
                                                    "name": "mirna",
                                                    "data": {
                                                        "definition": "miRNAs or microRNAs are short RNAs (~22 nucleotides long) that bind to complementary sequences in the three prime untranslated regions (3' UTRs) of target messenger RNA transcripts (mRNAs), usually resulting in gene silencing."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "mrna perturbagen",
                                            "name": "mrna perturbagen",
                                            "data": {
                                                "definition": "mRNA or messenger RNA is a genetic intermediate between DNA and protein. It is transcribed from DNA by RNA polymerases which is further processed by other enzymes. mRNA serves as the template to synthesize proteins on the ribosomes."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "morpholino oligonucleotide",
                                    "name": "morpholino oligonucleotide",
                                    "data": {
                                        "definition": "Morpholinos are antisense oligos that bind to specific mRNAs to reduce gene expression. They are small (~25 base) and possess altered back structures compared to DNA or RNA making them resistant to cleavage by nucleases. They are used extensively as tools to investigate zebrafish, Xenopus and sea urchin embryonic development."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "dna perturbagen",
                                    "name": "dna perturbagen",
                                    "data": {
                                        "definition": "The perturbagen is DNA or deoxyribonuleic acid. DNA is the genetic material in all the living things, and some viruses. It is a polymer of nucleotides, with a backbone made of deoxyribose sugar and phosphate groups joined together by ester bonds."
                                    },
                                    "children": [
                                        {
                                            "id": "dna aptamer",
                                            "name": "dna aptamer",
                                            "data": {
                                                "definition": "Aptamers are synthetic affinity probes. DNA aptamers are oligonucleotides that bind to a specific target molecule, such as small molecules, proteins, nucleic acids and cells. They are created by selecting them from a large random sequence pool through repeated rounds of in vitro selection."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "lna",
                                            "name": "lna",
                                            "data": {
                                                "definition": "Locked nucleic acid (LNA) is a nucleic acid analogue containing one or more LNA nucleotide monomers with a bicyclic furanose unit locked in an RNA mimicking sugar conformation. LNA oligonucleotides have high affinity toward complementary single-stranded RNA and complementary single- or double-stranded DNA. This makes LNA suitable for sequence specific targeting of RNA in vitro or in vivo."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "plasmid dna",
                                            "name": "plasmid dna",
                                            "data": {
                                                "definition": "Plasmid is a double stranded circular DNA molecule that can replicate independently of the chromosomal DNA. It exists naturally in bacteria and some yeast. In genetic engineering, plasmids are exploited to clone and multiply a gene of interest. Several plasmid vectors are available commercially to introduce cloned DNA into different cell types.."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "linear dna",
                                            "name": "linear dna",
                                            "data": {
                                                "definition": "DNA that is linear and not circular. Examples of linear DNA include synthetic oligonucleotide, restriction endonuclease digestion product, polymerase chain reaction product, mechanically sheared genomic DNA, etc."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "cdna",
                                            "name": "cdna",
                                            "data": {
                                                "definition": "cDNA or complementary DNA is complementary in sequence to a particular messenger RNA (mRNA). It is synthesized from mRNA in a reverse transcription reaction by reverse transcriptase enzyme. cDNA can be cloned in a vector and introduced into cells to bring about the expression of that particular gene in those cells."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "pna",
                                            "name": "pna",
                                            "data": {
                                                "definition": "PNA or protein nucleic acid is an artificially synthesized polymer whose backbone is composed of repeating N-(2-aminoethyl)-glycine units linked by peptide bonds. Since PNA does not contain charged phosphate groups in its backbone, the interaction between PNA-DNA is stronger than that between DNA-DNA due to the lack of electrostatic repulsion."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "monomeric compound",
                            "name": "monomeric compound",
                            "data": {
                                "definition": "Small molecule is an organic compound with a low molecular weight (<800 Da) which is used to screen against a biological target, e.g. protein, nucleic acid, cell or organism  to test its ability to bind it and bring about a change in activity. It is either isolated from a natural source or is synthesized."
                            },
                            "children": [
                                
                            ]
                        },
                        {
                            "id": "lipid",
                            "name": "lipid",
                            "data": {
                                "definition": "Lipids are a group of naturally occurring organic compounds that are soluble in nonpolar organic solvents, but insoluble in water. Examples include fatty acids, triglycerides, phospholipids, etc. Lipids function to store energy, form a component of cell membrane and as signaling molecules."
                            },
                            "children": [
                                
                            ]
                        }
                    ]
                },
                {
                    "id": "meta target",
                    "name": "meta target",
                    "data": {
                        "definition": "It is the biological entity that is the presumed subject of the bioassay. It is a description of what is known about the interaction of the perturbagen within the biological system. This could refer to a molecule whose activity is being regulated or a biological process such as neurite outgrowth.  A bioassay can have multiple targets."
                    },
                    "children": [
                        {
                            "id": "organism",
                            "name": "organism",
                            "data": {
                                "definition": "The organism related to the target / meta-target of the bioassay. It includes both bacterium and eukaryote."
                            },
                            "children": [
                                {
                                    "id": "eukaryote",
                                    "name": "eukaryote",
                                    "data": {
                                        "definition": "This includes fungi, vertebrates and invertebrates. Eukaryotic cells have a well-defined nucleus and membrane enveloped organelles namely, mitochondria, endoplasmic reticulum, golgi apparatus, and lysosomes."
                                    },
                                    "children": [
                                        {
                                            "id": "invertebrate",
                                            "name": "invertebrate",
                                            "data": {
                                                "definition": "These animals lack a vertebral column and many of which are used as model organisms in HTS screening including Drosophila, C.elegans, sea urchin, etc. "
                                            },
                                            "children": [
                                                {
                                                    "id": "non-protozoan",
                                                    "name": "non-protozoan",
                                                    "data": {
                                                        "definition": "null "
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "sea urchin",
                                                            "name": "sea urchin",
                                                            "data": {
                                                                "definition": "null "
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "drosophila melanogaster",
                                                            "name": "drosophila melanogaster",
                                                            "data": {
                                                                "definition": "Common name: fruit fly"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "caenorhabditis elegans",
                                                            "name": "caenorhabditis elegans",
                                                            "data": {
                                                                "definition": "Common name: worm"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "id": "protozoan parasite",
                                                    "name": "protozoan parasite",
                                                    "data": {
                                                        "definition": "They belong to the phylum called protozoa, which is the first subdivision of the animal kingdom and includes unicellular organisms. Some of the parasites from this phylum include Plasmodium falciparum, Leishmania mexicana, Trypanosoma brucei, etc which cause malaria, leishmaniasis, and sleeping sickness, respectively."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "leishmania major",
                                                            "name": "leishmania major",
                                                            "data": {
                                                                "definition": "Causes leishmaniasis diseases"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "trypanosoma cruzi ",
                                                            "name": "trypanosoma cruzi ",
                                                            "data": {
                                                                "definition": "Causes trypanosomiasis diseases"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "plasmodium falciparum ",
                                                            "name": "plasmodium falciparum ",
                                                            "data": {
                                                                "definition": "Causes malaria"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "vertebrate",
                                            "name": "vertebrate",
                                            "data": {
                                                "definition": "This includes animals having a vertebral column, and model organisms from this group include the zebrafish, mouse rat, etc"
                                            },
                                            "children": [
                                                {
                                                    "id": "non mammalian",
                                                    "name": "non mammalian",
                                                    "data": {
                                                        "definition": "This includes vertebrate model organisms such as the Danio rerio (zebrafish), Xenopus laevis, etc which are tested generally to understand development."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "xenopus laevis",
                                                            "name": "xenopus laevis",
                                                            "data": {
                                                                "definition": "Common name: South African clawed toad"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "danio rerio",
                                                            "name": "danio rerio",
                                                            "data": {
                                                                "definition": "Common name: Zebrafish"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "id": "mammalian",
                                                    "name": "mammalian",
                                                    "data": {
                                                        "definition": "This includes animal models typically used in drug discovery for toxicity or pharmacology studies, such as mouse, rat (mammals)."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "sus scrofa",
                                                            "name": "sus scrofa",
                                                            "data": {
                                                                "definition": "Common name: pig"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "rattus norvegicus",
                                                            "name": "rattus norvegicus",
                                                            "data": {
                                                                "definition": "Common name: rat"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "mus musculus",
                                                            "name": "mus musculus",
                                                            "data": {
                                                                "definition": "Common name: mouse"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "homo sapiens",
                                                            "name": "homo sapiens",
                                                            "data": {
                                                                "definition": "Common name: human"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "canis familiaris",
                                                            "name": "canis familiaris",
                                                            "data": {
                                                                "definition": "Common name: dog"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "fungi",
                                            "name": "fungi",
                                            "data": {
                                                "definition": "Fungi are either unicellular or multicellular and include yeasts, molds, mushrooms, etc. Several parasitic fungi exist, which infect animals and plants, e.g., Aspergillus flavus, Claviceps purpurea, Pneumocystis jirovecii, etc."
                                            },
                                            "children": [
                                                {
                                                    "id": "multicellular fungi",
                                                    "name": "multicellular fungi",
                                                    "data": {
                                                        "definition": "Most of the fungi are multicellular, e.g. molds, mushrooms, etc. They are made up of multicellular filaments known as hyphae. Several parasitic fungi exist, which infect animals and plants, e.g., Aspergillus flavus, Claviceps purpurea, Pneumocystis jirovecii, etc."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "candida albicans ",
                                                            "name": "candida albicans ",
                                                            "data": {
                                                                "definition": "Causes the candida infection"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "id": "unicellular fungi",
                                                    "name": "unicellular fungi",
                                                    "data": {
                                                        "definition": "It includes the baker's yeast (Saccharomyces cerevisiae) and fission yeast (Schizosaccharomyces pombe), which are unicellular."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "saccharomyces cerevisiae",
                                                            "name": "saccharomyces cerevisiae",
                                                            "data": {
                                                                "definition": "Common name: baker's yeast"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "bacterium",
                                    "name": "bacterium",
                                    "data": {
                                        "definition": "Bacteria lack membrane bound organelles and are surrounded by a rigid cell wall. Though they are unicellular, they function by aggregating into multicellular colonies."
                                    },
                                    "children": [
                                        {
                                            "id": "mycobacterium tuberculosis",
                                            "name": "mycobacterium tuberculosis",
                                            "data": {
                                                "definition": "Causes tuberculosis"
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "group a streptococcus",
                                            "name": "group a streptococcus",
                                            "data": {
                                                "definition": "Causes mild infections such as \"strep throat\" to severe diseases such as necrotizing fasciitis and streptococcal toxic shock syndrome."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "staphylococcus aureus",
                                            "name": "staphylococcus aureus",
                                            "data": {
                                                "definition": "Causes staph infections, which can lead to a range of diseases, such as skin infections, pneumonia, meningitis, endocarditis, etc."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "escherichia coli",
                                            "name": "escherichia coli",
                                            "data": {
                                                "definition": "Most strains are harmless, but some can cause serious food poisoning"
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "yersinia pseudotuberculosis",
                                            "name": "yersinia pseudotuberculosis",
                                            "data": {
                                                "definition": "Causes tuberculosis-like symptoms and gastroenteritis"
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "salmonella typhi",
                                            "name": "salmonella typhi",
                                            "data": {
                                                "definition": "Causes typhoid fever, paratyphoid fever, and foodborne illness"
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "vibrio harveyi",
                                            "name": "vibrio harveyi",
                                            "data": {
                                                "definition": "null "
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "pseudomonas aeruginosa ",
                                            "name": "pseudomonas aeruginosa ",
                                            "data": {
                                                "definition": "null "
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "salmonella typhimurium ",
                                            "name": "salmonella typhimurium ",
                                            "data": {
                                                "definition": "Causes typhoid fever, paratyphoid fever, and foodborne illness"
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "cell",
                            "name": "cell",
                            "data": {
                                "definition": "This refers to living cells of eukaryotic origin, which are either primary or established."
                            },
                            "children": [
                                {
                                    "id": "primary cell",
                                    "name": "primary cell",
                                    "data": {
                                        "definition": "Cells that are cultured directly from a subject (tissue or tumor) and which are not immortalized. Primary cultures are the closest representation of the in vivo differentiation state, and hence are the best choice for testing drugs. They often lose their phenotype and genotypes within several passages."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "permanent cell line",
                                    "name": "permanent cell line",
                                    "data": {
                                        "definition": "Immortalized cell, which has undergone transformation and can be passed indefinitely in culture. It has acquired the ability to proliferate indefinitely either through random mutation or deliberate modification, such as artificial expression of the telomerase gene."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "tissue",
                            "name": "tissue",
                            "data": {
                                "definition": "This includes information on the tissue used in the format of a bioassay."
                            },
                            "children": [
                                {
                                    "id": "diseased tissue",
                                    "name": "diseased tissue",
                                    "data": {
                                        "definition": "The tissue or tumor is obtained from an animal exhibiting a disease phenotype."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "healthy tissue",
                                    "name": "healthy tissue",
                                    "data": {
                                        "definition": "The tissue is obtained from a normal, healthy animal."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "plasma",
                                    "name": "plasma",
                                    "data": {
                                        "definition": "The liquid component of blood in which the blood cells are suspended. In addition to other proteins, plasma contains the clotting factors."
                                    },
                                    "children": [
                                        {
                                            "id": "platelet rich plasma",
                                            "name": "platelet rich plasma",
                                            "data": {
                                                "definition": "it is plasma enriched in platelets obtained by separation and concentration of blood. Being rich in growth factors (cytokines), it stimulates wound healing."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "molecular target",
                            "name": "molecular target",
                            "data": {
                                "definition": "It is either a protein or nucleic acid target which is modulated by a perturbagen. This assay is performed in a biochemical format."
                            },
                            "children": [
                                {
                                    "id": "protein target",
                                    "name": "protein target",
                                    "data": {
                                        "definition": "A protein or polypeptide is a long polymer of amino acids joined together by peptide bonds. The sequence of the amino acids is defined by the genetic code. A protein or a set of proteins is a target of a perturbagen whereby its normal physiological process or a disease pathology is modulated. This assay is performed in a biochemical format."
                                    },
                                    "children": [
                                        {
                                            "id": "receptor",
                                            "name": "receptor",
                                            "data": {
                                                "definition": "A membrane or intracellular protein that specifically binds a known stimulus of cellular activity, such as a hormone, growth factor, antigens, neurotransmitters, drugs, etc."
                                            },
                                            "children": [
                                                {
                                                    "id": "nuclear receptor",
                                                    "name": "nuclear receptor",
                                                    "data": {
                                                        "definition": "Nuclear receptors are localized in the cytosol and mediate the activity of hormones and certain vitamins. They are all transcription factors, which upon activation by ligand binding, translocate to the nucleus, bind specific DNA sequence and activate gene expression."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "transmembrance receptor",
                                                    "name": "transmembrance receptor",
                                                    "data": {
                                                        "definition": "Transmembrane receptors are integral membrane proteins in either the cell- or organelle-membrane. They bind specific hormones, neurotransmitters, cytokines, growth factors, etc and initiate cell signaling, which results in changes in the cell function."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "g protein coupled receptor",
                                                            "name": "g protein coupled receptor",
                                                            "data": {
                                                                "definition": "G protein-coupled receptors (GPCRs), comprise the largest integral membrane protein family with nearly 1000 members. They integrate extracellular signals from hormones, growth factors, neurotransmitters, etc to downstream cellular responses. They are important pharmacological targets, being associated with a number of diseases. Upon ligand binding extracellularly, the GPCR signals through a downstream heterotrimeric G protein complex, consisting of the α, β and γ subunits."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "id": "ion channel",
                                                    "name": "ion channel",
                                                    "data": {
                                                        "definition": "Ion channels are present in the cell membrane. They are pore-forming protein complexes that facilitate diffusion of ions across membranes and thus establish the small voltage gradient across the plasma membrane of living cells."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "enzyme",
                                            "name": "enzyme",
                                            "data": {
                                                "definition": "Enzymes are biological catalysts involved in important pathways that allow chemical reactions to occur at higher rates (velocities) than would be possible without the enzyme. Enzymes are generally globular proteins that have one or more substrate binding sites."
                                            },
                                            "children": [
                                                {
                                                    "id": "transferase",
                                                    "name": "transferase",
                                                    "data": {
                                                        "definition": "Transfer of a functional group from one substance to another. The group may be methyl-, acyl-, amino- or phosphate group."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "kinase",
                                                            "name": "kinase",
                                                            "data": {
                                                                "definition": "A protein kinase is an enzyme that modifies other proteins by chemically adding phosphate groups to them. The substrate protein's activity, localization and overall function is modulated by the kinase reaction."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "generic transferase",
                                                            "name": "generic transferase",
                                                            "data": {
                                                                "definition": "Includes all  transferases except kinases."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "id": "oxidoreductase",
                                                    "name": "oxidoreductase",
                                                    "data": {
                                                        "definition": "To catalyze oxidation/reduction reactions; transfer of H and O atoms or electrons from one substance to another."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "luciferase",
                                                            "name": "luciferase",
                                                            "data": {
                                                                "definition": "null "
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "id": "lyase",
                                                    "name": "lyase",
                                                    "data": {
                                                        "definition": "Non-hydrolytic addition or removal of groups from substrates. C-C, C-N, C-O or C-S bonds may be cleaved."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "ligase",
                                                    "name": "ligase",
                                                    "data": {
                                                        "definition": "Join together two molecules by synthesis of new C-O, C-S, C-N or C-C bonds with simultaneous breakdown of ATP."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "isomerase",
                                                    "name": "isomerase",
                                                    "data": {
                                                        "definition": "Intramolecule rearrangement, i.e. isomerization changes within a single molecule."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "hydrolase",
                                                    "name": "hydrolase",
                                                    "data": {
                                                        "definition": "Formation of two products from a substrate by hydrolysis."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "phosphatase",
                                                            "name": "phosphatase",
                                                            "data": {
                                                                "definition": "A phosphatase is an enzyme that removes a phosphate group from its substrate by hydrolysis of phosphoric acid esters."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "generic hydrolase",
                                                            "name": "generic hydrolase",
                                                            "data": {
                                                                "definition": "Includes all hydrolases except phosphatases."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "transporter",
                                            "name": "transporter",
                                            "data": {
                                                "definition": "A transporter protein is a transmembrane protein that helps transport small molecules and proteins across the membrane. This is done using facilitated diffusion or active transport."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "membrane protein",
                                            "name": "membrane protein",
                                            "data": {
                                                "definition": "Membrane proteins are associated with the cell membrane. They can be classified into two broad categoriesâintegral (intrinsic) and peripheral (extrinsic). Intrinsic proteins have one or more segments embedded in the phospholipid bilayer, while extrinsic proteins do not traverse the membrane."
                                            },
                                            "children": [
                                                {
                                                    "id": "g protein",
                                                    "name": "g protein",
                                                    "data": {
                                                        "definition": "There are two distinct G protein families, namely the heterotrimeric G proteins (consisting of the α, β and γ subunits) downstream of the G protein coupled receptors (GPCRs) and the monomeric G proteins that belong to the Ras superfamily of GTPases. Upon ligand binding, the GPCR undergoes a conformational change, which allows it to act as a guanine nucleotide exchange factor (GEF) of an associated G protein. The GTP-bound G α subunit then signals by regulating the activity of adenylate cyclase, phospholipase, or Rho GEFs."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "adhesion",
                                            "name": "adhesion",
                                            "data": {
                                                "definition": "The proteins located on the cell surface which are involved with the binding with other cells or the extracellular matrix. They belong to 5 families: Immunoglobulin superfamily, integrins, cadherins, selectins and the lymphocyte homing receptors."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "enzyme regulator",
                                            "name": "enzyme regulator",
                                            "data": {
                                                "definition": "It modulates the activity of an enzyme."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "cytosolic protein",
                                            "name": "cytosolic protein",
                                            "data": {
                                                "definition": "Proteins localized in the cytosol, but do not belong to the transcription factor, transporter, structural, enzyme, enzyme regulator and nuclear receptor classes of proteins in the cytosol."
                                            },
                                            "children": [
                                                {
                                                    "id": "chaperone",
                                                    "name": "chaperone",
                                                    "data": {
                                                        "definition": "They have an essential role in the regulation of protein conformation states. Molecular chaperones help nascent polypeptides fold correctly and multimeric protein complexes assemble properly, and reduce the possibility of protein aggregation."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "secreted",
                                            "name": "secreted",
                                            "data": {
                                                "definition": "These proteins are translocated from the inside of the cell to the cell's exterior. Proteins targeted for secretion undergo glycosylation and other postranslational modifications in the endoplasmic reticulum and the golgi complex, and are enclosed in secretory vesicles. The fusion of vesicles to the cell membrane releases the secretory proteins to the exterior. Examples include growth factors, tumor necrosis factors, cytokines, etc."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "transcription factor",
                                            "name": "transcription factor",
                                            "data": {
                                                "definition": "They are all transcription factors, which upon activation by ligand binding, translocate to the nucleus, bind specific DNA sequence and activate gene expression."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "structural",
                                            "name": "structural",
                                            "data": {
                                                "definition": "These proteins constitute the largest class with respect to the total protein mass. They are fibrous proteins, e.g. keratin, actin, myosin, etc whose primary function is to provide scaffolding that maintains cell shape."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "nucleic acid",
                                    "name": "nucleic acid",
                                    "data": {
                                        "definition": "The target is a nucleic acid, which is modulated by a perturbagen. A nucleic acid is a polymer of nucleotides, which carry genetic information."
                                    },
                                    "children": [
                                        {
                                            "id": "rna",
                                            "name": "rna",
                                            "data": {
                                                "definition": "RNA or ribonucleic acid is a polymer of nucleotides with a backbone made of ribose sugar and phosphate groups linked together by ester bonds. RNA has the base uracil instead of thymine, which is present in DNA."
                                            },
                                            "children": [
                                                {
                                                    "id": "trna",
                                                    "name": "trna",
                                                    "data": {
                                                        "definition": "tRNA or transfer RNA is a small RNA molecule that transfers a specific active amino acid to a growing polypeptide chain at the ribosomal site of protein synthesis."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "rrna",
                                                    "name": "rrna",
                                                    "data": {
                                                        "definition": "rRNA or ribosomal RNAs are present in ribosomes in a cell. Ribosomes are protein translation machinery which can read messenger RNAs and translate that information into proteins."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "mrna",
                                                    "name": "mrna",
                                                    "data": {
                                                        "definition": "mRNA or messenger RNA is a genetic intermediate between DNA and protein. It is transcribed from DNA by RNA polymerases which is further processed by other enzymes. mRNA serves as the template to synthesize proteins on the ribosomes."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "riboswitch",
                                                            "name": "riboswitch",
                                                            "data": {
                                                                "definition": "They are part of mRNA that regulate gene expression through ligand induced changes in mRNA secondary or tertiary structure. The ligand is generally a small metabolite which binds to the aptamer part of the riboswitch."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "id": "mirna",
                                                    "name": "mirna",
                                                    "data": {
                                                        "definition": "miRNAs or microRNAs are short RNAs (~22 nucleotides long) that bind to complementary sequences in the three prime untranslated regions (3' UTRs) of target messenger RNA transcripts (mRNAs), usually resulting in gene silencing."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "dna",
                                            "name": "dna",
                                            "data": {
                                                "definition": "DNA or deoxyribonucleic acid is the genetic material in all the living things, and some viruses. It is a polymer of nucleotides, with a backbone made of deoxyribose sugar and phosphate groups joined together by ester bonds."
                                            },
                                            "children": [
                                                {
                                                    "id": "extra chromosomal",
                                                    "name": "extra chromosomal",
                                                    "data": {
                                                        "definition": "This refers to the DNA located or maintained in a cell apart from the chromosomes, e.g. mitochondrial DNA (animals), chloroplast DNA (plants), plasmid DNA in bacteria, and parasitic viral DNA."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "chromosomal",
                                                    "name": "chromosomal",
                                                    "data": {
                                                        "definition": "It is the cell's genetic material organized as multiple long linear DNA molecules in complex with a large variety of proteins, such as histones, to form chromosomes. In eukaryotic cells, the genetic material is located in a membrane enveloped nucleus, while the prokaryotes lack a nucleus."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "biological process",
                            "name": "biological process",
                            "data": {
                                "definition": "A biological process is a process of a living organism. It is a recognized chemical reaction or molecular function with a defined beginning and an end. Examples include RNA and protein splicing, cell death, neurite outgrowth, etc."
                            },
                            "children": [
                                {
                                    "id": "signaling pathway",
                                    "name": "signaling pathway",
                                    "data": {
                                        "definition": "Signaling pathway is a mechanism that converts a mechanical/chemical stimulus that impinges on the cell into a specific cellular response. Signal transduction starts with a signal to a receptor, and ends with a change in cell function which is mediated through a network of macromolecules."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    "id": "format",
                    "name": "format",
                    "data": {
                        "definition": "Abstract concept representing the biological and chemical features common to each test condition in the assay.  Includes cell lines/types employed, reagent conditions, etc"
                    },
                    "children": [
                        {
                            "id": "cell-free",
                            "name": "cell-free",
                            "data": {
                                "definition": "This originates from a cell, but does not use intact (live) cells; does not include biochemical assays, which is one of the major formats that is categorized separately. It is most often a homogeneous assay type, but can be heterogeneous if a solid phase such as beads are used to immobilize the components."
                            },
                            "children": [
                                {
                                    "id": "whole cell lysate",
                                    "name": "whole cell lysate",
                                    "data": {
                                        "definition": "A cell whose membrane has been ruptured, either mechanically or by freeze-thawing. The entire cell lysate is used in the assay without extensive purification (or separation)."
                                    },
                                    "children": [
                                        {
                                            "id": "rabbit reticulocyte lysate",
                                            "name": "rabbit reticulocyte lysate",
                                            "data": {
                                                "definition": "The lysate contains the cellular components necessary for in vitro protein synthesis (tRNA, ribosomes, amino acids, initiation, elongation and termination factors). It is prepared from rabbit reticulocytes by lysis, followed by treatment with micrococcal nuclease to destroy endogenous mRNA for background reduction."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "subcellular",
                                    "name": "subcellular",
                                    "data": {
                                        "definition": "subcellular organelles / component (not individual proteins) obtained via cell lysis and differential centrifugation (or potentially other method of purification / extraction of the organelles)"
                                    },
                                    "children": [
                                        {
                                            "id": "nuclear extract",
                                            "name": "nuclear extract",
                                            "data": {
                                                "definition": "It is a soluble extract prepared from the cellular nuclei, which are obtained by differential centrifugation of the cell lysate."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "mitochondrion",
                                            "name": "mitochondrion",
                                            "data": {
                                                "definition": "An organelle found in the cytoplasm of eukaryotic cells that metabolizes glucose and other sugars to produce usable energy in the form of ATP. It contains its own genome which encodes proteins, tRNA and rRNA."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "microsome",
                                            "name": "microsome",
                                            "data": {
                                                "definition": "Microsomes are vesicle-like structures formed as an artifact from the endoplasmic reticulum after the cell has been ruptured by homogenization."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "cytosol",
                                            "name": "cytosol",
                                            "data": {
                                                "definition": "The fluid component of cytoplasm, without the organelles and the insoluble cytoplasmic components. "
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "cell membrane",
                                            "name": "cell membrane",
                                            "data": {
                                                "definition": "The thin outer covering of a cell consisting of lipid-bilayer embedded with proteins. It is semi-permeable and is also called \"plasma membrane\""
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "nucleosome",
                                            "name": "nucleosome",
                                            "data": {
                                                "definition": "They are the basic repeating units of the eukaryotic chromatin consisting of DNA wound around a histone protein core."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "tissue-based",
                            "name": "tissue-based",
                            "data": {
                                "definition": "Involves the use of a tissue derived from a living organism and is a heterogeneous assay type."
                            },
                            "children": [
                                
                            ]
                        },
                        {
                            "id": "cell-based",
                            "name": "cell-based",
                            "data": {
                                "definition": "Involves the use of living cells of eukaryotic origin and is a heterogeneous assay type."
                            },
                            "children": [
                                
                            ]
                        },
                        {
                            "id": "organism-based",
                            "name": "organism-based",
                            "data": {
                                "definition": "Involves the use of a living organism and is a heterogeneous assay type."
                            },
                            "children": [
                                
                            ]
                        },
                        {
                            "id": "biochemical",
                            "name": "biochemical",
                            "data": {
                                "definition": "It is an in vitro format used to measure the activity of a biological macromolecule  (protein or nucleic acid). It is most often a homogeneous assay type, but can be heterogeneous if solid phase such as beads are used to immobilize the macromolecule."
                            },
                            "children": [
                                {
                                    "id": "nucleic acid format",
                                    "name": "nucleic acid format",
                                    "data": {
                                        "definition": "In this format, the perturbagen targets a nucleic acid (DNA or RNA) to regulate its function.  "
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "protein format",
                                    "name": "protein format",
                                    "data": {
                                        "definition": "In this format, the perturbagen targets a protein to regulate its function.  "
                                    },
                                    "children": [
                                        {
                                            "id": "single protein",
                                            "name": "single protein",
                                            "data": {
                                                "definition": "one protein sequence"
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "protein complex",
                                            "name": "protein complex",
                                            "data": {
                                                "definition": "Two or more proteins interact to form a stable complex. Each of the members of this complex has a distinct role in mediating the overall function of the complex, which can be as a chaperone (DnaK, DnaJ and grpE), an enzyme (mitochondrial respiratory complexes), a signaling complex (G-proteins), etc. It can be viewed as a quaternary structure of the proteins."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "physicochemical",
                            "name": "physicochemical",
                            "data": {
                                "definition": "Assays in which the physical and chemical properties of perturbagens are measured, namely, aqueous solubility, octanol water partition, cell permeability models, for e.g.  Caco2 cells, parallel artificial membrane permeability assay (PAMPA), etc."
                            },
                            "children": [
                                
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": "specification",
            "name": "specification",
            "data": {
                "definition": "A standardized description"
            },
            "children": [
                {
                    "id": "id spec",
                    "name": "id spec",
                    "data": {
                        "definition": "It is the unique identification number of the bioassay, gene, protein, taxonomy, etc. Every assay in PubChem has an ID called the assay ID or AID. Gene and protein IDs are obtained by referring to other databases such as NCBI gene, Uniprot, etc."
                    },
                    "children": [
                        {
                            "id": "pmid",
                            "name": "pmid",
                            "data": {
                                "definition": "A PMID (PubMed Identifier or PubMed Unique Identifier) is a unique number assigned to each PubMed citation"
                            },
                            "children": [
                                
                            ]
                        },
                        {
                            "id": "taxonomy id",
                            "name": "taxonomy id",
                            "data": {
                                "definition": "It is the unique identification number pertaining to the taxonomy of the organism tested in the bioassay. It is obtained by referring to the UniProt taxonomy and NCBI taxonomy databases."
                            },
                            "children": [
                                {
                                    "id": "ncbi taxonomy id",
                                    "name": "ncbi taxonomy id",
                                    "data": {
                                        "definition": "It is the unique identification number pertaining to the taxonomy of the organism tested in the bioassay. It is obtained by referring to the NCBI taxonomy database."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "perturbagen id",
                            "name": "perturbagen id",
                            "data": {
                                "definition": "It is the unique identification number of each perturbagen that was screened in the bioassay. In PubChem, perturbagen ID is either represented as a compound ID (CID) or a substance ID (SID)."
                            },
                            "children": [
                                {
                                    "id": "pubchem cid",
                                    "name": "pubchem cid",
                                    "data": {
                                        "definition": "One of the perturbagen IDs in PubChem is compound ID (CID). A compound has a unique chemical structure which is derived from the substance database. Compounds are stored in a compound database. "
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "pubchem sid",
                                    "name": "pubchem sid",
                                    "data": {
                                        "definition": "One of the perturbagen IDs in PubChem is substance ID (SID). A substance is a small molecule whose descriptions are depositor specified. The substances descriptions are stored in the substance database."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "gene id",
                            "name": "gene id",
                            "data": {
                                "definition": "It is the unique identification number of the gene that was a target in the bioassay. Gene ID is obtained by referring to other databases such as the Entrez gene, EMBL Nucleotide Sequence Database, etc."
                            },
                            "children": [
                                {
                                    "id": "entrez gene accession number",
                                    "name": "entrez gene accession number",
                                    "data": {
                                        "definition": "It is the unique identification number of the gene that was a target in the bioassay. Gene ID is obtained by referring to other databases such as the NCBI Entrez."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "endpoint id",
                            "name": "endpoint id",
                            "data": {
                                "definition": "null "
                            },
                            "children": [
                                {
                                    "id": "pubchem tid",
                                    "name": "pubchem tid",
                                    "data": {
                                        "definition": "Endpoint ID from a PubChem bioassay"
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "bioassay id",
                            "name": "bioassay id",
                            "data": {
                                "definition": "It is the unique identification number of a bioassay in a database."
                            },
                            "children": [
                                {
                                    "id": "pubchem aid",
                                    "name": "pubchem aid",
                                    "data": {
                                        "definition": "It is the unique identification number of a bioassay. Every assay in the PubChem has an ID called the assay ID or AID."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "protein id",
                            "name": "protein id",
                            "data": {
                                "definition": "It is the unique identification number of the protein that was perturbed in the bioassay. It is obtained by referring to other databases such as NCBI protein, UniProt, etc."
                            },
                            "children": [
                                {
                                    "id": "uniprot id",
                                    "name": "uniprot id",
                                    "data": {
                                        "definition": "It is the unique identification number of the protein that was perturbed in the bioassay, which is obtained by referring to the UniProt database."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    "id": "screening campaign spec",
                    "name": "screening campaign spec",
                    "data": {
                        "definition": "Screening campaign specifications include details describing the screening campaign. Screening campaign refers to a number of assays, which are executed following a certain design with the goal of identifying a compound (or a series of compounds) with a specific function and - desirably a known mechanism of action. It typicaly includes primary (screening at single concentration), confirmatory, concentration-response (dose response), secondary, and also selectivity assays. The screening campain can also include further SAR studies."
                    },
                    "children": [
                        {
                            "id": "screening campaign name",
                            "name": "screening campaign name",
                            "data": {
                                "definition": "An abstract concept representing the motivation for conducting the screening campaign.  For example this can include the purpose of understanding a basic biological process or to identify a hits or leads for a drug devlopment program."
                            },
                            "children": [
                                
                            ]
                        }
                    ]
                },
                {
                    "id": "bioassay spec",
                    "name": "bioassay spec",
                    "data": {
                        "definition": "Description of an experiment carried out for the purpose of screening a perturbing agent in a biological system, measuring the effect of the agents using specified technology, and arriving at endpoints that satisfy particular criteria."
                    },
                    "children": [
                        {
                            "id": "assay measurement type",
                            "name": "assay measurement type",
                            "data": {
                                "definition": "This describes whether a change in an assay is measured once at one fixed end-point or over a  period of time at several time points."
                            },
                            "children": [
                                {
                                    "id": "kinetic assay",
                                    "name": "kinetic assay",
                                    "data": {
                                        "definition": "In this assay, change in activity is measured at several time points over a period of time."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "endpoint assay",
                                    "name": "endpoint assay",
                                    "data": {
                                        "definition": "In this assay, change in activity is measured at one time point."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "assay quality assessment",
                            "name": "assay quality assessment",
                            "data": {
                                "definition": "Commonly used statistical parameters for monitoring assay quality include Z and Zâ factors. Prior to starting a large screen, and after assay optimization and miniaturization, pilot screens are performed to assess the quality of the assay run and to assess / validate the suitability of a assay for a high-throughput screening run."
                            },
                            "children": [
                                {
                                    "id": "coefficient of variation",
                                    "name": "coefficient of variation",
                                    "data": {
                                        "definition": "Measure of the signal dispersion CV=100x[StdDev(sample)/Avg(sample)]"
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "signal to noise",
                                    "name": "signal to noise",
                                    "data": {
                                        "definition": "Measure of signal strength. S/N=[Avg(poscontrol)-Avg(negcontrol)]/[StdDev(poscontrol)+StdDev(negcontrol)] "
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "signal window",
                                    "name": "signal window",
                                    "data": {
                                        "definition": "Significant signal between positive and negative controls; assay dynamic range. SW={Avg(poscontrol)-Avg(negcontrol)-3x[StdDev(poscontrol)+StdDev(negcontrol)]}/StdDev(poscontrol)."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "z-prime factor",
                                    "name": "z-prime factor",
                                    "data": {
                                        "definition": "Describes the assay dynamic range considering range and data variation: Z=1-{3x[StdDev(poscontrol)+StdDev(negcontrol)]/[Avg(poscontrol)-Avg(negcontrol)]}"
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "z factor",
                                    "name": "z factor",
                                    "data": {
                                        "definition": "Describes the assay dynamic range considering range and data variation: Z=1-{3x[StdDev(sample)+StdDev(negcontrol)]/[Avg(sample)-Avg(negcontrol)]} Not to be confused with z-score. Can only be used for single concentration assays (not for concentration-response assays)."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "bioassay type",
                            "name": "bioassay type",
                            "data": {
                                "definition": "Categorization of bioassays based on the property or process that the assays is interrogating."
                            },
                            "children": [
                                {
                                    "id": "binding",
                                    "name": "binding",
                                    "data": {
                                        "definition": "Binding assays are performed to understand the interaction between two molecules, e.g. perturbagen-protein, protein-protein, protein-DNA, etc."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "functional",
                                    "name": "functional",
                                    "data": {
                                        "definition": "An assay where the measured signal corresponds to a complex response such as cell survival, proliferation, localization of a protein, nuclear translocation etc. The molecular target is not assumed."
                                    },
                                    "children": [
                                        {
                                            "id": "process",
                                            "name": "process",
                                            "data": {
                                                "definition": "A biological process is a process of a living organism. It is a recognized chemical reaction or molecular function with a defined beginning and end."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "signaling",
                                            "name": "signaling",
                                            "data": {
                                                "definition": "Signaling pathway is a mechanism that converts a mechanical/chemical stimulus that impinges on the cell into a specific cellular response. Signal transduction starts with a signal to a receptor, and ends with a change in cell function which is mediated through a network of macromolecules."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "reporter gene",
                                            "name": "reporter gene",
                                            "data": {
                                                "definition": "A reporter gene is a gene that is attached to a regulatory sequence of another gene of interest and introduced into cultured cells, animals or plants. Certain genes function as reporters because  they are easily identified and measured, or because they are selectable markers. Common reporter genes are luciferase, green fluorescent protein (GFP), beta-galactosidase and chloramphenicol acetyltransferase (CAT)."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "viability",
                                            "name": "viability",
                                            "data": {
                                                "definition": "Viability is a measure of the number of living cells in a culture.  Viability is determined by measuring tetrazolium salt and alamar blue reduction, staining with selective fluorescent dyes, or ATP content. ATP levels are detected using a luminescence based kit such as CellTiter-Glo (Promega). ATP values higher than controls (untreated cells) indicate proliferation and cultures with ATP concentrations lower than controls indicates cytotoxicity."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "enzyme activity",
                                            "name": "enzyme activity",
                                            "data": {
                                                "definition": "In this type of assay, the modulation of an enzyme activity by a perturbagen is measured. An enzyme is a protein that acts as a catalyst for a specific biochemical reaction, converting specific substrates into chemically distinct products."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "admet",
                                    "name": "admet",
                                    "data": {
                                        "definition": "ADMET stands for absorption, distribution, metabolism, excretion and toxicity. Admet assays are performed for verifying the bio-availability,  toxicity, metabolic stability, and drug-drug interaction potential of drugs: They quantify absorption of drugs in the intestine which is dependent on their solubility; distribution which ascertains their binding to plasma proteins, central nervous system penetration; metabolism where the in vivo clearance is monitored; and finally, toxicity in terms of inhibition of liver cytochrome p450 enzymes or hERG which could cause drug-drug interactions and cardiac arrhythmias, respectively."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "assay footprint",
                            "name": "assay footprint",
                            "data": {
                                "definition": "This describes the physical format such as plate density in which an assay is performed, which is generally a microplate format, but can also be an array format."
                            },
                            "children": [
                                {
                                    "id": "cuvette",
                                    "name": "cuvette",
                                    "data": {
                                        "definition": "Cuvettes are used to measure absorbance or fluorescence in a spectrophotometer or fluorimeter, respectively using either plastic, glass or quartz cuvettes."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "vial",
                                    "name": "vial",
                                    "data": {
                                        "definition": "Vials are small containers that are used in biological measurements, including scintillation counting."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "array",
                                    "name": "array",
                                    "data": {
                                        "definition": "The immobilization of  thousands of small molecules, genes or proteins on a glass slide for performing binding studies."
                                    },
                                    "children": [
                                        {
                                            "id": "protein array",
                                            "name": "protein array",
                                            "data": {
                                                "definition": "This includes spotting purified proteins, cell or tissue lysates, antibodies, or peptides in order to quantify the proteome, or study protein-protein, protein-DNA, protein-small molecule interactions and enzyme activity. "
                                            },
                                            "children": [
                                                {
                                                    "id": "purified protein array",
                                                    "name": "purified protein array",
                                                    "data": {
                                                        "definition": "In this method, many different purified proteins are spotted on a support, and used to assay biochemical functions such as protein-protein, protein-DNA, protein-small molecule interactions and enzyme activity."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "analytical array",
                                                    "name": "analytical array",
                                                    "data": {
                                                        "definition": "In this method, antibodies or peptides are spotted on a support to be hybridized to analytes in complex mixtures of proteins."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "lysate array",
                                                    "name": "lysate array",
                                                    "data": {
                                                        "definition": "This contains complex samples, such as cell or tissue lysates, spotted on an array surface and interrogated with antibodies."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "nucleic acid array",
                                            "name": "nucleic acid array",
                                            "data": {
                                                "definition": "Nucleic acids, either DNA or RNA are immobilized by spotting on a solid support such as a glass slide. They are hybridized with labeled nucleic acids from a sample and the signals measured, which reflects the composition of the sample nucleic acids. This is performed to quantify gene expression under various situations, e.g. diseases, development, etc."
                                            },
                                            "children": [
                                                {
                                                    "id": "gene array",
                                                    "name": "gene array",
                                                    "data": {
                                                        "definition": "Gene arrays include a collection of gene-specific nucleic acids that have been placed at defined locations on a solid support, either by spotting or direct synthesis. Labeled nucleic acids from a sample are hybridized with target genes on the array and the signals measured, which reflects the composition of the sample nucleic acids."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "oligonucleotide array",
                                                    "name": "oligonucleotide array",
                                                    "data": {
                                                        "definition": "Oligonucleotides are immobilized by spotting on a solid support, such as a glass slide. They are hybridized with labeled nucleic acids from a sample and the signals measured, which reflects the composition of the sample nucleic acids. This is performed to either quantify gene expression or genomic profiling to understand genetic variation in humans."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "small molecule array",
                                            "name": "small molecule array",
                                            "data": {
                                                "definition": "Small molecules which are either natural or synthetic are immobilized by robotic printing on a solid support, such as a functionalized glass slide. Protein targets either purified or from a lysate are hybridized to the immobilized small molecules in an attempt to identify the natural ligands for proteins, and also molecules that could regulate their function."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "microplate",
                                    "name": "microplate",
                                    "data": {
                                        "definition": "The assay is performed in a microplate, which can either be 96 well, 384 well, or 1536 well plates."
                                    },
                                    "children": [
                                        {
                                            "id": "24 well plate",
                                            "name": "24 well plate",
                                            "data": {
                                                "definition": "null "
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "1536 well plate",
                                            "name": "1536 well plate",
                                            "data": {
                                                "definition": "This microplate contains 1536 wells. It is highly miniaturized to accommodate more wells on one plate which reduces the plate handling time and allows working with very small volumes (few microliters) that reduces the cost of an assay."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "384 well plate",
                                            "name": "384 well plate",
                                            "data": {
                                                "definition": "This microplate contains 384 wells. It is suitable for a HTS assay."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "96 well plate",
                                            "name": "96 well plate",
                                            "data": {
                                                "definition": "This microplate contains 96 wells. It is suitable for a low throughput assay."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "assay measurement throughput quality",
                            "name": "assay measurement throughput quality",
                            "data": {
                                "definition": "Assay measurements throughput quality describes the quality of the measurements performed on each sample, such as single concentration, single repetition, concentration-response, multiple repetitions, etc."
                            },
                            "children": [
                                {
                                    "id": "single concentration multiple replicates",
                                    "name": "single concentration multiple replicates",
                                    "data": {
                                        "definition": "Multiple measurements (typically three) are preformed at a single concentration. This is often used as a mode of confirmatory screening following a primary screen."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "concentration-response single measurement",
                                    "name": "concentration-response single measurement",
                                    "data": {
                                        "definition": "Concentration-response (dose-response) screen in which each concentration is only measured once.  This is equivalent to quantitative high-throughput screening (qHTS)."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "single concentration single measurement",
                                    "name": "single concentration single measurement",
                                    "data": {
                                        "definition": "Regular high-throughput screening (primary screening) to identify hits."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "quantitative high throughput screening",
                                    "name": "quantitative high throughput screening",
                                    "data": {
                                        "definition": "Quantitative high throughput screening (qHTS) technology is employed at the NIH Chemical Genomics Center (NCGC). In this approach, biological systems are assayed against small molecule libraries containing ~100,000 compounds at 7-15 concentration points and concentration-response curves are then generated for every compound in the library."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "high throughput screening",
                                    "name": "high throughput screening",
                                    "data": {
                                        "definition": "High-throughput screening (HTS) is a method of scientific experiment which is used in drug discovery. It involves large scale screening of perturbagens against a biological target and requires robotics, liquid handling devices, sensitive detectors and data processing. The results of these experiments provide starting points for drug design or understanding the role of a specific biological process. In HTS, greater than 100,000 compounds are screened per assay."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "concentration response multiple replicates",
                                    "name": "concentration response multiple replicates",
                                    "data": {
                                        "definition": "Concentration-response assays in which each concentration is measured multiple times (typically 2 or 3) to get a high quality result. In a concentration-response assay each compound is serially diluted and tested over several concentrations. Generally, it is performed with those compounds which were found to be positive when tested at a single concentration. PubChem calls these assays confirmatory."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "pubchem spec",
                            "name": "pubchem spec",
                            "data": {
                                "definition": "null "
                            },
                            "children": [
                                {
                                    "id": "pubchem endpoint name",
                                    "name": "pubchem endpoint name",
                                    "data": {
                                        "definition": "PubChem endpoint name"
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "assay comment",
                                    "name": "assay comment",
                                    "data": {
                                        "definition": "This is the PubChem assay comment that can be assigned by depositors to a bioassay. "
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "assay depositor",
                                    "name": "assay depositor",
                                    "data": {
                                        "definition": "The organization or individual that deposited the assay to PubChem."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "assay protocol",
                                    "name": "assay protocol",
                                    "data": {
                                        "definition": "This includes the detailed methodology used to perform a particular bioassay and found on the PubChem website associated with each assay ID (AID). It includes the assay components and the order in which they were added, the incubation times, detection method and the kit used, etc."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "assay description",
                                    "name": "assay description",
                                    "data": {
                                        "definition": "This contains the background information and rationale for performing a bioassay and found on the PubChem website associated with each assay ID (AID). It also includes the assay design and some information on the related bioassays."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "assay title",
                                    "name": "assay title",
                                    "data": {
                                        "definition": "This contains the name of a bioassay and is found on the PubChem website associated with each assay ID (AID). It contains information about the target tested, the stage of the assay (primary or confirmatory), the assay format (biochemical or cell based), etc."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "summary comment",
                                    "name": "summary comment",
                                    "data": {
                                        "definition": "The comment deposited by the assay provider summarizes the screening campaign with respect to the identification of lead compounds and chemical probes."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "assay provider",
                                    "name": "assay provider",
                                    "data": {
                                        "definition": "It is the investigator who provided the assay to be run at the screening center."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "assay control",
                            "name": "assay control",
                            "data": {
                                "definition": "A compound that is routinely run in the same manner as the test compounds in every run of the assay. This term does not refer to the plate controls used to define the maximum and minimum responses, and they may or may not be a âliterature standardâ or âreferenceâ compound."
                            },
                            "children": [
                                {
                                    "id": "background control",
                                    "name": "background control",
                                    "data": {
                                        "definition": "Background control is the reading obtained from wells to which the sample or one of the reagents was not added. This is subtracted from all the experimental readings prior to further analysis."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "low control",
                                    "name": "low control",
                                    "data": {
                                        "definition": "The low control contains the substrate titration without enzyme or substrate and without inhibitor. The low controls should reflect the signal expected for no enzyme activity at each substrate concentration. Depending on the composition of the inhibitor stocks, DMSO might be needed in the control wells to assure consistency across all the experiments."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "high control",
                                    "name": "high control",
                                    "data": {
                                        "definition": "The high control contains the substrate titration without inhibitor to reflect the maximum enzyme activity at each substrate concentration."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "positive",
                                    "name": "positive",
                                    "data": {
                                        "definition": "A positive plate-based control is a chemical compound or reagent used in each plate of an assay to normalize the response of the test perturbagens (by plate). The positive control is known from previous experiments or is a previously established standard.  It is usually highly active resulting in a strong response of the intended effect.  In an inhibition assay the positive control would usually result in the complete inhibition, which is then used for normalization (i.e. set as 100%). In an activation assay it would be activator used to normalize activity. Using controls provides an external reference and reduces the number of false negatives and false positives. "
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "negative",
                                    "name": "negative",
                                    "data": {
                                        "definition": "The negative control is used to determine the base-line to compare the effect of the test perturbagen. Often the negative control is the solvent (e.g. DMSO) in which the perturbagen was dissolved."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "bioassay design spec",
                            "name": "bioassay design spec",
                            "data": {
                                "definition": "Design refers to the optimization guidelines used to minimize the time and cost of assay implementation, while providing reliable and robust assay performance."
                            },
                            "children": [
                                {
                                    "id": "incubation time",
                                    "name": "incubation time",
                                    "data": {
                                        "definition": "It is the interval of time between the addition of a perturbagen, substrate or cell modification and the measurement of change, as observed by a detection method in the bioassay."
                                    },
                                    "children": [
                                        {
                                            "id": "enzyme reaction",
                                            "name": "enzyme reaction",
                                            "data": {
                                                "definition": "It is the interval of time between the enzyme activity measurement in a kinetic assay"
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "coupled substrate incubation",
                                            "name": "coupled substrate incubation",
                                            "data": {
                                                "definition": "It is the interval of time between the addition of a coupled enzyme substrate and the reaction stopping procedure in a coupled enzyme assay."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "substrate incubation",
                                            "name": "substrate incubation",
                                            "data": {
                                                "definition": "It is the interval of time between the addition of an enzyme substrate and the measurement of change, as observed by a detection method in the bioassay."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "perturbagen incubation",
                                            "name": "perturbagen incubation",
                                            "data": {
                                                "definition": "It is the interval of time between the addition of a perturbagen and the measurement of change, as observed by a detection method in the bioassay."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "cell modification",
                                            "name": "cell modification",
                                            "data": {
                                                "definition": "It is the interval of time between the cell modification, namely transient transfection of plasmid DNA/ siRNA, viral transduction, etc and the measurement of change, as observed by a detection method in the bioassay."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "assay stage",
                            "name": "assay stage",
                            "data": {
                                "definition": "This describes to the order in which the assays are performed in the assay campaign. The primary (single concentration testing of perturbagens), secondary and confirmatory assays (concentration response) are run in the stated order. Exceptions include the qHTS assays performed at NCGC where the perturbagens are first tested in a concentration response."
                            },
                            "children": [
                                {
                                    "id": "lead optimization",
                                    "name": "lead optimization",
                                    "data": {
                                        "definition": "Assays performed in the lead optimization stage. They are typically concentration-response high quality assays. They can involve a number of different assays depending on the required profile of the desired lead compound."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "summary",
                                    "name": "summary",
                                    "data": {
                                        "definition": "PubChem-specific assay stage. A summary assay references and summarizes the assays in PubChem that correspond to a screening campaign or a probe development project."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "secondary",
                                    "name": "secondary",
                                    "data": {
                                        "definition": "Additional assays following the confirmatory stage to confirm the biological activity of chemical entities via a different type of assay or to eliminate certain active compounds based on their mechanism of action, toxicity or activity profile.  Secondary assays can also include selectivity and specificity assays."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "primary",
                                    "name": "primary",
                                    "data": {
                                        "definition": "The first assay performed in a in a screening campaign. The purpose of the primary assay is to identify hits, which are potentially biologically active chemical entities . Generally in this assay, the perturbagens are tested at a single concentration with only one measurement. It is followed by a confirmatory screen, which can be run at a single concentration with multiple replicates or as concentration-response assay."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "confirmatory",
                                    "name": "confirmatory",
                                    "data": {
                                        "definition": "Assay performed to confirm activity of compounds identified in the primary screen.  It can be performed as replicate measurements at a single screening concentration or as concentration-response assay."
                                    },
                                    "children": [
                                        
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "assay readout content",
                            "name": "assay readout content",
                            "data": {
                                "definition": "Throughput and information content generated. Categorizing multiplexed (i.e. mutiple targets measured simultaneously) and multiparametric assays and high content (image-based) and regular (plate reader) assays."
                            },
                            "children": [
                                {
                                    "id": "readout method",
                                    "name": "readout method",
                                    "data": {
                                        "definition": "null "
                                    },
                                    "children": [
                                        {
                                            "id": "regular screening",
                                            "name": "regular screening",
                                            "data": {
                                                "definition": "Typically uses a plate reader and generates one or two readouts, in contrast to high content screening, which can generate tens to hundreds of parameters."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "high content imaging",
                                            "name": "high content imaging",
                                            "data": {
                                                "definition": "High content screening (HCS) is an automated screening method using live cells to understand various aspects of their biology. HCS employs imaging (bright-field or fluorescence) and analyses in a high throughput format. HCS can be applied to monitor complex cellular aspects, namely, signaling, morphology, phenotype, proliferation, motility, intracellular trafficking, toxicology, RNAi knockdown, and receptor activation."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "content readout type",
                                    "name": "content readout type",
                                    "data": {
                                        "definition": "Categorizing multiplexed (i.e. multiple targets measured simultaneously), multiparametric and single readout assays."
                                    },
                                    "children": [
                                        {
                                            "id": "single readout",
                                            "name": "single readout",
                                            "data": {
                                                "definition": "Typically uses a plate reader and generates one or two readouts, in contrast to high content screening or multiplexed assays, which can generate tens to hundreds of parameters. The readout is typically from a single target / meta-target in each well."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "multiple readouts",
                                            "name": "multiple readouts",
                                            "data": {
                                                "definition": "null "
                                            },
                                            "children": [
                                                {
                                                    "id": "multiplexed readout",
                                                    "name": "multiplexed readout",
                                                    "data": {
                                                        "definition": "Tthe readout is from multiple targets from one well"
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "multiparametric readout",
                                                    "name": "multiparametric readout",
                                                    "data": {
                                                        "definition": "null "
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    "id": "bioassay component spec",
                    "name": "bioassay component spec",
                    "data": {
                        "definition": "A detailed description pertaining to an individual component of a bioassay."
                    },
                    "children": [
                        {
                            "id": "detection technology spec",
                            "name": "detection technology spec",
                            "data": {
                                "definition": "Recording the effect of a perturbagen on the meta target in a bioassay."
                            },
                            "children": [
                                {
                                    "id": "measured wavelength",
                                    "name": "measured wavelength",
                                    "data": {
                                        "definition": "For fluorescence measurements, it is the wavelength at which the fluorophore is excited and the wavelength at which it emits fluorescence. In the case of absorbance, it is the wavelength at which light is absorbed by a biological entity or a dye"
                                    },
                                    "children": [
                                        {
                                            "id": "absorbance wavelength",
                                            "name": "absorbance wavelength",
                                            "data": {
                                                "definition": "In absorbance measurements, it is the wavelength at which light is absorbed by a biological entity or a dye"
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "emission wavelength",
                                            "name": "emission wavelength",
                                            "data": {
                                                "definition": "For fluorescence measurements, it is the wavelength at which an excited fluorophore emits fluorescence."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "excitation wavelength",
                                            "name": "excitation wavelength",
                                            "data": {
                                                "definition": "For fluorescence measurements, it is the wavelength at which a fluorophore is excited"
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "molecular beacon",
                                    "name": "molecular beacon",
                                    "data": {
                                        "definition": "Molecular beacons are hairpin shaped oligonucleotide probes with an internally quenched fluorophore whose fluorescence is restored upon binding to a target nucleic acid. They are used to detect nucleic acids in solution."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "immunoassay quantitation",
                                    "name": "immunoassay quantitation",
                                    "data": {
                                        "definition": "A digital system used for quantitating proteins in gels, blots, etc."
                                    },
                                    "children": [
                                        {
                                            "id": "densitometric scan",
                                            "name": "densitometric scan",
                                            "data": {
                                                "definition": "Following a western analysis, the individual proteins in a gel, blot, or autoradiogram are quantitated by a densitometric scan over a limited range of protein concentration per band (1-10 ug). A scanner measures the optical density of the material by directing a beam of light and measuring its transmission or reflection."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "detection instrumentation",
                                    "name": "detection instrumentation",
                                    "data": {
                                        "definition": "Lists the type of equipment used for detection in an assay, e.g. FLIPR, ViewLux plate reader, PHERAstar, etc."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "immunofluorescence",
                                    "name": "immunofluorescence",
                                    "data": {
                                        "definition": "A mode of fluorescence microscopy in which a molecular target is labeled with a specific fluorescent antibody. Antibodies are labeled either by coupling directly with a fluorophore (termed direct immunofluorescence), or with a second fluorescent antibody that recognizes the primary antibody (indirect immunofluorescence)."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "detection kit information",
                                    "name": "detection kit information",
                                    "data": {
                                        "definition": "Information about the kit/component used in the assay, including the name of the manufacturer."
                                    },
                                    "children": [
                                        {
                                            "id": "enzyme used in kit",
                                            "name": "enzyme used in kit",
                                            "data": {
                                                "definition": "The details of enzyme used in the kit"
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "kit name",
                                            "name": "kit name",
                                            "data": {
                                                "definition": "The name of the kit and its manufacturer"
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "meta target spec",
                            "name": "meta target spec",
                            "data": {
                                "definition": "It is a description of the biological entity that is the presumed subject of the bioassay. This could refer to a molecule whose activity is being regulated or a biological process such as neurite outgrowth."
                            },
                            "children": [
                                {
                                    "id": "binding reporter spec",
                                    "name": "binding reporter spec",
                                    "data": {
                                        "definition": "null "
                                    },
                                    "children": [
                                        {
                                            "id": "interaction",
                                            "name": "interaction",
                                            "data": {
                                                "definition": "It refers to the interactions between two molecules, e.g. protein-protein, protein-DNA, protein-RNA, DNA-RNA, etc."
                                            },
                                            "children": [
                                                {
                                                    "id": "protein-rna",
                                                    "name": "protein-rna",
                                                    "data": {
                                                        "definition": "Protein-RNA interaction"
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "protein-protein",
                                                    "name": "protein-protein",
                                                    "data": {
                                                        "definition": "Protein-protein interaction"
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "protein-dna",
                                                    "name": "protein-dna",
                                                    "data": {
                                                        "definition": "Protein-DNA interaction"
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "dna-rna",
                                                    "name": "dna-rna",
                                                    "data": {
                                                        "definition": "DNA-RNA interaction"
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "protein spec",
                                    "name": "protein spec",
                                    "data": {
                                        "definition": "It is a description of the protein used in the assay, e.g. if the protein is wild type or mutated, its source and purification methods, etc."
                                    },
                                    "children": [
                                        {
                                            "id": "protein preparation method",
                                            "name": "protein preparation method",
                                            "data": {
                                                "definition": "This describes the method by which a protein is purified from its source."
                                            },
                                            "children": [
                                                {
                                                    "id": "recombinant expression",
                                                    "name": "recombinant expression",
                                                    "data": {
                                                        "definition": "This involves cloning the cDNA of the gene of interest in a plasmid or viral vector and introducing it into a bacterial, insect or mammalian cell for expression. The expressed protein is then purified after lysing the cells followed by a series of centrifugation and, or chromatography and electrophoresis methods."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "purification from natural source",
                                                    "name": "purification from natural source",
                                                    "data": {
                                                        "definition": "This involves the disruption of the tissue or cells either mechanically or chemically followed by a series of centrifugation and, or chromatography and electrophoresis methods."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "chemical synthesis",
                                                    "name": "chemical synthesis",
                                                    "data": {
                                                        "definition": "Peptides are synthesized chemically by linking amino acids step-by-step by peptide bonds. It is possible to synthesize peptides of 40 amino acids in length.. This synthesis called stepwise elongation starts at the C-terminal end of the peptide and ends at the N-terminus. The amino-termini of the amino acids are protected , which is typically an acid-sensitive tert-butoxycarbonyl (Boc) group or a base-sensitive 9-fluorenylmethyloxycarbonyl (Fmoc) group.  Longer peptides are synthsized by fragment condensation or chemical ligation methods."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "protein purity",
                                            "name": "protein purity",
                                            "data": {
                                                "definition": "The steps involved in purifying a protein from either its natural or recombinantly expressed sources. It involves lysis by either mechanical or chemical means, followed by a series of centrifugation and, or chromatography and electrophoresis methods."
                                            },
                                            "children": [
                                                {
                                                    "id": "partially purified protein",
                                                    "name": "partially purified protein",
                                                    "data": {
                                                        "definition": "The protein is only partially purified"
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "purified protein",
                                                    "name": "purified protein",
                                                    "data": {
                                                        "definition": "The protein is fully purified"
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "unpurified sample",
                                                    "name": "unpurified sample",
                                                    "data": {
                                                        "definition": "Crude protein preparation"
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "protein modification",
                                            "name": "protein modification",
                                            "data": {
                                                "definition": "It is a description about the type of changes that were introduced into a protein coding sequence prior to being used in the bioassay, e.g. introducing mutations, deletions, or fusion to another protein coding sequence, etc."
                                            },
                                            "children": [
                                                {
                                                    "id": "truncated protein",
                                                    "name": "truncated protein",
                                                    "data": {
                                                        "definition": "Protein truncation refers to a protein missing a portion of the protein towards the carboxyl-terminal  either due to a nonsense mutation or expressing  only a region of the protein by genetic engineering. A truncated protein is generally non-functional."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "mutated protein",
                                                    "name": "mutated protein",
                                                    "data": {
                                                        "definition": "Mutations are introduced into the cDNAs which encode the proteins of interest and introducing the cDNA constructs into cells for expression. Different types of mutations include missense, nonsense, deletions, insertions, etc."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "fused protein",
                                                    "name": "fused protein",
                                                    "data": {
                                                        "definition": "Fusion proteins or chimeric proteins are created through the joining of two or more cDNAs which encode different proteins. Upon expression, a single polypeptide containing each of the proteins results. Generally, the protein of interest is tagged with a reporter protein, e.g. luciferase, GFP, etc, whose expression could easily be monitored."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "wild type protein",
                                                    "name": "wild type protein",
                                                    "data": {
                                                        "definition": "Wild type is unmodified protein."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "virus",
                                    "name": "virus",
                                    "data": {
                                        "definition": "Viruses are small infectious agents that depend on the host cells to reproduce. Outside the host, they contain a protein coat called capsid that encloses their genetic material (either DNA or RNA). Viruses can infect bacteria, plants and animals and cause a number of diseases, e.g. smallpox, chickenpox, influenza, polio, rabies, etc."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "cell spec",
                                    "name": "cell spec",
                                    "data": {
                                        "definition": "This describes the cell culture conditions and modifications performed on the cell line. Modifications include plasmid transfection, viral transduction, cell fusion, etc."
                                    },
                                    "children": [
                                        {
                                            "id": "transfection spec",
                                            "name": "transfection spec",
                                            "data": {
                                                "definition": "This is a detailed description of the introduction of nucleic acids into cells using a plasmid construct. Plasmid constructs are created by the insertion of a gene or siRNA encoding DNA of interest into a plasmid. The constructs are transfected into cells by various methods."
                                            },
                                            "children": [
                                                {
                                                    "id": "transfection agent",
                                                    "name": "transfection agent",
                                                    "data": {
                                                        "definition": "The agent used to transfect cells, e.g., lipofectamine, fugene, etc."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "construct spec",
                                                    "name": "construct spec",
                                                    "data": {
                                                        "definition": "This is a description of the plasmid constructs that are created by the insertion of a gene or siRNA encoding DNA of interest into a plasmid. It includes information on the type of plasmid, the type of promoter, the selectable markers, etc, which are all considered when sub-cloning a gene of interest. The constructs are transfected into cells by various methods."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "cell modification",
                                            "name": "cell modification",
                                            "data": {
                                                "definition": "This describes the modifications performed on the cell line. Modifications include plasmid transfection, viral transduction, cell fusion, etc."
                                            },
                                            "children": [
                                                {
                                                    "id": "bacterial infection",
                                                    "name": "bacterial infection",
                                                    "data": {
                                                        "definition": "Live cell assay in which cells were infected with a bacterium."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "genetic",
                                                    "name": "genetic",
                                                    "data": {
                                                        "definition": "This refers to the introduction of cloned genes into cells by various methods, including transfection, transduction, electroporation, nucleofection, etc. It is generally performed to express the exogenously introduced gene and study its effect on the cell. In addition, DNA encoding regulatory moieties, such as siRNA is similarly introduced to inhibit the expression of a specific gene(s)."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "transfection",
                                                            "name": "transfection",
                                                            "data": {
                                                                "definition": "This refers to the introduction of nucleic acids into cells using a plasmid construct. Plasmids constructs are created by the insertion of a gene or siRNA encoding DNA of interest into a plasmid. The engineered plasmids are introduced into cells by various means, namely, transfection, electroporation, nucleofection, etc which regulates the efficiency and location of gene transfer, for e.g., nucleofection delivers the plasmid directly into the nucleus in contrast to the other methods that deliver into the cytosol."
                                                            },
                                                            "children": [
                                                                {
                                                                    "id": "transient transfection",
                                                                    "name": "transient transfection",
                                                                    "data": {
                                                                        "definition": "It is the temporary introduction of a gene or siRNA encoding DNA of interest into a cell using various plasmids. Usually, the transiently transfected DNA remains in the cell for 2-3 days and is functional: expresses the gene of interest. There are numerous transient transfection reagents and protocols available that have been optimized for various cell lines."
                                                                    },
                                                                    "children": [
                                                                        
                                                                    ]
                                                                },
                                                                {
                                                                    "id": "stable transfection",
                                                                    "name": "stable transfection",
                                                                    "data": {
                                                                        "definition": "In this method, the introduced plasmid construct is stably integrated in the cellular genome, and is hence a permanent modification of the cells. This is achieved by selecting for cells which express a plasmid-encoded selectable marker, which is commonly an antibiotic resistance protein."
                                                                    },
                                                                    "children": [
                                                                        
                                                                    ]
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            "id": "viral transduction",
                                                            "name": "viral transduction",
                                                            "data": {
                                                                "definition": "This refers to the infection of cells with either a pathogenic or a modified virus, which introduces viral genome into the cells. Pathogenic viruses include the influenza virus, measles virus, Japanese encephalitis virus, etc. For recombinant expression of proteins in cells or tissues, several types of viruses are utilized, including lentivirus, retrovirus, adenovirus, etc, with each having their own advantages and disadvantages."
                                                            },
                                                            "children": [
                                                                {
                                                                    "id": "pathogenic virus",
                                                                    "name": "pathogenic virus",
                                                                    "data": {
                                                                        "definition": "This refers to the infection of cells with a disease-causing virus, which could effect humans, animals and plants. Pathogenic viruses include the influenza virus, measles virus, Japanese encephalitis virus, etc."
                                                                    },
                                                                    "children": [
                                                                        
                                                                    ]
                                                                },
                                                                {
                                                                    "id": "recombinant virus for gene expression",
                                                                    "name": "recombinant virus for gene expression",
                                                                    "data": {
                                                                        "definition": "This refers to the infection of cells with a modified virus, which introduces viral genome containing the foreign genes. Several types of viruses are utilized for recombinant gene expression, including lentivirus, retrovirus, adenovirus, etc, with each having their own advantages and disadvantages."
                                                                    },
                                                                    "children": [
                                                                        
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "id": "fusion",
                                                    "name": "fusion",
                                                    "data": {
                                                        "definition": "Cell fusion is the formation of a hybrid cell from two different cells of same or different species. After such fusion, one of the parental cells has a dominant phenotype over the other, and the resulting hybrids will have the features of only one of the parental cells. Fusion is performed to understand the dominance of genetic material, DNA recombination events, etc."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "fusion hybrids",
                                                            "name": "fusion hybrids",
                                                            "data": {
                                                                "definition": "Cell fusion is the formation of a hybrid cell from two different cells of same or different species. Fusion hybrids are created by fusing two nucleated cells obtained from same or different species."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "fusion cybrids",
                                                            "name": "fusion cybrids",
                                                            "data": {
                                                                "definition": "Cell fusion is the formation of a hybrid cell from two different cells of same or different species. Fusion cybrids are created by fusing a nucleated and an enucleated (nucleus removed) cell. The resulting cybrids have one nucleus, but they contain the mitochondria from both the parental cells. This allows one to study the mitochondrial inheritance and to ascertain the role of mitochondrial DNA mutations in diseases."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "culture condition",
                                            "name": "culture condition",
                                            "data": {
                                                "definition": "This describes the specific medium in which a cell line is cultured, which is optimized for its growth. It includes the medium additives namely, serum, growth factors, buffers, amino acids, antibiotics, etc. This information can be obtained from ATCC or found in relevant publications."
                                            },
                                            "children": [
                                                {
                                                    "id": "assay serum",
                                                    "name": "assay serum",
                                                    "data": {
                                                        "definition": "null "
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "assay medium",
                                                    "name": "assay medium",
                                                    "data": {
                                                        "definition": "null "
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "culture serum",
                                                    "name": "culture serum",
                                                    "data": {
                                                        "definition": "Cultured cells require serum or growth factors for growth by cell division. Each cell type is grown in a medium supplemented with a variable concentration of serum (up to 20%) which is optimized for its growth. Specialized sera include dextran charcoal treated serum, which lacks certain hormones, growth factors, etc, dialyzed serum, which lacks low molecular weight molecules (below 10,000 MW), such as glucose, amino acids, low molecular weight hormones, cytokines, etc. These sera are used in certain assays to avoid interference from the normal serum components. Most commonly, fetal bovine serum is used in cell culture, but other sera such as horse serum are also used."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "culture medium",
                                                    "name": "culture medium",
                                                    "data": {
                                                        "definition": "This describes the medium used to grow cells, which is optimized for each cell type and includes additives such as growth factors, buffers, amino acids, antibiotics, etc. This information can be obtained from ATCC or found in relevant publications."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "bioassay component design spec",
                            "name": "bioassay component design spec",
                            "data": {
                                "definition": "A description of the underlying method and assay design used to determine the action of the perturbagen in the assay system."
                            },
                            "children": [
                                {
                                    "id": "measured entity",
                                    "name": "measured entity",
                                    "data": {
                                        "definition": "It is a molecular entity, which is the output of a biological reaction or process that is measured either directly (by the presence of a tag or probe) or indirectly in a coupled reaction."
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "signal direction",
                                    "name": "signal direction",
                                    "data": {
                                        "definition": "This refers to the measured readout signal increase or decrease in perturbagen treated wells when compared to the untreated or carrier-treated wells in an assay."
                                    },
                                    "children": [
                                        {
                                            "id": "signal decrease",
                                            "name": "signal decrease",
                                            "data": {
                                                "definition": "Increase in perturbation (reported by the endpoint, e.g. inhibition, activation) is correlated to decrease in signal."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "signal increase",
                                            "name": "signal increase",
                                            "data": {
                                                "definition": "Increase in perturbation (reported by the endpoint, e.g. inhibition, activation) is correlated to increase in signal."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "antibody",
                                    "name": "antibody",
                                    "data": {
                                        "definition": "Antibody is a protein (immunoglobulin) produced by the immune system of an organism in response to exposure to a foreign molecule (antigen) and characterized by its specific binding to a site of that molecule (antigenic determinant or epitope). Antibodies are used extensively as tools to characterize proteins. Antibodies are either monoclonal or polyclonal."
                                    },
                                    "children": [
                                        {
                                            "id": "polyclonal",
                                            "name": "polyclonal",
                                            "data": {
                                                "definition": "They are produced by immunizing a mammal such as mouse, rabbit or goat. They are a mixture of antibodies produced from different B cells raised against any of the different epitopes of the antigen."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "monoclonal",
                                            "name": "monoclonal",
                                            "data": {
                                                "definition": "They are highly specific antibodies produced by the clones of a single hybrid cell formed in the laboratory by the fusion of a B cell with a tumor cell."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "enzyme activity spec",
                                    "name": "enzyme activity spec",
                                    "data": {
                                        "definition": "It describes the different methods by which an enzyme activity can be measured and the ways it is expressed. "
                                    },
                                    "children": [
                                        {
                                            "id": "substrate depletion measurement",
                                            "name": "substrate depletion measurement",
                                            "data": {
                                                "definition": "In this assay, the enzyme activity is assessed by measuring the reduction of substrate. The substrate is measured by detecting the fluorophore tagged to it, or by its absorbance, etc"
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "product formation measurement",
                                            "name": "product formation measurement",
                                            "data": {
                                                "definition": "In this assay, the enzyme activity is assessed by measuring the reaction product. The product is measured by detecting the fluorophore tagged to it, or by its absorbance, etc"
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "perturbagen spec",
                            "name": "perturbagen spec",
                            "data": {
                                "definition": "A detailed description of the agents used to alter the activity of the target in a bioassay."
                            },
                            "children": [
                                {
                                    "id": "perturbagen delivery",
                                    "name": "perturbagen delivery",
                                    "data": {
                                        "definition": "This describes whether the perturbagens are tested individually or as mixtures / pools"
                                    },
                                    "children": [
                                        {
                                            "id": "mixture of perturbagens",
                                            "name": "mixture of perturbagens",
                                            "data": {
                                                "definition": "Mixtures / pools of perturbagens are tested in an assay"
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "single perturbagen",
                                            "name": "single perturbagen",
                                            "data": {
                                                "definition": "The perturbagens are tested individually in an assay"
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "method of perturbagen introduction",
                                    "name": "method of perturbagen introduction",
                                    "data": {
                                        "definition": "The reagent used to introduce siRNA into the cell, e.g. fugene, lipofectamine, etc"
                                    },
                                    "children": [
                                        
                                    ]
                                },
                                {
                                    "id": "perturbagen source",
                                    "name": "perturbagen source",
                                    "data": {
                                        "definition": "It refers to the origin of perturbagen, whether it is purified from a natural source or it is a synthetic compound, etc."
                                    },
                                    "children": [
                                        {
                                            "id": "unpurified synthetic compound",
                                            "name": "unpurified synthetic compound",
                                            "data": {
                                                "definition": "It refers to the unpurified compound, which is chemically synthesized."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "raw natural product extract compound",
                                            "name": "raw natural product extract compound",
                                            "data": {
                                                "definition": "It refers to the unpurified natural compound, which is extracted from either a plant or an animal source."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "purified synthetic compound",
                                            "name": "purified synthetic compound",
                                            "data": {
                                                "definition": "It refers to the purification of a perturbagen after it is chemically synthesized. Purification is a rate limiting step in the discovery of novel compounds. Several purification procedures exist, including, crystallization, sublimation, distillation, chromatography, differential extraction, etc."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "purified natural compound",
                                            "name": "purified natural compound",
                                            "data": {
                                                "definition": "It refers to a perturbagen that is purified from a natural source, namely, from a plant or animal., e.g. quinine from the cinchona plant bark, conotoxin from cone snail, etc."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "format spec",
                            "name": "format spec",
                            "data": {
                                "definition": "It is a detailed description of the biological and chemical features common to each test condition in the assay, e.g. whether an assay is homogeneous,  whether a protein is modified, and what reagents were added to the assay."
                            },
                            "children": [
                                {
                                    "id": "assay phase characteristic",
                                    "name": "assay phase characteristic",
                                    "data": {
                                        "definition": "It specifies whether all the assay components are in solution or some are in solid phase, which determines their ability to scatter light."
                                    },
                                    "children": [
                                        {
                                            "id": "heterogeneous assay",
                                            "name": "heterogeneous assay",
                                            "data": {
                                                "definition": "One or more assay components are present in solid phase at time of detection. (e.g.: cells, beads)."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "homogeneous assay",
                                            "name": "homogeneous assay",
                                            "data": {
                                                "definition": "All assay components exist in solution phase at the time of detection (e.g. none of the components are in beads or cells). Technically no component scatters light."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "reagent",
                                    "name": "reagent",
                                    "data": {
                                        "definition": "It is a substance that is used because of its biological or chemical  activity. It refers to the substrate, potentiator, detergent, redox agent, etc added to the assay, which is required for the completion of the assay."
                                    },
                                    "children": [
                                        {
                                            "id": "substrate",
                                            "name": "substrate",
                                            "data": {
                                                "definition": "The substance on which the enzyme acts to generate a product, e.g. ATP is the substrate for ATPase, which hydrolyzes it to ADP and Pi."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "redox reagent",
                                            "name": "redox reagent",
                                            "data": {
                                                "definition": "Redox reagents can either accept or donate electrons to facilitate the oxidation-reduction reactions. E.g. dithiothreitol (reducing agent), hydrogen peroxide (oxidizing agent) "
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "potentiator",
                                            "name": "potentiator",
                                            "data": {
                                                "definition": "It is a known activator of a target and is used to activate, prior to screening the compounds for selective inhibitors. E.g. It is necessary to raise the concentration of cAMP in the assay using forskolin prior to screening for activators of Gi subunit of GPCR, which is known to inhibit adenylate cyclase activity and lower the cAMP levels."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "inhibitor reagent",
                                            "name": "inhibitor reagent",
                                            "data": {
                                                "definition": "This is a known inhibitor of a target and is used to lower the activity, prior to screening the compounds for selective activators of that target."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "dmso",
                                            "name": "dmso",
                                            "data": {
                                                "definition": "Dimethyl sulfoxide (DMSO) is a common organic solvent that is used to solubilize the chemical compounds prior to their addition to an assay. DMSO can have a significant impact on enzyme activity and the concentration of DMSO in the wells containing compound should be identical to the concentration of DMSO in the control wells. DMSO can also impact the solubility of a compound and its observed potency. Therefore, the concentration of DMSO should be consistent in replicate experiments."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "detergent",
                                            "name": "detergent",
                                            "data": {
                                                "definition": "Detergents are primarily surfactants, which lower the surface tension of water. In biological assays, detergents are used to lyse the cells and tissues by solubilizing the membrane lipids, and to unfold the proteins by disrupting the bonds."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "coupled substrate",
                                            "name": "coupled substrate",
                                            "data": {
                                                "definition": "The rate of an enzyme is dependent on the presence of a substrate, whose concentration is regulated by the activity of a different enzyme. E.g., ATP: the concentration of ATP is dependent on a kinase, with ATP being its substrate, which in turn determines the rate of a coupled enzyme luciferase, since ATP is one of its substrates too."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "endpoint spec",
                            "name": "endpoint spec",
                            "data": {
                                "definition": "A description of the concept representing a measurement or parameter quantifying or qualifying a perturbation. An endpoint consists of a series of data points, one for each perturbing agent employed by the assay."
                            },
                            "children": [
                                {
                                    "id": "endpoint analysis",
                                    "name": "endpoint analysis",
                                    "data": {
                                        "definition": "Endpoint analysis"
                                    },
                                    "children": [
                                        {
                                            "id": "endpoint qualifier",
                                            "name": "endpoint qualifier",
                                            "data": {
                                                "definition": "Qualifies the endpoint as either the exact value (=) or less than (<) or greater than (>) the endpoint value specified. In case of less than or greater than, the endpoint cutoff is often determined manually; for example in a concentration response experiment the tested compound can be inactive at the minimum tested concentration; or a compound can still be active even at the lowest tested concentration."
                                            },
                                            "children": [
                                                
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "data manipulation spec",
                                    "name": "data manipulation spec",
                                    "data": {
                                        "definition": "The readings obtained from an assay are normalized and corrected using different parameters prior to arriving at the final endpoints, which could then be compared between samples."
                                    },
                                    "children": [
                                        {
                                            "id": "concentration endpoint",
                                            "name": "concentration endpoint",
                                            "data": {
                                                "definition": "It is the endpoint obtained in a dose response assay, where the perturbagens are tested at several concentrations obtained by serial dilution. The raw data from the assay is analyzed and concentration response (CR) curves are generated, which involves plate normalization for background readings using DMSO blanks and curve fitting using the Hill equation."
                                            },
                                            "children": [
                                                {
                                                    "id": "curve fit spec",
                                                    "name": "curve fit spec",
                                                    "data": {
                                                        "definition": "Has curve fit parameters, curve fit method, properties like Hill coefficient, concentration range, and measurement repetition. It involves capturing the trend in the data by assigning a single function across the entire range. The goal of curve fitting is to find the parameter values that most closely match the data."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "input data",
                                                            "name": "input data",
                                                            "data": {
                                                                "definition": "The readout obtained from a HTS assay which includes measurements from the positive and negative control wells in each plate. This is the data that needs to be processed."
                                                            },
                                                            "children": [
                                                                {
                                                                    "id": "raw data",
                                                                    "name": "raw data",
                                                                    "data": {
                                                                        "definition": "The readout obtained from a HTS assay which includes measurements from the positive and negative control wells in each plate. This is the data that needs to be processed."
                                                                    },
                                                                    "children": [
                                                                        
                                                                    ]
                                                                },
                                                                {
                                                                    "id": "normalized data",
                                                                    "name": "normalized data",
                                                                    "data": {
                                                                        "definition": "Data can be normalized by Z and Z' factors, signal to background, signal to noise, and coefficient of variance. Plate based normalization is usually based on controls. In the absence of controls, the Z-score can be used to normalize the data by plate."
                                                                    },
                                                                    "children": [
                                                                        
                                                                    ]
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            "id": "curve fit method",
                                                            "name": "curve fit method",
                                                            "data": {
                                                                "definition": "Curve fit methods include Levenberg-Marquardt, Simplex, Linear Regression (linear least-squares), etc. The Levenberg-Marquardt method is a robust algorithm and has become the standard for non-linear regression."
                                                            },
                                                            "children": [
                                                                {
                                                                    "id": "Hill equation",
                                                                    "name": "Hill equation",
                                                                    "data": {
                                                                        "definition": "The Hill equation is commonly used to estimate the number of ligand molecules that are required to bind to a receptor to produce a functional effect. The Hill coefficient can be considered as an interaction coefficient, reflecting the extent of cooperativity among multiple ligand binding sites."
                                                                    },
                                                                    "children": [
                                                                        
                                                                    ]
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            "id": "num replicates",
                                                            "name": "num replicates",
                                                            "data": {
                                                                "definition": "The number of replicate measurements (repetitions) per concentration value used in the curve fit procedure."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "num concentration values",
                                                            "name": "num concentration values",
                                                            "data": {
                                                                "definition": "The number of concentrations at which the perturbagen is screened to obtain the curve fit endpoint."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "num parameters",
                                                            "name": "num parameters",
                                                            "data": {
                                                                "definition": "The number of parameters used for curve fitting that most closely match the data."
                                                            },
                                                            "children": [
                                                                {
                                                                    "id": "3 parameter",
                                                                    "name": "3 parameter",
                                                                    "data": {
                                                                        "definition": "Three parameters were used for curve fitting that most closely match the data."
                                                                    },
                                                                    "children": [
                                                                        
                                                                    ]
                                                                },
                                                                {
                                                                    "id": "4 parameter",
                                                                    "name": "4 parameter",
                                                                    "data": {
                                                                        "definition": "Four parameters were used for curve fitting that most closely match the data."
                                                                    },
                                                                    "children": [
                                                                        
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "response endpoint",
                                            "name": "response endpoint",
                                            "data": {
                                                "definition": "The endpoint obtained after testing the perturbagens at a single concentration. Two kinds of inference errors can occur at this screening step: false positives and false negatives. To correct for this, the raw data is normalized using different statistical correction methods, namely, normalized percent distribution, Z-score, B-score, etc to derive at these endpoints."
                                            },
                                            "children": [
                                                {
                                                    "id": "endpoint correction",
                                                    "name": "endpoint correction",
                                                    "data": {
                                                        "definition": "This refers to the correction performed on the raw data values from an assay. B-score correction is a common method used for this purpose."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "b-score",
                                                            "name": "b-score",
                                                            "data": {
                                                                "definition": "The B score is an analog of the Z score, but is more resistant to the presence of outliers and to differences in the measurement error distributions of the compounds. It is calculated by first computing a two-way median to account for row and column effects of the plate,  which is then divided by their median absolute deviation to standardize for plate-to-plate variability."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "id": "endpoint normalization",
                                                    "name": "endpoint normalization",
                                                    "data": {
                                                        "definition": "Two kinds of inference errors can occur after screening at single perturbagen concentration: false positives and false negatives. To correct for this, the raw data is normalized using different statistical correction methods, namely, normalized percent distribution, Z-score, B-score, etc to derive at these endpoints."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "z-score",
                                                            "name": "z-score",
                                                            "data": {
                                                                "definition": "Random and systematic error can induce either underestimation (false negatives) or overestimation (false positives) of measured signals. Z score indicates how many standard deviations an observation is above or below the mean. For calculating this, the average of the plate values are subtracted from  individual raw values and dividing the difference by the standard deviation estimated from all measurements of the plate"
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "percent control",
                                                            "name": "percent control",
                                                            "data": {
                                                                "definition": "It is a measure of the compound activity. It is calculated by dividing the raw measurement obtained with the compound by the mean of the measurements with the positive controls multiplied by 100 in an antagonist assay."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "b-score",
                                                            "name": "b-score",
                                                            "data": {
                                                                "definition": "The B score is an analog of the Z score, but is more resistant to the presence of outliers and to differences in the measurement error distributions of the compounds. It is calculated by first computing a two-way median to account for row and column effects of the plate,  which is then divided by their median absolute deviation to standardize for plate-to-plate variability."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        },
                                                        {
                                                            "id": "normalized percent inhibition",
                                                            "name": "normalized percent inhibition",
                                                            "data": {
                                                                "definition": "It is another normalization method. It is obtained by dividing the difference of mean of the positive controls and sample measurement by the difference of the mean of the positive controls and mean of the negative controls."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "endpoint unit",
                                    "name": "endpoint unit",
                                    "data": {
                                        "definition": "It is the definite magnitude of a physical quantity or of time. It has a quantity and a unit associated with it."
                                    },
                                    "children": [
                                        {
                                            "id": "quantity",
                                            "name": "quantity",
                                            "data": {
                                                "definition": "It is an indefinite amount or number. An endpoint unit has a quantity and a unit associated with it."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "time unit",
                                            "name": "time unit",
                                            "data": {
                                                "definition": "Time is used to quantify the durations of events and the intervals between them. Units of time include hour, minute, second, etc."
                                            },
                                            "children": [
                                                
                                            ]
                                        },
                                        {
                                            "id": "physical unit",
                                            "name": "physical unit",
                                            "data": {
                                                "definition": "It is the magnitude of a physical quantity, e.g. a unit of length is meter."
                                            },
                                            "children": [
                                                {
                                                    "id": "concentration unit",
                                                    "name": "concentration unit",
                                                    "data": {
                                                        "definition": "It is the magnitude of the amount of a substance (perturbagen, biological molecule, etc) present in a defined volume, e.g. micro molar."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "id": "endpoint mode of action",
                                    "name": "endpoint mode of action",
                                    "data": {
                                        "definition": "This refers to the effect of the perturbagen on the target of an assay, whether it brings about inhibition, stimulation, cytotoxicity, etc. "
                                    },
                                    "children": [
                                        {
                                            "id": "binding",
                                            "name": "binding",
                                            "data": {
                                                "definition": "Concepts describing the type of perturbagen interaction with the receptor."
                                            },
                                            "children": [
                                                {
                                                    "id": "saturation binding",
                                                    "name": "saturation binding",
                                                    "data": {
                                                        "definition": "These experiments measure specific radioligand binding at equilibrium at various concentrations of the radioligand."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "allosteric",
                                                    "name": "allosteric",
                                                    "data": {
                                                        "definition": "Small molecule that can bind to sites other than the receptor active site and affect activity of the receptor."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "non competitive",
                                                    "name": "non competitive",
                                                    "data": {
                                                        "definition": "Non-competitive ligands interact with receptors at sites distinct from the competitive ligand binding pocket, conferring multiple modes of action."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "competitive",
                                                    "name": "competitive",
                                                    "data": {
                                                        "definition": "An assay based on the competition between a labeled and an unlabeled ligand in the reaction with a receptor binding agent (e.g. antibody, receptor, transport protein)."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "competing with cold ligand",
                                                            "name": "competing with cold ligand",
                                                            "data": {
                                                                "definition": "null "
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "id": "reversible",
                                                    "name": "reversible",
                                                    "data": {
                                                        "definition": "In this assay, the interaction of the ligand with a receptor can be dissociated by changing the binding conditions, e.g. buffer, salt concentration, etc."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "irreversible",
                                                    "name": "irreversible",
                                                    "data": {
                                                        "definition": "In this assay, the binding of the ligand to a receptor is permanent and cannot be dissociated by changing the binding conditions such as buffer, salt concentration, etc."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "ligand function",
                                            "name": "ligand function",
                                            "data": {
                                                "definition": "Data measuring the biological effect of a compound, e.g. % cell death, activation of reporter gene expression, inhibition of enzyme activity, etc."
                                            },
                                            "children": [
                                                {
                                                    "id": "stimulation",
                                                    "name": "stimulation",
                                                    "data": {
                                                        "definition": "Increase of a predefined stimulus. Unit of Measure is always % when normalized to the dynamic range of the assay."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "inverse agonist",
                                                    "name": "inverse agonist",
                                                    "data": {
                                                        "definition": "Inverse agonism is the binding of a perturbagen to the same receptor binding-site as an agonist for that receptor and reversing the constitutive activity of receptors. Inverse agonists exert the opposite pharmacological effect of a receptor agonist."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "inhibition",
                                                    "name": "inhibition",
                                                    "data": {
                                                        "definition": "Reduction of a predefined stimulus. Unit of Measure is always % when normalized to the dynamic range of the assay."
                                                    },
                                                    "children": [
                                                        {
                                                            "id": "growth inhibition",
                                                            "name": "growth inhibition",
                                                            "data": {
                                                                "definition": "Growth reduction of a predefined stimulus. Unit of Measure is always % when normalized to the dynamic range of the assay.  As a measure of viable or proliferating cells, different researchers measure different parameters, namely, protein content (by sulforhodamine B staining followed by absorbance measurement), mitochondrial dehydrogenase activity (by 3-(4,5-dimethylthiazol-2-yl)-2,5-diphenyl tetrasodium bromide, known as MTT staining followed by absorbance measurement), expression of proliferation associated antigens (by immunostaining for Ki-67), and ATP content (by using CellTiter-Glo reagent (Promega) followed by luminescence measurement."
                                                            },
                                                            "children": [
                                                                
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "id": "cytotoxicity",
                                                    "name": "cytotoxicity",
                                                    "data": {
                                                        "definition": "It is the toxic or cell-death inducing property of a perturbagen. Known cytotoxic agents mediate cell death commonly by necrosis, apoptosis, or autophagy. Cytotoxicity can be detected by measuring various aspects of the death process: e.g. membrane permeability, ATP concentration, DNA fragmentation, etc."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "antagonist",
                                                    "name": "antagonist",
                                                    "data": {
                                                        "definition": "Antagonism refers to the blocking the action of an agonist. A receptor antagonist does not provoke a biological response itself upon binding to a receptor, but blocks agonist-mediated responses."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "agonist",
                                                    "name": "agonist",
                                                    "data": {
                                                        "definition": "The binding of a drug to a receptor of a cell and triggering a response by the cell. An agonist often mimics the action of a naturally occurring substance."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "activation",
                                                    "name": "activation",
                                                    "data": {
                                                        "definition": "Increase of a predefined stimulus. Unit of Measure is always % when normalized to the dynamic range of the assay."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "id": "enzyme activity modulation",
                                            "name": "enzyme activity modulation",
                                            "data": {
                                                "definition": "Concepts describing the type of perturbagen interaction with the enzyme."
                                            },
                                            "children": [
                                                {
                                                    "id": "time dependent inhibition",
                                                    "name": "time dependent inhibition",
                                                    "data": {
                                                        "definition": "Time-dependent inhibitors bind slowly to the enzyme on the time scale of enzymatic turnover, and thus display a change in initial velocity with time. This has the effect of slowing the observed onset of inhibition."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "tight binding inhibition",
                                                    "name": "tight binding inhibition",
                                                    "data": {
                                                        "definition": "In this type of inhibition, the population of free, soluble inhibitor is significantly depleted by the formation of the enzyme-inhibitor or enzyme-substrate-inhibitor complex. While tight-binding inhibitors can bind to the target enzyme in a competitive, noncompetitive, or uncompetitive manner with respect to substrate binding, they can display noncompetitive phenotypes."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "allosteric modulation",
                                                    "name": "allosteric modulation",
                                                    "data": {
                                                        "definition": "An allosteric effector operates to enhance active site substrate affinity and/or catalysis. An allosteric inhibitor decreases activity by binding to an allosteric site, other than or in addition to the active site on the target. This interaction is characterized by a conformational change in the target enzyme that is required for inhibition."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "partial inhibition",
                                                    "name": "partial inhibition",
                                                    "data": {
                                                        "definition": "Partial inhibition results from the formation of an enzyme-substrate-inhibitor complex that can generate product with less efficiency than the enzyme-substrate complex."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "noncompetitive inhibition",
                                                    "name": "noncompetitive inhibition",
                                                    "data": {
                                                        "definition": "A noncompetitive inhibitor binds equally well to both free enzyme and the enzyme-substrate complex. These binding events occur exclusively at a site distinct from the precise active site occupied by substrate. It will lower the apparent Vmax value, yet there is no effect on the apparent KM value for its substrate. Essentially, the KI of the inhibitor does not change as a function of the substrate concentration."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "uncompetitive inhibition",
                                                    "name": "uncompetitive inhibition",
                                                    "data": {
                                                        "definition": "An uncompetitive inhibitor binds exclusively to the enzyme-substrate complex yielding an inactive enzyme-substrate-inhibitor complex. When encountered, the apparent Vmax value and the apparent KM value should both decrease."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                },
                                                {
                                                    "id": "competitive inhibition",
                                                    "name": "competitive inhibition",
                                                    "data": {
                                                        "definition": "A competitive inhibitor binds only to free enzyme. Often this binding event occurs on the active site of the target, precisely where substrate also binds. It will raise the apparent KM value for its substrate with no change in the apparent Vmax value. As a result, competitive inhibition can be overcome, observed by an increase in the apparent KI value, at higher concentrations of substrate."
                                                    },
                                                    "children": [
                                                        
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
	{
		"id": "bioassay",
       		"name": "bioassay",
            	"data": {
                "definition": "An experiment carried out for the purpose of screening a perturbing agent in a biological system, measuring one or multiple effect(s) of the agent facilitated by an assay design / assay technology to translate the perturbation into a detectable signal to arrive at one or multiple endpoint(s) that quantify or qualify the extent of the perturbation.  Bioassay is described by multiple bioassay components: format, perturbagen, meta target, technology, detection method, and endpoint. Bioassay includes one or multiple measure groups to describe multiparametric (or multiplexed) assays (assays that measure more than one effect of the perturbagen on the system that is screened)."
            	},
            	"children": [ ]
	},
	{
		"id": "measure group",
            	"name": "measure group",
            	"data": {
                "definition": "Measure group is an abstract concept to describe  multiparametric (or multiplexed) assays (assays that measure more than one effect of the perturbagen on the system that is screened). See definition of bioassay. Measure groups links to the bioassays components that can vary among different measures of a multiparametric assay: meta target, technology, detection method and endpoint."
        	},
            	"children": [ ]
	},
	{
		"id": "screening campaign",
            	"name": "screening campaign",
            	"data": {
                "definition": "Screening campaign refers to the goal of identifying a compound (or a series of compounds) with a specific function and it is desirable to have a known mechanism of action. It includes primary (screening at single concentration), secondary, and confirmatory (dose response) assays, structure activity relationship (SAR) studies, lead optimization, etc."
            	},
            	"children": [ ]
	}
    ]
}

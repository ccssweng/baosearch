/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */

/**
* Handle the response from get-concepts and make a query for all sub-concepts of the selected concept
* 
* @param (HTTPResponse) response
*/
function loadQuerySubsumption(response)
{
	var query = "";
	for(var i = 0; i < response.data.length; i++)
	{
		query += (response.data[i].name).toUpperCase();
		if(i < (response.data.length -1))
			query += ", ";
	}
	echoQuery(query);
}

/**
* Write the subsumed concepts as an extension of the query to a form field
* 
* @param (string) query
*/
function echoQuery(query)
{
	$("#searchstring").append(query)
}

/**
* Fetch the data for the charts depending on the charting mode (compound or assay or concepts)
* 
*/
function getChartData()
{
	if(mode == "assayWs")	
		$.getJSON("ws-chart?ws=0&"+querystring, drawCharts);                        
	else if(mode == "cpdWs")
						$.getJSON("ws-chart?ws=1&"+querystring, drawCharts);
	else
		$.getJSON("chart-feeder?"+querystring, drawCharts);
}	

/**
* Handle the response from any of the servlets called by getChartData() and render the charts using Google Viz API
* 
* @param (HTTPResponse) response
*/
function drawCharts(response)
{
	var epCharts = response.data;
	if(epCharts.length == 0)
	{
		$("#status").html("Your query did not return any results. Please <a href = \"#\" onClick = \"history.back();\">adjust the parameters</a> and try again.");
		return;				
	}
	aids = response.aids;
	for(var i = 0; i < epCharts.length; i++)
	{		

		if(chartMode == "endpoint")
		{
			$("#epHeader"+i).html(epList[epCharts[i].EndpointName].epName +" within ("+epMin[i]+","+epMax[i]+")");
								$("#selectedEp"+i).val(epCharts[i].EndpointName);
			$('#toolbox').html("<a href = \"download-filter?aids="+aids + "&"+ querystring+ "\" class = 'tool download'>Download Results</a>");
		}
		if(epCharts[i].Pies.length > 0)
		{
							for(var j = 0; j < 4; j++)
							{
				var data;
									data = new google.visualization.DataTable();
									data.addColumn('string', 'Concept');
									data.addColumn('number', 'Endpoint Value Count');			

									data.addRows(epCharts[i].Pies[j].Slices.length);
			
				for(var k = 0; k < epCharts[i].Pies[j].Slices.length; k++)
				{
					if(epCharts[i].Pies[j].PieName == "design")
						optionList[0][i] += "<option value = '"+epCharts[i].Pies[j].Slices[k].SliceId+"'>"+epCharts[i].Pies[j].Slices[k].SliceName+" ("+epCharts[i].Pies[j].Slices[k].Count+")</option>";
					if(epCharts[i].Pies[j].PieName == "detection technology")
						optionList[1][i] += "<option value = '"+epCharts[i].Pies[j].Slices[k].SliceId+"'>"+epCharts[i].Pies[j].Slices[k].SliceName+" ("+epCharts[i].Pies[j].Slices[k].Count+")</option>";
					if(epCharts[i].Pies[j].PieName == "format")
						optionList[2][i] += "<option value = '"+epCharts[i].Pies[j].Slices[k].SliceId+"'>"+epCharts[i].Pies[j].Slices[k].SliceName+" ("+epCharts[i].Pies[j].Slices[k].Count+")</option>";
					if(epCharts[i].Pies[j].PieName == "meta target")
						optionList[3][i] += "<option value = '"+epCharts[i].Pies[j].Slices[k].SliceId+"'>"+epCharts[i].Pies[j].Slices[k].SliceName+" ("+epCharts[i].Pies[j].Slices[k].Count+")</option>";

					data.setValue(k, 0, epCharts[i].Pies[j].Slices[k].SliceName);
					data.setValue(k, 1, epCharts[i].Pies[j].Slices[k].Count);
				}

									var chart = new google.visualization.PieChart(document.getElementById('chart'+i+j));
									chart.draw(data, {width: 220, height: 300, title: (epCharts[i].Pies[j].PieName).toUpperCase(), chartArea:{left:0,top:15,width:"100%",height:"50%"}});
			}
		}
		else
			$('#optionHolder'+i).html("Your query did not return any results for this endpoint. Please click the back button on your browser to adjust the parameters and try again.");
	}
	$("#status").html("");
	$("#message").html("");
	$("#dynamic").show();
}

/**
* Populate the dropdowns with the concepts reutrned for all the slices based on the selected concept type
* 
* @param (string) type
* @param (number) pieId
* @param (number) epIndex
*/
function populateSlices(type, pieId, epIndex)
{
	if(type.value != "default")
		$("#"+pieId).html(optionList[type.value][epIndex]);
}

/**
* Populate and load the hidden summary form to load filter summary if the endpoint option is selected else fetch all the assays for the given concept
* 
* @param (number) epIndex
* @param (number) chartMode
*/
function loadSummary(epIndex, chartMode)
{
	var sliceName = $("#selectedSlice"+epIndex+" option:selected").text();
	if(chartMode == "endpoint")
	{
		if(mode == "suggest")
			$("#concepts").val(concepts);
		if(mode == "cpdWs")
			$("#sids").val(sids);
		$("#aids").val(aids);
		$("#sliceId").val($("#selectedSlice"+epIndex).val());
		$("#selectedEp").val($("#selectedEp"+epIndex).val());
		$("#sliceName").val(sliceName.substring(0,sliceName.indexOf('(')));
		$("#epMin").val(epMin[epIndex]);				
		$("#epMax").val(epMax[epIndex]);				
		$("#mode").val(mode);				

		$("#summaryForm").submit();								
	}
	else
	{
		//$.getJSON("thing-to-assay",{thingId: thingId, sliceId: $("#selectedSlice"+epIndex).val()}, loadAssays);
		$.getJSON("thing-to-assay?"+querystring+"&aids="+aids+"&thingId="+thingId+"&sliceId="+$("#selectedSlice"+epIndex).val(), loadAssays);
	}
}

/**
* Handle the response from thing-to-assay and make a query for all assays of the selected concept by concatenating them. Change the location of the current page to show a list of all these assays with their endpoints
* 
* @param (HTTPResponse) response
*/
function loadAssays(response)
{
	location.href = "concept-assays?assayList="+response.assays.join();
}
/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */
 
/**
 * Function to request for the endpoints for the current selection after a delay of one second.
 * This is to avoid race conditions when a concept is being deleted from the suggest box and 
 * it requires a refetch of endpoints
 *
 */
function fetchEndpointsWithDelay()
{
	setTimeout( "fetchEndpoints()", 1000);
}

/**
 * Function to request for the endpoints for the currently selected concept, triggered after a new
 * concept is added to the list from the suggest box
 * 
 */
function fetchEndpoints()
{
	if ($("input[name='chartMode']:checked").val() == 'assay') 
		return;
	epArray = [];
	$("#epListContParent").hide();
	if($("#concepts").val())
	{
		$("#epListContParent").show();
		$("#epListContainer").html("<img src = '/baosearch/images/loader.gif'/>");			
		$.getJSON("get-eps", $("#filterForm").serialize() ,loadEndpoints);
	}
}

/**
 * Function to receive the HTTP request response for endpoint data and store it in a global variable
 *
 * @param (HTTPResponse) The object containing the endpoints list 
 */
function getEndpointList(response)
{
	epData = response;
}

/**
 * Function to receive the HTTP request response for endpoint data, generate a select list from it
 * and display it in a table. Also makes the filter button visible
 *
 * @param (HTTPResponse) The object containing the endpoints list and details
 */
function loadEndpoints(response)
{
	if(response.eptids.length == 0)
	{
		$("#epListContainer").html("No endpoints matched the current concept(s)");
		return;
	}

	var epOptionSet = "<select style = \"float: left;\" id = \"epList\" onChange = \"addEndpoint();\"><option value = \"default\">-- Select Endpoint --</option>";

	for(var i = 0; i < response.eptids.length; i++)
		epOptionSet += "<option value = \""+response.eptids[i]+"\">"+epData[response.eptids[i]].epName+"</option>";
	epOptionSet += "</select><br>";
	epOptionSet += "<br><span class = \"message\" id = \"eptMsg\"></span><div id = \"epTable\"></div>";
	
	$("#epListContainer").html(epOptionSet);
	$("#filterBtn").show();
}

/**
 * Render the endpoint table with fields for input whenever a change occurs to the 
 * number of endpoints selected for filtering
 *
 */
function renderEpTable()
{
	var epMin, epMax;
	var epTabStr = "<table id = \"epTable\"><tr id = \"epHeader\" style =\"display: none;\"><td style = \"padding: 5px;\">Endpoint<sub>min</sub></td>"
			+"<td style = \"padding: 5px;\">Endpoint Name</td><td style = \"padding: 5px;\">Endpoint<sub>max</sub></td></tr>";			

	for(var i = 0; i < epArray.length; i++)
	{
		epMin = epData[epArray[i]].epMin;
		epMax = epData[epArray[i]].epMax;
		epTabStr += "<tr><td><input type = \"hidden\" name = \"ep\" value = \""+epArray[i]+"\">"
			+ "<a href = \"#\" class =\"specialTip infoTip\" title = \"The minimum value in the database for this endpoint is "+epMin+"\"></a><input type =\"text\" name = \"epMin\" style = \"width: 95px;\"></td><td style = \"width: 180px; text-align: center;\"> &lt; "
			+ epData[epArray[i]].epName +" &lt; </td><td><input type =\"text\" name = \"epMax\" style = \"width: 95px;\"><a href = \"#\" class =\"specialTip infoTip\" title = \"The maximum value in the database for this endpoint is "+epMax+"\"></a>"
			+ "<a href = \"#\" onClick = \"removeEndpoint("+epArray[i]+");\"title = \"Remove Endpoint\" class = \"removeTip\"></a></td></tr>";
	}
	
	epTabStr += "</table>";
	$("#epTable").html(epTabStr);

	$('a.specialTip').tipsy({gravity: 's'})
}

/**
 * Function to add an endpoint to the current list if it doesn't already exist by incrementing the counter, 
 * adding to the array and showing a message about the operation
 * 
 */
function addEndpoint()
{	
	var intEp = parseInt($("#epList").val());
	if($.inArray(intEp,epArray) == -1 && $("#epList").val() != "default")
	{
		epArray.push(intEp);
		epCount++;
		renderEpTable();
	}
	else
	{
		$('#eptMsg').show();
		setTimeout("$('#eptMsg').hide()", 5000);
		$('#eptMsg').removeClass().addClass('errorHighlight');
		$('#eptMsg').html("This endpoint already exists in the current selection");

	}				
}

/**
 * Function to remove an endpoint from the current list by decrementing the counter,
 * removing from the array and showing a message about the operation
 * 
 */
function removeEndpoint(epId)
{
	if(epCount > 0)
	{
		epArray.splice(epArray.indexOf(epId), 1);
		epCount--;
		renderEpTable();
	}
}
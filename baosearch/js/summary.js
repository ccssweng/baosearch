/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */

/**
 * Global variables 
 */ 
var pagesize = 25;
var pagenum = 1;
var totalResults = 0;
var sort = "max";

/**
 * Get the endpoints list and show a loading graphic when the page is ready
 */
$(document).ready(function(){                
	$.getJSON("data/epList.js", getEpList);
	showLoader();
});

/**
 * Clear the message slot and show the loading graphic 
 */
function showLoader()
{
	$("#dynamic").html("");
	$("#message").html("<img src = '/baosearch/images/loader2.gif'>");
}

/**
 * Callback that receives the response containing the endpoints list, stores the sorting order and then sends the request to process chart selections for summary
 *
 * @param (HTTPResponse) response
 */
function getEpList(response) 
{ 
	epList = response; 
	sort = epList[selectedEp].sort;
	$("#selEpName").html(epList[selectedEp].epName);
	processQuery();
}

/**
 * Prepare and post the query from pie chart selections along with paging preferences to chart consumer
 *
 */
function processQuery()
{
	pagesize = parseInt($("#pagesize").val());
	if(parseInt($("#jumpPage").val()) <=  Math.ceil(totalResults/pagesize))
		pagenum =  parseInt($("#jumpPage").val());
	$.post("chart-consumer", {ep: selectedEp, sliceId: sliceId, epMax: epMax, epMin: epMin, pagesize: pagesize, pagenum: pagenum, sort: sort, concepts: conceptArray, mode: mode, aids: aids, sids: sids} , processResponse, "json");
	$("#jumpPage").val("");
}

/**
 * Callback that receives the response containing the summary data for the selected endpoint and constraints. It also prints the selected endpoint, 
 * selected slice and constructs the download servlet parameter list. It also renders the data in a table along with paging and structure display controls.
 *
 * @param (HTTPResponse) response
 */
function processResponse(response) 
{	
	$('#toolbox').html("");
	$("#selConcept").html(" for concept '"+sliceName+ "'");

	$("#selEpRange").html(" within ("+response.selectedEpMin+","+response.selectedEpMax+")");
	
	dlQueryStr += "&ep=" + selectedEp + "&epMin=" + response.selectedEpMin + "&epMax=" + response.selectedEpMax
	
	var data = response.data;

	if(data.length == 0)
	{
		$('#dynamic').html("No results were returned. Please try again");
		return;
	}

	totalResults = parseInt(response.sidCount);
	var aDataSet = "[ ";
	for(var i = 0; i < data.length; i++)
	{
		aDataSet += '["<input type = \'checkbox\' name = \'cpdWorksetList\' onClick = \'highlightRow(this);\' value = \''+data[i].sid+'\'>",';
		aDataSet += '"<a href = \'zoom-structure?sid='+data[i].sid+'\' class = \'zoom\' title = \'Click to Zoom\' id = \''+data[i].sid+'\'><img class = \'bgLoad\' src=\'/chemfile?type=sid&id='+data[i].sid+'&format=jpeg:w100,h100\'/></a>",'
					+'"<a href = \'#\' onClick = \'loadDetails('+data[i].sid+');\'><img src = \'/baosearch/images/fast-forward.png\'/></a>",'
					+'"'+data[i].sid+'",'
					+'"'+data[i].range+'","'+data[i].count+'"'
					+']';

		if(i != (data.length - 1))
				aDataSet += ",";
	}
	aDataSet += " ]";
	var colSet;

	colSet = "[ { \"sTitle\": \"<input type='checkbox' value = '0' name='selectAllB' id='selectAllB' onClick='jqCheckAll(this.name)'>\"}, "
			+"{ \"sTitle\": \"Structure\" }, "
			+"{ \"sTitle\": \"Details\" }, "
			+"{ \"sTitle\": \"Substance ID\" }, "
			+"{ \"sTitle\": \""+epList[selectedEp].epName+" <a href = '#' class = 'colHead' title = 'The range of values returned for conditions matching  the current query for each substance'>&nbsp;</a>\" },"
			+"{ \"sTitle\": \"Endpoint Values Count <a href = '#' class = 'colHead' title = 'The number of endpoint values returned for the conditions matching the current query for each substance'></a>\" }" 
			+"]";

	$('#dynamic').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>' );
	oTable = $('#example').dataTable( {
			//"bSort": false,
			"aaData": eval(aDataSet),
			"aoColumns": eval(colSet),
			"sDom": '<"top"<"clear">>rt<"bottom"<"clear">>',
			"iDisplayLength": pagesize,
			"aLengthMenu": [[25, 50, -1], [25, 50, "All"]],
			"bAutoWidth": false
			} );

	if(toggleStructs)
		fnShowHide(1);
	
	var pagination = "";			

	pagination = "Showing page "+ pagenum + " of " + Math.ceil(totalResults/pagesize) + " ";

	if(pagenum == 1)
	{
		if(pagenum < (Math.ceil(totalResults/pagesize)))
				pagination += "<a href  = \"#\" title = 'Previous Page' class = 'back-disabled'></a><a href = \"#\" onClick = 'loadResults(\"plus\");' title = 'Next Page' class = 'forward-enabled'></a>";
		else
			pagination += "<a href  = \"#\" title = 'Previous Page' class = 'back-disabled'></a><a href = \"#\" title= 'Next Page' class = 'forward-disabled'></a>";
	}
	else 
	{
		if(pagenum < (Math.ceil(totalResults/pagesize)))1
			pagination += "<a href  = \"#\" onClick = 'loadResults(\"minus\");' title = 'Previous Page' class = 'back-enabled'></a><a href = \"#\"  onClick = 'loadResults(\"plus\");' title= 'Next Page' class = 'forward-enabled'></a>";
		else
			pagination += "<a href  = \"#\" onClick = 'loadResults(\"minus\");' title = 'Previous Page' class = 'back-enabled'></a><a href = \"#\" title = 'Next Page' class = 'forward-disabled'></a>";
	}

	$('#pagingTop').html(pagination);
	$('#pagingBottom').html(pagination);

	$('a.zoom').click(popOut);
	$('a.colHead').tipsy({gravity: 's'});

	$("#message").html("");

	$('#toolbox').append('<a href = "#" class = "tool addWs" onClick = "$(\'#wsMode\').val(\'add\');return processWorkset(\'compound\',\'cpdWorksetForm\');">Add To Workset</a>');
	$('#toolbox').append("<a href = \"download-filter?" +dlQueryStr+ "\" class = 'tool download'>Download Results</a><a href = \"#\" class = 'tool toggle' onClick = \"toggleStructures()\">Toggle Structures View</a>");
}

/**
 * Write all the neccessary data from the summary request to a hidden form and then submit it to load details for the selected substance
 *
 * @param (Number) sid - the Substance ID of the selected substance
 */
function loadDetails(sid)
{
	$("#sliceId").val(sliceId);
	$("#selectedEp").val(selectedEp);
	$("#sliceName").val(sliceName);
	$("#epMin").val(epMin);				
	$("#epMax").val(epMax);				
	$("#sid").val(sid);
	$("#concepts").val(conceptArray);
	$("#mode").val(mode);
	$("#aids").val(aids);
	if(mode == "cpdWs")
		$("#sids").val(sids);

	$("#detailsForm").submit();		
}

/**
 * Increase or decrease current page number and send request to fetch data accordingly
 *
 * @param (String) The operation - plus or minus - for pagination
 */
function loadResults(oper)
{
	if(oper == "plus")
		pagenum++;
	else if(pagenum > 1)
		pagenum--;
	showLoader();
	processQuery();
}

/**
 * Toggle the visibility of the column containing substance structures on or off based on user input 
 *
 */
function toggleStructures()
{
	if(toggleStructs == false)
		toggleStructs = true;
	else
		toggleStructs = false;
	fnShowHide(1);
}

/**
 * Toggle the visibility of any column on or off 
 * 
 * @param (Number) iCol - the index of the column to be toggled
 */
function fnShowHide( iCol )
{
	var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
	oTable.fnSetColumnVis( iCol, bVis ? false : true );
}
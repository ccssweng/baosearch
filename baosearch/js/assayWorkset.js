/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */
 
/**
 * Function to receive the HTTP request response for endpoint data and store it in a global variable
 *
 * @param (HTTPResponse) The object containing the endpoints list and details
 */
function getEndpointList(response)
{
	epData = response;
}

 /**
 * Fetch endpoints from the database for the selected AIDs using AJAX, show a loader graphic
 *
 */
function fetchEndpoints()
{
	epArray = [];
	var aids = "";
			$("input[@name='assayWorksetList[]']:checked").each(function() {aids += $(this).val() + ",";});
			aids = aids.substring(0,(aids.length-1));

	$("#epListContainer").html("<img src = '/baosearch/images/loader.gif'/>");			
	$.getJSON("get-eps?aids="+aids, loadEndpoints);
}

 /**
 * Callback to receive the data requested by fetchEndpoints() and display it in a form
 *
 * @param (HTTPResponse) The object containing the list of endpoints and details for the selected AIDs
 * @return null if empty
 */
function loadEndpoints(response)
{
			if(response.eptids.length == 0)
			{
				  $("#epListContainer").html("No endpoints matched the current concept(s)");
				  return;
			}
	var epOptionSet = "<select style = \"float: left;\" id = \"epList\" onChange = \"addEndpoint();\"><option value = \"default\">-- Select Endpoint --</option>";
	for(var i = 0; i < response.eptids.length; i++)
		epOptionSet += "<option value = \""+response.eptids[i]+"\">"+epData[response.eptids[i]].epName+"</option>";
		epOptionSet += "</select><br>";
	epOptionSet += "<br><span class = \"message\" id = \"eptMsg\"></span><div id = \"epTable\"></div>";
	
	$("#epListContainer").html(epOptionSet);
	$("#filterBtn").show();
}

 /**
 * Renders each selected endpoint in a table containing a min and max values and input field
 *
 */
function renderEpTable()
{
	var epMin, epMax;
	var epTabStr = "<table id = \"epTable\"><tr id = \"epHeader\" style =\"display: none;\"><td style = \"padding: 5px;\">Endpoint<sub>min</sub></td>"
			+"<td style = \"padding: 5px;\">Endpoint Name</td><td style = \"padding: 5px;\">Endpoint<sub>max</sub></td></tr>";			
	for(var i = 0; i < epArray.length; i++)
	{
			epMin = epData[epArray[i]].epMin;
					epMax = epData[epArray[i]].epMax;
		epTabStr += "<tr><td><input type = \"hidden\" name = \"ep\" value = \""+epArray[i]+"\">"
			+ "<a href = \"#\" class =\"specialTip infoTip\" title = \"The minimum value in the database for this endpoint is "+epMin+"\"></a><input type =\"text\" name = \"epMin\" style = \"width: 95px;\"></td><td style = \"width: 180px; text-align: center;\"> &lt; "
			+ epData[epArray[i]].epName +" &lt; </td><td><input type =\"text\" name = \"epMax\" style = \"width: 95px;\"><a href = \"#\" class =\"specialTip infoTip\" title = \"The maximum value in the database for this endpoint is "+epMax+"\"></a>"
			+ "<a href = \"#\" onClick = \"removeEndpoint("+epArray[i]+");\"title = \"Remove Endpoint\" class = \"removeTip\"></a></td></tr>";
	}
	
	epTabStr += "</table>";
	$("#epTable").html(epTabStr);
	$('a.specialTip').tipsy({gravity: 's'})
}

/**
 * Check if the selected endpoint exists in the list before adding it and increasing the endpoint count
 *
 */
function addEndpoint()
{	
	var intEp = parseInt($("#epList").val());
	if($.inArray(intEp,epArray) == -1 && $("#epList").val() != "default")
	{
		epArray.push(intEp);
		epCount++;
		renderEpTable();
	}
	else
	{
		$('#eptMsg').show();
		setTimeout("$('#eptMsg').hide()", 5000);
		$('#eptMsg').removeClass().addClass('errorHighlight');
		$('#eptMsg').html("This endpoint already exists in the current selection");
	}				
}

/**
 * Remove the selected endpoint from the list and decrement the endpoint count
 *
 * @param (number) epId
 */
function removeEndpoint(epId)
{
	if(epCount > 0)
	{
		epArray.splice(epArray.indexOf(epId), 1);
		epCount--;
		renderEpTable();
	}
}


/**
 * Call endpoint-names to fetch all the assays from workset with their associated endpoints
 *
 */
function processQuery()
{
	if(assayWorkset != "ERROR" && assayWorkset != "RESET")
		$.ajax( {url: "endpoint-names", data: {input: assayWorkset}, success: processResponse, error: handleError, dataType: "json", type: "GET" } );
	else if(assayWorkset == "RESET")
	{
		$('#tabs').hide();
		$('#worksetResponse').html("Workset Cleared Successfully");
		$('#worksetResponse').removeClass().addClass('successHighlight');
	}
	else
	{
		$('#tabs').hide();
		$('#worksetResponse').html("The workset is currently empty. Please select and add assays to the workset.");
		$('#worksetResponse').removeClass().addClass('errorHighlight');
		$('#message').html("");
	}
		
}

/**
 * Prints an error message if something goes wrong while communicating with the server e.g. a broken pipe. 
 *
 */
function handleError()
{
	$("#dynamic").html("<span class = 'errorHighlight' style = 'height: 30px;'>An error occurred while connecting to the server. Please try again after some time.</span>");
}

/**
 * Receives the response to endpoint-names and saves it in a global variable. Calls the function to display the results
 *
 * @param (HTTPResponse) responseString
 */
function processResponse(responseString)
{
response = responseString;
data = response.assays;
showList();
}

/**
 * Renders the list of assays and their associated endpoints as a table. Adds in controls to the individual columns and to the global toolbox
 *
 */
function showList()
{
	var check = 0;
	var aDataSet = "[";
	for(var i = 0; i < data.length; i++)
	{
		aDataSet += "['<input type = \"checkbox\" name = \"assayWorksetList\" onClick = \"highlightRow(this);\" value = \""+data[i].AssayId+"\">"+
		"','<a href = \"/vivo/bao-individual?uri="+$.URLEncode("http://www.bioassayontology.org/bao/BAO_0000015_")+data[i].AssayId+"\" class = \"detailsPopout fancyTip\" title = \"View Details on BAO\" id = \""+data[i].AssayId+"\"><img src = \"/baosearch/images/bao-icon.png\"/></a>"+ 
		" <a href = \"http://pubchem.ncbi.nlm.nih.gov/assay/assay.cgi?aid="+data[i].AssayId+"\" target=\"_blank\" class = \"fancyTip\" title = \"View Details on PubChem\"><img src = \"/baosearch/images/ncbi-icon.png\"/></a>',"+
		"'"+data[i].AssayId+"','"+data[i].AssayName.replace(/'/gi, "`")+"',";

		for(var j = 0; j < response.globalEpNameSet.length; j++)
		{
			check = 0;
			for(var k = 0; k < response.globalEpNameSet.length; k++)
			{
				if(response.globalEpNameSet[j] == data[i].epNameSet[k])
				{
					aDataSet += "'<span style = \"display: none;\">Yes</span><img src = \"/baosearch/images/check.png\"/>'";
					check = 1;
				}
			}
			if(check == 0)
					aDataSet += "'<span style = \"display: none;\">No</span><img src = \"/baosearch/images/cross.png\"/>'";
			if(j != (response.globalEpNameSet.length - 1))
				aDataSet += ",";
		}
		aDataSet += " ]";
		if(i != (data.length - 1))
			aDataSet += ",";
	}
	aDataSet += " ]";
	var colSet;

	colSet = "[ { \"sTitle\": \"<input type='checkbox' value = '0' name='selectAllB' id='selectAllB' onClick='jqCheckAll(this.name)'>\"}, {\"sTitle\": \"Details\"},";
	colSet += "{ \"sTitle\": \"Assay ID\" },{ \"sTitle\": \"Assay Name\" },";
	
	for(var i = 0; i < response.globalEpNameSet.length; i++)
	{
		colSet += "{ \"sTitle\": \""+response.globalEpNameSet[i]+"\"}";
		if(i != (response.globalEpNameSet.length - 1))
									colSet += ",";
		//endpointOptionSet += "<option value = \""+response.globalEpNameSet[i]+"\">"+response.globalEpNameSet[i]+"</option>";
	}
	colSet += "]";

	$('#dynamic').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="assayData"></table>' );
	oTable = $('#assayData').dataTable( {
			//"bJQueryUI": true,
		"aaData": eval(aDataSet),
			"aoColumns": eval(colSet),
			"sDom": '<"top"if<"clear">>rt<"bottom"if<"clear">>',
			"iDisplayLength": -1,
		"bAutoWidth": false,
			"aLengthMenu": [[25, 50, -1], [25, 50, "All"]]
	} );

	$('#example_next').click(function() {
			$("#selectAllB").attr('checked', false);;
	});
	$('#example_previous').click(function() {
			$("#selectAllB").attr('checked', false);;
			jqCheckAll( 'selectAllB', 'assayWorksetList' );
	});


	var wsString = '<a href = "#" class = "tool deleteWs" onClick = "$(\'#mode\').val(\'delete\');return processWorkset(\'assay\', \'assayWorksetForm\');">Delete from Workset</a><a href = "#" class = "tool clearWs"  onClick = "$(\'#mode\').val(\'clear\');return processWorkset(\'assay\', \'assayWorksetForm\');">Clear Workset</a><a class = "tool filterOpts" href = "#" onClick = "showFilterPanel();">Filter Options</a>';

	$('#toolbox').html(wsString);
	$("a.fancyTip").tipsy({gravity: 's'});
	$('a.detailsPopout').click(popOut);	
	$('#message').html("");
}			


/**
 * Shows or hides the filter panel depending on user selection
 *
 * @return (boolean) false if validateCheckboxes fails, true otherwise
 */	
function showFilterPanel()
{
	if(validateCheckboxes("assayWorksetList"))
	{
		fetchEndpoints();
		$("#filterPanel").toggle();
	}
	else
		return false;
}


 /**
 * Checks if the filter options are valid and then builds the list of aids from the checkboxes
 *
 * @return (boolean) false if no filter validation failed, true otherwise
 */	
function filterEndpoints()
{
	if(!validateFilter())
		return false;
	var epList = "";
	var epOps = "";
	var epVals = "";
	var aids = "";

	$("input[@name='assayWorksetList[]']:checked").each(function() {aids += $(this).val() + ",";});
	
	aids = aids.substring(0,(aids.length-1));		
	$("#aids").val(aids);
	return true;
}

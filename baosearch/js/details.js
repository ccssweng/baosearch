/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */
 
 
 /**
 * Function to receive the HTTP request response for endpoint data and store it in a global variable and display the selected endpoint name
 *
 * @param (HTTPResponse) The object containing the endpoints list and details
 */
function getEpList(response) 
{ 
	epList = response;
	$("#selEpName").html(epList[selectedEp].epName);
	//if(epList[selectedEp-1].epUnit != "(null)")
	//        $("#selEpName").html(epList[selectedEp-1].epName+" (" + epList[selectedEp-1].epUnit + ")");
}

/**
 * Send three asynchronous requests to different servlets to get data for selected substance details
 *
 * 
 */
function processQuery()
{
	$.getJSON("compound-by-id?input="+<%=request.getParameter("sid")%>, processDetails);
	$.post("substance-details", {sid: sid, sliceId: sliceId, selectedEp: selectedEp, ep: selectedEp, epMax: epMax, epMin: epMin, concepts: concepts, mode: mode, sids: sids, aids: aids}, processResponse, "json");
	$.post("details-plus", {sid: sid, sliceId: sliceId, selectedEp: selectedEp, ep: selectedEp, epMax: epMax, epMin: epMin, concepts: concepts, mode: mode, sids: sids, aids: aids}, processResponsePlus, "json");
}

/**
 * Function to receive the HTTP request response for substance data and display it, also register the pop up window
 *
 * @param (HTTPResponse) The object containing the endpoints list and details
 */
function processDetails(response)
{
	$("#subDetails").html("<h3>"+response.data[0].SubstanceId+"<br>"+response.data[0].Name+"<br>"+response.data[0].Formula+"<br><a href = 'fetch-assays?input="+response.data[0].SubstanceId+ "' class = 'detailsPopout' title = 'View Details'>View All Assays</a><input type = 'checkbox' name = 'compoundWorksetList' value = '<%=request.getParameter("sid")%>' checked = 'true' style = 'display: none;'></h3>");
	$('a.detailsPopout').click(popOut);	
}

/**
 * Function to receive the HTTP request response for endpoint filter query and display it in a table. Also add the required buttons to the toolbox
 *
 * @param (HTTPResponse) The object containing the endpoints list and details
 */
function processResponse(response) 
{	
	var data = response.data;
	$('#toolbox').html("");
	$("#selConcept").html(" for concept '"+sliceName+ "'");

	$("#selEpRange").html(" within ("+response.selectedEpMin+","+response.selectedEpMax+")");

	var conc, qual;

	if(data.length == 0)
	{
		$('#dynamic').html("No results were returned. Please try again");
		return;
	}

	var aDataSet = "[ ";
	for(var i = 0; i < data.length; i++)
	{	
		if(data[i].conc == "null")
			conc = "N/A";
		else
			conc = data[i].conc;

		if(data[i].qualifier == "null")
				qual = " ";
		else
				qual = data[i].qualifier;

		if(data[i].unit && data[i].unit != "null")
				conc += " "+data[i].unit;

		aDataSet += "['"+ qual +"','"+
		data[i].value + "',";
		if(response.epType == "response")
			aDataSet += "'" + conc + "',"; 
		aDataSet += "'<a href = \"/vivo/bao-individual?uri="+$.URLEncode("http://www.bioassayontology.org/bao/BAO_0000015_")+data[i].aid+"\">"+data[i].aid+"</a>','"+
		data[i].target.replace(/'/gi, "`") + "']";
		if(i != (data.length - 1))
			aDataSet += ",";
	}
	aDataSet += " ]";
	var colSet;

	if(response.epType == "response")
		colSet = "[ { \"sTitle\": \" \" }, { \"sTitle\": \"Endpoint Value\" }, { \"sTitle\": \"Concentration\" }, { \"sTitle\": \"Assay ID\" }, { \"sTitle\": \"Target\" }]";
	else
		colSet = "[ { \"sTitle\": \" \" }, { \"sTitle\": \"Endpoint Value\" }, { \"sTitle\": \"Assay ID\" }, { \"sTitle\": \"Target\" }]";

	$('#dynamic').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>' );
	oTable = $('#example').dataTable( {
		//"bJQueryUI": true,
		"aaData": eval(aDataSet),
		"aoColumns": eval(colSet),
		"bSort": true,
		"sDom": '<"top"<"clear">>rt<"bottom"<"clear">>',
		"iDisplayLength": -1,
		"aLengthMenu": [[25, 50, -1], [25, 50, "All"]]
	} );

	
	$('a.zoom').click(popOut);
	
	$("#message").html("");

	$('#toolbox').append('<a href = "#" class = "tool addWs" onClick = "$(\'#mode\').val(\'add\');return processWorkset(\'compound\',\'cpdWorksetForm\');">Add To Workset</a>');
	$('#toolbox').append('<a href = "#" class = "tool clearWs" onClick = "$(\'#mode\').val(\'clear\');return processWorkset(\'compound\',\'cpdWorksetForm\');">Clear Workset</a>');
}


/**
 * Function to receive the HTTP request response for filter data on all assays for this comopound and show it in a table
 *
 * @param (HTTPResponse) The object containing the endpoints list and details
 */
function processResponsePlus(response) 
{	
	var data = response.data;
	var epVal, conc;

	if(data.length == 0)
	{
		$('#dynamicPlus').html("No results were returned.");
		return;
	}

	var aDataSet = "[ ";
	for(var i = 0; i < data.length; i++)
	{	
		if(data[i].conc == "null" || data[i].conc == "NaN")
			conc = "N/A";
		else
			conc = data[i].conc;

		if(data[i].unit && data[i].unit != "null")
				conc += " "+data[i].unit;

		if(data[i].value == "null" || data[i].value == "NaN")
				epVal = "Inactive";
		else
				epVal = data[i].value;

		aDataSet += "['"+epVal+"',";
		if(response.epType == "response")
			aDataSet += "'" + conc + "',";
		aDataSet += "'<a href = \"/vivo/individual?uri="+$.URLEncode("http://www.bioassayontology.org/bao#individual_BAO_0000015_")+data[i].aid+"\">"+data[i].aid+"</a>','"+
				data[i].target.replace(/'/gi, "`") + "']";
		if(i != (data.length - 1))
				aDataSet += ",";
	}
	aDataSet += " ]";
	var colSet;

	if(response.epType == "response")
			colSet = "[ { \"sTitle\": \"Endpoint Value\" }, { \"sTitle\": \"Concentration\" }, { \"sTitle\": \"Assay ID\" }, { \"sTitle\": \"Target\" }]";
	else
			colSet = "[ { \"sTitle\": \"Endpoint Value\" }, { \"sTitle\": \"Assay ID\" }, { \"sTitle\": \"Target\" }]";

	$('#dynamicPlus').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="plus"></table>' );
	oTable = $('#plus').dataTable( {
		"bJQueryUI": false,
						"aaData": eval(aDataSet),
		"bSort": true,
						"aoColumns": eval(colSet),
		"sDom": '<"top"<"clear">>rt<"bottom"<"clear">>',
		"iDisplayLength": -1,
		"aLengthMenu": [[25, 50, -1], [25, 50, "All"]]
	} );
	
	$('a.zoom').click(popOut);
}


/**
 * Send an AJAX reqest to the download servlet to allow the user to download the filter data
 *
 */
function downloadResults()
{
		$.get("download-filter?" + querystring);
}
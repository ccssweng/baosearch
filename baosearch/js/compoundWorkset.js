/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */
 
//Required for the form to allow the user to pick what columns to download, uncomment to enable
/*
jQuery.download = function(url, data, method,callback){

	var iframeX;
	//url and data options required
	if( url && data ){ 
		// remove old iframe if it exists
		if($("#iframeX")) $("#iframeX").remove(); 
	
		// creater new iframe
		iframeX= $('<iframe src="javascript:false;" name="iframeX" id="iframeX"></iframe>').appendTo('body').hide(); 
		
		iframeX.ready(function(){
			callback();
		}); 

		//data can be string of parameters or array/object
		data = typeof data == 'string' ? data : jQuery.param(data);
		//split params into form inputs
		var inputs = '';
		jQuery.each(data.split('&'), function(){ 
			var pair = this.split('=');
			inputs+='<input type="hidden" name="'+ pair[0] +'" value="'+ pair[1] +'" />'; 
		});
		//send request
		jQuery('<form action="'+ url +'" method="'+ (method||'post') + '" target="iframeX">'+inputs+'</form>').appendTo('body').submit().remove();
	};
};*/

/**
 * Function to allow the user to download the contents of the workset. By default, all columns are selected. Uncomment the sections marked "Required for the form to allow the user to pick what columns to download..." to display the form
 *
 * @return (Boolean) false to prevent the link from being followed
 */
function downloadWorkset()
{
	var cpdList = "";
	$('input[name=compoundWorksetList]').each(function() {cpdList += $(this).val() + ",";});
	cpdList = cpdList.substring(0,(cpdList.length-1));

	var columns = "";
	$("input[name=columns]:checked").each(function() {columns += $(this).val() + ",";});
	columns = columns.substring(0,(columns.length-1));

	var url = "columns="+columns+"&delimiter="+$('#delimiter').val()+"&input="+cpdList+"&filename="+$('#filename').val();

	$.download(
		'/baosearch/download-workset',
		url,
		'get',
		function() { 
			$("#message").html("File Generated Successfully");
			$('#message').removeClass().addClass('successHighlight');
			//Required for the form to allow the user to pick what columns to download, uncomment to enable 
			//$.colorbox.close();
			});
	return false;
}

/**
 * Perform the requested operations on the workset
 *
 * 
 */
function processQuery()
{
	$("#message").html("<img src = '/baosearch/images/loader2.gif'/>");

	if(cpdWorkset != "ERROR" && cpdWorkset != "RESET" && cpdWorkset.length > 0)
		$.ajax( {url: "compound-by-id", data: {input: cpdWorkset}, success: processResponse, error: handleError, dataType: "json", type: "GET" } );
	else if(cpdWorkset == "RESET")
	{
		$('#tabs').hide();
		$('#message').html("Workset Cleared Successfully");
		$('#message').removeClass().addClass('successHighlight');
	}
	else
	{
		$('#tabs').hide();
		$('#message').html("Sorry, no compounds are currently selected. Please select and add compounds to the workset.");
		$('#message').removeClass().addClass('errorHighlight');
	}
}



/**
 * Save the HTTP response from the successful request to compound-by-id to a global variable and forward to show a list. Change the call here if a matrix view should be default. 
 *
 * @param (HTTPResponse) response
 */
function processResponse(response)
{
	data = response.data;
	showList();
}

/**
 * Switch workset compound view to matrix. Also render the appropriate buttons in the toolbox in the top menu
 *
 * 
 */
function showMatrix()
{
	$('#toolbox').html("");
	var matrixData = "";
	for(var i = 0; i < data.length; i++)
	{
		matrixData += "<div class = \"matrixCell\">"
				+"<input type = 'checkbox' name = 'compoundWorksetList' onClick = 'highlightRow(this);' value = '"+data[i].SubstanceId+"'>"
				+"<a title = 'Click to Zoom' id = '"+data[i].SubstanceId+"' href = 'zoom-structure?sid="+data[i].SubstanceId+"' class = 'zoom'>"
				+"<img class = \"bgLoad\" src=\"/chemfile?type=sid&id="+data[i].SubstanceId+"&format=jpeg:w100,h100\"/>"
				+"</a><br><b>SID: </b>"+data[i].SubstanceId
				+"<br><b>IUPAC: </b>"+data[i].Name
				+"<br><b>Formula: </b>"+data[i].Formula
				+"</div>";
	}
	$('#dynamic').html(matrixData);
	$('a.zoom').click(popOut);
	$('#toolbox').append('<a href = "#" class = "tool deleteWs" onClick = "$(\'#mode\').val(\'delete\');return processWorkset(\'compound\',\'cpdWorksetForm\');">Delete from Workset</a>');
	$('#toolbox').append('<a href = "#" class = "tool clearWs" onClick = "$(\'#mode\').val(\'clear\');return processWorkset(\'compound\',\'cpdWorksetForm\');">Clear Workset</a>');
	$('#toolbox').append('<a class = "tool download" onClick = "downloadWorkset();" href = "#">Download Workset</a>');
	$("#message").html("");		
}

/**
 * Switch workset compound view to list. Also render the appropriate buttons in the toolbox in the top menu
 *
 * 
 */
function showList()
{
	$('#toolbox').html("");
	var aDataSet = '[ ';
	for(var i = 0; i < data.length; i++)
	{
		aDataSet += '["<input type = \'checkbox\' name = \'compoundWorksetList\' onClick = \'highlightRow(this);\' value = \''+data[i].SubstanceId+'\'>","'
		+'<a href = \'fetch-assays?input='+data[i].SubstanceId+'\' '
							+'class = \'actionLink detailsPopout\' title = \'View Assays connected to this substance\'>View Assays</a>'
							+'<br><a href = \'sub-summary?sid='+data[i].SubstanceId+'\' '
							+'class = \'actionLink\' title = \'View summary for this substance\'>View Summary</a>","'
		+'<a title = \'Click to Zoom\' id = \''+data[i].SubstanceId+'\' href = \'zoom-structure?sid='+data[i].SubstanceId+'\' class = \'zoom\'><img class = \'bgLoad\' src=\'/chemfile?type=sid&id='+data[i].SubstanceId+'&format=jpeg:w100,h100\'/></a>'
		+'","'+data[i].SubstanceId 
		+'","'+data[i].Name
		+'","'+data[i].MolecularWeight
		+'","'+data[i].Formula+'"]';

			if(i != (data.length - 1))
				aDataSet += ',';
	}
	aDataSet += ' ]';
	var colSet;

	//alert(aDataSet);

	colSet = "[ { \"sTitle\": \"<input type='checkbox' value = '0' name='selectAllB' id='selectAllB' onClick='jqCheckAll(this.name)'>\" }, { \"sTitle\": \"Actions\" }, { \"sTitle\": \"Structure\" }, { \"sTitle\": \"Substance ID\" }, { \"sTitle\": \"IUPAC Name(s)\" }, { \"sTitle\": \"Mol. Wt.\" } , { \"sTitle\": \"Mol. Formula\" }]";
	$('#dynamic').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>' );
	oTable = $('#example').dataTable( {
				"aaData": eval(aDataSet),
				"aoColumns": eval(colSet),
				"sDom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
				"iDisplayLength": 25,
				"aLengthMenu": [[25, 50, 100], [25, 50, 100]]
			} );
	$('#toolbox').append('<a href = "#" class = "tool deleteWs" onClick = "$(\'#mode\').val(\'delete\');return processWorkset(\'compound\',\'cpdWorksetForm\');">Delete from Workset</a>');
	$('#toolbox').append('<a href = "#" class = "tool clearWs" onClick = "$(\'#mode\').val(\'clear\');return processWorkset(\'compound\',\'cpdWorksetForm\');">Clear Workset</a>');
	$('#toolbox').append('<a class = "tool download" href = "#" onClick = "downloadWorkset();">Download Workset</a>');

	$('a.zoom').click(popOut);

	$('a.actionLink').tipsy({gravity: 's'})
	$('a.detailsPopout').click(popOut);	

	$("#message").html("");		
	//Required for the form to allow the user to pick what columns to download, uncomment to enable 
	//$(".dlPopup").colorbox({width:"50%", inline:true, href:"#dlForm"});
}			

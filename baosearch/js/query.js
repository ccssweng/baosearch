/**********************************************************************
Copyright (c) 2011, The University of Miami
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

***********************************************************************/

/**
 * @author ndatar
 * @version 1
 */
 
 
/**
 * Global variables
 */
var newClassGroup = "";					//Stores the currently selected classGroup for the tab
var worksetArr = new Array();			//Stores the list of selected assays
var colCount;							//The number of columns present in the data for this result
var targetArray = new Array();			//Stores the list of unique targets for all bioassays on the current page
var designArray = new Array();			//Stores the list of unique designs for all bioassays on the current page
var detectionArray = new Array();		//Stores the list of unique detection technologies for all bioassays on the current page
var formatArray = new Array();			//Stores the list of unique formats for all bioassays on the current page
var searchTerm = "";					//The current search term
var queryInfo = "";						//A string explaining what the search results are
var hitsLength = 0;						//The number of hits that were returned for the search query - 0 by default
var offset = 0;							//The offset from the beginning for the search results - 0 by default
var oldOffset = 0;						//The previous offset
var pageHits = 25;						//Number of hits per page - 25 by default
var pagenum = 1;						//The number of the current page of results, 1 is the first page 
var displayLength = 25;					//Number of rows to be displayed in the datatable
var currPage = 1;						//The page we are currently on, 1 by default
var tableDom = "";						//The options for the datatable that decide what controls should be shown above and below it
var data = "";							//A global object to store the "data" part of the HTTPResponse JSON object

/**
 * Show a loading indicator and call function to fetch results when the page is ready
 */
$(function() {
	$('#message').html("<img src = '/baosearch/images/loader2.gif'/>");
	processQuery();
});

/**
 * Function to receive the HTTP request response from search request and render it as a list of triples
 *
 * @param (HTTPResponse) The object containing the search results
 */
function renderText(response)
{
			$('#toolbox').hide();
	
	data = response.individuals;
			for(_obj in response.columnNames)
					colCount++;

	var txtString = "";

			for(var i = 0; i < data.length; i++)
			{
		txtString += "<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout\" title =\"View Details\">View Details</a><br/>";
					for(var j = 0; j < colCount; j++)
					{
			var valStr = "";
							if(data[i][response.columnNames[j]])
									if(data[i][response.columnNames[j]][0].name)
											valStr += (data[i][response.columnNames[j]][0].name);
									else
											valStr += (data[i][response.columnNames[j]]); 
							else
									valStr += "";

			if(valStr != "")
				txtString += "<b>"+response.columnNames[j]+":</b> "+ valStr + "<br>";

			if(j ==  colCount-1)
				txtString = txtString.substring(0, (txtString.length-4));

					}
					if(i != (data.length - 1))
							txtString += "<hr/>";
			}
	$('#dynamic').html(txtString);
	$('#message').html("");
}
	
/**
 * Function to receive the HTTP request response from search request and render it as a table of objects for each tab
 *
 * @param (HTTPResponse) The object containing the search results
 */
function renderTable(response) 
{
	$('#toolbox').show();

	hitsLength = response.hitsLength;	
	data = response.individuals;

	var wsString = "";
	var wsStringB = "";
	var wsStringT = "";
	var colSet = "";
	var asInitVals = new Array();
	var temp;
	var filterStr = "";

	if(pageHits == 0)
	{
		if(hitsLength > 25)
			pageHits = 25;
		else
			pageHits = hitsLength;
	}

	for(_obj in response.columnNames) 
		colCount++;
	var aDataSet = "[ ";

	wsString =  '<a href = "#" class = "tool clearWs"  onClick = "$(\'#mode\').val(\'clear\');return processWorkset(\'assay\',\'assayWorksetForm\');">Clear Workset</a>';

	//For the BIOASSAY tab
	if(response.typeName == "bioassay")
	{
		
		$("#pagination").html("Showing page "+pagenum+" of "+(Math.ceil(hitsLength/pageHits)));
		$("#pagination").show();			

		targetArray.length = 0;
		designArray.length = 0;
		detectionArray.length = 0;
		formatArray.length = 0; 

		//Set up the column headers
		colSet = "[ "+
			"{ \"sTitle\": \" \"},"+ 
			"{ \"sTitle\": \"Details\"},"+
			"{ \"sTitle\": \"AssayID\"},"+
			"{ \"sTitle\": \"Name\"},"+
			"{ \"sTitle\": \"Assay Stage\"},"+
			"{ \"sTitle\": \"Target\"},"+
			"{ \"sTitle\": \"Endpoint\"},"+
			"{ \"sTitle\": \"Design\"},"+
			"{ \"sTitle\": \"Detection\"},"+
			"{ \"sTitle\": \"Format\"}]";

			
		wsString =  '<a href = "#" class = "tool addWs" onClick = "$(\'#mode\').val(\'add\');return processWorkset(\'assay\',\'assayWorksetForm\');">Add To Workset</a><a href = "#" class = "tool clearWs"  onClick = "$(\'#mode\').val(\'clear\');return processWorkset(\'assay\',\'assayWorksetForm\');">Clear Workset</a>';

		//Set up the active filtering input fields
		filterStr = "<tr><th><input type='checkbox' class = 'fancyTip' title = 'Select All' name='selectAllB' id='selectAllB' onClick='jqCheckAll(this.name)'></th><th>&nbsp;</th>";
		filterStr += '<th><input type="text" name="filterAssayID" value="Filter" class="search_init" /></th>';
		filterStr += '<th><input type="text" name="filterName" value="Filter" class="search_init" /></th>';
		filterStr += '<th><input type="text" name="filterAssayStage" value="Filter" class="search_init" /></th>';
		filterStr += '<th><input type="text" name="filterTarget" value="Filter" class="search_init" /></th>';
		filterStr += '<th><input type="text" name="filterEndpoint" value="Filter" class="search_init" /></th>';
		filterStr += '<th><input type="text" name="filterDesign" value="Filter" class="search_init" /></th>';
		filterStr += '<th><input type="text" name="filterDetection" value="Filter" class="search_init" /></th>';
		filterStr += '<th><input type="text" name="filterFormat" value="Filter" class="search_init" /></th>';
		filterStr += "</tr>";

		/*
		 * Check if we have at least one result and then loop through and create the data string to be put into the table. Check if each column has a value present. 
		 * If a column is present and it is one of the tabs, then check if it is present in the array for that tab. If not, then insert it
		 */
		for(var i = 0; i < data.length; i++)
		{
			aDataSet += "['<input type = \"checkbox\" name = \"assayWorksetList\" onClick = \"highlightRow(this);\" value = \""+data[i]["has id"][0].name+
					"\">','<img title = \"View/Hide Details on BAO\" id = \""+data[i].uri+"\" class = \"infoRow\" src = \"/baosearch/images/bao-icon.png\"/>"+ 
					" <a href = \"http://pubchem.ncbi.nlm.nih.gov/assay/assay.cgi?aid="+data[i]["has id"][0].name+"\" target=\"_blank\" class = \"fancyTip\" title = \"View Details on PubChem\"><img src = \"/baosearch/images/ncbi-icon.png\"/></a>',";
			
							aDataSet += "'" + data[i]["has id"][0].name + "',";

							temp = (data[i]["hasName"][0]).toString().substring(0,120).replace(/'/gi, "`");
							aDataSet += "'" + temp.replace(/&#38;/gi, " and ");
			if(temp.length > 120)
				aDataSet += "...',";
			else
				aDataSet += "',";

			if(data[i]["hasStage"])
			{
				aDataSet += "'"+data[i]["hasStage"][0].name.replace(/'/gi, "`");
				if(data[i]["hasStage"].length > 1)
					aDataSet += "<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details\">("+(data[i]["hasStage"].length-1)+" more)</a>";
				aDataSet += "',";
			}
			else
				aDataSet += "'-',";


			if(data[i]["hasTarget"])
			{
				aDataSet += "'"+data[i]["hasTarget"][0].name.replace(/'/gi, "`");
				if(data[i]["hasTarget"].length > 1)
					aDataSet += "<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details\">("+(data[i]["hasTarget"].length-1)+" more)</a>";
				aDataSet += "',";
								for(var j = 0; j < data[i]["hasTarget"].length; j++)
										if(jQuery.inArray(data[i]["hasTarget"][j].name,targetArray) == -1)
												targetArray.push(data[i]["hasTarget"][j].name);

			}
			else
				aDataSet += "'-',";


			if(data[i]["hasEndpoint"])
			{
				aDataSet += "'"+(data[i]["hasEndpoint"][0].name).replace(/'/gi, "`");
				if(data[i]["hasEndpoint"].length > 1)
					aDataSet += "<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details\">("+(data[i]["hasEndpoint"].length-1)+" more)</a>";
				aDataSet += "',";
			}
			else
				aDataSet += "'-',";

			if(data[i]["hasDesign"])
			{
				aDataSet += "'"+data[i]["hasDesign"][0].name.replace(/'/gi, "`");
				if(data[i]["hasDesign"].length > 1)
					aDataSet += "<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details\">("+(data[i]["hasDesign"].length-1)+" more)</a>";
				aDataSet += "',";
								for(var j = 0; j < data[i]["hasDesign"].length; j++)
										if(jQuery.inArray(data[i]["hasDesign"][j].name,designArray) == -1)
												designArray.push(data[i]["hasDesign"][j].name);

			}
			else
				aDataSet += "'-',";

			if(data[i]["hasDetection"])
			{
				aDataSet += "'"+data[i]["hasDetection"][0].name.replace(/'/gi, "`");
				if(data[i]["hasDetection"].length > 1)
					aDataSet += "<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details\">("+(data[i]["hasDetection"].length-1)+" more)</a>";
				aDataSet += "',";
								for(var j = 0; j < data[i]["hasDetection"].length; j++)
										if(jQuery.inArray(data[i]["hasDetection"][j].name,detectionArray) == -1)
												detectionArray.push(data[i]["hasDetection"][j].name);

			}
			else
				aDataSet += "'-',";


			if(data[i]["has format"])
			{
				aDataSet += "'"+data[i]["has format"][0].name.replace(/'/gi, "`");
				if(data[i]["has format"].length > 1)
					aDataSet += "<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details\">("+(data[i]["has format"].length-1)+" more)</a>";
				aDataSet += "'";
								for(var j = 0; j < data[i]["has format"].length; j++)
										if(jQuery.inArray(data[i]["has format"][j].name,formatArray) == -1)
												formatArray.push(data[i]["has format"][j].name);
			}
			else
				aDataSet += "'-'";


			aDataSet += "]";
			if(i != (data.length - 1))
				aDataSet += ",";
		}
		
		/**
		 * Since VIVO cannot handle a search string containing more than 5000 characters, we limit that number of targets ORed together to 100. This magic number was decided upon
		 * after looking at the character length and number of targets from many queries and picking the upper bound. If the number of targets it too large, display a message asking
		 * the user to search for targets by using the suggest search for assays 
		 */
		if(targetArray.length > 100)
		{
			$("#tabTitles").hide();
			$("#lrgResultMsg").html("<span class = \"message alertHighlight\">Since the number of assays returned is too large, concept based display cannot be enabled. <a href = \"assays-by-concept\">Please try browsing for assays by concept here</a>.</span>");		
		}

	}
	//for the DESIGN tab
	else if(response.classgroupName == "design")
	{
					colSet = "[ "+
							"{ \"sTitle\": \"Details\"},"+
							"{ \"sTitle\": \"Design Name\"},"+
							"{ \"sTitle\": \"Assays with this Design\"},"+
							"{ \"sTitle\": \"Add to Workset\"}]";

					filterStr = "<tr><th>&nbsp;</th>";
					filterStr += '<th><input type="text" name="filterName" value="Filter" class="search_init" /></th>';
					filterStr += '<th><input type="text" name="filterAssayID" value="Filter" class="search_init" /></th>';
					filterStr += "<th>&nbsp;</th></tr>";

		for(var i = 0; i < data.length; i++)
		{
			aDataSet += "['<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details on BAO\"><img src = \"/baosearch/images/bao-icon.png\"/></a>',";
			aDataSet += "'"+data[i].name.replace(/'/gi, "`")+"',";
			aDataSet += "'"+data[i].isDesignOf[0].name;
			if(data[i].isDesignOf.length > 1)
				aDataSet += "<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details\">("+(data[i]["isDesignOf"].length-1)+" more)</a>";
			aDataSet += "','<a href = \"#\" onClick = worksetFromTabs("+i+",\"isDesignOf\");>Add</a>";				
			aDataSet += "']";
			if(i != (data.length - 1))
									aDataSet += ",";
		}
	}
	// for the TARGET tab
	else if(response.classgroupName == "meta target")
	{
					colSet = "[ "+
							"{ \"sTitle\": \"Details\"},"+
							"{ \"sTitle\": \"Target Name\"},"+
							"{ \"sTitle\": \"Assays with this Target\"},"+
							"{ \"sTitle\": \"Add to Workset\"}]";

					filterStr = "<tr><th>&nbsp;</th>";
					filterStr += '<th><input type="text" name="filterName" value="Filter" class="search_init" /></th>';
					filterStr += '<th><input type="text" name="filterAssayID" value="Filter" class="search_init" /></th>';
					filterStr += "<th>&nbsp;</th></tr>";

		for(var i = 0; i < data.length; i++)
		{
			aDataSet += "['<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details on BAO\"><img src = \"/baosearch/images/bao-icon.png\"/></a>',";
			aDataSet += "'"+data[i].name.replace(/'/gi, "`")+"',";
			aDataSet += "'"+data[i].isTargetOf[0].name;
			if(data[i].isTargetOf.length > 1)
				aDataSet += "<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details\">("+(data[i]["isTargetOf"].length-1)+" more)</a>";
							aDataSet += "','<a href = \"#\" onClick = worksetFromTabs("+i+",\"isTargetOf\")>Add</a>";
			aDataSet += "']";
			if(i != (data.length - 1))
									aDataSet += ",";
		}
	}
	//for the DETECTION tab
	else if(response.classgroupName == "detection technology")
	{
					colSet = "[ "+
							"{ \"sTitle\": \"Details\"},"+
							"{ \"sTitle\": \"Detection Technology Name\"},"+
							"{ \"sTitle\": \"Assays with this Detection Technology\"},"+
							"{ \"sTitle\": \"Add to Workset\"}]";


					filterStr = "<tr><th>&nbsp;</th>";
					filterStr += '<th><input type="text" name="filterName" value="Filter" class="search_init" /></th>';
					filterStr += '<th><input type="text" name="filterAssayID" value="Filter" class="search_init" /></th>';
					filterStr += "<th>&nbsp;</th></tr>";

		for(var i = 0; i < data.length; i++)
		{
			aDataSet += "['<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details on BAO\"><img src = \"/baosearch/images/bao-icon.png\"/></a>',";
			aDataSet += "'"+data[i].name.replace(/'/gi, "`")+"',";
			aDataSet += "'"+data[i].isDetectionOf[0].name;
			if(data[i].isDetectionOf.length > 1)
				aDataSet += "<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details\">("+(data[i]["isDetectionOf"].length-1)+" more)</a>";
							aDataSet += "','<a href = \"#\" onClick = worksetFromTabs("+i+",\"isDetectionOf\")>Add</a>";
			aDataSet += "']";

			if(i != (data.length - 1))
									aDataSet += ",";
		}
	}
	//for the FORMAT tab
	else if(response.classgroupName == "format")
	{
					colSet = "[ "+
							"{ \"sTitle\": \"Details\"},"+
							"{ \"sTitle\": \"Format Name\"},"+
							"{ \"sTitle\": \"Assays with this Format\"},"+
							"{ \"sTitle\": \"Add to Workset\"}]";


					filterStr = "<tr><th>&nbsp;</th>";
					filterStr += '<th><input type="text" name="filterName" value="Filter" class="search_init" /></th>';
					filterStr += '<th><input type="text" name="filterAssayID" value="Filter" class="search_init" /></th>';
					filterStr += "<th>&nbsp;</th></tr>";

		for(var i = 0; i < data.length; i++)
		{
			aDataSet += "['<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details on BAO\"><img src = \"/baosearch/images/bao-icon.png\"/></a>',";
			aDataSet += "'"+data[i].name.replace(/'/gi, "`")+"',";
			aDataSet += "'"+data[i]["is format of"][0].name;
			if(data[i]["is format of"].length > 1)
				aDataSet += "<a href = \"/vivo/bao-individual?uri="+$.URLEncode(data[i].uri)+"\" class = \"detailsPopout fancyTip\" title = \"View Details\">("+(data[i]["is format of"].length-1)+" more)</a>";
							aDataSet += "','<a href = \"#\" onClick = worksetFromTabs("+i+",\"isFormatOf\")>Add</a>";
			aDataSet += "']";
			if(i != (data.length - 1))
									aDataSet += ",";
		}
	}

	aDataSet += "]";


	//Some code used in the debugging process when there is a mismatch between number of header columns and number or table body columns. Uncomment to print out the string before it is inserted into the table
	//	$("#check").text(aDataSet);
	//	alert(colSet);

	$('#dynamic').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>' );
	oTable = $('#example').dataTable( {
		"aaData": eval(aDataSet),
		"aoColumns": eval(colSet),
		"sDom": tableDom,
		//"bPaginate": false
		"iDisplayLength": displayLength,
		"bAutoWidth": false,
		//"aaSorting": [[3, 'asc']],
		"aLengthMenu": [[25, 50, 100], [25, 50, 100]],
		"sSearch": "Search all columns:"
	} );	


	//Register the next page button to perform specialized actions depending on the selected tab
	$('#example_next').click(function() {
		$("#selectAllB").attr('checked', false);
		jqCheckAll( 'selectAllB', 'assayWorksetList' );
		if(pagenum != (Math.ceil(hitsLength/pageHits)))
		{
			pagenum++;
			$("#pagination").show();
			$("#pagination").html("Showing page "+pagenum+" of "+(Math.ceil(hitsLength/pageHits)));
			processQuery("plus");				
			$("#dynamic").html("");	
			$('#message').html("<img src = '/baosearch/images/loader2.gif'/>");
		}
		if(newClassGroup != "http://www.bioassayontology.org/bao#BAO_0000015")
			processQuery("plus");
	});
			
	//Register the previous page button to perform specialized actions depending on the selected tab
	$('#example_previous').click(function() {
		$("#selectAllB").attr('checked', false);
		jqCheckAll( 'selectAllB', 'assayWorksetList' );
		if(pagenum != 1)
		{
			pagenum--;
			$("#pagination").show();
			$("#pagination").html("Showing page "+pagenum+" of "+(Math.ceil(hitsLength/pageHits)));
			processQuery("minus");
			$("#dynamic").html("");	
			$('#message').html("<img src = '/baosearch/images/loader2.gif'/>");
		}
		if(newClassGroup != "http://www.bioassayontology.org/bao#BAO_0000015")
				processQuery("minus");
	});


	$("table#example thead").append(filterStr);

	$("thead input").keyup( function () {
		//alert("searching for "+this.value+" in column "+($("thead input").index(this)+2));
		oTable.fnFilter( this.value, ($("thead input").index(this)+1) );
	} );
	

	/*
	 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in 
	 * the header
	 */
	$("thead input").each( function (i) {
		asInitVals[this.name] = this.value;
	} );
	
	$("thead input").focus( function () {
		if ( this.className == "search_init" )
		{
			this.className = "";
			this.value = "";
		}
	} );

	$("thead input").blur( function (i) {
		if ( this.value == "" )
		{
			this.className = "search_init";
			this.value = asInitVals[this.name];
		}
	} );

	if(wsString != "")	
		$('#toolbox').html(wsString+wsStringB);

			$('#message').html("");


	/* Add event listener for opening and closing details
	 * Note that the indicator for showing which row is open is not controlled by DataTables,
	 * rather it is done here
	 */
	$('#example tbody td img.infoRow').live('click', function () {
		var nTr = this.parentNode.parentNode;
		if ( this.src.match('bao-icon-close') )
		{
			/* This row is already open - close it */
			this.src = "/baosearch/images/bao-icon.png";
			this.title = "View Details on BAO";
			oTable.fnClose( nTr );
		}
		else
		{
			/* Open this row */
			uri = "/vivo/bao-individual?uri="+$.URLEncode(this.id);
			this.src = "/baosearch/images/bao-icon-close.png";
			this.title = "Hide Details";
			oTable.fnOpen( nTr, "<iframe src = '"+uri+"' style = 'height: 450px; width: 95%; margin: 0px 5px; border: 0px;'>Please upgrade your browser to support iframes</iframe>", 'moreInfo' );
		}
		$('img.infoRow').tipsy({gravity: 's'});
		return false;
	} );

	$('.fancyTip').tipsy({gravity: 's'});
	$('img.infoRow').tipsy({gravity: 's'});
	$('select[name=example_length]').change( function() {$("#selectAllB").attr('checked', false);jqCheckAll( 'selectAllB', 'assayWorksetList' ); pageHits = $('select[name=example_length]').val(); processQuery('noop'); $('#message').html("<img src = '/baosearch/images/loader2.gif'/>"); });
} 

/**
 * Utility function to remove all spaces in the string passed in and convert the next character to uppercase this making it camelcase
 *
 * @param (String) The string to be converted to camelcase
 */
function camelcase( s ) { s = $.trim(s);
	return ( /\S[A-Z]/.test( s ) ) ?
	s.replace( /(.)([A-Z])/g, function(t,a,b) { return a + ' ' + b.toLowerCase(); } ) :
	s.replace( /( )([a-z])/g, function(t,a,b) { return b.toUpperCase(); } );
}

/**
 * Toggle visibility of the specified column
 *
 * @param (Number) The index of the column to be toggled
 */
function fnShowHide( iCol )
{
	var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
	oTable.fnSetColumnVis( iCol, bVis ? false : true );
}

/**
 * Function to save the currently selected tab (classGroup) in a global variable
 *
 * @param (String) The name of the currently selected tab
 */
function setClassGroup(classGroup)
{
	newClassGroup = classGroup;
	processQuery("noop");
}

/**
 * Depending on the operation specified, prepare a query string by concatenating all the elements in the arrat passed in with a logical OR sign (||) and send it to Vivo
 *
 * @param (Array) The array containing the list of concepts to be queried
 * @param (String) The operation to be performed on the paging or noop if the first page is to be loaded
 */
function executeQuery(arr, op)
{
	var qText = "";
	var maxLength = arr.length;
	displayLength = -1;	
	tableDom = '<"top"f<"clear">>rt<"bottom"f<"clear">>';
		
	if(op == "plus")
	{
		if((pagenum+1)*20 < arr.length || ((pagenum+1)*20 - arr.length > 0 && (pagenum+1)*20 - arr.length < 20))
		{	
			pagenum++;
			if(pagenum*20 < arr.length)
				maxLength = pagenum*20;
			else
				maxLength = arr.length;
		}
		else
			return;
	}
	else if(op == "minus")
	{
		if(pagenum-1 > 0)
		{
			pagenum--;
			if(pagenum*20 < arr.length)
				maxLength = pagenum*20;
			else
				maxLength = arr.length;
		}
		else
			return;
	}
	else
	{
		if(arr.length > 20)
			maxLength = 20;
	}

	$("#pagination").hide();
	$('#message').html("<img src = '/baosearch/images/loader2.gif'/>");

	for(var i = 0; i < arr.length; i++)
	{
			qText += "\""+arr[i]+"\"";
			if(i != arr.length - 1)
					qText += "||";
	}

	$.ajax( {url:"/vivo/baoSearch", data: {querytext: qText, classgroup: newClassGroup, hitsPerPage: 100}, success: processResponse, dataType: "json", type: "GET" } );
	
}

/**
 * Depending on the operation specified and the class group, either farm out the request to executeQuery or hit Vivo directly
 *
 * @param (String) The operation to be performed on the paging or noop if the first page is to be loaded
 */
function processQuery(op)
{
	searchTerm = "<%= request.getParameter("searchTerm") %>";
	$("#searchTerm").val(searchTerm);		

	if(newClassGroup == 'http://localhost:8080/vivo/individual/vitroClassGroupBAO_0000020')
		executeQuery(targetArray, op);
	else if(newClassGroup == 'http://localhost:8080/vivo/individual/vitroClassGroupBAO_0000023')
		executeQuery(designArray, op);
	else if(newClassGroup == 'http://localhost:8080/vivo/individual/vitroClassGroupBAO_0000035')
			executeQuery(detectionArray, op);
	else if(newClassGroup == 'http://localhost:8080/vivo/individual/vitroClassGroupBAO_0000019')
			executeQuery(formatArray, op);
	else if(op != "noop" || newClassGroup == 'http://www.bioassayontology.org/bao#BAO_0000015')
	{
		displayLength = pageHits;
		tableDom = '<"top"lfp<"clear">>rt<"bottom"lf<"clear">>';

		if(newClassGroup == "all")
			 $.getJSON("/vivo/baoSearch", {querytext: searchTerm, hitsPerPage: 25, startIndex: offset}, processResponse );
		else
		{
			newClassGroup = 'http://www.bioassayontology.org/bao#BAO_0000015';
			$.getJSON("/vivo/baoSearch", {startIndex: ((pagenum-1)*pageHits), hitsPerPage: pageHits, querytext: searchTerm, type: newClassGroup}, processResponse);
		}
		queryInfo = "Query Results For \""+searchTerm+"\"";
		return false;
	}
	queryInfo = "Query Results for \""+searchTerm+"\" related to bioassay matches on page "+pagenum+". <a class = \"alertHighlight\" href = \"assays-by-concept\">(Search for all assays by concept)</a>";
	return false;
}

/**
 * If there are no results for the current search term on BAOSearch, perform the same search on PubChem and print the number of available results
 *
 * @param (HTTPResponse) The JSON object containing the hit count from other sources
 */
function showPubChemResults(response)
{
	var pcCount = "";
	if(response.count > 0)
		pcCount = "Data sources that may contain results for this query:<br><a target = 'blank' href ='http://www.ncbi.nlm.nih.gov/sites/entrez?db=pcassay&term="+searchTerm+"'>PubChem("+response.count+")</a>";
	else
		pcCount = "There were no results on other data sources (PubChem) for this query."
	$('#dynamic').html("Your search for <b>"+searchTerm+"</b> did not match any BAO curated assays. Please click <a href = \"http://bioassayontology.org/index.php?option=com_contact&view=contact&id=3&Itemid=74\">here</a> to let us know. <br><br>"+pcCount);
}	

/**
 * Handle the response object containing results for the current search term and send the same query to PubChem in case of no results. 
 * If valid results are present, render them using either the text or table option
 *
 * @param (HTTPResponse) The JSON object containing results
 */
function processResponse(response)
{
	$('#toolbox').html("");
	colCount = 0;
	$("#queryText").html("<h1>"+queryInfo+"</h1>");
	if(response.error)
	{
		$('#message').html("");
		$.getJSON("pubchem-redirect", {keyword: searchTerm},  showPubChemResults);
		return;
	}
	if(newClassGroup == "all")
		renderText(response);
	else
		renderTable(response);
	$("#dynamic").show();
}

/**
 * Set the active tab based on user input
 *
 * @param (HTMLElement) The tab that was clicked
 */
function changeTabs(elem)
{
	$("#dynamic").hide();
	$("#tabTitles a").removeClass().addClass("category");
	$(elem).removeClass().addClass("currCategory");
	$('#message').removeClass();
	$('#message').html("<img src = '/baosearch/images/loader2.gif'/>");
	$('#message').show();
}

/**
 * Add the asssays for the currently selected concept to the workset
 *
 * @param (Number) The index of the concept in its respective array
 * @param (String) The name of the currently selected tab
 */
function worksetFromTabs(selIndex, tabType)
{
	var assayList = "mode=add";
	for(var i = 0; i < data[selIndex][tabType].length; i++)
		assayList += "&assayWorksetList="+(data[selIndex][tabType][i].name);
	$.post("update-assay-workset", assayList, worksetResult);
}

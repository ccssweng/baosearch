var labelType, useGradients, nativeTextSupport, animate;

(function() {
  var ua = navigator.userAgent,
      iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
      typeOfCanvas = typeof HTMLCanvasElement,
      nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
      textSupport = nativeCanvasSupport 
        && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
  //I'm setting this based on the fact that ExCanvas provides text support for IE
  //and that as of today iPhone/iPad current text support is lame
  labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
  nativeTextSupport = labelType == 'Native';
  useGradients = nativeCanvasSupport;
  animate = !(iStuff || !nativeCanvasSupport);
})();

var Log = {
  elem: false,
  write: function(text){
    if (!this.elem) 
      this.elem = document.getElementById('log');
    this.elem.innerHTML = text;
    this.elem.style.left = (500 - this.elem.offsetWidth / 2) + 'px';
  }
};


var defaultJson = {
    "children": [
        {
            "children": [
                {
                    "children": [
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "cell phenotype",
                            "name": "cell phenotype" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "cell movement",
                            "name": "cell movement" 
                        } 
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "morphology reporter",
                    "name": "morphology reporter" 
                },
                {
                    "children": [
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "electrophysiological measurements",
                            "name": "electrophysiological measurements" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "47",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "dye binding",
                            "name": "dye binding" 
                        } 
                    ],
                    "data": {
                        "assaycount": "47",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "membrane potential reporter",
                    "name": "membrane potential reporter" 
                },
                {
                    "children": [
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "3",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "protein conformation",
                            "name": "protein conformation" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "nucleic acid conformation",
                            "name": "nucleic acid conformation" 
                        } 
                    ],
                    "data": {
                        "assaycount": "3",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "conformation reporter",
                    "name": "conformation reporter" 
                },
                {
                    "children": [
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "caspase activity",
                            "name": "caspase activity" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "dehydrogenase activity",
                            "name": "dehydrogenase activity" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "86",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "atp content",
                            "name": "atp content" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "mitochondrial membrane potential",
                            "name": "mitochondrial membrane potential" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "esterase activity",
                            "name": "esterase activity" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "phospholipid redistribution",
                            "name": "phospholipid redistribution" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "chromatin condensation",
                            "name": "chromatin condensation" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "protease activity",
                            "name": "protease activity" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "membrane permeability",
                            "name": "membrane permeability" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "dna content",
                            "name": "dna content" 
                        } 
                    ],
                    "data": {
                        "assaycount": "88",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "viability reporter",
                    "name": "viability reporter" 
                },
                {
                    "children": [
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "metal ion redistribution",
                            "name": "metal ion redistribution" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "2",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "protein redistribution",
                            "name": "protein redistribution" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "45",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "second messenger",
                            "name": "second messenger" 
                        } 
                    ],
                    "data": {
                        "assaycount": "47",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "redistribution reporter",
                    "name": "redistribution reporter" 
                },
                {
                    "children": [
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "63",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "beta lactamase induction",
                            "name": "beta lactamase induction" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "4",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "beta galactosidase induction",
                            "name": "beta galactosidase induction" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "128",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "luciferase induction",
                            "name": "luciferase induction" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "chloramphenicol acetyltransferase induction",
                            "name": "chloramphenicol acetyltransferase induction" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "fluorescent protein induction",
                            "name": "fluorescent protein induction" 
                        } 
                    ],
                    "data": {
                        "assaycount": "195",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "inducible reporter",
                    "name": "inducible reporter" 
                },
                {
                    "children": [
                        {
                            "children": [
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
                                        "assaycount": "0",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "ubiquitin coupled enzyme",
                                    "name": "ubiquitin coupled enzyme" 
                                },
                                {
                                    "children": [
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "26",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "luciferin coupled enzyme",
                                            "name": "luciferin coupled enzyme" 
                                        },
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "35",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "atp coupled enzyme",
                                            "name": "atp coupled enzyme" 
                                        } 
                                    ],
                                    "data": {
                                        "assaycount": "61",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "substrate coupled enzyme",
                                    "name": "substrate coupled enzyme" 
                                },
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
                                        "assaycount": "5",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "chaperone coupled enzyme",
                                    "name": "chaperone coupled enzyme" 
                                } 
                            ],
                            "data": {
                                "assaycount": "69",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "coupled enzyme",
                            "name": "coupled enzyme" 
                        },
                        {
                            "children": [
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
                                        "assaycount": "6",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "enzyme activation",
                                    "name": "enzyme activation" 
                                },
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
                                        "assaycount": "69",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "enzyme inhibition",
                                    "name": "enzyme inhibition" 
                                } 
                            ],
                            "data": {
                                "assaycount": "88",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "enzyme activity",
                            "name": "enzyme activity" 
                        } 
                    ],
                    "data": {
                        "assaycount": "157",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "enzyme reporter",
                    "name": "enzyme reporter" 
                },
                {
                    "children": [
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "6",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "protein fragment complementation assay",
                            "name": "protein fragment complementation assay" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "12",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "radioligand binding",
                            "name": "radioligand binding" 
                        },
                        {
                            "children": [
                                {
                                    "children": [
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "western blot",
                                            "name": "western blot" 
                                        },
                                        {
                                            "children": [
                                                {
                                                    "children": [
                                                        
                                                    ],
                                                    "data": {
                                                        "assaycount": "14",
                                                        "$color": "#6f8dff",
                                                        "$area": 100 
                                                    },
                                                    "id": "competitive immunoassay",
                                                    "name": "competitive immunoassay" 
                                                },
                                                {
                                                    "children": [
                                                        
                                                    ],
                                                    "data": {
                                                        "assaycount": "0",
                                                        "$color": "#6f8dff",
                                                        "$area": 100 
                                                    },
                                                    "id": "antigen down assay",
                                                    "name": "antigen down assay" 
                                                },
                                                {
                                                    "children": [
                                                        
                                                    ],
                                                    "data": {
                                                        "assaycount": "0",
                                                        "$color": "#6f8dff",
                                                        "$area": 100 
                                                    },
                                                    "id": "sandwich elisa",
                                                    "name": "sandwich elisa" 
                                                } 
                                            ],
                                            "data": {
                                                "assaycount": "22",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "elisa",
                                            "name": "elisa" 
                                        } 
                                    ],
                                    "data": {
                                        "assaycount": "22",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "ex situ immunoassay",
                                    "name": "ex situ immunoassay" 
                                },
                                {
                                    "children": [
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "immunocytochemistry",
                                            "name": "immunocytochemistry" 
                                        },
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "immunohistochemistry",
                                            "name": "immunohistochemistry" 
                                        } 
                                    ],
                                    "data": {
                                        "assaycount": "0",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "in situ immunoassay",
                                    "name": "in situ immunoassay" 
                                } 
                            ],
                            "data": {
                                "assaycount": "22",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "immunoassay",
                            "name": "immunoassay" 
                        } 
                    ],
                    "data": {
                        "assaycount": "163",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "binding reporter",
                    "name": "binding reporter" 
                } 
            ],
            "data": {
                "assaycount": "695",
                "$color": "#6f8dff",
                "$area": 100 
            },
            "id": "design",
            "name": "design" 
        } 
    ],
    "data": {
        "$area": 100 
    },
    "id": "Thing",
    "name": "Thing" 
};
  //end

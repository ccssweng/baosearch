var labelType, useGradients, nativeTextSupport, animate;

(function() {
  var ua = navigator.userAgent,
      iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
      typeOfCanvas = typeof HTMLCanvasElement,
      nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
      textSupport = nativeCanvasSupport 
        && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
  //I'm setting this based on the fact that ExCanvas provides text support for IE
  //and that as of today iPhone/iPad current text support is lame
  labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
  nativeTextSupport = labelType == 'Native';
  useGradients = nativeCanvasSupport;
  animate = !(iStuff || !nativeCanvasSupport);
})();

var Log = {
  elem: false,
  write: function(text){
    if (!this.elem) 
      this.elem = document.getElementById('log');
    this.elem.innerHTML = text;
    this.elem.style.left = (500 - this.elem.offsetWidth / 2) + 'px';
  }
};


function init(){
  //init data
  var json = {
    "children": [ 
		{
		
				"children": [
			{
				"children": [{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"DetectionKit",
				"name":"DetectionKit"
				},
				{
				"children": [{
				"children": [{
				"children": [{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"IonIntensityMeasurementDetection",
				"name":"IonIntensityMeasurementDetection"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"SpectralCountingDetection",
				"name":"SpectralCountingDetection"
				},],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"MassSpectrometryDetection",
				"name":"MassSpectrometryDetection"
				},
{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Nmr",
				"name":"Nmr"
				},
{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"X-rayDiffractionDetection",
				"name":"X-rayDiffractionDetection"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"ChemicalShiftDetection",
				"name":"ChemicalShiftDetection"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"ThermalShiftDetection",
				"name":"ThermalShiftDetection"
				},{
				"children": [{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"PatchClampDetection",
				"name":"PatchClampDetection"
				},
{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"CurrentClampDetection",
				"name":"CurrentClampDetection"
				},

{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"VoltageClampDetection",
				"name":"VoltageClampDetection"
				},
{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Impedance",
				"name":"Impedance"
				},],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"ElectricBasedDetection",
				"name":"ElectricBasedDetection"
				},{
				"children": [{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"ReasonantWaveguideGathering",
				"name":"ReasonantWaveguideGathering"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"OpticalWaveguideGrating",
				"name":"OpticalWaveguideGrating"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"FiberOpticWaveguide",
				"name":"FiberOpticWaveguide"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"BioLayerInterferometry",
				"name":"BioLayerInterferometry"
				},],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"OpticalBasedDetection",
				"name":"OpticalBasedDetection"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"HasPlasmonResonance",
				"name":"HasPlasmonResonance"
				},],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"LabelFreeDetection",
				"name":"LabelFreeDetection"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"RadiometricBasedDetection",
				"name":"RadiometricBasedDetection"
				},{
				"children": [{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"BiolumimescenceBasedDetection",
				"name":"BiolumimescenceBasedDetection"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"ChemilumimescenceBasedDetection",
				"name":"ChemilumimescenceBasedDetection"
				},],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"EmissionBasedDetection",
				"name":"EmissionBasedDetection"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Fluorescence",
				"name":"Fluorescence"
				},{
				"children": [{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Absorptance",
				"Name":"Absorptance"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Transmittance",
				"name":"Transmittance"
				},],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"SpectroscopicBasedDetection",
				"name":"SpectroscopicBasedDetection"
				},],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Detection",
				"name":"Detection"
				},
{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"CurveFitParameter",
				"name":"CurveFitParameter"
				},
				{
				"children": [{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Phenotype",
				"name":"Phenotype"
				},
                                {
				"children": [{
				"children": [{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Reversible",
				"name":"Reversible"
				},
{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Irreversible",
				"name":"Irreversible"
				},],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"BindingModeOfAction",
				"name":"BindingModeOfAction"
				},{
				"children": [{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"InverseAgonist",
				"name":"InverseAgonist"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Antagonist",
				"name":"Antagonist"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Agonist",
				"name":"Agonist"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Activation",
				"name":"Activation"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Stimulation",
				"name":"Stimulation"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Inhibition",
				"name":"Inhibition"
				},],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"FunctionModeOfAction",
				"name":"FunctionModeOfAction"
				},],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"EndpointModeOfAction",
				"name":"EndpointModeOfAction"
				},
                                {
				"children": [{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Score",
				"name":"Score"
				},{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Quantity",
				"name":"Quantity"
				},{
				"children": [{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"ConcentrationUnit",
				"name":"ConcentrationUnit"
				},],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"PhysicalUnit",
				"name":"PhysicalUnit"
				},],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"EndpointUnit",
				"name":"EndpointUnit"
				},],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"EndpointSpecification",
				"name":"EndpointSpecification"
				},
],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"BioassayComponentSpecification",
				"name":"BioassayComponentSpecification"
				},
			{
				"children": [{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"AssayProvider",
				"name":"AssayProvider"
				},
{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"AssayQuality",
				"name":"AssayQuality"
				},
{
				"children": [],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"AssayStage",
				"name":"AssayStage"
				},
],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"BioassayDesignSpecification",
				"name":"BioassayDesignSpecification"
				},
],
				"data":
	    				 {
	     				"playcount": "276",
	     				"$color": "#8E7032",
	     				"$area": 276
	     				},
				"id":"Specification",
				"name":"Specification"
			
  		},
 ],
  "data":
      {
      "playcount": 547,
      "$area": 547
      },
 "id":"Thing",
  "name":"Thing"
 };
  //end
  //init TreeMap
  var tm = new $jit.TM.Squarified({
    //where to inject the visualization
    injectInto: 'infovis',
    //parent box title heights
    titleHeight: 15,
    //enable animations
    animate: animate,
    //box offsets
    offset: 1,
    //Attach left and right click events
    Events: {
      enable: true,
      onClick: function(node) {
        if(node) tm.enter(node);
      },
      onRightClick: function() {
        tm.out();
      }
    },
    duration: 1000,
    //Enable tips
    Tips: {
      enable: true,
      //add positioning offsets
      offsetX: 20,
      offsetY: 20,
      //implement the onShow method to
      //add content to the tooltip when a node
      //is hovered
      onShow: function(tip, node, isLeaf, domElement) {
        var html = "<div class=\"tip-title\">" + node.name 
          + "</div><div class=\"tip-text\">";
        var data = node.data;
        if(data.playcount) {
          html += "play count: " + data.playcount;
        }
        if(data.image) {
          html += "<img src=\""+ data.image +"\" class=\"album\" />";
        }
        tip.innerHTML =  html; 
      }  
    },
    //Add the name of the node in the correponding label
    //This method is called once, on label creation.
    onCreateLabel: function(domElement, node){
        domElement.innerHTML = node.name;
        var style = domElement.style;
        style.display = '';
        style.border = '1px solid transparent';
        domElement.onmouseover = function() {
          style.border = '1px solid #9FD4FF';
        };
        domElement.onmouseout = function() {
          style.border = '1px solid transparent';
        };
    }
  });
  tm.loadJSON(json);
  tm.refresh();
  //end
  //add events to radio buttons
  var sq = $jit.id('r-sq'),
      st = $jit.id('r-st'),
      sd = $jit.id('r-sd');
  var util = $jit.util;
  util.addEvent(sq, 'change', function() {
    if(!sq.checked) return;
    util.extend(tm, new $jit.Layouts.TM.Squarified);
    tm.refresh();
  });
  util.addEvent(st, 'change', function() {
    if(!st.checked) return;
    util.extend(tm, new $jit.Layouts.TM.Strip);
    tm.layout.orientation = "v";
    tm.refresh();
  });
  util.addEvent(sd, 'change', function() {
    if(!sd.checked) return;
    util.extend(tm, new $jit.Layouts.TM.SliceAndDice);
    tm.layout.orientation = "v";
    tm.refresh();
  });
  //add event to the back button
  var back = $jit.id('back');
  $jit.util.addEvent(back, 'click', function() {
    tm.out();
  });
}


var labelType, useGradients, nativeTextSupport, animate;

(function() {
  var ua = navigator.userAgent,
      iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
      typeOfCanvas = typeof HTMLCanvasElement,
      nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
      textSupport = nativeCanvasSupport 
        && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
  //I'm setting this based on the fact that ExCanvas provides text support for IE
  //and that as of today iPhone/iPad current text support is lame
  labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
  nativeTextSupport = labelType == 'Native';
  useGradients = nativeCanvasSupport;
  animate = !(iStuff || !nativeCanvasSupport);
})();

var Log = {
  elem: false,
  write: function(text){
    if (!this.elem) 
      this.elem = document.getElementById('log');
    this.elem.innerHTML = text;
    this.elem.style.left = (500 - this.elem.offsetWidth / 2) + 'px';
  }
};


var defaultJson = {
    "children": [
        {
            "children": [
                
                {
                    "children": [
{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "alphascreen",
                    "name": "alphascreen" 
                },{
                    "children": [{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "chemiluminescence resonance energy transfer",
                    "name": "chemiluminescence resonance energy transfer" 
                }
                        
                    ],
                    "data": {
                        "assaycount": "6",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "chemiluminescence",
                    "name": "chemiluminescence" 
                },{
                    "children": [{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "bioluminescence resonance energy transfer",
                    "name": "bioluminescence resonance energy transfer" 
                }
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "bioluminescence",
                    "name": "bioluminescence" 
                }
                        
                    ],
                    "data": {
                        "assaycount": "306",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "luminescence",
                    "name": "luminescence" 
                },
                {
                    "children": [ {
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "149",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "fluorescence intensity",
                    "name": "fluorescence intensity" 
                }, {
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "113",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "fluorescence resonance energy transfer",
                    "name": "fluorescence resonance energy transfer" 
                }, {
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "fluorescence cytometry",
                    "name": "fluorescence cytometry" 
                }, {
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "14",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "fluorescence polarization",
                    "name": "fluorescence polarization" 
                }, {
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "1",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "thermal shift",
                    "name": "thermal shift" 
                }
                        
                    ],
                    "data": {
                        "assaycount": "352",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "fluorescence",
                    "name": "fluorescence" 
                },
                {
                    "children": [{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "transmittance",
                    "name": "transmittance" 
                },{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "17",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "absorbance",
                    "name": "absorbance" 
                }
                        
                    ],
                    "data": {
                        "assaycount": "28",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "spectrophotometry",
                    "name": "spectrophotometry" 
                },
                {
                    "children": [{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "scintillation proximity assay",
                    "name": "scintillation proximity assay" 
                },

{
                            "children": [
                                
                            ],
                            "data": {
                                "assaycount": "1",
                                "$color": "#6f8dff",
                                "$area": 276 
                            },
                            "id": "lysate based",
                            "name": "lysate based" 
                        },
{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "9",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "filter assay",
                    "name": "filter assay" 
                }
                        
                    ],
                    "data": {
                        "assaycount": "10",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "scintillation counting",
                    "name": "scintillation counting" 
                },
                {
                    "children": [{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "quartz crystal microbalance",
                    "name": "quartz crystal microbalance" 
                },{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "1",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "isothermal titration calorimetry",
                    "name": "isothermal titration calorimetry" 
                },{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "x ray crystallography",
                    "name": "x ray crystallography" 
                },{
                    "children": [{
                    "children": [
                        {
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "voltage clamp",
                    "name": "voltage clamp" 
                },{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "patch clamp",
                    "name": "patch clamp" 
                },{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "current clamp",
                    "name": "current clamp" 
                }
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "microelectrode measurement",
                    "name": "microelectrode measurement" 
                },{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "carbon nanotube based sensor",
                    "name": "carbon nanotube based sensor" 
                },{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "impedance",
                    "name": "impedance" 
                }
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "electrical sensor",
                    "name": "electrical sensor" 
                },{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "circular dichroism",
                    "name": "circular dichroism" 
                },{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "1",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "nuclear magnetic resonance",
                    "name": "nuclear magnetic resonance" 
                },{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "optical based",
                    "name": "optical based" 
                },{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "2",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "mass spectrometry",
                    "name": "mass spectrometry" 
                }
                        
                    ],
                    "data": {
                        "assaycount": "4",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "label free technology",
                    "name": "label free technology" 
                },
                {
                    "children": [
                          {
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "scanning probe microscopy",
                    "name": "scanning probe microscopy" 
                },  {
                    "children": [{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "phase contrast microscopy",
                    "name": "phase contrast microscopy" 
                },{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "brightfield microscopy",
                    "name": "brightfield microscopy" 
                },{
                    "children": [{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "total internal reflection fluorescence microscopy",
                    "name": "total internal reflection fluorescence microscopy" 
                },{
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "confocal microscopy",
                    "name": "confocal microscopy" 
                }
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "fluorescence microscopy",
                    "name": "fluorescence microscopy" 
                }
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "optical microscopy",
                    "name": "optical microscopy" 
                },  {
                    "children": [ {
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "1",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "transmission electron microscopy",
                    "name": "transmission electron microscopy" 
                }, {
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "scanning electron microscopy",
                    "name": "scanning electron microscopy" 
                }
                        
                    ],
                    "data": {
                        "assaycount": "1",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "electron microscopy",
                    "name": "electron microscopy" 
                }
                    ],
                    "data": {
                        "assaycount": "1",
                        "$color": "#6f8dff",
                        "$area": 276 
                    },
                    "id": "microscopy",
                    "name": "microscopy" 
                } 
            ],
            "data": {
                "assaycount": "694",
                "$color": "#6f8dff",
                "$area": 276 
            },
            "id": "detection technology",
            "name": "detection technology" 
        } 
    ],
    "data": {
        "assaycount": 695,
        "$area": 695 
    },
    "id": "Thing",
    "name": "Thing" 
};  //end

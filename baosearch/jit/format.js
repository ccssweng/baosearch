var labelType, useGradients, nativeTextSupport, animate;

(function() {
  var ua = navigator.userAgent,
      iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
      typeOfCanvas = typeof HTMLCanvasElement,
      nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
      textSupport = nativeCanvasSupport 
        && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
  //I'm setting this based on the fact that ExCanvas provides text support for IE
  //and that as of today iPhone/iPad current text support is lame
  labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
  nativeTextSupport = labelType == 'Native';
  useGradients = nativeCanvasSupport;
  animate = !(iStuff || !nativeCanvasSupport);
})();

var Log = {
  elem: false,
  write: function(text){
    if (!this.elem) 
      this.elem = document.getElementById('log');
    this.elem.innerHTML = text;
    this.elem.style.left = (500 - this.elem.offsetWidth / 2) + 'px';
  }
};


  var defaultJson ={
    "children": [
        {
            "children": [
                {
                  
                    "data": {
			"assaycount": "2",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "tissue-based",
                    "name": "tissue-based" 
                },
          {
                    "children": [
                        {
                            "children": [
                              {
                                    "children": [
                                        
                                    ],
                                    "data": {
					"assaycount": "0",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "nucleosome",
                                    "name": "nucleosome" 
                                },
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
					"assaycount": "0",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "cytosol",
                                    "name": "cytosol" 
                                },
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
					"assaycount": "0",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "mitochondrion",
                                    "name": "mitochondrion" 
                                },
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
					"assaycount": "0",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "microsome",
                                    "name": "microsome" 
                                },
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
					"assaycount": "0",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "nuclear extract",
                                    "name": "nuclear extract" 
                                },
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
					"assaycount": "8",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "cell membrane",
                                    "name": "cell membrane" 
                                } 
                            ],
                            "data": {
				"assaycount": "8",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "subcellular",
                            "name": "subcellular" 
                        },
                        {
                            "children": [
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
					"assaycount": "3",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "rabbit reticulocyte lysate",
                                    "name": "rabbit reticulocyte lysate" 
                                } 
                            ],
                            "data": {
				"assaycount": "3",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "whole cell lysate",
                            "name": "whole cell lysate" 
                        } 
                    ],
                    "data": {
			"assaycount": "11",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "cell-free",
                    "name": "cell-free" 
                },
                {
                    "children": [
                        {
                            "children": [
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
					"assaycount": "209",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "single protein",
                                    "name": "single protein" 
                                },
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
					"assaycount": "36",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "protein complex",
                                    "name": "protein complex" 
                                } 
                            ],
                            "data": {
				"assaycount": "245",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "protein format",
                            "name": "protein format" 
                        },
                        {
                            "children": [
                                
                            ],
                            "data": {
				"assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "nucleic acid format",
                            "name": "nucleic acid format" 
                        } 
                    ],
                    "data": {
			"assaycount": "245",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "biochemical",
                    "name": "biochemical" 
                },
                {
                    "children": [
                        
                    ],
                    "data": {
			"assaycount": "8",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "organism-based",
                    "name": "organism-based" 
                } ,
                {
                    "children": [
                        
                    ],
                    "data": {
			"assaycount": "0",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "physicochemical",
                    "name": "physicochemical" 
                } ,
                {
                    "children": [
                        
                    ],
                    "data": {
			"assaycount": "430",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "cell-based",
                    "name": "cell-based" 
                }
            ],
            "data": {
		"assaycount": "695",
                "$color": "#8E7032",
                "$area": 100 
            },
            "id": "format",
            "name": "format" 
        } 
    ],
    "data": {
        "$area": 100 
    },
    "id": "Thing",
    "name": "Thing" 
};


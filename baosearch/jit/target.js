var labelType, useGradients, nativeTextSupport, animate;

(function() {
  var ua = navigator.userAgent,
      iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
      typeOfCanvas = typeof HTMLCanvasElement,
      nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
      textSupport = nativeCanvasSupport 
        && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
  //I'm setting this based on the fact that ExCanvas provides text support for IE
  //and that as of today iPhone/iPad current text support is lame
  labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
  nativeTextSupport = labelType == 'Native';
  useGradients = nativeCanvasSupport;
  animate = !(iStuff || !nativeCanvasSupport);
})();

var Log = {
  elem: false,
  write: function(text){
    if (!this.elem) 
      this.elem = document.getElementById('log');
    this.elem.innerHTML = text;
    this.elem.style.left = (500 - this.elem.offsetWidth / 2) + 'px';
  }
};


var defaultJson = {
    "children": [
        {
            "children": [
                {
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "237",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "biological process",
                    "name": "biological process" 
                },
                {
                    "children": [
                        
                    ],
                    "data": {
                        "assaycount": "52",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "signaling pathway",
                    "name": "signaling pathway" 
                },
                {
                    "children": [
                        {
                            "children": [
                                {
                                    "children": [
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "28",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "oxidoreductase",
                                            "name": "oxidoreductase" 
                                        },
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "lyase",
                                            "name": "lyase" 
                                        },
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "ligase",
                                            "name": "ligase" 
                                        },
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "2",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "isomerase",
                                            "name": "isomerase" 
                                        },
                                        {
                                            "children": [
                                                {
                                                    "children": [
                                                        
                                                    ],
                                                    "data": {
                                                        "assaycount": "0",
                                                        "$color": "#6f8dff",
                                                        "$area": 100 
                                                    },
                                                    "id": "generic transferase",
                                                    "name": "generic transferase" 
                                                },
                                                {
                                                    "children": [
                                                        
                                                    ],
                                                    "data": {
                                                        "assaycount": "22",
                                                        "$color": "#6f8dff",
                                                        "$area": 100 
                                                    },
                                                    "id": "phosphatase",
                                                    "name": "phosphatase" 
                                                },
                                                {
                                                    "children": [
                                                        
                                                    ],
                                                    "data": {
                                                        "assaycount": "36",
                                                        "$color": "#6f8dff",
                                                        "$area": 100 
                                                    },
                                                    "id": "kinase",
                                                    "name": "kinase" 
                                                } 
                                            ],
                                            "data": {
                                                "assaycount": "56",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "transferase",
                                            "name": "transferase" 
                                        },
                                        {
                                            "children": [
                                                {
                                                    "children": [
                                                        
                                                    ],
                                                    "data": {
                                                        "assaycount": "46",
                                                        "$color": "#6f8dff",
                                                        "$area": 100 
                                                    },
                                                    "id": "generic hydrolase",
                                                    "name": "generic hydrolase" 
                                                } 
                                            ],
                                            "data": {
                                                "assaycount": "74",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "hydrolase",
                                            "name": "hydrolase" 
                                        } 
                                    ],
                                    "data": {
                                        "assaycount": "160",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "enzyme",
                                    "name": "enzyme" 
                                },
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
                                        "assaycount": "0",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "transporter",
                                    "name": "transporter" 
                                } ,
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
                                        "assaycount": "0",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "adhesion",
                                    "name": "adhesion" 
                                } ,
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
                                        "assaycount": "4",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "enzyme regulator",
                                    "name": "enzyme regulator" 
                                } ,
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
                                        "assaycount": "8",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "secreted",
                                    "name": "secreted" 
                                } ,
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
                                        "assaycount": "8",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "transcription factor",
                                    "name": "transcription factor" 
                                } ,
                                {
                                    "children": [
                                        
                                    ],
                                    "data": {
                                        "assaycount": "1",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "structural",
                                    "name": "structural" 
                                } ,
                                {
                                    "children": [
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "g protein",
                                            "name": "g protein" 
                                        } 
                                    ],
                                    "data": {
                                        "assaycount": "51",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "membrane protein",
                                    "name": "membrane protein" 
                                } ,
                                {
                                    "children": [
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "chaperon",
                                            "name": "chaperon" 
                                        } 
                                    ],
                                    "data": {
                                        "assaycount": "20",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "cytosolic protein",
                                    "name": "cytosolic protein" 
                                },
                                {
                                    "children": [
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "28",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "nuclear receptor",
                                            "name": "nuclear receptor" 
                                        },
                                        {
                                            "children": [
                                                {
                                                    "children": [
                                                        
                                                    ],
                                                    "data": {
                                                        "assaycount": "218",
                                                        "$color": "#6f8dff",
                                                        "$area": 100 
                                                    },
                                                    "id": "g protein coupled receptor",
                                                    "name": "g protein coupled receptor" 
                                                } 
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "transmembrane receptor",
                                            "name": "transmembrane receptor" 
                                        } ,
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "14",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "ion channel",
                                            "name": "ion channel" 
                                        } 
                                    ],
                                    "data": {
                                        "assaycount": "260",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "receptor",
                                    "name": "receptor" 
                                } 
                            ],
                            "data": {
                                "assaycount": "493",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "protein target",
                            "name": "protein target" 
                        },
                        {
                            "children": [
                                {
                                    "children": [
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "chromosomal",
                                            "name": "chromosomal" 
                                        },
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "extra chromosomal",
                                            "name": "extra chromosomal" 
                                        } 
                                    ],
                                    "data": {
                                        "assaycount": "0",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "dna",
                                    "name": "dna" 
                                },
                                {
                                    "children": [
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "trna",
                                            "name": "trna" 
                                        },
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "rrna",
                                            "name": "rrna" 
                                        },
                                        {
                                            "children": [
                                                
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "mirna",
                                            "name": "mirna" 
                                        } ,
                                        {
                                            "children": [
                                                {
                                                    "children": [
                                                        
                                                    ],
                                                    "data": {
                                                        "assaycount": "0",
                                                        "$color": "#6f8dff",
                                                        "$area": 100 
                                                    },
                                                    "id": "riboswitch",
                                                    "name": "riboswitch" 
                                                } 
                                            ],
                                            "data": {
                                                "assaycount": "0",
                                                "$color": "#6f8dff",
                                                "$area": 100 
                                            },
                                            "id": "mrna",
                                            "name": "mrna" 
                                        } 
                                    ],
                                    "data": {
                                        "assaycount": "0",
                                        "$color": "#6f8dff",
                                        "$area": 100 
                                    },
                                    "id": "rna",
                                    "name": "rna" 
                                } 
                            ],
                            "data": {
                                "assaycount": "0",
                                "$color": "#6f8dff",
                                "$area": 100 
                            },
                            "id": "nucleic acid",
                            "name": "nucleic acid" 
                        } 
                    ],
                    "data": {
                        "assaycount": "493",
                        "$color": "#6f8dff",
                        "$area": 100 
                    },
                    "id": "molecular target",
                    "name": "molecular target" 
                } 
            ],
            "data": {
                "assaycount": "729",
                "$color": "#6f8dff",
                "$area": 100 
            },
            "id": "meta target",
            "name": "meta target" 
        } 
    ],
    "data": {
        
        "$area": 100 
    },
    "id": "Thing",
    "name": "Thing" 
};  //end

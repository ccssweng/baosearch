package edu.miami.ccs.bao;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        jacksonTest();
    }

    private static void jacksonTest() {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> userData = new HashMap<String, Object>();
        Map<String, String> nameStruct = new HashMap<String, String>();
        nameStruct.put("first", "Joe");
        nameStruct.put("last", "Sixpack");
        userData.put("name", nameStruct);
        userData.put("gender", "MALE");
        userData.put("verified", Boolean.FALSE);
        userData.put("userImage", "Rm9vYmFyIQ==");
        try {
            mapper.writeValue(System.out, userData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package edu.miami.ccs.bao.controller;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.RDFS;
import edu.cornell.mannlib.vitro.webapp.beans.*;
import edu.cornell.mannlib.vitro.webapp.controller.VitroHttpServlet;
import edu.cornell.mannlib.vitro.webapp.controller.VitroRequest;
import edu.cornell.mannlib.vitro.webapp.dao.*;
import edu.cornell.mannlib.vitro.webapp.flags.PortalFlag;
import edu.cornell.mannlib.vitro.webapp.search.SearchException;
import edu.cornell.mannlib.vitro.webapp.search.beans.Searcher;
import edu.cornell.mannlib.vitro.webapp.search.beans.VitroHighlighter;
import edu.cornell.mannlib.vitro.webapp.search.beans.VitroQuery;
import edu.cornell.mannlib.vitro.webapp.search.beans.VitroQueryFactory;
import edu.cornell.mannlib.vitro.webapp.search.lucene.Entity2LuceneDoc;
import edu.cornell.mannlib.vitro.webapp.search.lucene.LuceneIndexer;
import edu.cornell.mannlib.vitro.webapp.search.lucene.LuceneSetup;
import edu.cornell.mannlib.vitro.webapp.search.lucene.VitroQueryParser;
import edu.cornell.mannlib.vitro.webapp.utils.FlagMathUtils;
import edu.cornell.mannlib.vitro.webapp.utils.Html2Text;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.codehaus.jackson.map.ObjectMapper;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * This is the servlet that provide search results for BAOSearch with the given output.
 * This code may subject to change.
 *
 * @author Saminda Abeyruwan
 * @version 1.0
 * @since 05/27/2010
 */
public class BaoPagedSearchController extends VitroHttpServlet implements Searcher {

    private static final Log log = LogFactory.getLog(BaoPagedSearchController.class.getName());

    private IndexSearcher searcher = null;

    private int defaultHitsPerPage = 25;

    private int defaultMaxSearchSize = 4000;

    private final String defaultSearchField = "ALLTEXT";

    /**
     * To get the superclass restricted types if not directly linked in the individual
     */
    private WebappDaoFactory myAssertionsWebappDaoFactory;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        LuceneIndexer indexer = (LuceneIndexer) getServletContext()
                .getAttribute(LuceneIndexer.class.getName());
        indexer.addSearcher(this);

        try {
            String indexDir = getIndexDir(getServletContext());
            getIndexSearcher(indexDir);
        } catch (Exception ex) {
            log.warn("An error throws when the index is created in the init method", ex);
        }
    }

    /*
    Get the directory of the index files
     */

    private String getIndexDir(ServletContext servletContext) throws SearchException {
        Object obj = servletContext.getAttribute(LuceneSetup.INDEX_DIR);
        if (obj == null || !(obj instanceof String)) {
            throw new SearchException("Could not get IndexDir for luecene index");
        } else {
            return (String) obj;
        }
    }

    /*
    Main object where search is conducted on
     */

    private synchronized IndexSearcher getIndexSearcher(String indexDir) {
        if (searcher == null) {
            try {
                Directory fsDir = FSDirectory.getDirectory(indexDir);
                searcher = new IndexSearcher(fsDir);
            } catch (IOException e) {
                log.error("LuceneSearcher: could not make indexSearcher " + e);
                log.error(
                        "It is likely that you have not made a directory for the lucene index.  " +
                        "Create the directory indicated in the error and set " +
                        "permissions/ownership so" +
                        " that the tomcat server can read/write to it.");
                //The index directory is created by LuceneIndexer.makeNewIndex()
            }
        }
        return searcher;
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        //Implementation of the search logic
        ObjectMapper jsonMapper = new ObjectMapper();
        // this is the main structure that glues the JSON message
        // send to the client
        Map<String, Object> userData = new HashMap<String, Object>();
        try {
            super.doGet(request, response);
            VitroRequest vreq = new VitroRequest(request);
            Portal portal = vreq.getPortal();
            PortalFlag portalFlag = vreq.getPortalFlag();

            //make sure a IndividualDao is available
            if (vreq.getWebappDaoFactory() == null
                || vreq.getWebappDaoFactory().getIndividualDao() == null) {
                log.error("makeUsableBeans() could not get IndividualDao ");
                doSearchError("Could not access Model", portalFlag, userData);
                writeJsonOut(response, jsonMapper, userData);
                return;
            }
            IndividualDao iDao = vreq.getWebappDaoFactory().getIndividualDao();
            VClassGroupDao grpDao = vreq.getWebappDaoFactory().getVClassGroupDao();
            VClassDao vclassDao = vreq.getWebappDaoFactory().getVClassDao();
            String alphaFilter = vreq.getParameter(BaoPagedSearchConstants.ALPHA);

            // if supported, we want to show only the asserted superclasses and subclasses.
            // Don't want to see anonymous classes, restrictions, etc.
            VClassDao vcDao;
            if (getAssertionsWebappDaoFactory() != null) {
                vcDao = getAssertionsWebappDaoFactory().getVClassDao();
            } else {
                vcDao = getWebappDaoFactory().getVClassDao();
            }


            int startIndex = 0;
            try {
                startIndex =
                        Integer.parseInt(request.getParameter(BaoPagedSearchConstants.START_INDEX));
            } catch (Throwable e) {
                startIndex = 0;
            }

            int hitsPerPage = defaultHitsPerPage;
            try {
                hitsPerPage = Integer.parseInt(
                        request.getParameter(BaoPagedSearchConstants.HIT_PER_PAGE));
            } catch (Throwable e) {
                hitsPerPage = defaultHitsPerPage;
            }

            int maxHitSize = defaultMaxSearchSize;
            if (startIndex >= defaultMaxSearchSize - hitsPerPage) {
                maxHitSize = startIndex + defaultMaxSearchSize;
            }
            if (alphaFilter != null) {
                maxHitSize = maxHitSize * 2;
                hitsPerPage = maxHitSize;
            }

            String indexDir = getIndexDir(getServletContext());

            String qtxt = vreq.getParameter(VitroQuery.QUERY_PARAMETER_NAME);
            Analyzer analyzer = getAnalyzer(getServletContext());
            Query query = getQuery(vreq, portalFlag, analyzer, indexDir);
            log.debug("BaoSearch query for '" + qtxt + "' is " + query.toString());

            if (query == null) {
                doNoQuery(request, userData);
                writeJsonOut(response, jsonMapper, userData);
                return;
            }

            IndexSearcher searcherForRequest = getIndexSearcher(indexDir);

            TopDocs topDocs;
            try {
                topDocs = searcherForRequest.search(query, null, maxHitSize);
            } catch (Throwable t) {
                log.error("in first pass at search: " + t);
                // this is a hack to deal with odd cases where search and index threads interact
                try {
                    wait(150);
                    topDocs = searcherForRequest.search(query, null, maxHitSize);
                } catch (Exception ex) {
                    log.error(ex);
                    String msg = makeBadSearchMessage(qtxt, ex.getMessage());
                    if (msg == null) {
                        msg = "The search request contained errors";
                    }
                    doFailedSearch(request, msg, qtxt, userData);
                    writeJsonOut(response, jsonMapper, userData);
                    return;
                }
            }

            if (topDocs == null || topDocs.scoreDocs == null) {
                log.error("topDocs for a search was null");
                String msg = "The search request contained errors";
                doFailedSearch(request, msg, qtxt, userData);
                writeJsonOut(response, jsonMapper, userData);
                return;
            }

            int hitsLength = topDocs.scoreDocs.length;
            if (hitsLength < 1) {
                doFailedSearch(request, BaoPagedSearchConstants.NO_RESULT_MSG, qtxt, userData);
                writeJsonOut(response, jsonMapper, userData);
                return;
            }
            log.debug("BAOSearch found " + hitsLength + " hits");

            // paging
            int lastHitToShow = 0;
            if ((startIndex + hitsPerPage) > hitsLength) {
                lastHitToShow = hitsLength;
            } else {
                lastHitToShow = startIndex + hitsPerPage - 1;
            }

            List<Individual> beans = new LinkedList<Individual>();
            for (int i = startIndex; i < topDocs.scoreDocs.length; i++) {
                try {
                    if ((i >= startIndex) && (i <= lastHitToShow)) {
                        Document doc = searcherForRequest.doc(topDocs.scoreDocs[i].doc);
                        String uri = doc.get(Entity2LuceneDoc.term.URI);
                        Individual ent = new IndividualImpl();
                        ent.setURI(uri);
                        ent = iDao.getIndividualByURI(uri);
                        if (ent != null) {
                            beans.add(ent);
                        }
                    }
                } catch (Exception e) {
                    log.error("problem getting usable Individuals from search " +
                              "hits" + e.getMessage());
                }
            }
            //search request for no classgroup and no type so add classgroup search refinement links.
            if (request.getParameter(BaoPagedSearchConstants.CLASS_GROUP) == null &&
                request.getParameter(BaoPagedSearchConstants.RDF_TYPE) == null) {
                Map<String, Integer> individualCountForEachGroup = new HashMap<String, Integer>();
                List<VClassGroup> classGroupList =
                        getClassGroups(grpDao, topDocs, searcherForRequest,
                                       individualCountForEachGroup);
                List<Map<String, Object>> baoTypesList = new ArrayList<Map<String, Object>>();
                for (Iterator<VClassGroup> iterator = classGroupList.iterator();
                     iterator.hasNext(); ) {
                    Map<String, Object> typeInfo = new HashMap<String, Object>();
                    VClassGroup group = iterator.next();
                    typeInfo.put(BaoPagedSearchConstants.JsonCode.TAB_NAME,
                                 StringEscapeUtils.escapeHtml(group.getPublicName()));
                    typeInfo.put(BaoPagedSearchConstants.CLASS_GROUP, group.getURI());
                    typeInfo.put(BaoPagedSearchConstants.JsonCode.TAB_COUNT,
                                 individualCountForEachGroup.get(group.getURI()));
                    baoTypesList.add(typeInfo);
                }
                // fail safe method
                if (individualCountForEachGroup.size() == 0) {
                    Map<String, Object> typeInfo = new HashMap<String, Object>();
                    typeInfo.put(BaoPagedSearchConstants.JsonCode.TAB_COUNT, 0);
                    baoTypesList.add(typeInfo);
                }
                userData.put(BaoPagedSearchConstants.JsonCode.TABS, baoTypesList);
                request.setAttribute(BaoPagedSearchConstants.CLASS_GROUPS, classGroupList);
                request.setAttribute("refinment", "");
            } else {

                //search request for a classgroup so add rdf:type search refinement links
                //but try to filter out classes that are subclasses
                if (request.getParameter(BaoPagedSearchConstants.CLASS_GROUP) != null &&
                    request.getParameter(BaoPagedSearchConstants.RDF_TYPE) == null) {
                    //TODO: set
                    List<VClass> vClassesList = getVClasses(vclassDao, topDocs, searcherForRequest);
                    List<Map<String, Object>> baoTypesList = new ArrayList<Map<String, Object>>();
                    for (Iterator<VClass> vcIterator = vClassesList.iterator();
                         vcIterator.hasNext(); ) {
                        Map<String, Object> typeInfo = new HashMap<String, Object>();
                        VClass vClass = vcIterator.next();
                        typeInfo.put(BaoPagedSearchConstants.JsonCode.TAB_NAME,
                                     StringEscapeUtils.escapeHtml(vClass.getName()));
                        typeInfo.put(BaoPagedSearchConstants.JsonCode.TYPE, vClass.getURI());
                        typeInfo.put(BaoPagedSearchConstants.JsonCode.TAB_COUNT, beans.size());
                        baoTypesList.add(typeInfo);

                    }
                    userData.put(BaoPagedSearchConstants.JsonCode.TABS, baoTypesList);
                    /*request.setAttribute(BaoPagedSearchConstants.RDF_TYPE,
                            vClassesList);*/
                    //TODO: set
                    /*request.setAttribute("refinment", "&classgroup="
                            + URLEncoder
                            .encode(request.getParameter(BaoPagedSearchConstants.CLASS_GROUP),
                                    "UTF-8"));*/
                } else {
                    /*if (alphaFilter != null && !"".equals(alphaFilter)) {
                        //TODO: set
                        request.setAttribute(BaoPagedSearchConstants.ALPHAS,
                                getAlphas(topDocs, searcherForRequest));
                        alphaSortIndividuals(beans);
                    } else {
                        //TODO: set
                        request.setAttribute("refinment", "&type="
                                + URLEncoder.encode(request.getParameter("type"), "UTF-8"));
                    }*/
                }
            }

            beans = highlightBeans(beans,
                                   vreq.getWebappDaoFactory().getDataPropertyDao(),
                                   vreq.getWebappDaoFactory().getObjectPropertyDao(),
                                   new BaoHighlighter(query, analyzer));

            //stick the results in the requestScope and prepare to forward to JSP
            //TODO: set
            request.setAttribute("beans", beans);
            // total grid concepts and their types. All restrictions will be ignored.
            // This is semantically incorrect throw
            Map<String, Integer> gridConceptsCnt = new HashMap<String, Integer>();
            List<String> dataTypePropertyName = new ArrayList<String>();
            List<String> objectTypePropertyName = new ArrayList<String>();
            List<Object> individuals = new ArrayList<Object>();
            for (Iterator<Individual> individualIterator = beans.iterator();
                 individualIterator.hasNext(); ) {
                Individual individual = individualIterator.next();
                Map<String, Object> individualMap = new HashMap<String, Object>();
                // data type properties
                individualMap.put(BaoPagedSearchConstants.JsonCode.BAO_NAME,
                                  limitedLength(individual.getName()));
                List<String> complexConcept = new ArrayList<String>();
                for (Iterator<VClass> vClassIterator = individual.getVClasses(true).iterator();
                     vClassIterator.hasNext(); ) {
                    VClass vClass = vClassIterator.next();
                    if (!(vClass.getName().indexOf("restriction on") > -1)) {
                        complexConcept.add(vClass.getName());
                        if (gridConceptsCnt.containsKey(vClass.getName())) {
                            gridConceptsCnt.put(vClass.getName(),
                                                gridConceptsCnt.get(vClass.getName()) + 1);
                        } else {
                            gridConceptsCnt.put(vClass.getName(), 1);
                        }
                    }
                }
                if (complexConcept.size() > 0) {
                    individualMap.put(BaoPagedSearchConstants.JsonCode.TYPE, complexConcept);
                    dataTypePropertyName.add(BaoPagedSearchConstants.JsonCode.TYPE);
                } else {
                    individualMap.put(BaoPagedSearchConstants.JsonCode.TYPE, individual.getName());
                    dataTypePropertyName.add(BaoPagedSearchConstants.JsonCode.TYPE);
                }

                /**
                 * populate stuff related to individual
                 */
                List<Map<String, Object>> mainTypeList =
                        (List<Map<String, Object>>) userData
                                .get(BaoPagedSearchConstants.JsonCode.TABS);
                // add the superclasses from the asserted model
                OntModel ontModel = vreq.getJenaOntModel();
                List<String> indiSupperClasses = new ArrayList<String>();
                List<VClass> vClassList = individual.getVClasses(true);
                for (VClass vClass : vClassList) {
                    String specificType = vClass.getURI();
                    OntClass clazz = ontModel.getOntClass(specificType);
                    for (ExtendedIterator<OntClass> i = clazz.listSuperClasses(false);
                         i.hasNext(); ) {
                        OntClass c = i.next();
                        if (c.isClass()) {
                            Statement anLabel = c.getProperty(RDFS.label);
                            if (anLabel != null && anLabel.getLiteral() != null &&
                                anLabel.getLiteral().getString() != null) {
                                indiSupperClasses.add(anLabel.getLiteral().getString());
                            }
                        }
                    }
                }
                individualMap.put("superClasses", indiSupperClasses);
                individualJsonPopulate(
                        individualMap,
                        individual,
                        vreq,
                        vclassDao,
                        mainTypeList,
                        dataTypePropertyName,
                        objectTypePropertyName,
                        gridConceptsCnt);
                individuals.add(individualMap);
            }
            List<Map<String, String>> listOfGridConceptCntList =
                    new ArrayList<Map<String, String>>();
            Set<Map.Entry<String, Integer>> entries = gridConceptsCnt.entrySet();
            for (Iterator<Map.Entry<String, Integer>> entryIterator = entries.iterator();
                 entryIterator.hasNext(); ) {
                Map.Entry<String, Integer> integerEntry = entryIterator.next();
                Map<String, String> mapEntry = new HashMap<String, String>();
                mapEntry.put("gridName", integerEntry.getKey());
                mapEntry.put("gridCount", integerEntry.getValue().toString());
                listOfGridConceptCntList.add(mapEntry);
            }
            // not used
//            userData.put("baoGrid", listOfGridConceptCntList);
            Map<Integer, String> cumulativePropertyNames = new HashMap<Integer, String>();
            for (String v : dataTypePropertyName) {
                if (!cumulativePropertyNames.containsValue(v)) {
                    cumulativePropertyNames.put(cumulativePropertyNames.size(), v);
                }
            }
            for (String v : objectTypePropertyName) {
                if (!cumulativePropertyNames.containsValue(v)) {
                    cumulativePropertyNames.put(cumulativePropertyNames.size(), v);
                }
            }
//            userData.put("baoDataTypeColumn", dataTypePropertyName);
//            userData.put("baoObjectTypeColumn", objectTypePropertyName);
            userData.put(BaoPagedSearchConstants.JsonCode.COLUMN_NAMES, cumulativePropertyNames);
//            userData.put("baoTotalCount", beans.size());
            userData.put(BaoPagedSearchConstants.JsonCode.INDIVIDUALS, individuals);

            userData.put(BaoPagedSearchConstants.JsonCode.SEARCH_TITLE,
                         qtxt + " - " + portal.getAppName() + " Search Results");
            userData.put(BaoPagedSearchConstants.JsonCode.QUERY_TEXT, qtxt);
            userData.put(BaoPagedSearchConstants.JsonCode.START_INDEX, startIndex);
            userData.put("hitsPerPage", hitsPerPage);
            userData.put(BaoPagedSearchConstants.JsonCode.HITS_LENGTH, hitsLength);
            userData.put(BaoPagedSearchConstants.JsonCode.MAX_HIT_SIZE, maxHitSize);


            String param = request.getParameter(BaoPagedSearchConstants.CLASS_GROUP);
            if (param != null && !"".equals(param)) {
                VClassGroup grp = grpDao.getGroupByURI(param);
                if (grp != null && grp.getPublicName() != null) {
                    userData.put(BaoPagedSearchConstants.CLASS_GROUP_NAME, grp.getPublicName());
                }
            }
            param = request.getParameter(BaoPagedSearchConstants.RDF_TYPE);
            if (param != null && !"".equals(param)) {
                VClass type = vclassDao.getVClassByURI(param);
                if (type != null && type.getName() != null) {
                    userData.put(BaoPagedSearchConstants.RDF_TYPE_NAME, type.getName());
                }
            }
            writeJsonOut(response, jsonMapper, userData);
        } catch (Throwable e) {
            log.error("SearchController.doGet(): " + e);
            doSearchError(e.getMessage(), null, userData);
            writeJsonOut(response, jsonMapper, userData);
            return;
        }
    }

    /**
     * gets assertions-only WebappDaoFactory with no filtering
     */
    public WebappDaoFactory getAssertionsWebappDaoFactory() {
        return (myAssertionsWebappDaoFactory != null) ? myAssertionsWebappDaoFactory :
               (WebappDaoFactory) getServletContext().getAttribute("assertionsWebappDaoFactory");
    }

    private void writeJsonOut(HttpServletResponse response,
                              ObjectMapper jsonMapper,
                              Map<String, Object> userData) throws IOException {
        jsonMapper.writeValue(response.getOutputStream(), userData);
    }

    private String limitedLength(String value) {
        if (value.length() > 130) {
            return value.substring(0, 130);
        }
        return value;
    }

    private void individualJsonPopulate(Map<String, Object> individualMap,
                                        Individual individual,
                                        VitroRequest vreq,
                                        VClassDao vcDao,
                                        List<Map<String, Object>> baoTypesList,
                                        List<String> dataTypePropertyName,
                                        List<String> objectTypePropertyName,
                                        Map<String, Integer> gridConceptsCnt) {
        // TODO: add other static name-value pairs if necessary. This should be configured through
        // TODO: XML config file.
        /*if (individual.getBlurb() != null) {
            individualMap.put("baoBlurb", individual.getBlurb());
        }
        if (individual.getDescription() != null) {
            individualMap.put("baoDescription", individual.getDescription());
        }
        if (individual.getCitation() != null) {
            individualMap.put("baoCitation", individual.getCitation());
        }*/
        if (individual.getURI() != null) {
            individualMap.put(BaoPagedSearchConstants.JsonCode.URI, individual.getURI());
            if (!dataTypePropertyName.contains(BaoPagedSearchConstants.JsonCode.URI)) {
                dataTypePropertyName.add(BaoPagedSearchConstants.JsonCode.URI);
            }
        }
        //data type properties
        if (individual.getDataPropertyList() != null) {
            for (Iterator<DataProperty> iterator = individual.getDataPropertyList().iterator();
                 iterator.hasNext(); ) {
                DataProperty dp = iterator.next();
                try {
                    if (getDataPropertyBlacklist().contains(dp.getURI())) {
                        continue;
                    }

                    for (DataPropertyStatement dps : dp.getDataPropertyStatements()) {
                        if (dp.getPublicName().startsWith("has_")) {
                            // remove all internal dp from displaying
                            continue;
                        }
                        if (dp.getPublicName().equals("has display info")) {
                            // skip
                            continue;
                        }
                        if (dp.getPublicName().equals("has specification")) {
                            // skip
                            continue;
                        }
                        /*if (dp.getPublicName().equals("has format")) {
                            // skip
                            continue;
                        }*/
                        if (dp.getPublicName().equals("has measure group")) {
                            // skip
                            continue;
                        }
                        if (dp.getPublicName().equals("has perturbagen")) {
                            // skip
                            continue;
                        }
                        if (dataTypePropertyName.contains(dp.getPublicName())) {
                            // data type already in the list
                            List<String> dpLiteralList =
                                    (List<String>) individualMap.get(dp.getPublicName());
                            if (dpLiteralList != null) {
                                dpLiteralList.add(limitedLength(dps.getData()));
                            } else {
                                // this is a new entry
                                dpLiteralList = new ArrayList<String>();
                                dpLiteralList.add(limitedLength(dps.getData()));
                                individualMap.put(dp.getPublicName(), dpLiteralList);
                                dataTypePropertyName.add(dp.getPublicName());
                            }
                        } else {
                            // this is a new entry
                            List<String> dpLiteralList = new ArrayList<String>();
                            dpLiteralList.add(limitedLength(dps.getData()));
                            individualMap.put(dp.getPublicName(), dpLiteralList);
                            dataTypePropertyName.add(dp.getPublicName());
                        }
                    }
                } catch (Throwable e) {
                    log.debug("Error highlighting data property statment " +
                              "for individual " + individual.getURI());
                }
            }
        }

        if (individual.getObjectPropertyList() != null) {
            for (Iterator<ObjectProperty> iterator = individual.getObjectPropertyList().iterator();
                 iterator.hasNext(); ) {
                ObjectProperty op = iterator.next();
                try {
                    if (getObjectPropertyBlacklist().contains(op.getURI())) {
                        continue;
                    }
                    if (op.getDomainPublic().startsWith("has_")) {
                        // skip
                        continue;
                    }
                    if (op.getDomainPublic().equals("has specification")) {
                        // skip
                        continue;
                    }
                    /*if (op.getDomainPublic().equals("has format")) {
                        // skip
                        continue;
                    }*/
                    if (op.getDomainPublic().equals("has measure group")) {
                        // skip
                        continue;
                    }
                    if (op.getDomainPublic().equals("has perturbagen")) {
                        // skip
                        continue;
                    }

                    for (ObjectPropertyStatement stmt : op.getObjectPropertyStatements()) {
                        if (op.getDomainPublic() != null && stmt.getObject().getName() != null) {
                            // objct props stuff
                            //List<String> objectPropertyTypeInfo = new ArrayList<String>();
                            Map<String, Object> objectPropertyExpandedInfoOneLevel =
                                    new HashMap<String, Object>();
                            objectPropertyExpandedInfoOneLevel
                                    .put(BaoPagedSearchConstants.JsonCode.BAO_NAME,
                                         limitedLength(stmt.getObject().getName()));
                            // I am told not to add this property
//                            objectPropertyTypeInfo.add(stmt.getObject().getVClass().getURI());                            //
                            List<String> objectPropertyType = new ArrayList<String>();
                            for (Iterator<VClass> vClassIterator =
                                         stmt.getObject().getVClasses(true).iterator();
                                 vClassIterator.hasNext(); ) {
                                VClass vClass = vClassIterator.next();
                                if (!(vClass.getName().indexOf("restriction on") > -1)) {
                                    objectPropertyType.add(vClass.getName());
                                    if (gridConceptsCnt.containsKey(vClass.getName())) {
                                        gridConceptsCnt.put(vClass.getName(),
                                                            gridConceptsCnt.get(vClass.getName())
                                                            + 1);
                                    } else {
                                        gridConceptsCnt.put(vClass.getName(), 1);
                                    }
                                }
                            }
                            if (objectPropertyType.size() > 0) {
                                objectPropertyExpandedInfoOneLevel
                                        .put(BaoPagedSearchConstants.JsonCode.TYPE,
                                             objectPropertyType);
                            }
                            // one level down for data type properties
                            Individual relatedIndividual = stmt.getObject();
                            // populate
                            IndividualDao iwDao = vreq.getWebappDaoFactory().getIndividualDao();
                            relatedIndividual =
                                    iwDao.getIndividualByURI(relatedIndividual.getURI());
                            if (relatedIndividual != null) {
                                if (relatedIndividual.getDataPropertyList() != null) {
                                    for (Iterator<DataProperty> iteratorRelatedIndividual =
                                                 relatedIndividual.getDataPropertyList().iterator();
                                         iteratorRelatedIndividual.hasNext(); ) {
                                        DataProperty dp = iteratorRelatedIndividual.next();
                                        try {
                                            if (getDataPropertyBlacklist().contains(dp.getURI())) {
                                                continue;
                                            }
                                            if (dp.getPublicName().startsWith("has_")) {
                                                // skip
                                                continue;
                                            }
                                            if (dp.getPublicName().equals("has display info")) {
                                                // skip
                                                continue;
                                            }
                                            if (dp.getPublicName().equals("has specification")) {
                                                // skip
                                                continue;
                                            }
                                            /*if (dp.getPublicName().equals("has format")) {
                                                // skip
                                                continue;
                                            }*/
                                            if (dp.getPublicName().equals("has measure group")) {
                                                // skip
                                                continue;
                                            }
                                            if (dp.getPublicName().equals("has perturbagen")) {
                                                // skip
                                                continue;
                                            }
                                            for (DataPropertyStatement dps : dp
                                                    .getDataPropertyStatements()) {
                                                if (objectPropertyExpandedInfoOneLevel
                                                            .get(dp.getPublicName()) != null) {
                                                    // public name already exists
                                                    List<String> oneLevelLiteralMap =
                                                            (List<String>) objectPropertyExpandedInfoOneLevel
                                                                    .get(dp.getPublicName());
                                                    if (oneLevelLiteralMap != null) {
                                                        oneLevelLiteralMap
                                                                .add(limitedLength(dps.getData()));
                                                    } else {
                                                        // this is a new entry
                                                        oneLevelLiteralMap =
                                                                new ArrayList<String>();
                                                        oneLevelLiteralMap.add(limitedLength(
                                                                dps.getData()));
                                                        objectPropertyExpandedInfoOneLevel
                                                                .put(dp.getPublicName(),
                                                                     oneLevelLiteralMap);
                                                    }
                                                } else {
                                                    // this is a new entry
                                                    List<String> oneLevelLiteralMap =
                                                            new ArrayList<String>();
                                                    oneLevelLiteralMap.add(limitedLength(
                                                            dps.getData()));
                                                    objectPropertyExpandedInfoOneLevel
                                                            .put(dp.getPublicName(),
                                                                 oneLevelLiteralMap);
                                                }

                                                if (!dataTypePropertyName
                                                        .contains(dp.getPublicName())) {
                                                    dataTypePropertyName.add(dp.getPublicName());
                                                }
                                            }
                                        } catch (Throwable e) {
                                            log.debug("Error highlighting data property statment " +
                                                      "for individual " + individual.getURI());
                                        }
                                    }
                                }
                            }


                            if (objectTypePropertyName.contains(op.getDomainPublic())) {
                                // this is an old entry. so lets add to it
                                List<Map<String, Object>> objectPropertyMapList =
                                        (List<Map<String, Object>>) individualMap
                                                .get(op.getDomainPublic());
                                // there could be a situation that this entry might be getting from
                                // another individual. Therefore if objectPropertyMapList is null,
                                // add a new one
                                if (objectPropertyMapList != null) {
                                    objectPropertyMapList.add(objectPropertyExpandedInfoOneLevel);
                                } else {
                                    objectPropertyMapList = new ArrayList<Map<String, Object>>();
                                    objectPropertyMapList.add(objectPropertyExpandedInfoOneLevel);
                                    individualMap.put(op.getDomainPublic(), objectPropertyMapList);
                                }

                            } else {
                                // this is a very first entry
                                List<Map<String, Object>> objectPropertyMapList =
                                        new ArrayList<Map<String, Object>>();
                                objectPropertyMapList.add(objectPropertyExpandedInfoOneLevel);
                                individualMap.put(op.getDomainPublic(), objectPropertyMapList);
                                objectTypePropertyName.add(op.getDomainPublic());
                            }

                        }
                    }
                } catch (Throwable e) {
                    log.debug("Error highlighting object property " +
                              "statement for individual " + individual.getURI());
                }
            }
            //this stuff is fixed
            if (!(objectTypePropertyName.contains(BaoPagedSearchConstants.JsonCode.BAO_NAME))) {
                objectTypePropertyName.add(BaoPagedSearchConstants.JsonCode.BAO_NAME);
            }
        }
    }

    private List<Individual> highlightBeans(List<Individual> beans,
                                            DataPropertyDao dpDao, ObjectPropertyDao opDao,
                                            VitroHighlighter highlighter) {
        if (beans == null) {
            log.debug("List of beans passed to highlightBeans() was null");
            return Collections.EMPTY_LIST;
        } else if (highlighter == null) {
            log.debug("Null highlighter passed to highlightBeans()");
            return beans;
        }
        Iterator<Individual> it = beans.iterator();
        while (it.hasNext()) {
            Individual ent = it.next();
            try {
                dpDao.fillDataPropertiesForIndividual(ent);
                opDao.fillObjectPropertiesForIndividual(ent);
                fragmentHighlight(ent, highlighter);
            } catch (Exception ex) {
                log.debug("Error while doing search highlighting", ex);
            }
        }
        return beans;
    }

    /**
     * Highlights the name and then replaces the description with
     * highlighted fragments.
     *
     * @param ent ent
     * @param hl  hl
     */
    public void fragmentHighlight(Individual ent, VitroHighlighter hl) {
        try {
            if (ent == null) return;

            Html2Text h2t = new Html2Text();
            StringBuffer sb = new StringBuffer("");
            if (ent.getBlurb() != null) {
                sb.append(ent.getBlurb()).append(' ');
            }

            if (ent.getDescription() != null) {
                sb.append(ent.getDescription()).append(' ');
            }

            if (ent.getCitation() != null) {
                sb.append(ent.getCitation()).append(' ');
            }

            if (ent.getDataPropertyList() != null) {
                Iterator edIt = ent.getDataPropertyList().iterator();
                while (edIt.hasNext()) {
                    try {
                        DataProperty dp = (DataProperty) edIt.next();
                        if (getDataPropertyBlacklist().contains(dp.getURI())) {
                            continue;
                        }
                        for (DataPropertyStatement dps : dp.getDataPropertyStatements()) {
                            sb.append(dp.getPublicName()).append(' ')
                              .append(limitedLength(dps.getData())).append(' ');
                        }
                    } catch (Throwable e) {
                        log.debug("Error highlighting data property statment " +
                                  "for individual " + ent.getURI());
                    }
                }
            }

            if (ent.getObjectPropertyList() != null) {
                Iterator edIt = ent.getObjectPropertyList().iterator();
                String t = null;
                while (edIt.hasNext()) {
                    try {
                        ObjectProperty op = (ObjectProperty) edIt.next();
                        if (getObjectPropertyBlacklist().contains(op.getURI())) {
                            continue;
                        }
                        for (ObjectPropertyStatement stmt : op.getObjectPropertyStatements()) {
                            sb.append(((t = op.getDomainPublic()) != null) ? t : "");
                            sb.append(' ');
                            sb.append(((t = stmt.getObject().getName()) != null) ? t : "");
                            sb.append(' ');
                        }
                    } catch (Throwable e) {
                        log.debug("Error highlighting object property " +
                                  "statement for individual " + ent.getURI());
                    }
                }
            }

            String keywords = ent.getKeywordString();
            if (keywords != null) {
                sb.append(keywords);
            }

            ent.setDescription(hl.getHighlightFragments(h2t.stripHtml(sb.toString())));
        } catch (Throwable th) {
            log.debug("could not hightlight for entity " + ent.getURI(), th);
        }
    }

    private HashSet<String> getDataPropertyBlacklist() {
        HashSet<String> dpBlacklist = (HashSet<String>)
                getServletContext().getAttribute(LuceneSetup.SEARCH_DATAPROPERTY_BLACKLIST);
        return dpBlacklist;
    }

    private HashSet<String> getObjectPropertyBlacklist() {
        HashSet<String> opBlacklist = (HashSet<String>)
                getServletContext().getAttribute(LuceneSetup.SEARCH_OBJECTPROPERTY_BLACKLIST);
        return opBlacklist;
    }

    private void alphaSortIndividuals(List<Individual> beans) {
        Collections.sort(beans, new Comparator<Individual>() {
            public int compare(Individual o1, Individual o2) {
                if (o1 == null || o1.getName() == null) {
                    return 1;
                } else {
                    return o1.getName().compareTo(o2.getName());
                }
            }
        });
    }

    private List<String> getAlphas(TopDocs topDocs, IndexSearcher searcher) {
        Set<String> alphas = new HashSet<String>();
        for (int i = 0; i < topDocs.scoreDocs.length; i++) {
            Document doc;
            try {
                doc = searcher.doc(topDocs.scoreDocs[i].doc);
                String name = doc.get(Entity2LuceneDoc.term.NAME);
                if (name != null && name.length() > 0) {
                    alphas.add(name.substring(0, 1));
                }
            } catch (CorruptIndexException e) {
                log.debug("Could not get alphas for document", e);
            } catch (IOException e) {
                log.debug("Could not get alphas for document", e);
            }

        }
        return new ArrayList<String>(alphas);
    }

    private List<VClass> getVClasses(VClassDao vclassDao, TopDocs topDocs,
                                     IndexSearcher searherForRequest) {
        HashSet<String> typesInHits = getVClassUrisForHits(topDocs, searherForRequest);
        List<VClass> classes = new ArrayList<VClass>(typesInHits.size());

        Iterator<String> it = typesInHits.iterator();
        while (it.hasNext()) {
            String typeUri = it.next();
            try {
                if (VitroVocabulary.OWL_THING.equals(typeUri)) {
                    continue;
                }
                VClass type = vclassDao.getVClassByURI(typeUri);
                if (!type.isAnonymous() &&
                    type.getName() != null && !"".equals(type.getName()) &&
                    type.getGroupURI() !=
                    null) //don't display classes that aren't in classgroups
                {
                    classes.add(type);
                }
            } catch (Exception ex) {
                if (log.isDebugEnabled()) {
                    log.debug("could not add type " + typeUri, ex);
                }
            }
        }
        Collections.sort(classes, new Comparator<VClass>() {
            public int compare(VClass o1, VClass o2) {
                return o1.compareTo(o2);
            }
        });
        return classes;
    }

    private HashSet<String> getVClassUrisForHits(TopDocs topDocs,
                                                 IndexSearcher searcherForRequest) {
        HashSet<String> typesInHits = new HashSet<String>();
        for (int i = 0; i < topDocs.scoreDocs.length; i++) {
            try {
                Document doc = searcherForRequest.doc(topDocs.scoreDocs[i].doc);
                Field[] types = doc.getFields(Entity2LuceneDoc.term.RDFTYPE);
                if (types != null) {
                    for (int j = 0; j < types.length; j++) {
                        String typeUri = types[j].stringValue();
                        typesInHits.add(typeUri);
                    }
                }
            } catch (Exception e) {
                log.error("problems getting rdf:type for search hits", e);
            }
        }
        return typesInHits;
    }

    /**
     * Get the class groups represented for the individuals in the topDocs.
     */
    private List<VClassGroup> getClassGroups(VClassGroupDao grpDao, TopDocs topDocs,
                                             IndexSearcher searcherForRequest,
                                             Map<String, Integer> individualCountForEachGroup) {
        LinkedHashMap<String, VClassGroup> grpMap = grpDao.getClassGroupMap();
        int n = grpMap.size();

        HashSet<String> classGroupsInHits = new HashSet<String>(n);
        int grpsFound = 0;

        for (int i = 0; i < topDocs.scoreDocs.length && n > grpsFound; i++) {
            try {
                Document doc = searcherForRequest.doc(topDocs.scoreDocs[i].doc);
                Field[] grps = doc.getFields(Entity2LuceneDoc.term.CLASSGROUP_URI);
                if (grps != null || grps.length > 0) {
                    for (int j = 0; j < grps.length; j++) {
                        String groupUri = grps[j].stringValue();
                        if (groupUri != null) {
                            if (individualCountForEachGroup.containsKey(groupUri)) {
                                int cnt = individualCountForEachGroup.get(groupUri) + 1;
                                individualCountForEachGroup.put(groupUri, cnt);
                            } else {
                                individualCountForEachGroup.put(groupUri, 1);
                            }
                        }
                        if (groupUri != null && !classGroupsInHits.contains(groupUri)) {
                            classGroupsInHits.add(groupUri);
                            grpsFound++;
                            if (grpsFound >= n) {
                                break;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                log.error("problem getting VClassGroups from search hits "
                          + e.getMessage());
            }
        }

        List<String> classgroupURIs = Collections.list(Collections.enumeration(classGroupsInHits));
        List<VClassGroup> classgroups = new ArrayList<VClassGroup>(classgroupURIs.size());
        for (String cgUri : classgroupURIs) {
            if (cgUri != null && !"".equals(cgUri)) {
                VClassGroup vcg = grpDao.getGroupByURI(cgUri);
                if (vcg == null) {
                    log.debug("could not get classgroup for URI " + cgUri);
                } else {
                    classgroups.add(vcg);
                }
            }
        }
        grpDao.sortGroupList(classgroups);
        return classgroups;
    }


    /**
     * Creates the custom exception message when query text faild
     *
     * @param querytext    querytext
     * @param exceptionMsg exceptionMsg
     * @return exception message
     */
    private String makeBadSearchMessage(String querytext, String exceptionMsg) {
        return querytext + " produced the exception " + exceptionMsg;
    }


    private void doFailedSearch(HttpServletRequest request,
                                String message,
                                String querytext,
                                Map<String, Object> jsonError)
            throws ServletException, IOException {
        log.error("query search failed: " + querytext + " : " + message);
        Portal portal = (new VitroRequest(request)).getPortal();
        jsonError.put(BaoPagedSearchConstants.ErrorCode.ERROR_NAME,
                      BaoPagedSearchConstants.ErrorCode.SEARCH_FAILD);
        jsonError.put(BaoPagedSearchConstants.ErrorCode.ERROR_REASON, "failed search");
        if (querytext != null) {
            jsonError.put(BaoPagedSearchConstants.JsonCode.QUERY_TEXT, querytext);
            jsonError.put("title", querytext + " - " + portal.getAppName() + " Search");
        } else {
            jsonError.put("title", portal.getAppName() + " Search");
            jsonError.put(BaoPagedSearchConstants.JsonCode.QUERY_TEXT, "");
        }
        if (message != null && message.length() > 0) {
            jsonError.put("message", message);
        }
    }

    private Analyzer getAnalyzer(ServletContext servletContext) throws SearchException {
        Object obj = servletContext.getAttribute(LuceneSetup.ANALYZER);
        if (obj == null || !(obj instanceof Analyzer)) {
            throw new SearchException("Could not get anlyzer");
        } else {
            return (Analyzer) obj;
        }
    }

    private Query getQuery(VitroRequest request, PortalFlag portalState,
                           Analyzer analyzer, String indexDir) throws SearchException {
        Query query = null;
        try {
            String querystr = request.getParameter(VitroQuery.QUERY_PARAMETER_NAME);
            if (querystr == null) {
                log.error("There was no Parameter '" + VitroQuery.QUERY_PARAMETER_NAME
                          + "' in the request.");
                return null;
            } else if (querystr.length() > BaoPagedSearchConstants.MAX_QUERY_LENGTH) {
                log.debug("The search was too long. The maximum " +
                          "query length is " + BaoPagedSearchConstants.MAX_QUERY_LENGTH);
                return null;
            }
            QueryParser parser = getQueryParser(analyzer);
            query = parser.parse(querystr);

            String alpha = request.getParameter(BaoPagedSearchConstants.ALPHA);
            if (alpha != null && !"".equals(alpha) && alpha.length() == 1) {
                BooleanQuery boolQuery = new BooleanQuery();
                boolQuery.add(query, BooleanClause.Occur.MUST);
                boolQuery.add(
                        new WildcardQuery(new Term(Entity2LuceneDoc.term.NAME, alpha + '*')),
                        BooleanClause.Occur.MUST);
                query = boolQuery;
            }

            //check if this is classgroup filtered
            Object param = request.getParameter(BaoPagedSearchConstants.CLASS_GROUP);
            if (param != null && !"".equals(param)) {
                BooleanQuery boolQuery = new BooleanQuery();
                boolQuery.add(query, BooleanClause.Occur.MUST);
                boolQuery.add(new TermQuery(
                        new Term(Entity2LuceneDoc.term.CLASSGROUP_URI,
                                 (String) param)),
                              BooleanClause.Occur.MUST);
                query = boolQuery;
            }

            //check if this is rdf:type filtered
            param = request.getParameter(BaoPagedSearchConstants.RDF_TYPE);
            if (param != null && !"".equals(param)) {
                BooleanQuery boolQuery = new BooleanQuery();
                boolQuery.add(query, BooleanClause.Occur.MUST);
                boolQuery.add(new TermQuery(
                        new Term(Entity2LuceneDoc.term.RDFTYPE,
                                 (String) param)),
                              BooleanClause.Occur.MUST);
                query = boolQuery;
            }

            //if we have a flag/portal query then we add
            //it by making a BooelanQuery.
            Query flagQuery = makeFlagQuery(portalState);
            if (flagQuery != null) {
                BooleanQuery boolQuery = new BooleanQuery();
                boolQuery.add(query, BooleanClause.Occur.MUST);
                boolQuery.add(flagQuery, BooleanClause.Occur.MUST);
                query = boolQuery;
            }
        } catch (Exception ex) {
            throw new SearchException(ex.getMessage());
        }

        return query;
    }

    private QueryParser getQueryParser(Analyzer analyzer) {
        //defaultSearchField indicates which field search against when there is no term
        //indicated in the query string.
        //The analyzer is needed so that we use the same analyzer on the search queries as
        //was used on the text that was indexed.
        VitroQueryParser qp = new VitroQueryParser(defaultSearchField, analyzer);
        //this sets the query parser to AND all of the query terms it finds.
        qp.setDefaultOperator(QueryParser.AND_OPERATOR);
        //set up the map of stemmed field names -> unstemmed field names
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Entity2LuceneDoc.term.ALLTEXT, Entity2LuceneDoc.term.ALLTEXTUNSTEMMED);
        qp.setStemmedToUnstemmed(map);
        return qp;
    }
    /*
    @see edu.cornell.mannlib.vitro.webapp.search.controller.PagedSearchController
     */

    private Query makeFlagQuery(PortalFlag flag) {
        if (flag == null || !flag.isFilteringActive()
            || flag.getFlag1DisplayStatus() == flag.SHOW_ALL_PORTALS) {
            return null;
        }

        // make one term for each bit in the numeric flag that is set
        Collection<TermQuery> terms = new LinkedList<TermQuery>();
        int portalNumericId = flag.getFlag1Numeric();
        Long[] bits = FlagMathUtils.numeric2numerics(portalNumericId);
        for (Long bit : bits) {
            terms.add(new TermQuery(new Term(Entity2LuceneDoc.term.PORTAL, Long
                    .toString(bit))));
        }

        // make a boolean OR query for all of those terms
        BooleanQuery boolQuery = new BooleanQuery();
        if (terms.size() > 0) {
            for (TermQuery term : terms) {
                boolQuery.add(term, BooleanClause.Occur.SHOULD);
            }
            return boolQuery;
        } else {
            //we have no flags set, so no flag filtering
            return null;
        }
    }

    private void doSearchError(String message,
                               Object object,
                               Map<String, Object> jsonObject)
            throws ServletException, IOException {
        jsonObject.put(BaoPagedSearchConstants.ErrorCode.ERROR_NAME,
                       BaoPagedSearchConstants.ErrorCode.SEARCH_ERROR);
        jsonObject.put(BaoPagedSearchConstants.ErrorCode.ERROR_REASON, message);
        if (object != null) {
            // TODO: provide the error code: I need the spec
        }
    }


    private void doNoQuery(HttpServletRequest request, Map<String, Object> jsonError)
            throws ServletException, IOException {
        Portal portal = (new VitroRequest(request)).getPortal();
        jsonError.put(BaoPagedSearchConstants.ErrorCode.ERROR_NAME,
                      BaoPagedSearchConstants.ErrorCode.NO_QUERY);
        jsonError.put(BaoPagedSearchConstants.ErrorCode.ERROR_REASON, "no query");
        jsonError.put("title", "Search " + portal.getAppName());
    }


    public VitroQueryFactory getQueryFactory() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public List search(VitroQuery query) throws SearchException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public VitroHighlighter getHighlighter(VitroQuery q) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void close() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}

package edu.miami.ccs.bao.controller;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 * Created by IntelliJ IDEA.
 * User: sam
 * Date: May 27, 2010
 * Time: 6:02:06 PM
 * To change this template use File | Settings | File Templates.
 */
public final class BaoPagedSearchConstants {

    public static final String NO_RESULT_MSG = "The BAOSearch returned no results.";

    public static final String ALPHA = "alpha";

    public static final String ALPHAS = "alphas";

    public static final String START_INDEX = "startIndex";

    public static final String HIT_PER_PAGE = "hitsPerPage";

    public static final int MAX_QUERY_LENGTH = 6000;

    public static final String CLASS_GROUP= "classgroup";

    public static final String CLASS_GROUP_NAME = "classgroupName";

    public static final String CLASS_GROUPS= "classgroups";

    // rdf:type
    public static final String RDF_TYPE = "type";

    public static final String RDF_TYPE_NAME = "typeName";

    public static class JsonCode {

        public static final String SEARCH_TITLE = "searchTitle";

        public static final String TABS = "tabs";

        public static final String TAB_NAME = "tabName";

        public static final String TAB_COUNT = "tabCount";

        public static final String TYPE = "type";

        public static final String BAO_NAME = "name";

        public static final String BAO_TYPE = TYPE;

        public static final String MAX_HIT_SIZE = "maxHitSize";

        public static final String QUERY_TEXT = "queryText";

        public static final String HITS_LENGTH = "hitsLength";

        public static final String START_INDEX = "startIndex";

        public static final String INDIVIDUALS = "individuals";

        public static final String COLUMN_NAMES = "columnNames";

        public static final String LABEL = "label";

        public static final String URI = "uri";

    }

    public static class ErrorCode {
        public static final String ERROR_NAME = "error";

        public static final String ERROR_REASON = "reason";

        public static final String ERROR_PREFIX = "ERR";

        public static  final String SEARCH_ERROR = ERROR_PREFIX + "001";

        public static final String NO_QUERY = ERROR_PREFIX + "002";

        public static final String SEARCH_FAILD = ERROR_PREFIX + "003";
    }


}

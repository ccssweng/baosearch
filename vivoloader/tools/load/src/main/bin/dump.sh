#!/bin/bash
echo "---------------------------------\n"
echo "BAO Dump Tool: @author: Saminda Abeyruwan \n"
echo "---------------------------------"
THE_CLASSPATH=
for i in `ls ../lib/*.jar`
do
  THE_CLASSPATH=${THE_CLASSPATH}:${i}
  echo ${i}
done

#---------------------------#
# rut the program #
#---------------------------#
echo "Run the program with input arg0=$1"
echo "Run the program with input arg1=$2"

java -Xmx2048m -cp ".:${THE_CLASSPATH}"  \
          edu.miami.ccs.bao.dump.TdbToOwl $1 $2

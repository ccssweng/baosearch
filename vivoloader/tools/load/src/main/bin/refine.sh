#!/bin/bash
echo "---------------------------------\n"
echo "BAO Refine Tool: @author: Saminda Abeyruwan \n"
echo "---------------------------------"
THE_CLASSPATH=
for i in `ls ../lib/*.jar`
do
  THE_CLASSPATH=${THE_CLASSPATH}:${i}
  echo ${i}
done

#---------------------------#
# rut the program #
#---------------------------#
echo "Run the refine program"

java -Xmx2048m -cp ".:${THE_CLASSPATH}"  \
          edu.miami.ccs.bao.rules.impl.DataSourceRefine

package edu.miami.ccs.bao.direct.impl.perturbagen;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.AbstractConcept;
import edu.miami.ccs.bao.direct.api.Constants;
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Perturbagen class creates the subject, object and, predicate for endpoints
 * for a particular endpoint taking the information from database
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class Perturbagen extends AbstractConcept {

	private final String id = "021";

	private final String hasId = "383";

	private final String hasIdValue = "203";

	// private final String pubchemCid = "396";

	private final String pubchemSid = "395";

	private final String isPerturbagenOf = "361";

	private final String hasPerturbagen = "185";

	// private final static String perturbagen = "Perturbagen";

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	public void visit(DirectContext directContext, Individual subject,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int substanceId) {

		String id = Util.getIndividualIri(directContext.getConfiguration()
				.getNs(NS_BAO), Constants.INDI_PREFIX + getId_(this.id)
				+ substanceId);

		Individual perturbagenIndividaul = aBoxModel.getIndividual(id);
		if (perturbagenIndividaul == null) {
			perturbagenIndividaul = createIndividual(
					getOntClass(tBoxModel, getId(this.id), directContext), id,
					String.valueOf(substanceId), aBoxModel,
					LabelModifier.CLASS_LABEL);

			// has id to pubchem cid
			ObjectProperty op = getObjectProperty(tBoxModel, getId(hasId),
					directContext);
			String pubchemCId = Util.getIndividualIri(directContext
					.getConfiguration().getNs(NS_BAO), Constants.INDI_PREFIX
					+ getId_(this.pubchemSid) + substanceId);
			Individual object = createIndividual(
					getOntClass(tBoxModel, getId(this.pubchemSid),
							directContext), pubchemCId,
					String.valueOf(substanceId), aBoxModel,
					LabelModifier.CLASS_LABEL);
			perturbagenIndividaul.addProperty(op, object);
			// data type property
			DatatypeProperty dp = getDatatypeProperty(tBoxModel,
					getId(hasIdValue), directContext);
			addLiteral(object, dp, 'i', aBoxModel, substanceId + "");
		}

		// subject is the endpoint
		subject.addProperty(predicate, perturbagenIndividaul);

		// link back to bio assay
		String aid = Util.getValue(directContext.getCsvDataSource().getHead(),
				currentRow, annonAid);
		String bioassayId = Util.getIndividualIri(directContext
				.getConfiguration().getNs(NS_BAO), Constants.INDI_PREFIX
				+ getId_(this.bioassyId) + aid);
		Individual bioassayIndividual = getABoxIndividual(aBoxModel, bioassayId);
		ObjectProperty op = getObjectProperty(tBoxModel,
				getId(isPerturbagenOf), directContext);
		perturbagenIndividaul.addProperty(op, bioassayIndividual);
		op = getObjectProperty(tBoxModel, getId(hasPerturbagen), directContext);
		bioassayIndividual.addProperty(op, perturbagenIndividaul);

	}

	/*
	 * 
	 * @see edu.miami.ccs.bao.direct.api.Concept#doData(java.sql.ResultSet,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {

	}
}

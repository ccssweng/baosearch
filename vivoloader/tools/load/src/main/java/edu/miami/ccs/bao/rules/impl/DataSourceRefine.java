package edu.miami.ccs.bao.rules.impl;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import edu.miami.ccs.bao.rules.api.CsvDataSource;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class DataSourceRefine {

	private CsvDataSource source;

	private Map<Integer, int[]> refineMap = new ConcurrentHashMap<Integer, int[]>();

	private File outFile;

	/**
	 * 
	 * @param source
	 * @param outFile
	 */
	public DataSourceRefine(CsvDataSource source, File outFile) {
		this.source = source;
		this.outFile = outFile;
		// ic50
		refineMap.put(1891, new int[] { 2 });
		refineMap.put(2723, new int[] { 4 });
		refineMap.put(2735, new int[] { 2 });
		refineMap.put(1218, new int[] { 2 });
		refineMap.put(1266, new int[] { 2 });
		refineMap.put(1219, new int[] { 2 });
		refineMap.put(1289, new int[] { 2 });
		refineMap.put(1290, new int[] { 2 });
		refineMap.put(1293, new int[] { 2 });
		refineMap.put(1294, new int[] { 2 });
		refineMap.put(1366, new int[] { 2 });
		refineMap.put(1371, new int[] { 2 });
		refineMap.put(1374, new int[] { 2 });
		refineMap.put(2089, new int[] { 2 });
		// refineMap.put(2098, new int[]{3, 4, 5, 6});
		refineMap.put(2383, new int[] { 2 });
		refineMap.put(1988, new int[] { 2 });
		refineMap.put(1990, new int[] { 2 });
		refineMap.put(1994, new int[] { 2 });
		// % inhibition
		// refineMap.put(1006, new int[]{1});
		refineMap.put(463, new int[] { 1 });
	}

	/**
	 * 
	 * @throws FileNotFoundException
	 */
	public void refine() throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(outFile);
		// write the header two times
		Set<Integer> set = source.getHead().getRow().keySet();
		for (int cnt = 0; cnt < 2; cnt++) {
			for (int i = 0; i < set.size(); i++) {
				write(writer, source.getHead().getValue(i));
			}
			writeLast(writer);
			newLine(writer);
		}
		List<SSRow> list = source.getRows();
		int rowCnt = 1;
		for (SSRow row : list) {
			String value = Util.getValue(source.getHead(), row, "assay_id");
			try {
				if (refineMap.containsKey(Integer.parseInt(value.trim()))) {
					set = row.getRow().keySet();
					for (int i = 0; i < set.size(); i++) {
						write(writer, row.getValue(i));
					}
					writeLast(writer);
					newLine(writer);
				}
			} catch (NumberFormatException e) {
				System.err.println("number error : " + value);
			}
			System.err.println("row : " + (rowCnt++));
		}
		writer.flush();
		writer.close();
	}

	private void write(PrintWriter w, String val) {
		w.append("\"").append(val).append("\"").append(",");
	}

	private void writeLast(PrintWriter w) {
		w.append("\"").append("__EMPTY__").append("\"");
	}

	private void newLine(PrintWriter w) {
		w.append("\n");
	}

	public static void main(String[] args) throws Exception {
		CsvDataSource source = new CsvDataSourceImpl();
		source.read(new FileInputStream("/home/bao/bao/csv/data_file2.csv"));
		DataSourceRefine refine = new DataSourceRefine(source, new File(
				"/home/bao/bao/data_file3.csv"));

		// source.read(new
		// FileInputStream("/home/sam/projects/bao/tmp/test_data.csv"));
		// DataSourceRefine refine =
		// new DataSourceRefine(source, new
		// File("/home/sam/projects/bao/tmp/data_file3.csv"));
		refine.refine();
	}

}

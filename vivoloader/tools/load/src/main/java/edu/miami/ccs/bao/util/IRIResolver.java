package edu.miami.ccs.bao.util;
/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.RDFS;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class checks whether the given ontology follows the OBO naming
 * conventions. The current state of the ontology is written out so that the
 * domain expert can fix any inconsistent phenomenon.
 * 
 * @author Sam Abeyruwan
 * @since 28/07/2011
 */
public class IRIResolver {

	private OntModel model;

	private String primaryRegularExp;

	private String secondaryRegularExp;

	/**
	 * 
	 * @param owlFile
	 * @param altEntries
	 * @throws FileNotFoundException
	 */
	public IRIResolver(File owlFile, Map<String, String> altEntries)
			throws FileNotFoundException {
		// load the T-box
		model = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
		InputStream in = new FileInputStream(owlFile);
		if (altEntries != null && altEntries.size() > 0) {
			for (Map.Entry<String, String> entry : altEntries.entrySet()) {
				model.getDocumentManager().addAltEntry(entry.getKey(),
						entry.getValue());
			}
		}
		model.read(in, null);
	}

	/**
	 * 
	 * @param primaryRegularExp
	 * @param secondaryRegularExp
	 */
	public void updateRegularExp(String primaryRegularExp,
			String secondaryRegularExp) {
		this.primaryRegularExp = primaryRegularExp;
		this.secondaryRegularExp = secondaryRegularExp;
	}

	public void match() {
		// iterate through the named classes
		Map<String, String> namedClassMap = new ConcurrentHashMap<String, String>();
		ExtendedIterator<OntClass> iterator = model.listNamedClasses();
		while (iterator.hasNext()) {
			OntClass aClass = iterator.next();
			Statement anLabel = aClass.getProperty(RDFS.label);
			if (anLabel != null && anLabel.getLiteral() != null
					&& anLabel.getLiteral().getString() != null) {
				// main assumption; all named classes have a label
				namedClassMap.put(anLabel.getLiteral().getString()
						.toLowerCase(), aClass.getURI());
			}
		}

		Pattern primaryPattern = Pattern.compile(primaryRegularExp);
		Pattern secondaryPattern = Pattern.compile(secondaryRegularExp);
		Matcher primaryMatcher, secondaryMatcher;
		int cnt = 0;
		for (Map.Entry<String, String> entry : namedClassMap.entrySet()) {
			primaryMatcher = primaryPattern.matcher(entry.getValue());
			if (!primaryMatcher.matches()) {
				secondaryMatcher = secondaryPattern.matcher(entry.getValue());
				if (secondaryMatcher.matches()) {

				}
			}
		}

	}

}

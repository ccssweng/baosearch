package edu.miami.ccs.bao.tmp;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Standardized endpoint class is used to test the annotation file endpoints
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class StandardizedEndpointTest {
	/**
	 * 
	 * @param args
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws FileNotFoundException {

		List<String> endpoints = new ArrayList<String>();

		List<String> implementedEndpoints = new ArrayList<String>();
		implementedEndpoints.add("Percent activation");
		implementedEndpoints.add("Percent stimulation");
		implementedEndpoints.add("Percent growth inhibition");
		implementedEndpoints.add("Percent inhibition");
		implementedEndpoints.add("AC50");
		implementedEndpoints.add("Percent viability");
		implementedEndpoints.add("Activation");
		implementedEndpoints.add("Activity direction");
		implementedEndpoints.add("CC50");
		implementedEndpoints.add("EC30");
		implementedEndpoints.add("EC50");
		implementedEndpoints.add("IC50");
		// implementedEndpoints.add("Growth inhibition");
		implementedEndpoints.add("Inhibition");
		implementedEndpoints.add("Luminescence intensity");
		implementedEndpoints.add("Mode of action");
		implementedEndpoints.add("Maximal response");
		implementedEndpoints.add("Raw activity reported");
		implementedEndpoints.add("Kd");

		Scanner scanner = new Scanner(new FileInputStream(new File(
				"/home/sam/tmp/bao/bao", "sEndpoints.txt")));
		while (scanner.hasNext()) {
			String line = scanner.nextLine();
			if (line != null && line.length() > 0) {
				String[] toks = line.trim().split(";");
				for (String tok : toks) {
					if (!endpoints.contains(tok)) {
						endpoints.add(tok);
					}
				}
			}
		}
		scanner.close();

		for (String endpoint : endpoints) {
			if (!implementedEndpoints.contains(endpoint)) {

			}
		}
	}
}

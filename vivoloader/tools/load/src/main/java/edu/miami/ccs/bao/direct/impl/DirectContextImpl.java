package edu.miami.ccs.bao.direct.impl;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.Concept;
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.direct.api.GlobalInformation;
import edu.miami.ccs.bao.main.Configuration;
import edu.miami.ccs.bao.rules.api.CsvDataSource;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.rules.impl.CsvDataSourceImpl;
import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.openjena.atlas.logging.java.TextFormatter;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Direct Context uses the ontology and updates the model
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */

public class DirectContextImpl implements DirectContext {

	private CsvDataSource csvDataSource;

	private PoolingDataSource dataSource;

	private int maxDbRead;

	private GlobalInformation globalInformation = new GlobalInformation();

	private Configuration config;

	private Logger logger;

	/**
	 * 
	 * @param config
	 */
	public DirectContextImpl(Configuration config) {
		this.config = config;
	}

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.DirectContext#initDataSource(java.lang.String
	 * , java.lang.String, java.lang.String)
	 */
	public void initDataSource(String url, String userName, String password)
			throws Exception {
		// mysql database connection
		Class.forName("org.gjt.mm.mysql.Driver").newInstance();

		//
		// First, we'll need a ObjectPool that serves as the
		// actual pool of connections.
		//
		// We'll use a GenericObjectPool instance, although
		// any ObjectPool implementation will suffice.
		//
		ObjectPool connectionPool = new GenericObjectPool(null);

		//
		// Next, we'll create a ConnectionFactory that the
		// pool will use to create Connections.
		// We'll use the DriverManagerConnectionFactory,
		// using the connect string passed in the command line
		// arguments.
		//
		ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(
				url, userName, password);

		//
		// Now we'll create the PoolableConnectionFactory, which wraps
		// the "real" Connections created by the ConnectionFactory with
		// the classes that implement the pooling functionality.
		//
		PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(
				connectionFactory, connectionPool, null, null, false, true);

		//
		// Finally, we create the PoolingDriver itself,
		// passing in the object pool we created.
		//
		dataSource = new PoolingDataSource(connectionPool);

	}

	/*
	 * 
	 * 
	 * @see
	 * edu.miami.ccs.bao.direct.api.DirectContext#initCsvSource(java.lang.String
	 * )
	 */
	public void initCsvSource(String file) throws Exception {
		csvDataSource = new CsvDataSourceImpl();
		csvDataSource.read(new FileInputStream(new File(file)));
	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.DirectContext#getDataSource()
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.DirectContext#getCsvDataSource()
	 */
	public CsvDataSource getCsvDataSource() {
		return csvDataSource;
	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.DirectContext#getConfiguration()
	 */
	@Override
	public Configuration getConfiguration() {
		return config;
	}

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.DirectContext#doResult(java.lang.String,
	 * edu.miami.ccs.bao.direct.api.Concept,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	public void doResult(String sql, Concept thisSubject,
			DirectContext directContext, Individual subject,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rset = null;
		try {
			conn = dataSource.getConnection();
			stmt = conn.createStatement();
			rset = stmt.executeQuery(sql);
			thisSubject.doData(rset, directContext, subject, predicate,
					currentRow, tBoxModel, aBoxModel);
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			try {
				if (rset != null) {
					rset.close();
				}
			} catch (Exception e) {
				// do nothing
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				// do nothing
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				// do nothing
			}
		}
	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.DirectContext#setMaxDbRead(int)
	 */
	public void setMaxDbRead(int maxDbRead) {
		this.maxDbRead = maxDbRead;
	}

	public int getMaxDbRead() {
		return maxDbRead;
	}

	public GlobalInformation getGlobalInformation() {
		return globalInformation;
	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.DirectContext#getLogger()
	 */
	@Override
	public Logger getLogger() {
		return logger;
	}

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.DirectContext#initLogger(java.lang.String)
	 */
	@Override
	public void initLogger(String fileName) {
		logger = Logger.getLogger("BAO_LOADER_LOG");
		logger.setLevel(Level.ALL);
		try {
			FileHandler handler = new FileHandler(fileName);
			handler.setFormatter(new TextFormatter());
			logger.addHandler(handler);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-99);
		}
	}
}

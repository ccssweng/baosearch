package edu.miami.ccs.bao.rules.impl;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.ontology.*;
import edu.miami.ccs.bao.rules.api.*;
import edu.miami.ccs.bao.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class TypeBEngineImpl implements RuleEngine, MutateRuleEngineWithJena {

	private ObjectProperty mainObjectProperty;

	private Individual mainSourceIndividual;

	/*
	 * 
	 * 
	 * @see
	 * edu.miami.ccs.bao.rules.api.Engine#pre(edu.miami.ccs.bao.rules.api.Mapper
	 * .Individual, edu.miami.ccs.bao.rules.api.SSRow,
	 * com.hp.hpl.jena.ontology.OntModel, com.hp.hpl.jena.ontology.OntModel,
	 * edu.miami.ccs.bao.rules.api.Document, java.lang.String, java.lang.String)
	 */
	public void pre(Mapper.Individual individual, SSRow row,
			OntModel tBoxModel, OntModel aBoxModel, Document document,
			String indiPrefix, String indCnt) {
		// nothing
	}

	private class LogicMap {
		String left;

		String right;

		private LogicMap(String left, String right) {
			this.left = left;
			this.right = right;
		}

		public String getLeft() {
			return left;
		}

		public String getRight() {
			return right;
		}
	}

	/**
	 * @param logic
	 * @param state
	 * @param stateChain
	 * @param height
	 * @param uHead
	 * @param uRow
	 * @param dHead
	 * @param dRow
	 */
	private void processIf(List<LogicMap> logic, List<IfState> state,
			List<List<IfState>> stateChain, int height, SSRow uHead,
			SSRow uRow, SSRow dHead, SSRow dRow) {
		if (height <= (logic.size() - 1)) {
			LogicMap entry = logic.get(height);
			String left = entry.getLeft();
			String right = entry.getRight();
			/**
			 * list(variable_1) = list(variable_2) list(variable) = constant
			 * 
			 */
			if (Util.isPrefix(Constants.PREFIX_U, left)) {
				// variable
				String uVar = left.substring(2, left.length() - 1);
				String uVarUnifiedValue = Util.getValue(uHead, uRow, uVar);
				String[] uVarUnifiedValueToks = uVarUnifiedValue.split(";");
				for (String uVarUnifiedValueTok : uVarUnifiedValueToks) {
					if (Util.isPrefix(Constants.PREFIX_D, right)) {
						// variable
						String dVar = right.substring(2, right.length() - 1);
						String dVarUnifiedValue = Util.getValue(dHead, dRow,
								dVar);
						if (uVarUnifiedValueTok.equals(dVarUnifiedValue)) {
							state.add(new IfState(uVar, uVarUnifiedValueTok,
									dVar, dVarUnifiedValue, true));
						} else {
							state.add(new IfState(uVar, uVarUnifiedValueTok,
									dVar, dVarUnifiedValue, false));
						}
						processIf(logic, state, stateChain, (height + 1),
								uHead, uRow, dHead, dRow);
					} else {
						if (uVarUnifiedValueTok.equals(right)) {
							state.add(new IfState(uVar, uVarUnifiedValueTok,
									null, right, true));
						} else {
							state.add(new IfState(uVar, uVarUnifiedValueTok,
									null, right, false));
						}
						processIf(logic, state, stateChain, (height + 1),
								uHead, uRow, dHead, dRow);
					}
					// remove last
					state.remove(state.size() - 1);
				}

			}
		} else {
			List<IfState> clonedStates = (List<IfState>) ((ArrayList) state)
					.clone();
			stateChain.add(clonedStates);
		}
	}

	/**
	 * @param individual
	 * @param uHead
	 * @param uRow
	 * @param dHead
	 * @param dRow
	 * @param tBoxModel
	 * @param aBoxModel
	 * @param document
	 * @param indiPrefix
	 * @param indCnt
	 * @param dIndCnt
	 * @param anIf
	 */
	private void exeLogic(Mapper.Individual individual, SSRow uHead,
			SSRow uRow, SSRow dHead, SSRow dRow, OntModel tBoxModel,
			OntModel aBoxModel, Document document, String indiPrefix,
			String indCnt, String dIndCnt, Mapper.If anIf) {

		String test = anIf.getTest();
		String[] parts = test.split("AND");

		List<LogicMap> logicMap = new ArrayList<LogicMap>();

		for (String part : parts) {
			// only 2 atoms
			String[] atoms = part.split("=");
			logicMap.add(new LogicMap(atoms[0].trim(), atoms[1].trim()));
		}

		List<List<IfState>> stateChain = new ArrayList<List<IfState>>();
		List<IfState> state = new ArrayList<IfState>();
		//
		processIf(logicMap, state, stateChain, 0, uHead, uRow, dHead, dRow);
		//
		boolean trueState = false;
		for (int anStateCnt = 0; anStateCnt < stateChain.size(); anStateCnt++) {
			List<IfState> anState = stateChain.get(anStateCnt);
			for (IfState anIfState : anState) {
				if (anIfState.isState()) {
					trueState = true;
				} else {
					trueState = false;
					break;
				}
			}

			if (trueState) {
				// create the individual
				String ns = document.getMapper().getNamespace().getNs()
						.get(individual.getNsPrefix());
				OntClass mainClass = tBoxModel.getOntClass(ns
						+ anIf.getConceptType().getOnt());
				if (mainClass == null) {
					error("main class not found", anIf.getConceptType()
							.getOnt());
				}
				Individual ontIndividual = aBoxModel.createIndividual(
						getConceptName(ns, indiPrefix, anIf.getConceptType()
								.getOnt() + "_", indCnt + "_" + dIndCnt + "_"
								+ anStateCnt), mainClass);
				// data types
				for (Mapper.DataType dataType : anIf.getDataTypes()) {
					DatatypeProperty datatypeProperty = tBoxModel
							.getDatatypeProperty(ns + dataType.getOnt());
					if (datatypeProperty == null) {
						error("data type is not found", dataType.getOnt());
					}
					String value = Util.getValue(uHead, uRow, dHead, dRow,
							dataType.getData());
					value = (value != null) ? value : "";
					if (dataType.getXsd().equals("string")) {
						ontIndividual.addProperty(datatypeProperty, value);
					} else if (dataType.getXsd().equals("integer")) {
						try {
							ontIndividual.addLiteral(datatypeProperty,
									aBoxModel.createTypedLiteral(
											Integer.parseInt(value),
											XSDDatatype.XSDinteger));
						} catch (NumberFormatException e) {
							String err = TypeBEngineImpl.class.getName()
									+ " value : " + value + " for "
									+ Util.getStrippedEntry(dataType.getData());

						}
					} else if (dataType.getXsd().equals("float")) {
						try {
							ontIndividual.addLiteral(datatypeProperty,
									aBoxModel.createTypedLiteral(
											Float.parseFloat(value),
											XSDDatatype.XSDfloat));
						} catch (NumberFormatException e) {
							String err = TypeBEngineImpl.class.getName()
									+ " value : " + value + " for "
									+ Util.getStrippedEntry(dataType.getData());

						}
					}

				}
				// object types

				List<Mapper.ObjType> objTypeList = anIf.getObjTypes();
				for (Mapper.ObjType objType : objTypeList) {
					ObjectProperty objectProperty = tBoxModel
							.getObjectProperty(ns + objType.getOnt());
					if (objectProperty == null) {
						error("object property not found ",
								ns + objType.getOnt());
					}

					String refSingleOntologyIndividual = objType
							.getRefSingleOntologyIndividual();
					if (refSingleOntologyIndividual != null) {
						Mapper.SingleOntologyIndividual singleIndividual = document
								.getMapper().getSingleOntologyIndividual()
								.get(refSingleOntologyIndividual);
						if (singleIndividual == null) {
							error("single individual should not be null",
									refSingleOntologyIndividual);
						} else {
							Individual refOntIndividual = aBoxModel
									.getIndividual(ns
											+ singleIndividual.getId());
							if (refOntIndividual == null) {
								error("ont individual should not be null",
										refSingleOntologyIndividual);
							} else {
								ontIndividual.addProperty(objectProperty,
										refOntIndividual);
							}
						}
					}

					String refTBoxIndividual = objType.getRefTBoxIndividual();
					if (refTBoxIndividual != null) {
						Individual tBoxOntIndividual = tBoxModel
								.getIndividual(ns + refTBoxIndividual);
						if (tBoxOntIndividual == null) {
							error("T-Box individual does not exists",
									refTBoxIndividual);
						} else {
							ontIndividual.addProperty(objectProperty,
									tBoxOntIndividual);
						}
					}

					String refTypeC = objType.getRefTypeC();
					if (refTypeC != null) {
						// target individual
						Mapper.IndividualTypeC refIndividual = document
								.getMapper().getIndividualsTypeC()
								.get(refTypeC);
						Engine typeCEngine = EngineFactory.getTypeCEngine(
								objectProperty, ontIndividual, uHead, uRow);
						typeCEngine.pre(refIndividual, dRow, tBoxModel,
								aBoxModel, document, indiPrefix, indCnt + "_"
										+ dIndCnt + "_" + anStateCnt);
						typeCEngine.post(refIndividual, dRow, tBoxModel,
								aBoxModel, document, indiPrefix, indCnt + "_"
										+ dIndCnt + "_" + anStateCnt);
					}
				}
				// add the individual
				mainSourceIndividual.addProperty(mainObjectProperty,
						ontIndividual);
			}

			trueState = false;
		}
	}

	/*
	 * 
	 * 
	 * @see
	 * edu.miami.ccs.bao.rules.api.Engine#post(edu.miami.ccs.bao.rules.api.Mapper
	 * .Individual, edu.miami.ccs.bao.rules.api.SSRow,
	 * com.hp.hpl.jena.ontology.OntModel, com.hp.hpl.jena.ontology.OntModel,
	 * edu.miami.ccs.bao.rules.api.Document, java.lang.String, java.lang.String)
	 */
	public void post(Mapper.Individual individual, SSRow uRow,
			OntModel tBoxModel, OntModel aBoxModel, Document document,
			String indiPrefix, String indCnt) {
		// do something
		SSRow uHead = document.getAnnotatedDataSource().getHead();
		SSRow dHead = document.getDatabaseDataSource().getHead();
		int dIndCnt = 1;
		for (SSRow dRow : document.getDatabaseDataSource().getRows()) {
			for (Mapper.If anIf : ((Mapper.IndividualTypeB) individual)
					.getIfs()) {
				exeLogic(individual, uHead, uRow, dHead, dRow, tBoxModel,
						aBoxModel, document, indiPrefix, indCnt, dIndCnt + "",
						anIf);
			}
			dIndCnt++;
		}
	}

	public void error(String err1, String err2) {

		System.exit(-98);
	}

	public void message(String message) {

	}

	public String getConceptName(String ns, String prefix, String ont,
			String cnt) {
		return ns + prefix + ont + cnt;
	}

	public void setJenaObjectProperty(ObjectProperty objectProperty) {
		this.mainObjectProperty = objectProperty;
	}

	public void setJenaIndividual(Individual sourceIndividual) {
		this.mainSourceIndividual = sourceIndividual;
	}
}

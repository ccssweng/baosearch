package edu.miami.ccs.bao.direct.impl.target;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.*;
import edu.miami.ccs.bao.direct.impl.specification.MetaTargetSpec;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Protein class creates the subject, object and, predicate for target protein
 * for a particular assay taking the information from Annotation file
 * 
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class ProteinTarget extends AbstractConcept {

	private static String targetProtein1 = "TargetProtein1";

	private static String targetProtein2 = "TargetProtein2";

	private static String hasSpecification = "214";

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	public void visit(DirectContext directContext, Individual measureGroup,
			ObjectProperty hasMetaTarget, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int mult) {
		// impl
		// let do a simple error handling
		String descriptionNameList = Util.getValue(directContext
				.getCsvDataSource().getHead(), currentRow, targetProtein1);
		// error checking
		if (descriptionNameList == null || descriptionNameList.length() == 0) {
			directContext.getLogger().warning("##### ProteinTarget is missing");
		} else {
			Individual mySubject;
			// corresponding mySubject name
			String[] toks = descriptionNameList.trim().split(";");
			String correspondingIndividualName = Util.getValue(directContext
					.getCsvDataSource().getHead(), currentRow, targetProtein2);
			if (correspondingIndividualName == null
					|| correspondingIndividualName.length() == 0) {
				directContext.getLogger().warning(
						"##### target 2 is inconsistent");

				for (String tok : toks) {
					mySubject = createGlobalIndividualParam1(
							tok,
							null,
							directContext.getGlobalInformation().proteinTargetMapping,
							directContext, measureGroup, hasMetaTarget,
							tBoxModel, aBoxModel, IdModifier.LOCAL_ID);
					addBaoSearchObjectPropertyToBioAssay(directContext,
							tBoxModel, aBoxModel, currentRow, mySubject,
							"hasTarget", "isTargetOf");

				}

			} else {
				String[] cinToks = correspondingIndividualName.split(";");

				if (toks.length != cinToks.length) {
					directContext.getLogger().warning(
							"##### incorrect target1 and target2");
					for (String tok : toks) {
						mySubject = createGlobalIndividualParam1(
								tok,
								null,
								directContext.getGlobalInformation().proteinTargetMapping,
								directContext, measureGroup, hasMetaTarget,
								tBoxModel, aBoxModel, IdModifier.LOCAL_ID);
						addBaoSearchObjectPropertyToBioAssay(directContext,
								tBoxModel, aBoxModel, currentRow, mySubject,
								"hasTarget", "isTargetOf");

					}
				} else {

					for (int cTok = 0; cTok < cinToks.length; cTok++) {
						// TODO: again we are making a very huge mistake
						OntClass clazz = getOntClass(
								tBoxModel,
								getCellToDecIdMapping(directContext, tBoxModel,
										toks[cTok]), directContext);

						if (clazz == null) {
							clazz = getOntClass(tBoxModel, getId("020"),
									directContext);
						}

						mySubject = createGlobalIndividualParam2(
								cinToks[cTok],
								clazz,
								LabelModifier.SHORT_ID,
								null,
								directContext.getGlobalInformation().proteinTargetMapping,
								directContext, measureGroup, hasMetaTarget,
								tBoxModel, aBoxModel);

						// baoseach link to bioassay
						// link back to bio assay
						addBaoSearchObjectPropertyToBioAssay(directContext,
								tBoxModel, aBoxModel, currentRow, mySubject,
								"hasTarget", "isTargetOf");
					}
				}
			}

		}

	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.Concept#doData(java.sql.ResultSet,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {
		// not used
	}
}

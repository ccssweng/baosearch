package edu.miami.ccs.bao.chembl.endpoint;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.AbstractConcept;
import edu.miami.ccs.bao.direct.api.Constants;
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * ChemblEndpointUnit class creates the subject, object and, predicate for endpoints units
 * for a particular endpoint value taking the information from filter redesign database
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public final class ChemblEndpointUtil extends AbstractConcept {

	protected final String hasPerturbagenConcentrationUnit = "183";

	protected final String hasPerturbagenConcentrationValue = "336";

	protected final String hasPerturbagenConcentration = "338";

	protected final String screeningConcentrationId = "098";

	// protected final String isEndpointOf = "559";

	// protected final String hasEndpoint = "208";

	protected final String hasModeOfAction = "196";

	protected final String hasPercentResponse = "337";

	protected final String hasPercentResponseValue = "195";

	protected final String microMolarIndividual = "301";

	protected final String perturbagenId = "021";

	protected final String hasId = "383";

	protected final String hasIdValue = "203";

	protected final String pubchemSid = "395";

	protected final String isPerturbagenOf = "361";

	protected final String hasPerturbagen = "185";

	protected final String hasTemperatureUnit = "097";

	protected final String hasTemperatureValue = "109";

	protected final String degreeCelsiusIndividual = "304";

	/**
	 * 
	 * @param concUnit
	 * @param concValue
	 * @param pertSid
	 * @param endpointIndividual
	 * @param modeOfActionIndividualIdSuffix
	 * @param percentResponseIndividualIdSuffix
	 * @param chembelBioAssayAid
	 * @param tBoxModel
	 * @param aBoxModel
	 * @param directContext
	 */
	public void createEndpointConcentration(String concUnit, String concValue,
			String pertSid, Individual endpointIndividual,
			String modeOfActionIndividualIdSuffix,
			String percentResponseIndividualIdSuffix,
			String chembelBioAssayAid, OntModel tBoxModel, OntModel aBoxModel,
			DirectContext directContext) {
		// op
		ObjectProperty op = getObjectProperty(tBoxModel,
				getId(hasModeOfAction), directContext);
		endpointIndividual.addProperty(
				op,
				getTBoxIndividual(tBoxModel,
						getId(modeOfActionIndividualIdSuffix), directContext));

		op = getObjectProperty(tBoxModel, getId(hasPercentResponse),
				directContext);
		endpointIndividual
				.addProperty(
						op,
						getTBoxIndividual(tBoxModel,
								getId(percentResponseIndividualIdSuffix),
								directContext));

		op = getObjectProperty(tBoxModel,
				getId(hasPerturbagenConcentrationUnit), directContext);
		// TODO fixthis with concUnit
		endpointIndividual.addProperty(
				op,
				getTBoxIndividual(tBoxModel, getId(microMolarIndividual),
						directContext));
		// data type
		DatatypeProperty dp = getDatatypeProperty(tBoxModel,
				getId(hasPerturbagenConcentrationValue), directContext);
		addLiteral(endpointIndividual, dp, 'f', aBoxModel, concValue);

		if (pertSid != null && pertSid.length() > 0) {
			// a whole bunch of garbage to handle perturbagen
			int substanceId = Integer.parseInt(pertSid);

			String id = Util.getIndividualIri(directContext.getConfiguration()
					.getNs(NS_BAO), Constants.INDI_PREFIX
					+ getId_(perturbagenId) + substanceId);
			Individual perturbagenIndividaul = aBoxModel.getIndividual(id);
			if (perturbagenIndividaul == null) {
				perturbagenIndividaul = createIndividual(
						getOntClass(tBoxModel, getId(perturbagenId),
								directContext), id,
						String.valueOf(substanceId), aBoxModel,
						LabelModifier.COMPOSITE);

				// /////////////////////////// has id to pubchem cid
				op = getObjectProperty(tBoxModel, getId(hasId), directContext);
				String pubchemCId = Util.getIndividualIri(directContext
						.getConfiguration().getNs(NS_BAO),
						Constants.INDI_PREFIX + getId_(this.pubchemSid)
								+ substanceId);
				Individual object = createIndividual(
						getOntClass(tBoxModel, getId(this.pubchemSid),
								directContext), pubchemCId,
						String.valueOf(substanceId), aBoxModel,
						LabelModifier.COMPOSITE);
				perturbagenIndividaul.addProperty(op, object);
				// data type property
				dp = getDatatypeProperty(tBoxModel, getId(hasIdValue),
						directContext);
				addLiteral(object, dp, 'i', aBoxModel, substanceId + "");
			}

			// subject is the endpoint
			ObjectProperty predicate = getObjectProperty(tBoxModel,
					getId(hasPerturbagen), directContext);
			endpointIndividual.addProperty(predicate, perturbagenIndividaul);

			// link back to bio assay
			String bioassayId = Util.getIndividualIri(directContext
					.getConfiguration().getNs(NS_BAO), Constants.INDI_PREFIX
					+ getId_(this.bioassyId) + chembelBioAssayAid);
			Individual bioassayIndividual = getABoxIndividual(aBoxModel,
					bioassayId);
			op = getObjectProperty(tBoxModel, getId(isPerturbagenOf),
					directContext);
			perturbagenIndividaul.addProperty(op, bioassayIndividual);
			op = getObjectProperty(tBoxModel, getId(hasPerturbagen),
					directContext);
			bioassayIndividual.addProperty(op, perturbagenIndividaul);
		}

	}

	/**
	 * 
	 * @param concUnit
	 * @param concValue
	 * @param pertSid
	 * @param endpointIndividual
	 * @param chembelBioAssayAid
	 * @param tBoxModel
	 * @param aBoxModel
	 * @param directContext
	 */

	public void createEndpointPerturbagenProteinAffinity(String concUnit,
			String concValue, String pertSid, Individual endpointIndividual,
			String chembelBioAssayAid, OntModel tBoxModel, OntModel aBoxModel,
			DirectContext directContext) {
		ObjectProperty op;
		DatatypeProperty dp;

		op = getObjectProperty(tBoxModel,
				getId(hasPerturbagenConcentrationUnit), directContext);
		// TODO fixthis with concUnit
		endpointIndividual.addProperty(
				op,
				getTBoxIndividual(tBoxModel, getId(microMolarIndividual),
						directContext));
		// data type
		dp = getDatatypeProperty(tBoxModel,
				getId(hasPerturbagenConcentrationValue), directContext);
		addLiteral(endpointIndividual, dp, 'f', aBoxModel, concValue);

		if (pertSid != null && pertSid.length() > 0) {
			// a whole bunch of garbage to handle perturbagen
			int substanceId = Integer.parseInt(pertSid);

			String id = Util.getIndividualIri(directContext.getConfiguration()
					.getNs(NS_BAO), Constants.INDI_PREFIX
					+ getId_(perturbagenId) + substanceId);
			Individual perturbagenIndividaul = aBoxModel.getIndividual(id);
			if (perturbagenIndividaul == null) {
				perturbagenIndividaul = createIndividual(
						getOntClass(tBoxModel, getId(perturbagenId),
								directContext), id,
						String.valueOf(substanceId), aBoxModel,
						LabelModifier.COMPOSITE);

				// /////////////////////////// has id to pubchem cid
				op = getObjectProperty(tBoxModel, getId(hasId), directContext);
				String pubchemCId = Util.getIndividualIri(directContext
						.getConfiguration().getNs(NS_BAO),
						Constants.INDI_PREFIX + getId_(this.pubchemSid)
								+ substanceId);
				Individual object = createIndividual(
						getOntClass(tBoxModel, getId(this.pubchemSid),
								directContext), pubchemCId,
						String.valueOf(substanceId), aBoxModel,
						LabelModifier.COMPOSITE);
				perturbagenIndividaul.addProperty(op, object);
				// data type property
				dp = getDatatypeProperty(tBoxModel, getId(hasIdValue),
						directContext);
				addLiteral(object, dp, 'i', aBoxModel, substanceId + "");
			}

			// subject is the endpoint
			ObjectProperty predicate = getObjectProperty(tBoxModel,
					getId(hasPerturbagen), directContext);
			endpointIndividual.addProperty(predicate, perturbagenIndividaul);

			// link back to bio assay
			String bioassayId = Util.getIndividualIri(directContext
					.getConfiguration().getNs(NS_BAO), Constants.INDI_PREFIX
					+ getId_(this.bioassyId) + chembelBioAssayAid);
			Individual bioassayIndividual = getABoxIndividual(aBoxModel,
					bioassayId);
			op = getObjectProperty(tBoxModel, getId(isPerturbagenOf),
					directContext);
			perturbagenIndividaul.addProperty(op, bioassayIndividual);
			op = getObjectProperty(tBoxModel, getId(hasPerturbagen),
					directContext);
			bioassayIndividual.addProperty(op, perturbagenIndividaul);
		}

	}

	/**
	 * 
	 * @param concUnit
	 * @param concValue
	 * @param respValue
	 * @param pertSid
	 * @param endpointIndividual
	 * @param modeOfActionIndividualIdSuffix
	 * @param chembelBioAssayAid
	 * @param tBoxModel
	 * @param aBoxModel
	 * @param directContext
	 */
	public void createEndpointResponse(String concUnit, String concValue,
			String respValue, String pertSid, Individual endpointIndividual,
			String modeOfActionIndividualIdSuffix, String chembelBioAssayAid,
			OntModel tBoxModel, OntModel aBoxModel, DirectContext directContext) {

		// op
		ObjectProperty op = getObjectProperty(tBoxModel,
				getId(hasModeOfAction), directContext);
		endpointIndividual.addProperty(
				op,
				getTBoxIndividual(tBoxModel,
						getId(modeOfActionIndividualIdSuffix), directContext));

		DatatypeProperty dp = getDatatypeProperty(tBoxModel,
				getId(hasPercentResponseValue), directContext);
		addLiteral(endpointIndividual, dp, 'f', aBoxModel, respValue);

		if (pertSid != null && pertSid.length() > 0) {
			// a whole bunch of garbage to handle perturbagen

			// screening concentration
			op = getObjectProperty(tBoxModel,
					getId(hasPerturbagenConcentration), directContext);
			String id = Util.getIndividualIri(directContext.getConfiguration()
					.getNs(NS_BAO), Constants.INDI_PREFIX
					+ getId(screeningConcentrationId) + _unique());
			Individual screeningConcentrationIndividual = createIndividual(
					getOntClass(tBoxModel, getId(perturbagenId), directContext),
					id, "", aBoxModel, LabelModifier.COMPOSITE);
			endpointIndividual
					.addProperty(op, screeningConcentrationIndividual);

			op = getObjectProperty(tBoxModel,
					getId(hasPerturbagenConcentrationUnit), directContext);
			// TODO fixthis with concUnit
			screeningConcentrationIndividual.addProperty(
					op,
					getTBoxIndividual(tBoxModel, getId(microMolarIndividual),
							directContext));
			// data type
			dp = getDatatypeProperty(tBoxModel,
					getId(hasPerturbagenConcentrationValue), directContext);
			addLiteral(screeningConcentrationIndividual, dp, 'f', aBoxModel,
					concValue);

			int substanceId = Integer.parseInt(pertSid);

			id = Util
					.getIndividualIri(
							directContext.getConfiguration().getNs(NS_BAO),
							Constants.INDI_PREFIX + getId_(perturbagenId)
									+ substanceId);
			Individual perturbagenIndividaul = aBoxModel.getIndividual(id);
			if (perturbagenIndividaul == null) {
				perturbagenIndividaul = createIndividual(
						getOntClass(tBoxModel, getId(perturbagenId),
								directContext), id,
						String.valueOf(substanceId), aBoxModel,
						LabelModifier.COMPOSITE);

				// /////////////////////////// has id to pubchem cid
				op = getObjectProperty(tBoxModel, getId(hasId), directContext);
				String pubchemCId = Util.getIndividualIri(directContext
						.getConfiguration().getNs(NS_BAO),
						Constants.INDI_PREFIX + getId_(this.pubchemSid)
								+ substanceId);
				Individual object = createIndividual(
						getOntClass(tBoxModel, getId(this.pubchemSid),
								directContext), pubchemCId,
						String.valueOf(substanceId), aBoxModel,
						LabelModifier.COMPOSITE);
				perturbagenIndividaul.addProperty(op, object);
				// data type property
				dp = getDatatypeProperty(tBoxModel, getId(hasIdValue),
						directContext);
				addLiteral(object, dp, 'i', aBoxModel, substanceId + "");
			}

			// subject is the endpoint
			ObjectProperty predicate = getObjectProperty(tBoxModel,
					getId(hasPerturbagen), directContext);
			screeningConcentrationIndividual.addProperty(predicate,
					perturbagenIndividaul);

			// link back to bio assay
			String bioassayId = Util.getIndividualIri(directContext
					.getConfiguration().getNs(NS_BAO), Constants.INDI_PREFIX
					+ getId_(this.bioassyId) + chembelBioAssayAid);
			Individual bioassayIndividual = getABoxIndividual(aBoxModel,
					bioassayId);
			op = getObjectProperty(tBoxModel, getId(isPerturbagenOf),
					directContext);
			perturbagenIndividaul.addProperty(op, bioassayIndividual);
			op = getObjectProperty(tBoxModel, getId(hasPerturbagen),
					directContext);
			bioassayIndividual.addProperty(op, perturbagenIndividaul);
		}

	}

	/**
	 * 
	 * @param pertSid
	 * @param endpointIndividual
	 * @param chembelBioAssayAid
	 * @param tBoxModel
	 * @param aBoxModel
	 * @param directContext
	 */
	public void createBindingConstants(String pertSid,
			Individual endpointIndividual, String chembelBioAssayAid,
			OntModel tBoxModel, OntModel aBoxModel, DirectContext directContext) {
		// data type
		ObjectProperty op;
		DatatypeProperty dp;

		if (pertSid != null && pertSid.length() > 0) {
			// a whole bunch of garbage to handle perturbagen
			int substanceId = Integer.parseInt(pertSid);

			String id = Util.getIndividualIri(directContext.getConfiguration()
					.getNs(NS_BAO), Constants.INDI_PREFIX
					+ getId_(perturbagenId) + substanceId);
			Individual perturbagenIndividaul = aBoxModel.getIndividual(id);
			if (perturbagenIndividaul == null) {
				perturbagenIndividaul = createIndividual(
						getOntClass(tBoxModel, getId(perturbagenId),
								directContext), id,
						String.valueOf(substanceId), aBoxModel,
						LabelModifier.COMPOSITE);

				// /////////////////////////// has id to pubchem cid
				op = getObjectProperty(tBoxModel, getId(hasId), directContext);
				String pubchemCId = Util.getIndividualIri(directContext
						.getConfiguration().getNs(NS_BAO),
						Constants.INDI_PREFIX + getId_(this.pubchemSid)
								+ substanceId);
				Individual object = createIndividual(
						getOntClass(tBoxModel, getId(this.pubchemSid),
								directContext), pubchemCId,
						String.valueOf(substanceId), aBoxModel,
						LabelModifier.COMPOSITE);
				perturbagenIndividaul.addProperty(op, object);
				// data type property
				dp = getDatatypeProperty(tBoxModel, getId(hasIdValue),
						directContext);
				addLiteral(object, dp, 'i', aBoxModel, substanceId + "");
			}

			// subject is the endpoint
			ObjectProperty predicate = getObjectProperty(tBoxModel,
					getId(hasPerturbagen), directContext);
			endpointIndividual.addProperty(predicate, perturbagenIndividaul);

			// link back to bio assay
			String bioassayId = Util.getIndividualIri(directContext
					.getConfiguration().getNs(NS_BAO), Constants.INDI_PREFIX
					+ getId_(this.bioassyId) + chembelBioAssayAid);
			Individual bioassayIndividual = getABoxIndividual(aBoxModel,
					bioassayId);
			op = getObjectProperty(tBoxModel, getId(isPerturbagenOf),
					directContext);
			perturbagenIndividaul.addProperty(op, bioassayIndividual);
			op = getObjectProperty(tBoxModel, getId(hasPerturbagen),
					directContext);
			bioassayIndividual.addProperty(op, perturbagenIndividaul);
		}

	}

	/**
	 * 
	 * @param temUnit
	 * @param temValue
	 * @param pertSid
	 * @param endpointIndividual
	 * @param chembelBioAssayAid
	 * @param tBoxModel
	 * @param aBoxModel
	 * @param directContext
	 */
	public void createEndpointTemperature(String temUnit, String temValue,
			String pertSid, Individual endpointIndividual,
			String chembelBioAssayAid, OntModel tBoxModel, OntModel aBoxModel,
			DirectContext directContext) {
		createBindingConstants(pertSid, endpointIndividual, chembelBioAssayAid,
				tBoxModel, aBoxModel, directContext);
		ObjectProperty op = getObjectProperty(tBoxModel,
				getId(hasTemperatureUnit), directContext);
		endpointIndividual.addProperty(
				op,
				getTBoxIndividual(tBoxModel, getId(degreeCelsiusIndividual),
						directContext));
		DatatypeProperty dp = getDatatypeProperty(tBoxModel,
				getId(hasTemperatureValue), directContext);
		addLiteral(endpointIndividual, dp, 'f', aBoxModel, temValue);
	}

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	@Override
	public void visit(DirectContext directContext, Individual subject,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int mult) {

	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.Concept#doData(java.sql.ResultSet,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	@Override
	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {

	}
}

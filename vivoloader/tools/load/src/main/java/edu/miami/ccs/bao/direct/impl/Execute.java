package edu.miami.ccs.bao.direct.impl;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.direct.api.StoreType;
import edu.miami.ccs.bao.main.Configuration;
import edu.miami.ccs.bao.rules.api.Ontology;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
/**
 * Excecute class is the one that deligates information to ontology and direct context
 * It also takes all the imported ontologies to create a comple bao ontology
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class Execute {

	public static void exec(String[] args) throws Exception {

		String userName = "username";
		String password = "password";
		String url = "jdbc:mysql://localhost/pcrelmir3";
		String annotationFile = "/home/user/ontology/test3333.csv";
		// String annotationFile =
		// "/home/sam/tmp/tmp4/STAT_campaign_for_loading.csv";
		// String inOwl = "/home/sam/projects/bao/ontology/bioassay_new.owl";
		String inOwl = "/home/user/ontology/bao_search_ont/baoSearch.owl";
		// String inOwl = "/home/sam/tmp/baomerged.owl";
		String aBoxDir = "baoABoxTdb";
		int maxDbRead = 1;
		StoreType storeType = StoreType.TDB_JOSEKI;
		// StoreType storeType = StoreType.SDB;
		String outSdbUserName = "username";
		String outSdbPassWord = "password";
		String outSdb = "jdbc:mysql://localhost/bao_aboxdb";

		Configuration config = new Configuration();
		if (args.length == 0) {

			config.put(Configuration.jdbcUrl, url);
			config.put(Configuration.jdbcUserName, userName);
			config.put(Configuration.jdbcPassWord, password);
			config.put(Configuration.annonFile, annotationFile);
			config.put(Configuration.outDirectory, aBoxDir);
			config.put(Configuration.owlFile, inOwl);
			config.put(Configuration.storeType, storeType.getTypeName()
					.toLowerCase());
			config.put(Configuration.maxDbRead, "1");
			config.put(Configuration.outSdbUserName, outSdbUserName);
			config.put(Configuration.outSdbPassWord, outSdbPassWord);
			config.put(Configuration.outSdb, outSdb);
			config.put(Configuration.baoSearchConceptualization, "true");

			Map<String, String> namespaces = new HashMap<String, String>();
			namespaces.put("bao", "http://www.bioassayontology.org/bao#");
			namespaces.put("go", "http://purl.org/obo/owl/GO#");
			namespaces.put("ncbit", "http://purl.org/obo/owl/NCBITaxon#");
			namespaces.put("baosearch",
					"http://www.bioassayontology.org/baosearch#");

			Map<String, String> altEntries = new HashMap<String, String>();

			altEntries.put("http://www.bioassayontology.org/bao",
					"file:/home/user/ontology/bioassay_new.owl");

			altEntries
					.put("http://bioassayontology.org/bao/external/CLO_import.owl",
							"file:/home/user/ontology/onto_fox/CLO_import.owl");
			altEntries
					.put("http://bioassayontology.org/bao/external/IAO_import.owl",
							"file:/home/user/import/IAO_import.owl");
			altEntries
					.put("http://bioassayontology.org/bao/external/OBI_import.owl",
							"file:/home/user/ontology/import/OBI_import.owl");
			altEntries
					.put("http://bioassayontology.org/bao/external/UO_import.owl",
							"file:/home/user/ontology/import/UO_import.owl");
			altEntries
					.put("http://bioassayontology.org/bao/external/organism_import.owl",
							"file:/home/user/ontology/onto_fox/organism_import.owl");
			altEntries
					.put("http://bioassayontology.org/bao/external/signaling_pathway_import.owl",
							"file:/home/user/ontology/onto_fox/signaling_pathway_import.owl");
			altEntries
					.put("http://www.bioassayontology.org/bao/external/biological_processes_import.owl",
							"file:/home/user/ontology/onto_fox/biological_processes_import.owl");

			for (Map.Entry<String, String> entry : namespaces.entrySet()) {
				config.putNs(entry.getKey(), entry.getValue());
			}

			for (Map.Entry<String, String> entry : altEntries.entrySet()) {
				config.putAltEntry(entry.getKey(), entry.getValue());
			}
		}

		if (args.length > 0) {
			String configFile = args[0];
			config.read(new File(configFile));
			url = config.get(Configuration.jdbcUrl);
			userName = config.get(Configuration.jdbcUserName);
			password = config.get(Configuration.jdbcPassWord);
			annotationFile = config.get(Configuration.annonFile);
			storeType = StoreType.getType(config.get(Configuration.storeType));
			maxDbRead = Integer.parseInt(config.get(Configuration.maxDbRead));
		}

		DirectContext directContext = new DirectContextImpl(config);

		directContext.setMaxDbRead(maxDbRead);
		directContext.initCsvSource(annotationFile);
		directContext.initDataSource(url, userName, password);
		directContext.initLogger("bao_full_logger.txt");
		// main loop
		Ontology ont = new GenericOntologyImpl(directContext);

		ont.loadOntModel(null, null, storeType);

		ont.updateModel();

		ont.write(null, storeType);

	}
}

package edu.miami.ccs.bao.rules.impl;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.ontology.*;
import edu.miami.ccs.bao.rules.api.*;
import edu.miami.ccs.bao.util.Util;

import java.util.List;

/**
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class TypeAEngineImpl implements NaiveEngine {

	/*
	 * @see
	 * edu.miami.ccs.bao.rules.api.Engine#pre(edu.miami.ccs.bao.rules.api.Mapper
	 * .Individual, edu.miami.ccs.bao.rules.api.SSRow,
	 * com.hp.hpl.jena.ontology.OntModel, com.hp.hpl.jena.ontology.OntModel,
	 * edu.miami.ccs.bao.rules.api.Document, java.lang.String, java.lang.String)
	 */
	 public void pre(Mapper.Individual individual,
             SSRow row,
             OntModel tBoxModel,
             OntModel aBoxModel,
             Document document,
             String indiPrefix,
             String indCnt) {
		// type A individual engine

		String ns = document.getMapper().getNamespace().getNs()
				.get(individual.getNsPrefix());
		SSRow head = document.getAnnotatedDataSource().getHead();

		OntClass mainClass = tBoxModel.getOntClass(ns
				+ individual.getConceptType().getOnt());

		if (mainClass == null) {
			error("main class not found", individual.getConceptType().getOnt());
		}

		Individual ontIndividual;
		if (individual.getConceptType().getXref() != null) {
			String xrefValue = Util.getValue(head, row, null, null, individual
					.getConceptType().getXref());
			ontIndividual = aBoxModel.createIndividual(
					getConceptName(ns, indiPrefix, individual.getConceptType()
							.getOnt(), xrefValue), mainClass);
		} else {
			ontIndividual = aBoxModel.createIndividual(
					getConceptName(ns, indiPrefix, individual.getConceptType()
							.getOnt(), indCnt), mainClass);
		}

		// data types
		for (Mapper.DataType dataType : individual.getDataTypes()) {
			DatatypeProperty datatypeProperty = tBoxModel
					.getDatatypeProperty(ns + dataType.getOnt());
			if (datatypeProperty == null) {
				error("data type is not found", dataType.getOnt());
			}
			String value = Util.getValue(head, row, null, null,
					dataType.getData());
			value = (value != null) ? value : "";
			if (dataType.getXsd().equals("string")) {
				ontIndividual.addProperty(datatypeProperty, value);
			} else if (dataType.getXsd().equals("integer")) {
				try {
					ontIndividual.addLiteral(datatypeProperty, aBoxModel
							.createTypedLiteral(Integer.parseInt(value),
									XSDDatatype.XSDinteger));
				} catch (NumberFormatException e) {
					String err = TypeAEngineImpl.class.getName() + " value : "
							+ value + " for "
							+ Util.getStrippedEntry(dataType.getData());

				}
			} else if (dataType.getXsd().equals("float")) {
				try {
					ontIndividual.addLiteral(datatypeProperty, aBoxModel
							.createTypedLiteral(Float.parseFloat(value),
									XSDDatatype.XSDfloat));
				} catch (NumberFormatException e) {
					String err = TypeAEngineImpl.class.getName() + " value : "
							+ value + " for "
							+ Util.getStrippedEntry(dataType.getData());

				}
			}

		}

		// work with the object properties

	}

	/*
	 * @see
	 * edu.miami.ccs.bao.rules.api.Engine#post(edu.miami.ccs.bao.rules.api.Mapper
	 * .Individual, edu.miami.ccs.bao.rules.api.SSRow,
	 * com.hp.hpl.jena.ontology.OntModel, com.hp.hpl.jena.ontology.OntModel,
	 * edu.miami.ccs.bao.rules.api.Document, java.lang.String, java.lang.String)
	 */
	public void post(Mapper.Individual source, SSRow row, OntModel tBoxModel,
			OntModel aBoxModel, Document document, String indiPrefix,
			String indCnt) {

		String ns = document.getMapper().getNamespace().getNs()
				.get(source.getNsPrefix());
		SSRow head = document.getAnnotatedDataSource().getHead();

		Individual sourceIndividual;
		if (source.getConceptType().getXref() != null) {
			String xrefValue = Util.getValue(head, row, null, null, source
					.getConceptType().getXref());
			sourceIndividual = aBoxModel.getIndividual(getConceptName(ns,
					indiPrefix, source.getConceptType().getOnt(), xrefValue));
		} else {
			sourceIndividual = aBoxModel.getIndividual(getConceptName(ns,
					indiPrefix, source.getConceptType().getOnt(), indCnt));
		}

		if (sourceIndividual == null) {
			error("source is not found",
					getConceptName(ns, indiPrefix, source.getConceptType()
							.getOnt(), indCnt));
		} else {
			List<Mapper.ObjType> objTypeList = source.getObjTypes();
			for (Mapper.ObjType objType : objTypeList) {
				ObjectProperty objectProperty = tBoxModel.getObjectProperty(ns
						+ objType.getOnt());
				if (objectProperty == null) {
					error("object property not found ", ns + objType.getOnt());
				}

				// case 1, refer to type A individual
				String refTypeA = objType.getRefTypeA();

				if (refTypeA != null) {
					Individual targetIndividual = null;
					for (Mapper.Individual refIndividualTypeA : document
							.getMapper().getIndividualsTypeA().values()) {
						if (refIndividualTypeA.getId().equals(refTypeA)) {
							targetIndividual = aBoxModel
									.getIndividual(getConceptName(ns,
											indiPrefix, refIndividualTypeA
													.getConceptType().getOnt(),
											indCnt));
							break;

						}
					}
					if (targetIndividual == null) {
						error("ref source is not found", refTypeA);
					} else {
						sourceIndividual.addProperty(objectProperty,
								targetIndividual);
					}
				}

				// case 2, refer to type B individual
				String refTypeB = objType.getRefTypeB();
				if (refTypeB != null) {
					// target individual
					Mapper.IndividualTypeB refIndividual = document.getMapper()
							.getIndividualsTypeB().get(refTypeB);
					if (refIndividual != null) {
						Engine ruleEngine = EngineFactory.getTypeBEngine(
								objectProperty, sourceIndividual);
						ruleEngine.post(refIndividual, row, tBoxModel,
								aBoxModel, document, indiPrefix, indCnt);
					} else {
						error("ref individual not found : ", refTypeB);
					}
				}

				String refSingleOntologyIndividual = objType
						.getRefSingleOntologyIndividual();
				if (refSingleOntologyIndividual != null) {
					Mapper.SingleOntologyIndividual singleIndividual = document
							.getMapper().getSingleOntologyIndividual()
							.get(refSingleOntologyIndividual);
					if (singleIndividual == null) {
						error("single individual should not be null",
								refSingleOntologyIndividual);
					} else {
						Individual refOntIndividual = aBoxModel
								.getIndividual(ns + singleIndividual.getId());
						if (refOntIndividual == null) {
							error("ont individual should not be null",
									refSingleOntologyIndividual);
						} else {
							sourceIndividual.addProperty(objectProperty,
									refOntIndividual);
						}
					}
				}

			}
		}

	}

	public void error(String err1, String err2) {

		System.exit(-98);
	}

	public void message(String message) {

	}

	public String getConceptName(String ns, String prefix, String ont,
			String cnt) {
		return ns + prefix + ont + "_" + cnt;
	}
}

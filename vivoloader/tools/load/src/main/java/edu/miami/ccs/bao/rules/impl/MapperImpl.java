package edu.miami.ccs.bao.rules.impl;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import edu.miami.ccs.bao.rules.api.Mapper;
import org.apache.axiom.om.OMElement;

import javax.xml.namespace.QName;
import java.util.*;

/**
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class MapperImpl implements Mapper {

	private Namespace namespace;

	private Map<String, IndividualTypeA> individualTypeAMap = new HashMap<String, IndividualTypeA>();

	private Map<String, IndividualTypeB> individualTypeBMap = new HashMap<String, IndividualTypeB>();

	private Map<String, IndividualTypeC> individualTypeCMap = new HashMap<String, IndividualTypeC>();

	private Map<String, SingleOntologyIndividual> singleOntologyIndividualMap = new HashMap<String, SingleOntologyIndividual>();

	public Namespace getNamespace() {
		return namespace;
	}

	public Map<String, IndividualTypeA> getIndividualsTypeA() {
		return individualTypeAMap;
	}

	public Map<String, IndividualTypeB> getIndividualsTypeB() {
		return individualTypeBMap;
	}

	public Map<String, IndividualTypeC> getIndividualsTypeC() {
		return individualTypeCMap;
	}

	public Map<String, SingleOntologyIndividual> getSingleOntologyIndividual() {
		return singleOntologyIndividualMap;
	}

	/*
	 * @see
	 * edu.miami.ccs.bao.rules.api.Mapper#build(org.apache.axiom.om.OMElement)
	 */
	public void build(OMElement doc) {
		for (Iterator individualIterator = doc.getChildElements(); individualIterator
				.hasNext();) {
			OMElement element = (OMElement) individualIterator.next();
			// state machine
			if (element.getLocalName().equals("namespace")) {
				namespace = new NamespaceImpl();
				namespace.build(element);
				continue;
			}

			if (element.getLocalName().equals("singleOntologyIndividual")) {
				SingleOntologyIndividual individual = new SingleOntologyIndividualImpl();
				individual.build(element);
				singleOntologyIndividualMap.put(individual.getId(), individual);
				continue;
			}

			if (element.getLocalName().equals("individualTypeA")) {
				IndividualTypeA individual = new IndividualTypeAImpl();
				individual.build(element);
				individualTypeAMap.put(individual.getId(), individual);
				continue;
			}

			if (element.getLocalName().equals("individualTypeB")) {
				IndividualTypeB individual = new IndividualTypeBImpl();
				individual.build(element);
				individualTypeBMap.put(individual.getId(), individual);
				continue;
			}

			if (element.getLocalName().equals("individualTypeC")) {
				IndividualTypeC individual = new IndividualTypeCImpl();
				individual.build(element);
				individualTypeCMap.put(individual.getId(), individual);
			}

		}
	}

	class SingleOntologyIndividualImpl extends IndividualImpl implements
			SingleOntologyIndividual {

	}

	class IndividualTypeAImpl extends IndividualImpl implements IndividualTypeA {

	}

	class IndividualTypeBImpl extends IndividualImpl implements IndividualTypeB {

		List<If> ifs = new ArrayList<If>();

		public List<If> getIfs() {
			return ifs;
		}

		/*
		 * @see
		 * edu.miami.ccs.bao.rules.impl.MapperImpl.IndividualImpl#build(org.
		 * apache.axiom.om.OMElement)
		 */
		@Override
		public void build(OMElement ele) {
			super.build(ele);
			for (Iterator iterator = ele.getChildElements(); iterator.hasNext();) {
				OMElement element = (OMElement) iterator.next();
				if (element.getLocalName().equals("if")) {
					If anIf = new IfImpl();
					anIf.build(element);
					ifs.add(anIf);
				}
			}

		}
	}

	class IndividualTypeCImpl extends IndividualImpl implements IndividualTypeC {

	}

	class NamespaceImpl implements Namespace {

		Map<String, String> nsMap = new HashMap<String, String>();

		String xmlBaseNs;

		public Map<String, String> getNs() {
			return nsMap;
		}

		public String getXmlBaseNs() {
			return xmlBaseNs;
		}

		/*
		 * @see
		 * edu.miami.ccs.bao.rules.api.Mapper.Build#build(org.apache.axiom.om
		 * .OMElement)
		 */
		public void build(OMElement ele) {
			for (Iterator iterator = ele.getChildElements(); iterator.hasNext();) {
				OMElement element = (OMElement) iterator.next();
				if (element.getLocalName().equals("ns")) {
					String key = element.getAttributeValue(new QName("prefix"))
							.trim();
					String value = element.getText();
					nsMap.put(key, value);
					if (element.getAttribute(new QName("xmlbase")) != null
							&& element.getAttributeValue(new QName("xmlbase"))
									.equalsIgnoreCase("true")) {
						xmlBaseNs = value;
					}
				}
			}
		}
	}

	class ConceptTypeImpl implements ConceptType {

		String ont;

		String xref;

		public String getOnt() {
			return ont;
		}

		public String getXref() {
			return xref;
		}

		public void build(OMElement ele) {
			for (Iterator elementsIterator = ele.getChildElements(); elementsIterator
					.hasNext();) {
				OMElement element = (OMElement) elementsIterator.next();
				if (element.getLocalName().equals("ont")) {
					ont = element.getText().trim();
					continue;
				}
				if (element.getLocalName().equals("xref")) {
					xref = element.getText().trim();
				}
			}
		}
	}

	class DataTypeImpl implements DataType {

		String xsd;

		String xref;

		String ont;

		String data;

		public String getXsd() {
			return xsd;
		}

		public String getXref() {
			return xref;
		}

		public String getData() {
			return data;
		}

		public String getOnt() {
			return ont;
		}

		public void build(OMElement ele) {
			for (Iterator elementsIterator = ele.getChildElements(); elementsIterator
					.hasNext();) {
				OMElement element = (OMElement) elementsIterator.next();
				if (element.getLocalName().equals("ont")) {
					ont = element.getText().trim();
					continue;
				}
				if (element.getLocalName().equals("xsd")) {
					xsd = element.getText().trim();
					continue;
				}
				if (element.getLocalName().equals("data")) {
					data = element.getText().trim();
				}
			}
		}
	}

	class ObjTypeImpl implements ObjType {

		String ont;

		String refTypeA;

		String refTypeB;

		String refTypeC;

		String refSingleOntologyIndividual;

		String refTBoxIndividual;

		public String getOnt() {
			return ont;
		}

		public String getRefSingleOntologyIndividual() {
			return refSingleOntologyIndividual;
		}

		public String getRefTypeA() {
			return refTypeA;
		}

		public String getRefTypeB() {
			return refTypeB;
		}

		public String getRefTypeC() {
			return refTypeC;
		}

		public String getRefTBoxIndividual() {
			return refTBoxIndividual;
		}

		/*
		 * @see
		 * edu.miami.ccs.bao.rules.api.Mapper.Build#build(org.apache.axiom.om
		 * .OMElement)
		 */
		public void build(OMElement ele) {
			for (Iterator elementsIterator = ele.getChildElements(); elementsIterator
					.hasNext();) {
				OMElement element = (OMElement) elementsIterator.next();
				if (element.getLocalName().equals("ont")) {
					ont = element.getText().trim();
					continue;
				}
				if (element.getLocalName().equals("refTypeA")) {
					refTypeA = element.getText().trim();
					continue;
				}
				if (element.getLocalName().equals("refTypeB")) {
					refTypeB = element.getText().trim();
					continue;
				}
				if (element.getLocalName().equals("refTypeC")) {
					refTypeC = element.getText().trim();
					continue;
				}
				if (element.getLocalName()
						.equals("refSingleOntologyIndividual")) {
					refSingleOntologyIndividual = element.getText().trim();
					continue;
				}
				if (element.getLocalName().equals("refTBoxIndividual")) {
					refTBoxIndividual = element.getText().trim();
				}
			}
		}
	}

	class IfImpl implements If {

		String test;

		ConceptType conceptType;

		List<ObjType> objTypes = new ArrayList<ObjType>();

		List<DataType> dataTypes = new ArrayList<DataType>();

		public String getTest() {
			return test;
		}

		public ConceptType getConceptType() {
			return conceptType;
		}

		public List<ObjType> getObjTypes() {
			return objTypes;
		}

		public List<DataType> getDataTypes() {
			return dataTypes;
		}

		/*
		 * @see
		 * edu.miami.ccs.bao.rules.api.Mapper.Build#build(org.apache.axiom.om
		 * .OMElement)
		 */
		public void build(OMElement ele) {
			test = ele.getAttributeValue(new QName("test")).trim();
			for (Iterator iterator = ele.getChildElements(); iterator.hasNext();) {
				OMElement element = (OMElement) iterator.next();
				if (element.getLocalName().equals("conceptType")) {
					conceptType = new ConceptTypeImpl();
					conceptType.build(element);
				}
				if (element.getLocalName().equals("dataType")) {
					DataType dataType = new DataTypeImpl();
					dataType.build(element);
					dataTypes.add(dataType);
				}

				if (element.getLocalName().equals("objType")) {
					ObjType objType = new ObjTypeImpl();
					objType.build(element);
					objTypes.add(objType);
				}
			}

		}
	}

	abstract class IndividualImpl implements Individual {

		String id;

		String nsPrefix;

		ConceptType conceptType;

		List<DataType> dataTypes = new ArrayList<DataType>();

		List<ObjType> objTypes = new ArrayList<ObjType>();

		public String getId() {
			return id;
		}

		public String getNsPrefix() {
			return nsPrefix;
		}

		public ConceptType getConceptType() {
			return conceptType;
		}

		public List<DataType> getDataTypes() {
			return dataTypes;
		}

		public List<ObjType> getObjTypes() {
			return objTypes;
		}

		/*
		 * @see
		 * edu.miami.ccs.bao.rules.api.Mapper.Build#build(org.apache.axiom.om
		 * .OMElement)
		 */
		public void build(OMElement ele) {
			id = ele.getAttributeValue(new QName("id"));
			nsPrefix = ele.getAttributeValue(new QName("nsPrefix"));

			for (Iterator elementsIterator = ele.getChildElements(); elementsIterator
					.hasNext();) {
				OMElement element = (OMElement) elementsIterator.next();
				if (element.getLocalName().equals("conceptType")) {
					conceptType = new ConceptTypeImpl();
					conceptType.build(element);
					continue;
				}

				if (element.getLocalName().equals("dataType")) {
					DataType dataType = new DataTypeImpl();
					dataType.build(element);
					dataTypes.add(dataType);
					continue;
				}

				if (element.getLocalName().equals("objType")) {
					ObjType objType = new ObjTypeImpl();
					objType.build(element);
					objTypes.add(objType);
				}
			}
		}
	}
}

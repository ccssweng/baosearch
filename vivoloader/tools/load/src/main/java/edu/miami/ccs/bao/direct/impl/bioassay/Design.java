package edu.miami.ccs.bao.direct.impl.bioassay;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.AbstractConcept;
import edu.miami.ccs.bao.direct.api.Concept;
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.direct.impl.specification.DesignSpecification;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Design class creates individuals of design and it calls design specification
 * class as well
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class Design extends AbstractConcept {

	protected static String designMainConcept = "DesignMainConcept";

	protected static String designDetail1 = "Design_Detail1";

	protected static String designDetail2 = "DesignDetail2";

	protected static String hasSpecification = "214";

	protected static String isDesignOf = "391";

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	public void visit(DirectContext directContext, Individual subject,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int mult) {
		// Let do a simple error handling

		String designMainConceptParam1 = Util.getValue(directContext
				.getCsvDataSource().getHead(), currentRow, designMainConcept);

		String designDetail1Param1 = Util.getValue(directContext
				.getCsvDataSource().getHead(), currentRow, designDetail1);
		String designDetail2Param2 = Util.getValue(directContext
				.getCsvDataSource().getHead(), currentRow, designDetail2);

		List<String> inverseRelationList = new ArrayList<String>();
		inverseRelationList.add(isDesignOf);

		if (designDetail2Param2 != null && designDetail2Param2.length() > 0) {
			String[] designDetail2Param2Toks = designDetail2Param2.split(";");
			for (String designDetail2Param2Tok : designDetail2Param2Toks) {

				Individual localIndividualParam1 = createGlobalIndividualParam1(
						designDetail2Param2Tok, inverseRelationList,
						directContext.getGlobalInformation().designMapping,
						directContext, subject, predicate, tBoxModel,
						aBoxModel, IdModifier.LOCAL_ID);

				// baoseach link to bioassay
				// link back to bio assay
				addBaoSearchObjectPropertyToBioAssay(directContext, tBoxModel,
						aBoxModel, currentRow, localIndividualParam1,
						"hasDesign", "isDesignOf");
			}

		} else if (designDetail1Param1 != null
				&& designDetail1Param1.length() > 0) {
			String[] designDetail1Param1Toks = designDetail1Param1.split(";");
			for (String designDetail1Param1Tok : designDetail1Param1Toks) {
				/*
				 * Individual localIndividualParam1 =
				 * createLocalIndividualParam1( designDetail1Param1Tok,
				 * inverseRelationList, directContext, subject, predicate,
				 * tBoxModel, aBoxModel);
				 * createHasSpecification(localIndividualParam1, directContext,
				 * currentRow, tBoxModel, aBoxModel);
				 */

				Individual localIndividualParam1 = createGlobalIndividualParam1(
						designDetail1Param1Tok, inverseRelationList,
						directContext.getGlobalInformation().designMapping,
						directContext, subject, predicate, tBoxModel,
						aBoxModel, IdModifier.LOCAL_ID);

				// baoseach link to bioassay
				// link back to bio assay
				addBaoSearchObjectPropertyToBioAssay(directContext, tBoxModel,
						aBoxModel, currentRow, localIndividualParam1,
						"hasDesign", "isDesignOf");
			}

		} else if (designMainConceptParam1 != null
				&& designMainConceptParam1.length() > 0) {
			String[] designMainConceptParam1Toks = designMainConceptParam1
					.split(";");
			for (String designMainConceptParam1Tok : designMainConceptParam1Toks) {
				/*
				 * Individual localIndividualParam1 =
				 * createLocalIndividualParam1( designMainConceptParam1Tok,
				 * inverseRelationList, directContext, subject, predicate,
				 * tBoxModel, aBoxModel);
				 * createHasSpecification(localIndividualParam1, directContext,
				 * currentRow, tBoxModel, aBoxModel);
				 */

				Individual localIndividualParam1 = createGlobalIndividualParam1(
						designMainConceptParam1Tok, inverseRelationList,
						directContext.getGlobalInformation().designMapping,
						directContext, subject, predicate, tBoxModel,
						aBoxModel, IdModifier.LOCAL_ID);
				// baoseach link to bioassay
				// link back to bio assay
				addBaoSearchObjectPropertyToBioAssay(directContext, tBoxModel,
						aBoxModel, currentRow, localIndividualParam1,
						"hasDesign", "isDesignOf");
			}

		} else {
			directContext.getLogger().warning(
					Design.class.getName()
							+ " There is no design details for this assay");
		}
	}

	/**
	 * 
	 * @param localIndividual
	 * @param directContext
	 * @param currentRow
	 * @param tBoxModel
	 * @param aBoxModel
	 */
	private void createHasSpecification(Individual localIndividual,
			DirectContext directContext, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel) {
		if (localIndividual != null) {
			// //design spec
			ObjectProperty op = getObjectProperty(tBoxModel,
					getId(hasSpecification), directContext);
			Concept designSpec = new DesignSpecification();
			designSpec.visit(directContext, localIndividual, op, currentRow,
					tBoxModel, aBoxModel, -1);
		}
	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.Concept#doData(java.sql.ResultSet,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {
		// not used

	}

}

package edu.miami.ccs.bao.direct.api;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.RDFS;
import edu.miami.ccs.bao.main.Configuration;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *  This counter will take care of the unique uri creation
 *  64-bit number. 64-bit signed two's complement integer.
 *	It has a minimum value of -9,223,372,036,854,775,808 and a
 *	maximum value of 9,223,372,036,854,775,807 (inclusive) 
 *
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public abstract class AbstractConcept implements Concept {

	

	public static long globalCounter = 0;

	// heavily used
	protected final String bioassyId = "015";
	protected final String annonAid = "aid";

	protected static String NS_BAO = "bao";

	protected static String NS_GO = "go";

	// protected static String NS_NCBIT = "ncbit";

	protected final String baoSearchNs = "http://www.bioassayontology.org/baosearch#";

	public enum LabelModifier {
		SHORT_ID, CLASS_LABEL, COMPOSITE
	}

	public enum IdModifier {
		LOCAL_ID, IRI, DIRECT_ID
	}

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#getTBoxLabel(edu.miami.ccs.bao.direct
	 * .api.DirectContext, java.lang.String, com.hp.hpl.jena.ontology.OntModel)
	 */
	public String getTBoxLabel(DirectContext directContext, String localName,
			OntModel tBoxModel) {
		Resource resource = getFirstResource(directContext, tBoxModel,
				localName);
		if (resource != null) {
			Statement statement = resource.getProperty(RDFS.label);
			if (statement != null) {
				return statement.getLiteral().getString();
			}
		}
		return null;
	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.Concept#error(java.lang.String,
	 * java.lang.String)
	 */
	public void error(String err1, String err2) {
		System.err.println(err1 + " : " + err2);
		System.exit(-101);
	}

	public void message(String msg) {
		System.out.println(msg);
	}

	public void warning(String warning) {
		System.err.println(warning);
	}

	/**
	 * @param suffix
	 *            3-digit suffix
	 * @return suffix
	 */
	public String getId_(String suffix) {
		return "BAO_0000" + suffix + "_";
	}

	public String getId(String suffix) {
		return "BAO_0000" + suffix;
	}

	/**
	 * 
	 * @param tBoxModel
	 * @param localName
	 * @param directContext
	 * @return
	 */
	public OntClass getOntClass(OntModel tBoxModel, String localName,
			DirectContext directContext) {
		OntClass ontClazz = getFirstOntClass(directContext, tBoxModel,
				localName);
		if (ontClazz != null) {
			return ontClazz;
		} else {
			directContext.getLogger().warning("No ont class for " + localName);
			error("ont class is not found", localName);

		}
		return null;
	}

	/**
	 * 
	 * @param tBoxModel
	 * @param localName
	 * @return
	 */
	public boolean isCellToDecIdMappingInExistence(OntModel tBoxModel,
			String localName) {
		ExtendedIterator<OntClass> iterator = tBoxModel.listNamedClasses();
		while (iterator.hasNext()) {
			OntClass aClass = iterator.next();
			Statement anLabel = aClass.getProperty(RDFS.label);
			if (anLabel != null && anLabel.getLiteral() != null
					&& anLabel.getLiteral().getString() != null) {
				if (anLabel.getLiteral().getString().toLowerCase()
						.equals(localName.toLowerCase())) { // normalized check
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * @param ctx
	 * @param tBoxModel
	 * @param localName
	 * @return
	 */
	public String getCellToDecIdMapping(DirectContext ctx, OntModel tBoxModel,
			String localName) {
		ExtendedIterator<OntClass> iterator = tBoxModel.listNamedClasses();
		while (iterator.hasNext()) {
			OntClass aClass = iterator.next();
			Statement anLabel = aClass.getProperty(RDFS.label);
			if (anLabel != null && anLabel.getLiteral() != null
					&& anLabel.getLiteral().getString() != null) {
				if (anLabel.getLiteral().getString().toLowerCase()
						.equals(localName.toLowerCase())) { // normalized check
					// return
					// anLabel.getLiteral().getString().trim().toLowerCase();
					return aClass.getLocalName();
				}
			}
		}
		ctx.getLogger().warning(
				"Local name to id mapping failed with " + localName);
		error("local name to id mapping failed", localName);
		return null;
	}

	/**
	 * This method is written in order to compensate mistakes in bad IRI
	 * naming conventions
	 * 
	 * @param ctx
	 *            direct context
	 * @param tBoxModel
	 *            t-box model
	 * @param localName
	 *            rdfs:label
	 * @return IRI of the resource
	 */
	public String getCellToDecIRIMapping(DirectContext ctx, OntModel tBoxModel,
			String localName) {
		ExtendedIterator<OntClass> iterator = tBoxModel.listNamedClasses();
		while (iterator.hasNext()) {
			OntClass aClass = iterator.next();
			Statement anLabel = aClass.getProperty(RDFS.label);
			if (anLabel != null && anLabel.getLiteral() != null
					&& anLabel.getLiteral().getString() != null) {
				if (anLabel.getLiteral().getString().toLowerCase()
						.equals(localName.toLowerCase())) { // normalized check
					// return
					// anLabel.getLiteral().getString().trim().toLowerCase();
					return aClass.getURI();
				}
			}
		}
		ctx.getLogger().warning(
				"Local name to id mapping failed with " + localName);
		error("local name to id mapping failed", localName);
		return null;
	}

	/*
	 * public String getCellToBaoIdMappingWarning(OntModel tBoxModel, String
	 * localName) { ExtendedIterator<OntClass> iterator =
	 * tBoxModel.listNamedClasses(); while (iterator.hasNext()) { OntClass
	 * aClass = iterator.next(); Statement anLabel =
	 * aClass.getProperty(RDFS.label); if (anLabel != null &&
	 * anLabel.getLiteral() != null && anLabel.getLiteral().getString() != null)
	 * { if (anLabel.getLiteral().getString().equals(localName)) { return
	 * aClass.getLocalName(); } } } warning("local name to id mapping failed " +
	 * localName); return null; }
	 */

	public Individual getTBoxIndividual(OntModel tBoxModel, String localName,
			DirectContext directContext) {
		Individual individual = getFirstIndividual(directContext, tBoxModel,
				localName);
		if (individual != null) {
			return individual;
		} else {
			error("ont individual is not found", localName);
		}
		return null;
	}

	/**
	 * 
	 * @param aBoxModel
	 * @param id
	 * @return
	 */

	public Individual getABoxIndividual(OntModel aBoxModel, String id) {
		Individual individual = aBoxModel.getIndividual(id);
		if (individual != null) {
			return individual;
		} else {
			error("aBox individual is not found", id);
		}
		return null;
	}

	/**
	 * 
	 * @param tBoxModel
	 * @param localName
	 * @param directContext
	 * @return
	 */
	public ObjectProperty getObjectProperty(OntModel tBoxModel,
			String localName, DirectContext directContext) {
		ObjectProperty objectProperty = getFirstObjectProperty(directContext,
				tBoxModel, localName);
		if (objectProperty != null) {
			return objectProperty;
		} else {
			error("object property is not found", localName);
		}
		return null;
	}

	/**
	 * 
	 * @param tBoxModel
	 * @param namespace
	 * @param localName
	 * @return
	 */

	public ObjectProperty getObjectProperty(OntModel tBoxModel,
			String namespace, String localName) {
		ObjectProperty objectProperty = tBoxModel.getObjectProperty(namespace
				+ localName);
		if (objectProperty != null) {
			return objectProperty;
		} else {
			error("object property is not found", namespace + localName);
		}
		return null;
	}

	/**
	 * 
	 * @param tBoxModel
	 * @param localName
	 * @param directContext
	 * @return
	 */

	public DatatypeProperty getDatatypeProperty(OntModel tBoxModel,
			String localName, DirectContext directContext) {
		DatatypeProperty datatypeProperty = getFirstDatatypeProperty(
				directContext, tBoxModel, localName);
		if (datatypeProperty != null) {
			return datatypeProperty;
		} else {
			error("datatype property is not found", localName);
		}
		return null;
	}

	/**
	 * This is the method that should be called by a class to create an
	 * individual of the concept. This method provides facility to add specific
	 * labels and comments
	 * 
	 * @param ontClazz
	 *            Jena class description
	 * @param id
	 *            IRI of the individual
	 * @param shortId
	 *            Short id is some short description of the individual that can
	 *            be used by other utilities such as Lucene
	 * @param aBoxModel
	 *            A-Box model
	 * @param modifier
	 *            This provides the switch to use the rdfs:label modifier.
	 * @return Jena individual
	 */
	public Individual createIndividual(OntClass ontClazz, String id,
			String shortId, OntModel aBoxModel, LabelModifier modifier) {
		Individual indi = aBoxModel.createIndividual(id, ontClazz);
		Statement anLabel = ontClazz.getProperty(RDFS.label);
		if (anLabel != null && anLabel.getLiteral() != null
				&& anLabel.getLiteral().getString() != null) {
			if (modifier == null) {
				indi.addProperty(RDFS.label, anLabel.getLiteral().getString()
						+ " " + shortId);
			} else if (modifier == LabelModifier.COMPOSITE) {
				indi.addProperty(RDFS.label, anLabel.getLiteral().getString()
						+ " " + shortId);
			} else if (modifier == LabelModifier.SHORT_ID) {
				indi.addProperty(RDFS.label, shortId);
			} else if (modifier == LabelModifier.CLASS_LABEL) {
				indi.addProperty(RDFS.label, anLabel.getLiteral().getString());
			}
			// add a comment so that an indexer would use this information
			// succinctly
			indi.addComment(anLabel.getLiteral().getString() + " " + shortId,
					"EN");
		}
		return indi;
	}

	/**
	 * 
	 * @return
	 */

	public synchronized String _unique() {
		return "_" + (globalCounter++);
	}

	// protected static String dbAssayId = "assay_id";

	// protected static String dbCompoundId = "compoundid";

	protected static String dbSubstanceId = "substanceid";

	// protected static String dbName = "name";

	protected static String dbValueOf = "valueof";

	protected static String dbTid = "tid";

	protected static String dbTestedConcentration = "testedConcentration";

	// protected static String dbTestedConcentrationUnit =
	// "testedConcentrationUnit";

	/**
	 * 
	 * @param aid
	 * @param tid
	 * @return
	 */
	public String mainMySqlQuery(String aid, String tid) {
		return "select assay_id,compoundid,substanceid,name,split_str(valueof,',',tid) as valueof,tid,testedConcentration,testedConcentrationUnit\n"
				+ " from\n"
				+ "((\n"
				+ "SELECT all_values as valueof,pr.aid as assay_id,pr.cid as compoundid,pr.sid as substanceid, pc.tid as tid,pc.name,pc.testedConcentration,pc.testedConcentrationUnit\n"
				+ " from pcassay_result pr\n"
				+ "    inner join pcassay_column pc on (pc.assay_Aid = pr.aid)\n"
				+ "    where pr.Aid = "
				+ aid
				+ " \n"
				+ "       and pc.tid = "
				+ tid + "\n" + ") as T1)";
	}

	/**
	 * 
	 * @param subject
	 * @param dp
	 * @param type
	 * @param aBoxModel
	 * @param literal
	 */
	public void addLiteral(Individual subject, DatatypeProperty dp, char type,
			OntModel aBoxModel, String literal) {
		if (literal == null) {
			warning("literal is null and set to empty");
			literal = "";
		}
		try {
			switch (type) {
			case 's':
				subject.addLiteral(dp, aBoxModel.createTypedLiteral(literal,
						XSDDatatype.XSDstring));
				break;
			case 'i':
				subject.addLiteral(dp, aBoxModel.createTypedLiteral(
						Integer.parseInt(literal), XSDDatatype.XSDinteger));
				break;
			case 'f':
				subject.addLiteral(dp, aBoxModel.createTypedLiteral(
						Float.parseFloat(literal), XSDDatatype.XSDfloat));
				break;
			case 'd':
				subject.addLiteral(dp, aBoxModel.createTypedLiteral(
						Double.parseDouble(literal), XSDDatatype.XSDdouble));
			case '@':
				subject.addProperty(dp, literal);
				break;
			}
		} catch (NumberFormatException e) {
			String msg = "number format problem " + e.getMessage() + " for "
					+ literal;
			warning(msg);
		}

	}

	/**
	 * @param directContext
	 * @param tBoxModel
	 * @param aBoxModel
	 * @param currentRow
	 * @param specificIndividual
	 * @param baoSearchForwardName
	 * @param baoSearchInvName
	 */
	protected void addBaoSearchObjectPropertyToBioAssay(
			DirectContext directContext, OntModel tBoxModel,
			OntModel aBoxModel, SSRow currentRow,
			Individual specificIndividual, String baoSearchForwardName,
			String baoSearchInvName) {
		String value = directContext.getConfiguration().get(
				Configuration.baoSearchConceptualization);
		if (value != null && Boolean.valueOf(value)) {
			// baoseach link to bioassay
			// link back to bio assay
			String aid = Util.getValue(directContext.getCsvDataSource()
					.getHead(), currentRow, annonAid);
			String bioassayId = Util.getIndividualIri(directContext
					.getConfiguration().getNs(NS_BAO), Constants.INDI_PREFIX
					+ getId_(this.bioassyId) + aid);
			Individual bioassayIndividual = getABoxIndividual(aBoxModel,
					bioassayId);
			ObjectProperty op = getObjectProperty(tBoxModel, baoSearchNs,
					baoSearchForwardName);
			bioassayIndividual.addProperty(op, specificIndividual);
			op = getObjectProperty(tBoxModel, baoSearchNs, baoSearchInvName);
			specificIndividual.addProperty(op, bioassayIndividual);
			System.out.println("#### added the baosearch concpet "
					+ baoSearchNs + baoSearchForwardName + " "
					+ baoSearchInvName);
		}
	}

	/**
	 * 
	 * @param directContext
	 * @param tBoxModel
	 * @param aBoxModel
	 * @param currentRow
	 * @param dataTypePropertyName
	 * @param dataTypePropertyValue
	 */
	protected void addBaoSearchDataTypePropertyToBioAssay(
			DirectContext directContext, OntModel tBoxModel,
			OntModel aBoxModel, SSRow currentRow, String dataTypePropertyName,
			String dataTypePropertyValue) {
		String value = directContext.getConfiguration().get(
				Configuration.baoSearchConceptualization);
		if (value != null && Boolean.valueOf(value)) {
			// baoseach link to bioassay
			// link back to bio assay
			String aid = Util.getValue(directContext.getCsvDataSource()
					.getHead(), currentRow, annonAid);
			String bioassayId = Util.getIndividualIri(directContext
					.getConfiguration().getNs(NS_BAO), Constants.INDI_PREFIX
					+ getId_(this.bioassyId) + aid);
			Individual bioassayIndividual = getABoxIndividual(aBoxModel,
					bioassayId);
			DatatypeProperty dp = getDatatypeProperty(tBoxModel,
					dataTypePropertyName, directContext);
			addLiteral(bioassayIndividual, dp, 's', aBoxModel,
					dataTypePropertyValue);
			System.out.println("#### added dp " + baoSearchNs
					+ dataTypePropertyName + " " + dataTypePropertyValue);
		}

	}

	/**
	 * 
	 * @param param1
	 * @param objPropSuffixList
	 * @param directContext
	 * @param subject
	 * @param predicate
	 * @param tBoxModel
	 * @param aBoxModel
	 * @return
	 */
	// this method return null if param1 could not be found
	protected Individual createLocalIndividualParam1(String param1,
			List<String> objPropSuffixList, DirectContext directContext,
			Individual subject, ObjectProperty predicate, OntModel tBoxModel,
			OntModel aBoxModel) {
		// error checking
		Individual localIndividual = null;
		if (param1 == null || param1.length() == 0) {
			warning("param1 name is empty " + param1);
		} else {
			param1 = param1.toLowerCase();
			OntClass clazz = getOntClass(tBoxModel,
					getCellToDecIdMapping(directContext, tBoxModel, param1),
					directContext);
			// corresponding individual name
			String id = Util.getIndividualIri(directContext.getConfiguration()
					.getNs(NS_BAO),
					Constants.INDI_PREFIX + clazz.getLocalName() + _unique());
			localIndividual = createIndividual(clazz, id, param1, aBoxModel,
					LabelModifier.CLASS_LABEL);
			subject.addProperty(predicate, localIndividual);

			if (objPropSuffixList != null) {
				for (String op : objPropSuffixList) {
					localIndividual.addProperty(
							getObjectProperty(tBoxModel, getId(op),
									directContext), subject);
				}
			}

		}
		return localIndividual;
	}

	/**
	 * 
	 * @param param1
	 * @param objPropSuffixList
	 * @param globalStorage
	 * @param directContext
	 * @param subject
	 * @param predicate
	 * @param tBoxModel
	 * @param aBoxModel
	 * @param idModifier
	 * @return
	 */
	protected Individual createGlobalIndividualParam1(String param1,
			List<String> objPropSuffixList,
			Map<String, List<NameToId>> globalStorage,
			DirectContext directContext, Individual subject,
			ObjectProperty predicate, OntModel tBoxModel, OntModel aBoxModel,
			IdModifier idModifier) {
		// error checking
		if (param1 == null || param1.length() == 0) {
			warning("param1 name is empty " + param1);
			return null;
		} else {
			param1 = param1.toLowerCase();
			if (idModifier != IdModifier.DIRECT_ID
					&& !isCellToDecIdMappingInExistence(tBoxModel, param1)) {
				directContext.getLogger().warning(
						param1 + " does not exist!"
								+ " createGlobalIndividualParam1");
				return null;
			}
			OntClass clazz;
			if (idModifier == IdModifier.LOCAL_ID) {
				clazz = getOntClass(
						tBoxModel,
						getCellToDecIdMapping(directContext, tBoxModel, param1),
						directContext);
			} else if (idModifier == IdModifier.IRI) {
				clazz = tBoxModel.getOntClass(getCellToDecIRIMapping(
						directContext, tBoxModel, param1));
			} else if (idModifier == IdModifier.DIRECT_ID) {
				clazz = getOntClass(tBoxModel, getId(param1), directContext);
			} else {
				throw new RuntimeException(
						"createGlobalIndividualParam1 execution holds");
			}

			// special individual exists
			List<NameToId> nameToIdList = globalStorage.get(param1);
			if (nameToIdList == null) {
				globalStorage.put(param1, new ArrayList<NameToId>());
			}
			// corresponding individual name
			nameToIdList = globalStorage.get(param1);
			if (!nameToIdList.contains(NameToId.searchObj(param1))) {
				NameToId specificObj = new NameToId(param1, _unique());
				nameToIdList.add(specificObj);
				String id = Util.getIndividualIri(directContext
						.getConfiguration().getNs(NS_BAO),
						Constants.INDI_PREFIX + clazz.getLocalName()
								+ specificObj.getId());
				createIndividual(clazz, id, specificObj.getName(), aBoxModel,
						LabelModifier.CLASS_LABEL);
			}

			NameToId specificObj = nameToIdList.get(nameToIdList
					.indexOf(NameToId.searchObj(param1)));
			String id = Util.getIndividualIri(
					directContext.getConfiguration().getNs(NS_BAO),
					Constants.INDI_PREFIX + clazz.getLocalName()
							+ specificObj.getId());
			Individual specificIndividual = getABoxIndividual(aBoxModel, id);
			subject.addProperty(predicate, specificIndividual);

			if (objPropSuffixList != null) {
				for (String op : objPropSuffixList) {
					specificIndividual.addProperty(
							getObjectProperty(tBoxModel, getId(op),
									directContext), subject);
				}
			}
			return specificIndividual;
		}
	}

	/**
	 * @param param1
	 * @param dataPropSuffixMap
	 * @param objPropSuffixList
	 * @param globalStorage
	 * @param directContext
	 * @param subject
	 * @param predicate
	 * @param tBoxModel
	 * @param aBoxModel
	 * @param idModifier
	 */
	protected void createGlobalIndividualParam1(String param1,
			Map<String, Object> dataPropSuffixMap,
			List<String> objPropSuffixList,
			Map<String, List<NameToId>> globalStorage,
			DirectContext directContext, Individual subject,
			ObjectProperty predicate, OntModel tBoxModel, OntModel aBoxModel,
			IdModifier idModifier) {
		// error checking
		if (param1 == null || param1.length() == 0) {
			warning("param1 name is empty " + param1);
		} else {
			param1 = param1.toLowerCase();
			OntClass clazz;
			if (idModifier == IdModifier.LOCAL_ID) {
				clazz = getOntClass(
						tBoxModel,
						getCellToDecIdMapping(directContext, tBoxModel, param1),
						directContext);
			} else if (idModifier == IdModifier.IRI) {
				clazz = tBoxModel.getOntClass(getCellToDecIRIMapping(
						directContext, tBoxModel, param1));
			} else {
				throw new RuntimeException(
						"createGlobalIndividualParam1 execution holds");
			}

			// special individual exists
			List<NameToId> nameToIdList = globalStorage.get(param1);
			if (nameToIdList == null) {
				globalStorage.put(param1, new ArrayList<NameToId>());
			}
			// corresponding individual name
			nameToIdList = globalStorage.get(param1);
			if (!nameToIdList.contains(NameToId.searchObj(param1))) {
				NameToId specificObj = new NameToId(param1, _unique());
				nameToIdList.add(specificObj);
				String id = Util.getIndividualIri(directContext
						.getConfiguration().getNs(NS_BAO),
						Constants.INDI_PREFIX + clazz.getLocalName()
								+ specificObj.getId());
				Individual specificIndividual = createIndividual(clazz, id,
						specificObj.getName(), aBoxModel,
						LabelModifier.CLASS_LABEL);
				if (dataPropSuffixMap != null) {
					for (Map.Entry<String, Object> entry : dataPropSuffixMap
							.entrySet()) {
						Object value = entry.getValue();
						DatatypeProperty dp = getDatatypeProperty(tBoxModel,
								getId(entry.getKey()), directContext);
						if (value instanceof String) {
							addLiteral(specificIndividual, dp, 's', aBoxModel,
									value.toString());
						} else if (value instanceof Integer) {
							addLiteral(specificIndividual, dp, 'i', aBoxModel,
									value.toString());
						} else if (value instanceof Float) {
							addLiteral(specificIndividual, dp, 'f', aBoxModel,
									value.toString());
						} else if (value instanceof Double) {
							addLiteral(specificIndividual, dp, 'd', aBoxModel,
									value.toString());
						} else {
							addLiteral(specificIndividual, dp, '@', aBoxModel,
									value.toString());
						}
					}
				}
			}

			NameToId specificObj = nameToIdList.get(nameToIdList
					.indexOf(NameToId.searchObj(param1)));
			String id = Util.getIndividualIri(
					directContext.getConfiguration().getNs(NS_BAO),
					Constants.INDI_PREFIX + clazz.getLocalName()
							+ specificObj.getId());
			Individual specificIndividual = getABoxIndividual(aBoxModel, id);
			subject.addProperty(predicate, specificIndividual);

			if (objPropSuffixList != null) {
				for (String op : objPropSuffixList) {
					specificIndividual.addProperty(
							getObjectProperty(tBoxModel, getId(op),
									directContext), subject);
				}
			}
		}

	}

	/**
	 * 
	 * @param param1
	 * @param param2
	 * @param enableGoodLabel
	 * @param objPropSuffixList
	 * @param globalStorage
	 * @param directContext
	 * @param subject
	 * @param predicate
	 * @param tBoxModel
	 * @param aBoxModel
	 * @return
	 */
	protected Individual createGlobalIndividualParam2(
			String param1,
			String param2, // conceptname
			boolean enableGoodLabel, List<String> objPropSuffixList,
			Map<String, List<NameToId>> globalStorage,
			DirectContext directContext, Individual subject,
			ObjectProperty predicate, OntModel tBoxModel, OntModel aBoxModel) {
		// error checking
		if (param1 == null || param1.length() == 0) {
			warning("param1 name is empty " + param1);
			return null;
		} else {
			param1 = param1.toLowerCase();
			OntClass clazz = getOntClass(tBoxModel, getId(param2),
					directContext);

			// special individual exists
			List<NameToId> nameToIdList = globalStorage.get(param2);
			if (nameToIdList == null) {
				globalStorage.put(param2, new ArrayList<NameToId>());
			}
			// corresponding individual name
			nameToIdList = globalStorage.get(param2);
			if (!nameToIdList.contains(NameToId.searchObj(param1))) {
				NameToId specificObj = new NameToId(param1, _unique());
				nameToIdList.add(specificObj);
				String id = Util.getIndividualIri(directContext
						.getConfiguration().getNs(NS_BAO),
						Constants.INDI_PREFIX + clazz.getLocalName()
								+ specificObj.getId());
				createIndividual(clazz, id, specificObj.getName(), aBoxModel,
						(enableGoodLabel ? LabelModifier.COMPOSITE
								: LabelModifier.CLASS_LABEL));
			}

			NameToId specificObj = nameToIdList.get(nameToIdList
					.indexOf(NameToId.searchObj(param1)));
			String id = Util.getIndividualIri(
					directContext.getConfiguration().getNs(NS_BAO),
					Constants.INDI_PREFIX + clazz.getLocalName()
							+ specificObj.getId());
			Individual specificIndividual = getABoxIndividual(aBoxModel, id);
			subject.addProperty(predicate, specificIndividual);

			// bunch of inverse properties
			if (objPropSuffixList != null) {
				for (String op : objPropSuffixList) {
					specificIndividual.addProperty(
							getObjectProperty(tBoxModel, getId(op),
									directContext), subject);
				}
			}

			return specificIndividual;
		}

	}

	/**
	 * 
	 * @param param1
	 * @param param2
	 * @param labelModifier
	 * @param objPropSuffixList
	 * @param globalStorage
	 * @param directContext
	 * @param subject
	 * @param predicate
	 * @param tBoxModel
	 * @param aBoxModel
	 * @return
	 */
	protected Individual createGlobalIndividualParam2(
			String param1,
			String param2, // conceptname
			LabelModifier labelModifier, List<String> objPropSuffixList,
			Map<String, List<NameToId>> globalStorage,
			DirectContext directContext, Individual subject,
			ObjectProperty predicate, OntModel tBoxModel, OntModel aBoxModel) {
		// error checking
		if (param1 == null || param1.length() == 0) {
			warning("param1 name is empty " + param1);
			return null;
		} else {
			param1 = param1.toLowerCase();
			OntClass clazz = getOntClass(tBoxModel, getId(param2),
					directContext);

			// special individual exists
			List<NameToId> nameToIdList = globalStorage.get(param2);
			if (nameToIdList == null) {
				globalStorage.put(param2, new ArrayList<NameToId>());
			}
			// corresponding individual name
			nameToIdList = globalStorage.get(param2);
			if (!nameToIdList.contains(NameToId.searchObj(param1))) {
				NameToId specificObj = new NameToId(param1, _unique());
				nameToIdList.add(specificObj);
				String id = Util.getIndividualIri(directContext
						.getConfiguration().getNs(NS_BAO),
						Constants.INDI_PREFIX + clazz.getLocalName()
								+ specificObj.getId());
				createIndividual(clazz, id, specificObj.getName(), aBoxModel,
						labelModifier);
			}

			NameToId specificObj = nameToIdList.get(nameToIdList
					.indexOf(NameToId.searchObj(param1)));
			String id = Util.getIndividualIri(
					directContext.getConfiguration().getNs(NS_BAO),
					Constants.INDI_PREFIX + clazz.getLocalName()
							+ specificObj.getId());
			Individual specificIndividual = getABoxIndividual(aBoxModel, id);
			subject.addProperty(predicate, specificIndividual);

			// bunch of inverse properties
			if (objPropSuffixList != null) {
				for (String op : objPropSuffixList) {
					specificIndividual.addProperty(
							getObjectProperty(tBoxModel, getId(op),
									directContext), subject);
				}
			}

			return specificIndividual;
		}

	}

	/**
	 * 
	 * @param param1
	 * @param clazz
	 * @param labelModifier
	 * @param objPropSuffixList
	 * @param globalStorage
	 * @param directContext
	 * @param subject
	 * @param predicate
	 * @param tBoxModel
	 * @param aBoxModel
	 * @return
	 */
	protected Individual createGlobalIndividualParam2(
			String param1,
			OntClass clazz, // conceptname
			LabelModifier labelModifier, List<String> objPropSuffixList,
			Map<String, List<NameToId>> globalStorage,
			DirectContext directContext, Individual subject,
			ObjectProperty predicate, OntModel tBoxModel, OntModel aBoxModel) {
		// error checking
		if (param1 == null || param1.length() == 0) {
			warning("param1 name is empty " + param1);
			return null;
		} else {
			param1 = param1.toLowerCase();

			// special individual exists
			List<NameToId> nameToIdList = globalStorage.get(clazz
					.getLocalName());
			if (nameToIdList == null) {
				globalStorage.put(clazz.getLocalName(),
						new ArrayList<NameToId>());
			}
			// corresponding individual name
			nameToIdList = globalStorage.get(clazz.getLocalName());
			if (!nameToIdList.contains(NameToId.searchObj(param1))) {
				NameToId specificObj = new NameToId(param1, _unique());
				nameToIdList.add(specificObj);
				String id = Util.getIndividualIri(directContext
						.getConfiguration().getNs(NS_BAO),
						Constants.INDI_PREFIX + clazz.getLocalName()
								+ specificObj.getId());
				createIndividual(clazz, id, specificObj.getName(), aBoxModel,
						labelModifier);
			}

			NameToId specificObj = nameToIdList.get(nameToIdList
					.indexOf(NameToId.searchObj(param1)));
			String id = Util.getIndividualIri(
					directContext.getConfiguration().getNs(NS_BAO),
					Constants.INDI_PREFIX + clazz.getLocalName()
							+ specificObj.getId());
			Individual specificIndividual = getABoxIndividual(aBoxModel, id);
			subject.addProperty(predicate, specificIndividual);

			// bunch of inverse properties
			if (objPropSuffixList != null) {
				for (String op : objPropSuffixList) {
					specificIndividual.addProperty(
							getObjectProperty(tBoxModel, getId(op),
									directContext), subject);
				}
			}

			return specificIndividual;
		}

	}

	/**
	 * 
	 * @param ctx
	 * @param tBoxModel
	 * @param localName
	 * @return
	 */
	private Resource getFirstResource(DirectContext ctx, OntModel tBoxModel,
			String localName) {
		Set<Map.Entry<String, String>> nsEntrySet = ctx.getConfiguration()
				.getNsEntrySet();
		Resource resource = null;
		for (Map.Entry<String, String> entry : nsEntrySet) {
			resource = tBoxModel.getResource(entry.getValue() + localName);
			if (resource != null) {
				break;
			}
		}
		return resource;
	}

	/**
	 * 
	 * @param ctx
	 * @param tBoxModel
	 * @param localName
	 * @return
	 */
	private ObjectProperty getFirstObjectProperty(DirectContext ctx,
			OntModel tBoxModel, String localName) {
		Set<Map.Entry<String, String>> nsEntrySet = ctx.getConfiguration()
				.getNsEntrySet();
		ObjectProperty objectProperty = null;
		for (Map.Entry<String, String> entry : nsEntrySet) {
			objectProperty = tBoxModel.getObjectProperty(entry.getValue()
					+ localName);
			if (objectProperty != null) {
				break;
			}
		}
		return objectProperty;
	}

	/**
	 * 
	 * @param ctx
	 * @param tBoxModel
	 * @param localName
	 * @return
	 */
	private DatatypeProperty getFirstDatatypeProperty(DirectContext ctx,
			OntModel tBoxModel, String localName) {
		Set<Map.Entry<String, String>> nsEntrySet = ctx.getConfiguration()
				.getNsEntrySet();
		DatatypeProperty datatypeProperty = null;
		for (Map.Entry<String, String> entry : nsEntrySet) {
			datatypeProperty = tBoxModel.getDatatypeProperty(entry.getValue()
					+ localName);
			if (datatypeProperty != null) {
				break;
			}
		}
		return datatypeProperty;
	}

	/**
	 * 
	 * @param ctx
	 * @param tBoxModel
	 * @param localName
	 * @return
	 */
	private OntClass getFirstOntClass(DirectContext ctx, OntModel tBoxModel,
			String localName) {
		Set<Map.Entry<String, String>> nsEntrySet = ctx.getConfiguration()
				.getNsEntrySet();
		OntClass ontClazz = null;

		for (Map.Entry<String, String> entry : nsEntrySet) {
			ontClazz = tBoxModel.getOntClass(entry.getValue() + localName);
			if (ontClazz != null) {
				break;
			}
		}
		return ontClazz;
	}

	/**
	 * 
	 * @param ctx
	 * @param tBoxModel
	 * @param localName
	 * @return
	 */
	private Individual getFirstIndividual(DirectContext ctx,
			OntModel tBoxModel, String localName) {
		Set<Map.Entry<String, String>> nsEntrySet = ctx.getConfiguration()
				.getNsEntrySet();
		Individual individual = null;
		for (Map.Entry<String, String> entry : nsEntrySet) {
			individual = tBoxModel.getIndividual(entry.getValue() + localName);
			if (individual != null) {
				break;
			}
		}
		return individual;
	}

	/**
	 * 
	 * @param concept
	 * @param directContext
	 * @param subject
	 * @param predicate
	 * @param currentRow
	 * @param tBoxModel
	 * @param aBoxModel
	 * @param tid
	 */
	protected void endpointDatabaseVisitHelper(AbstractConcept concept,
			DirectContext directContext, Individual subject,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int tid) {
		// for all TID

		// database
		String aid = Util.getValue(directContext.getCsvDataSource().getHead(),
				currentRow, annonAid);
		String sql = mainMySqlQuery(aid, Integer.toString(tid));

		// / data base connection
		directContext.doResult(sql, concept, directContext, subject, predicate,
				currentRow, tBoxModel, aBoxModel);
	}

	/**
	 * 
	 * @param directContext
	 * @param subject
	 * @param predicate
	 * @param currentRow
	 * @param tBoxModel
	 * @param aBoxModel
	 * @param idd
	 */
	protected void halfDoneConcept(DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel, String idd) {
		// create an individual and plug that in
		String id = Util
				.getIndividualIri(directContext.getConfiguration()
						.getNs(NS_BAO), Constants.INDI_PREFIX + getId_(idd)
						+ _unique());
		Individual endpointIndividual = createIndividual(
				getOntClass(tBoxModel, getId(idd), directContext), id, "",
				aBoxModel, LabelModifier.CLASS_LABEL);
		// relation (subject is measure group)
		subject.addProperty(predicate, endpointIndividual);

		addBaoSearchObjectPropertyToBioAssay(directContext, tBoxModel,
				aBoxModel, currentRow, endpointIndividual, "hasEndpoint",
				"isEndpointOf");
	}

}

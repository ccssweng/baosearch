package edu.miami.ccs.bao.direct.impl.format;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.AbstractConcept;
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.direct.impl.specification.FormatSpec;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;
import edu.miami.ccs.bao.direct.api.Concept;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Format class creates the subject, object and, predicate for format
 * for a particular assay taking the information from annpotation file
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class Format extends AbstractConcept {

	// private static String format = "Format 1 MainClassification";

	private static String format3 = "Format 3";

	private static String format2 = "Format 2";

	private static String format1 = "Format 1 MainClassification";

	private static String isFormatOf = "555";

	private static String hasSpecification = "214";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	public void visit(DirectContext directContext, Individual subject,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int mult) {
		String param1 = Util.getValue(directContext.getCsvDataSource()
				.getHead(), currentRow, format1);
		String param2 = Util.getValue(directContext.getCsvDataSource()
				.getHead(), currentRow, format2);
		String param3 = Util.getValue(directContext.getCsvDataSource()
				.getHead(), currentRow, format3);

		List<String> inverseRelationList = new ArrayList<String>();
		inverseRelationList.add(isFormatOf);

		// TODO:

		if (param3 != null && param3.length() > 0) {
			createGlobalIndividualParam1(param3, inverseRelationList,
					directContext.getGlobalInformation().formatMapping,
					directContext, subject, predicate, tBoxModel, aBoxModel,
					IdModifier.LOCAL_ID);
			/*
			 * createHasSpecification( createLocalIndividualParam1(param3,
			 * inverseRelationList, directContext, subject, predicate,
			 * tBoxModel, aBoxModel), directContext, currentRow, tBoxModel,
			 * aBoxModel);
			 */
		} else if (param2 != null && param2.length() > 0) {

			createGlobalIndividualParam1(param2, inverseRelationList,
					directContext.getGlobalInformation().formatMapping,
					directContext, subject, predicate, tBoxModel, aBoxModel,
					IdModifier.LOCAL_ID);

			/*
			 * createHasSpecification( createLocalIndividualParam1(param2,
			 * inverseRelationList, directContext, subject, predicate,
			 * tBoxModel, aBoxModel), directContext, currentRow, tBoxModel,
			 * aBoxModel);
			 */
		} else if (param1 != null && param1.length() > 0) {

			createGlobalIndividualParam1(param1, inverseRelationList,
					directContext.getGlobalInformation().formatMapping,
					directContext, subject, predicate, tBoxModel, aBoxModel,
					IdModifier.LOCAL_ID);
			/*
			 * createHasSpecification( createLocalIndividualParam1(param1,
			 * inverseRelationList, directContext, subject, predicate,
			 * tBoxModel, aBoxModel), directContext, currentRow, tBoxModel,
			 * aBoxModel);
			 */

		}

	}

	/**
	 * 
	 * @param localIndividual
	 * @param directContext
	 * @param currentRow
	 * @param tBoxModel
	 * @param aBoxModel
	 */
	private void createHasSpecification(Individual localIndividual,
			DirectContext directContext, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel) {
		if (localIndividual != null) {
			// //format spec
			ObjectProperty op = getObjectProperty(tBoxModel,
					getId(hasSpecification), directContext);
			Concept formatSpec = new FormatSpec();
			formatSpec.visit(directContext, localIndividual, op, currentRow,
					tBoxModel, aBoxModel, -1);
		}
	}

	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {
		// not used

	}
}

package edu.miami.ccs.bao.chembl.endpoint;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.AbstractConcept;
import edu.miami.ccs.bao.direct.api.Constants;
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * ChemblEndpoint class creates the subject, object and, predicate for endpoints
 * for a particular assay taking the information from filter redesign database
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */

public class ChemblEndpoint extends AbstractConcept {

	protected int chembelBioAssayAid;

	protected ChemblEndpointUtil chemblEndpointUtil;

	public ChemblEndpoint() {
		chemblEndpointUtil = new ChemblEndpointUtil();
	}

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	@Override
	public void visit(DirectContext directContext, Individual subject,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int chembelBioAssayAid) {
		this.chembelBioAssayAid = chembelBioAssayAid;
		String sql = "select distinct sid, epval_num, ep_name, ep_unit, scr_conc from filter_redesign.epval v, "
				+ "filter_redesign.ep_details ep\n"
				+ "where v.EPT_ID=ep.EPT_ID and v.ASSAY_ID="
				+ chembelBioAssayAid;

		directContext.doResult(sql, this, directContext, subject, predicate,
				null, tBoxModel, aBoxModel);
	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.Concept#doData(java.sql.ResultSet,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	@Override
	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {
		while (rset.next()) {
			// this is where all the endpoints are called
			String epName = rset.getString("ep_name");
			String concUnit = rset.getString("ep_unit");
			String concValue = rset.getString("epval_num");
			String respValue = rset.getString("scr_conc");
			String pertSid = rset.getString("sid");
			if (concValue == null || concValue.length() == 0) {
				concValue = "-1";
			}
			if (respValue == null || respValue.length() == 0) {
				respValue = "-1";
			}
			if (epName != null && epName.length() > 0) {
				epName = epName.toLowerCase();
				Individual endpointIndividual;
				String shortId = _unique();
				String id = Util
						.getIndividualIri(
								directContext.getConfiguration().getNs(NS_BAO),
								Constants.INDI_PREFIX
										+ getId_(getStandardizedEndpointIdSuffix(epName))
										+ shortId);
				endpointIndividual = createIndividual(
						getOntClass(tBoxModel,
								getId(getStandardizedEndpointIdSuffix(epName)),
								directContext), id, shortId, aBoxModel,
						LabelModifier.CLASS_LABEL);
				subject.addProperty(predicate, endpointIndividual);
				if (epName.equals("ac50") || epName.equals("ic50")) {
					String modeOfActionIndividualIdSuffix = "374";
					String percentResponseIndividualIdSuffix = "341";
					chemblEndpointUtil.createEndpointConcentration(concUnit,
							concValue, pertSid, endpointIndividual,
							modeOfActionIndividualIdSuffix,
							percentResponseIndividualIdSuffix,
							String.valueOf(chembelBioAssayAid), tBoxModel,
							aBoxModel, directContext);

				} else if (epName.equals("ec50")) {
					String modeOfActionIndividualIdSuffix = "339";
					String percentResponseIndividualIdSuffix = "343";
					chemblEndpointUtil.createEndpointConcentration(concUnit,
							concValue, pertSid, endpointIndividual,
							modeOfActionIndividualIdSuffix,
							percentResponseIndividualIdSuffix,
							String.valueOf(chembelBioAssayAid), tBoxModel,
							aBoxModel, directContext);

				} else if (epName.equals("percent activation")) {
					String modeOfActionIndividualIdSuffix = "339";
					chemblEndpointUtil.createEndpointResponse(concUnit,
							concValue, respValue, pertSid, endpointIndividual,
							modeOfActionIndividualIdSuffix,
							String.valueOf(chembelBioAssayAid), tBoxModel,
							aBoxModel, directContext);

				} else if (epName.equals("percent inhibition")) {
					String modeOfActionIndividualIdSuffix = "374";
					chemblEndpointUtil.createEndpointResponse(concUnit,
							concValue, respValue, pertSid, endpointIndividual,
							modeOfActionIndividualIdSuffix,
							String.valueOf(chembelBioAssayAid), tBoxModel,
							aBoxModel, directContext);

				} else if (epName.equals("bmax")) {
					chemblEndpointUtil.createBindingConstants(pertSid,
							endpointIndividual,
							String.valueOf(chembelBioAssayAid), tBoxModel,
							aBoxModel, directContext);

				} else if (epName.equals("kd")) {
					chemblEndpointUtil.createBindingConstants(pertSid,
							endpointIndividual,
							String.valueOf(chembelBioAssayAid), tBoxModel,
							aBoxModel, directContext);

				} else if (epName.equals("ki")) {
					chemblEndpointUtil
							.createEndpointPerturbagenProteinAffinity(concUnit,
									concValue, pertSid, endpointIndividual,
									String.valueOf(chembelBioAssayAid),
									tBoxModel, aBoxModel, directContext);

				} else if (epName.equals("tm")) {
					chemblEndpointUtil.createEndpointTemperature(concUnit,
							concValue, pertSid, endpointIndividual,
							String.valueOf(chembelBioAssayAid), tBoxModel,
							aBoxModel, directContext);

				} else {
					throw new RuntimeException("Not implemented");
				}

			} else {
				error(ChemblEndpoint.class.getCanonicalName()
						+ " does not have epName ", epName);
			}
		}
	}

	/**
	 * basing on what the endpoint is it gies the BAO id
	 * 
	 * @param epName
	 * @return BAOID
	 */
	private String getStandardizedEndpointIdSuffix(String epName) {
		if (epName.equals("ac50")) {
			return "186";
		} else if (epName.equals("ec50")) {
			return "188";
		} else if (epName.equals("ic50")) {
			return "190";
		} else if (epName.equals("percent activation")) {
			return "200";
		} else if (epName.equals("percent inhibition")) {
			return "201";
		} else if (epName.equals("bmax")) {
			return "033";
		} else if (epName.equals("kd")) {
			return "034";
		} else if (epName.equals("ki")) {
			return "192";
		} else if (epName.equals("tm")) {
			return "581";
		} else {
			throw new RuntimeException("Not implemented");
		}
	}
}

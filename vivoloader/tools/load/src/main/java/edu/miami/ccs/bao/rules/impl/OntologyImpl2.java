package edu.miami.ccs.bao.rules.impl;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFWriter;
import edu.miami.ccs.bao.direct.api.StoreType;
import edu.miami.ccs.bao.rules.api.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/**
 * @deprecated
 */
public class OntologyImpl2 implements Ontology {

	private OntModel tBoxModel;

	private OntModel aBoxModel;

	private Document document;

	public final static String INDI_PREFIX = "individual";

	public OntologyImpl2(Document document) {
		this.document = document;
	}

	public void loadOntModel(String url, String aBoxStore, StoreType type)
			throws Exception {
		tBoxModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM,
				null);
		aBoxModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
		InputStream in = new FileInputStream(new File(url));
		tBoxModel.read(in, null);
	}

	// pre a separate tBoxModel

	/*
	 * @see edu.miami.ccs.bao.rules.api.Ontology#updateModel()
	 */
	public void updateModel() {

		// single ontology individuals :: no harm
		for (Mapper.SingleOntologyIndividual individual : document.getMapper()
				.getSingleOntologyIndividual().values()) {
			String ns = document.getMapper().getNamespace().getNs()
					.get(individual.getNsPrefix());
			Individual singleIndividual = aBoxModel.getIndividual(ns
					+ individual.getId());
			if (singleIndividual == null) {
				message("create single individual " + individual.getId());
				OntClass ontClazz = tBoxModel.getOntClass(ns
						+ individual.getConceptType().getOnt());
				if (ontClazz == null) {
					error("class does not exists in T-Box ", individual
							.getConceptType().getOnt());
				} else {
					aBoxModel.createIndividual(ns + individual.getId(),
							ontClazz);
				}
			}
		}
		// ///////////////////////////////////////////////////////////////
		int indCnt = 1;
		List<SSRow> rows = document.getAnnotatedDataSource().getRows();

		for (SSRow row : rows) {
			// each type A individual
			// pre
			for (Mapper.IndividualTypeA individual : document.getMapper()
					.getIndividualsTypeA().values()) {
				// selecting the rule engine
				Engine engine = EngineFactory.getEngine(individual);
				engine.pre(individual, row, tBoxModel, aBoxModel, document,
						INDI_PREFIX, indCnt + "");
			}
			// post
			for (Mapper.IndividualTypeA individual : document.getMapper()
					.getIndividualsTypeA().values()) {
				// selecting the rule engine
				Engine engine = EngineFactory.getEngine(individual);
				engine.post(individual, row, tBoxModel, aBoxModel, document,
						INDI_PREFIX, indCnt + "");
			}
			// //////////////////////////////
			indCnt++;
		}
	}

	//

	/*
	 * @see edu.miami.ccs.bao.rules.api.Ontology#write(java.io.OutputStream,
	 * edu.miami.ccs.bao.direct.api.StoreType)
	 */
	public void write(OutputStream out, StoreType type) throws Exception {
		RDFWriter writer = aBoxModel.getWriter("RDF/XML-ABBREV");
		writer.setProperty("xmlbase", document.getMapper().getNamespace()
				.getXmlBaseNs());
		for (Map.Entry<String, String> entry : document.getMapper()
				.getNamespace().getNs().entrySet()) {
			aBoxModel.setNsPrefix(entry.getKey(), entry.getValue());
		}
		writer.write(aBoxModel, out, document.getMapper().getNamespace()
				.getXmlBaseNs());
		out.flush();
		out.close();
	}

	public void message(String message) {

	}

	public void error(String err1, String err2) {

		System.exit(-89);
	}
}

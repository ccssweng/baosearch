package edu.miami.ccs.bao.direct.impl.specification;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.AbstractConcept;
import edu.miami.ccs.bao.direct.api.Concept;
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.direct.impl.target.Organism;
import edu.miami.ccs.bao.rules.api.SSRow;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * MetatargetSpec class creates the subject, object and, predicate for a metetargetspec
 * for a particular metatarget taking the information from Annotation file
 * 
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class MetaTargetSpec extends AbstractConcept {

	/*
	 * 
	 * 
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	@Override
	public void visit(DirectContext directContext, Individual subject,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int mult) {

		// ////////cell line
		Concept cellSpec = new CellSpec();
		cellSpec.visit(directContext, subject, predicate, currentRow,
				tBoxModel, aBoxModel, mult);

		// ////////organism
		Concept organism = new Organism();
		organism.visit(directContext, subject, predicate, currentRow,
				tBoxModel, aBoxModel, mult);

		// ////////transfection
		Concept transfection = new Transfection();
		transfection.visit(directContext, subject, predicate, currentRow,
				tBoxModel, aBoxModel, mult);

		// ////////construct
		Concept construct = new Construct();
		construct.visit(directContext, subject, predicate, currentRow,
				tBoxModel, aBoxModel, mult);

		// ///////transfection agent
		Concept transfectionAgent = new TransfectionAgent();
		transfectionAgent.visit(directContext, subject, predicate, currentRow,
				tBoxModel, aBoxModel, mult);

		// ////////serum
		Concept serum = new Serum();
		serum.visit(directContext, subject, predicate, currentRow, tBoxModel,
				aBoxModel, mult);

		// ////////medium
		Concept medium = new Medium();
		medium.visit(directContext, subject, predicate, currentRow, tBoxModel,
				aBoxModel, mult);

		// ////////assay serum
		Concept assaySerum = new AssaySerum();
		assaySerum.visit(directContext, subject, predicate, currentRow,
				tBoxModel, aBoxModel, mult);

		// ////////assay medium
		Concept assayMedium = new AssayMedium();
		assayMedium.visit(directContext, subject, predicate, currentRow,
				tBoxModel, aBoxModel, mult);

		// ////////ProteinTarget Modification
		Concept proteinModification = new ProteinModification();
		proteinModification.visit(directContext, subject, predicate,
				currentRow, tBoxModel, aBoxModel, mult);

		// ////////ProteinTarget Preparation
		Concept proteinPreparation = new ProteinPreparation();
		proteinPreparation.visit(directContext, subject, predicate, currentRow,
				tBoxModel, aBoxModel, mult);

		// ////////ProteinTarget ProteinTarget Interaction
		Concept proteinProteinInteraction = new ProteinProteinInteraction();
		proteinProteinInteraction.visit(directContext, subject, predicate,
				currentRow, tBoxModel, aBoxModel, mult);

		// ////////ProteinTarget Purity
		Concept proteinPurity = new ProteinPurity();
		proteinPurity.visit(directContext, subject, predicate, currentRow,
				tBoxModel, aBoxModel, mult);

	}

	/*
	 * 
	 * 
	 * @see edu.miami.ccs.bao.direct.api.Concept#doData(java.sql.ResultSet,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	@Override
	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {

	}

}

package edu.miami.ccs.bao.stat;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import edu.miami.ccs.bao.rules.api.CsvDataSource;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.rules.impl.CsvDataSourceImpl;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This is the class that checks the statistics of Uma's annotation endpoint
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class SSStat {

	private Map<String, Integer> standardizeEndpoint = new HashMap<String, Integer>();

	private Map<String, Integer> pubChemEndpoint = new HashMap<String, Integer>();

	private Map<Integer, int[]> statCollector = new HashMap<Integer, int[]>();

	private static String endpointStandardized = "EndpointStandardized";

	private static String endpointPubChemName = "EndpointPubChemName";

	private CsvDataSource data = new CsvDataSourceImpl();

	/**
	 * 
	 * @param dataFile
	 * @throws Exception
	 */
	public void read(File dataFile) throws Exception {
		data.read(new FileInputStream(dataFile));
	}

	public void initStores() {
		int index1 = data.getHead().getIndex(endpointStandardized);
		int index2 = data.getHead().getIndex(endpointPubChemName);
		for (int i = 0; i < data.getRows().size(); i++) {
			SSRow currentRow = data.getRows().get(i);
			// standardizeEndpoint
			String value = currentRow.getValue(index1);
			String[] valueToks = value.split(";");
			for (String valueTok : valueToks) {
				valueTok = valueTok.trim().toLowerCase();
				if (!standardizeEndpoint.containsKey(valueTok)) {
					standardizeEndpoint.put(valueTok,
							standardizeEndpoint.size());
				}
			}
			// EndpointPubChemName
			value = currentRow.getValue(index2);
			valueToks = value.split(";");
			for (String valueTok : valueToks) {
				valueTok = valueTok.trim().toLowerCase();
				if (!pubChemEndpoint.containsKey(valueTok)) {
					pubChemEndpoint.put(valueTok, pubChemEndpoint.size());
				}
			}
		}
	}

	public void collectStat() {
		int index1 = data.getHead().getIndex(endpointStandardized);
		int index2 = data.getHead().getIndex(endpointPubChemName);
		for (int i = 0; i < data.getRows().size(); i++) {
			SSRow currentRow = data.getRows().get(i);
			// standardizeEndpoint
			String value1 = currentRow.getValue(index1);
			String value2 = currentRow.getValue(index2);
			String[] value1Toks = value1.split(";");
			String[] value2Toks = value2.split(";");
			for (int j = 0; j < value1Toks.length; j++) {
				int[] statsVector;
				if (!statCollector.containsKey(standardizeEndpoint
						.get(value1Toks[j].trim().toLowerCase()))) {
					statsVector = new int[pubChemEndpoint.size()];
					statsVector[pubChemEndpoint.get(value2Toks[j].trim()
							.toLowerCase())]++;
					statCollector.put(standardizeEndpoint.get(value1Toks[j]
							.trim().toLowerCase()), statsVector);
				} else {
					statsVector = statCollector.get(standardizeEndpoint
							.get(value1Toks[j].trim().toLowerCase()));
					statsVector[pubChemEndpoint.get(value2Toks[j].trim()
							.toLowerCase())]++;
				}
			}

		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public StringBuffer printStat() throws Exception {
		// write the heading
		StringBuffer sb = new StringBuffer();
		sb.append(csvs("SE")).append(",");
		Map.Entry<String, Integer>[] entriesPchem = (Map.Entry<String, Integer>[]) pubChemEndpoint
				.entrySet().toArray(
						new Map.Entry[pubChemEndpoint.entrySet().size()]);
		for (int i = 0; i < pubChemEndpoint.size(); i++) {
			sb.append(csvs(getKeyBasedOnValue(entriesPchem, i)));
			if (!(i == (pubChemEndpoint.size() - 1))) {
				sb.append(",");
			}
		}
		sb.append("\n");
		Map.Entry<String, Integer>[] entriesSe = (Map.Entry<String, Integer>[]) standardizeEndpoint
				.entrySet().toArray(
						new Map.Entry[standardizeEndpoint.entrySet().size()]);
		for (Map.Entry<Integer, int[]> collector : statCollector.entrySet()) {
			int key = collector.getKey();
			sb.append(csvs(getKeyBasedOnValue(entriesSe, key))).append(",");
			for (int j = 0; j < collector.getValue().length; j++) {
				sb.append(csvi(collector.getValue()[j]));
				if (!(j == (collector.getValue().length - 1))) {
					sb.append(",");
				}
			}
			sb.append("\n");
		}
		return sb;
	}

	/**
	 * 
	 * @param entries
	 * @param uniqueV
	 * @return
	 * @throws Exception
	 */
	private String getKeyBasedOnValue(Map.Entry<String, Integer>[] entries,
			int uniqueV) throws Exception {
		for (int i = 0; i < entries.length; i++) {
			if (entries[i].getValue().equals(uniqueV)) {
				return entries[i].getKey();
			}
		}
		throw new Exception("Unique entry cannot be found : " + uniqueV);
	}

	public void writeBuffer(StringBuffer sb, PrintWriter out) {
		out.write(sb.toString());
		out.flush();
		out.close();
	}

	private String csvs(String v) {
		return "\"" + v + "\"";
	}

	private String csvi(int v) {
		return "" + v;
	}

	/**
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		File inFile = new File("/home/sam/projects/bao/csv",
				"Luciferase_assay_annotations_19Aug2010.csv");
		File outFile = new File("/home/sam/projects/bao/tmp", "stat.csv");
		SSStat stat = new SSStat();
		stat.read(inFile);
		stat.initStores();
		stat.collectStat();
		stat.writeBuffer(stat.printStat(), new PrintWriter(
				new FileOutputStream(outFile)));
	}

}

package edu.miami.ccs.bao.rules.impl;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.ontology.*;
import edu.miami.ccs.bao.rules.api.MutateRuleEngineWithSSRowData;
import edu.miami.ccs.bao.rules.api.*;
import edu.miami.ccs.bao.util.Util;

import java.util.List;

/**
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class TypeCEngineImpl implements NaiveEngine, MutateRuleEngineWithJena,
		MutateRuleEngineWithSSRowData {

	private ObjectProperty mainObjectProperty;

	private Individual mainSourceIndividual;

	private SSRow uHead;

	private SSRow uRow;

	/*
	 * @see
	 * edu.miami.ccs.bao.rules.api.Engine#pre(edu.miami.ccs.bao.rules.api.Mapper
	 * .Individual, edu.miami.ccs.bao.rules.api.SSRow,
	 * com.hp.hpl.jena.ontology.OntModel, com.hp.hpl.jena.ontology.OntModel,
	 * edu.miami.ccs.bao.rules.api.Document, java.lang.String, java.lang.String)
	 */
	public void pre(Mapper.Individual individual, SSRow dRow,
			OntModel tBoxModel, OntModel aBoxModel, Document document,
			String indiPrefix, String indCnt) {
		String ns = document.getMapper().getNamespace().getNs()
				.get(individual.getNsPrefix());
		// =======================================
		SSRow dHead = document.getDatabaseDataSource().getHead();

		OntClass mainClass = tBoxModel.getOntClass(ns
				+ individual.getConceptType().getOnt());

		if (mainClass == null) {
			error("main class not found", individual.getConceptType().getOnt());
		}

		Individual ontIndividual;
		if (individual.getConceptType().getXref() != null) {
			String xrefValue = Util.getValue(uHead, uRow, dHead, dRow,
					individual.getConceptType().getXref());
			ontIndividual = aBoxModel.createIndividual(
					getConceptName(ns, indiPrefix, individual.getConceptType()
							.getOnt() + "_", xrefValue), mainClass);
		} else {
			ontIndividual = aBoxModel.createIndividual(
					getConceptName(ns, indiPrefix, individual.getConceptType()
							.getOnt() + "_", indCnt), mainClass);
		}

		// data types
		for (Mapper.DataType dataType : individual.getDataTypes()) {
			DatatypeProperty datatypeProperty = tBoxModel
					.getDatatypeProperty(ns + dataType.getOnt());
			if (datatypeProperty == null) {
				error("data type is not found", dataType.getOnt());
			}
			String value = Util.getValue(uHead, uRow, dHead, dRow,
					dataType.getData());
			value = (value != null) ? value : "";
			if (dataType.getXsd().equals("string")) {
				ontIndividual.addProperty(datatypeProperty, value);
			} else if (dataType.getXsd().equals("integer")) {
				try {
					ontIndividual.addLiteral(datatypeProperty, aBoxModel
							.createTypedLiteral(Integer.parseInt(value),
									XSDDatatype.XSDinteger));
				} catch (NumberFormatException e) {
					String err = TypeCEngineImpl.class.getName() + " value : "
							+ value + " for "
							+ Util.getStrippedEntry(dataType.getData());

				}
			} else if (dataType.getXsd().equals("float")) {
				try {
					ontIndividual.addLiteral(datatypeProperty, aBoxModel
							.createTypedLiteral(Float.parseFloat(value),
									XSDDatatype.XSDfloat));
				} catch (NumberFormatException e) {
					String err = TypeCEngineImpl.class.getName() + " value : "
							+ value + " for "
							+ Util.getStrippedEntry(dataType.getData());

				}
			}

		}
		mainSourceIndividual.addProperty(mainObjectProperty, ontIndividual);

	}

	/*
	 * @see
	 * edu.miami.ccs.bao.rules.api.Engine#post(edu.miami.ccs.bao.rules.api.Mapper
	 * .Individual, edu.miami.ccs.bao.rules.api.SSRow,
	 * com.hp.hpl.jena.ontology.OntModel, com.hp.hpl.jena.ontology.OntModel,
	 * edu.miami.ccs.bao.rules.api.Document, java.lang.String, java.lang.String)
	 */
	public void post(Mapper.Individual target, SSRow dRow, OntModel tBoxModel,
			OntModel aBoxModel, Document document, String indiPrefix,
			String indCnt) {
		String ns = document.getMapper().getNamespace().getNs()
				.get(target.getNsPrefix());
		// =======================================
		SSRow dHead = document.getDatabaseDataSource().getHead();

		OntClass mainClass = tBoxModel.getOntClass(ns
				+ target.getConceptType().getOnt());

		if (mainClass == null) {
			error("main class not found", target.getConceptType().getOnt());
		}
		Individual targetIndividual;
		if (target.getConceptType().getXref() != null) {
			String xrefValue = Util.getValue(uHead, uRow, dHead, dRow, target
					.getConceptType().getXref());
			targetIndividual = aBoxModel.getIndividual(getConceptName(ns,
					indiPrefix, target.getConceptType().getOnt() + "_",
					xrefValue));
		} else {
			targetIndividual = aBoxModel
					.getIndividual(getConceptName(ns, indiPrefix, target
							.getConceptType().getOnt() + "_", indCnt));
		}

		List<Mapper.ObjType> objTypeList = target.getObjTypes();
		if (objTypeList.size() > 0) {
			for (Mapper.ObjType objType : objTypeList) {
				ObjectProperty objectProperty = tBoxModel.getObjectProperty(ns
						+ objType.getOnt());
				if (objectProperty == null) {
					error("object property not found ", ns + objType.getOnt());
				}

				String refTBoxIndividual = objType.getRefTBoxIndividual();
				if (refTBoxIndividual != null) {
					Individual tBoxOntIndividual = tBoxModel.getIndividual(ns
							+ refTBoxIndividual);
					if (tBoxOntIndividual == null) {
						error("T-Box individual does not exists",
								refTBoxIndividual);
					} else {
						targetIndividual.addProperty(objectProperty,
								tBoxOntIndividual);
					}
				}
			}
			mainSourceIndividual.addProperty(mainObjectProperty,
					targetIndividual);
		}
	}

	public void error(String err1, String err2) {

		System.exit(-91);
	}

	public void message(String message) {

	}

	public String getConceptName(String ns, String prefix, String ont,
			String cnt) {
		return ns + prefix + ont + cnt;
	}

	public void setJenaObjectProperty(ObjectProperty objectProperty) {
		mainObjectProperty = objectProperty;
	}

	public void setJenaIndividual(Individual sourceIndividual) {
		mainSourceIndividual = sourceIndividual;
	}

	public void setHead(SSRow head) {
		uHead = head;
	}

	public void setRow(SSRow row) {
		uRow = row;
	}
}

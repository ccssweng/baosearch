package edu.miami.ccs.bao.direct.impl.endpoint;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.AbstractConcept;
import edu.miami.ccs.bao.direct.api.Concept;
import edu.miami.ccs.bao.direct.api.Constants;
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.direct.impl.perturbagen.Perturbagen;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * creates AC50 individuals
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class Ac50 extends AbstractConcept {

	protected final String id = "186";

	protected final String hasPerturbagenConcentrationValue = "336";

	protected final String hasModeOfAction = "196";

	protected final String inhibitionIndividual = "340";

	protected final String hasPercentResponse = "337";

	protected final String fifityPercentInhibitionIndividual = "341";

	protected final String hasPerturbagenConcentrationUnit = "183";

	protected final String micorMolarIndividual = "301";

	// ////////////////////////////////////////////////////////////
	protected final String hasPerturbagen = "185";

	// ////////////////////////////////////////////////////////////

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	public void visit(DirectContext directContext, Individual subject,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int tid) {
		endpointDatabaseVisitHelper(this, directContext, subject, predicate,
				currentRow, tBoxModel, aBoxModel, tid);
	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.Concept#doData(java.sql.ResultSet,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {
		int i = 0; // limit
		boolean executedAtLeastOnce = false;
		while (rset.next() && i < directContext.getMaxDbRead()) {
			executedAtLeastOnce = true;
			// each raw is potential endpoint for BioAssay
			String dbValueOf = rset.getString(AbstractConcept.dbValueOf);
			// TODO: database inconsistancy
			try {
				Float.parseFloat(dbValueOf);
			} catch (NumberFormatException e) {

				directContext.getLogger().warning(
						"## annotation file concentration is used");
				dbValueOf = "0";
			}

			String dbTid = rset.getString(AbstractConcept.dbTid);
			String shortId = dbTid + _unique();
			String id = Util.getIndividualIri(directContext.getConfiguration()
					.getNs(NS_BAO), Constants.INDI_PREFIX + getId_(this.id)
					+ shortId);
			Individual endpointIndividual = createIndividual(
					getOntClass(tBoxModel, getId(this.id), directContext), id,
					shortId, aBoxModel, LabelModifier.CLASS_LABEL);
			// relation (subject is measure group)
			subject.addProperty(predicate, endpointIndividual);

			// object
			ObjectProperty op = getObjectProperty(tBoxModel,
					getId(hasModeOfAction), directContext);
			endpointIndividual.addProperty(
					op,
					getTBoxIndividual(tBoxModel, getId(inhibitionIndividual),
							directContext));
			op = getObjectProperty(tBoxModel, getId(hasPercentResponse),
					directContext);
			endpointIndividual.addProperty(
					op,
					getTBoxIndividual(tBoxModel,
							getId(fifityPercentInhibitionIndividual),
							directContext));
			op = getObjectProperty(tBoxModel,
					getId(hasPerturbagenConcentrationUnit), directContext);
			endpointIndividual.addProperty(
					op,
					getTBoxIndividual(tBoxModel, getId(micorMolarIndividual),
							directContext));

			// data type
			DatatypeProperty dp = getDatatypeProperty(tBoxModel,
					getId(hasPerturbagenConcentrationValue), directContext);

			addLiteral(endpointIndividual, dp, 'f', aBoxModel, dbValueOf);
			// relations to other individuals: Perturbagen

			String compoundId = rset.getString(AbstractConcept.dbSubstanceId);
			if (compoundId != null && compoundId.length() > 0) {
				// create the perturbagen individual
				op = getObjectProperty(tBoxModel, getId(hasPerturbagen),
						directContext);
				Concept perturbagen = new Perturbagen();
				perturbagen.visit(directContext, endpointIndividual, op,
						currentRow, tBoxModel, aBoxModel,
						Integer.parseInt(compoundId));
			} else {

			}
			// baoseach link to bioassay
			// link back to bio assay
			addBaoSearchObjectPropertyToBioAssay(directContext, tBoxModel,
					aBoxModel, currentRow, endpointIndividual, "hasEndpoint",
					"isEndpointOf");
			i++;
		}

		if (directContext.getMaxDbRead() > 0 && !executedAtLeastOnce) {
			String shortId = _unique();
			String id = Util.getIndividualIri(directContext.getConfiguration()
					.getNs(NS_BAO), Constants.INDI_PREFIX + getId_(this.id)
					+ shortId);
			Individual endpointIndividual = createIndividual(
					getOntClass(tBoxModel, getId(this.id), directContext), id,
					shortId, aBoxModel, LabelModifier.CLASS_LABEL);
			// relation (subject is measure group)
			subject.addProperty(predicate, endpointIndividual);
			addBaoSearchObjectPropertyToBioAssay(directContext, tBoxModel,
					aBoxModel, currentRow, endpointIndividual, "hasEndpoint",
					"isEndpointOf");
		}

	}

}

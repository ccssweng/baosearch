package edu.miami.ccs.bao.rules.impl;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import edu.miami.ccs.bao.rules.api.CsvDataSource;
import edu.miami.ccs.bao.rules.api.SSRow;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class CsvDataSourceImpl implements CsvDataSource {

	private SSRow head = new SSRowImpl();

	private List<SSRow> rows = new ArrayList<SSRow>();

	public SSRow getHead() {
		return head;
	}

	public List<SSRow> getRows() {
		return rows;
	}

	/*
	 * @see edu.miami.ccs.bao.rules.api.CsvDataSource#read(java.io.InputStream)
	 */
	public void read(InputStream in) throws Exception {
		CsvReader reader = new CsvReader(new InputStreamReader(in));
		boolean isHeaderRead = false;
		boolean isSkipRowRead = false;
		String[] nextLine;
		while ((nextLine = reader.readNext()) != null) {
			// First line in the annotation file is the headers
			if (!isHeaderRead) {
				isHeaderRead = true;
				for (int i = 0; i < nextLine.length; i++) {
					head.addRow(nextLine[i]);
				}
				continue;
			}

			// Annotations folks put another line which is of no informative
			// value;
			// so I skipped it.
			if (!isSkipRowRead) {
				isSkipRowRead = true;
				continue;
			}

			SSRow currRow = new SSRowImpl();
			for (int i = 0; i < nextLine.length; i++) {
				currRow.addRow(nextLine[i]);
			}
			rows.add(currRow);
		}

	}
}

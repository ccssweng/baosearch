package edu.miami.ccs.bao.direct.impl.target;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.AbstractConcept;
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Biological process class creates the subject, object and, predicate for GO related biological process
 * for a particular assay taking the information from Annotation file
 * 
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class BiologicalProcess extends AbstractConcept {

	protected final String biologicalProcess = "BiologicalProcessTarget";

	protected final String biologicalProcessId = "264";

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	@Override
	public void visit(DirectContext directContext, Individual measureGroup,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int mult) {

		String biologicalProcessIndividualName = Util.getValue(directContext
				.getCsvDataSource().getHead(), currentRow, biologicalProcess);

		// error checking
		if (biologicalProcessIndividualName == null
				|| biologicalProcessIndividualName.length() == 0) {
			warning("description name is empty");
			directContext.getLogger().warning("biological process in empty");
		} else {
			Individual specificIndividual;
			String[] bpinToks = biologicalProcessIndividualName.trim().split(
					";");
			for (String bpin : bpinToks) {
				if (!isCellToDecIdMappingInExistence(tBoxModel, bpin.trim()
						.toLowerCase())) {
					directContext.getLogger().warning(
							BiologicalProcess.class.getName() + " " + bpin
									+ " does not exists and "
									+ getId(biologicalProcessId)
									+ "is used #######");

					specificIndividual = createGlobalIndividualParam2(
							bpin,
							biologicalProcessId,
							LabelModifier.SHORT_ID,
							null,
							directContext.getGlobalInformation().biologicalProcessMapping,
							directContext, measureGroup, predicate, tBoxModel,
							aBoxModel);
				} else {
					specificIndividual = createGlobalIndividualParam1(
							bpin.trim().toLowerCase(),
							null,
							directContext.getGlobalInformation().biologicalProcessMapping,
							directContext, measureGroup, predicate, tBoxModel,
							aBoxModel, IdModifier.LOCAL_ID);
				}

				directContext.getLogger().warning(
						"ArtificialRegulatoryElement missing #############");
				directContext.getLogger().warning(
						"Inducer missing #############");
				directContext.getLogger().warning(
						"TranscriptionFactor missing #############");

				// baoseach link to bioassay
				// link back to bio assay
				addBaoSearchObjectPropertyToBioAssay(directContext, tBoxModel,
						aBoxModel, currentRow, specificIndividual, "hasTarget",
						"isTargetOf");
			}
		}

	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.Concept#doData(java.sql.ResultSet,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	@Override
	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {
		// Not used yet
	}
}

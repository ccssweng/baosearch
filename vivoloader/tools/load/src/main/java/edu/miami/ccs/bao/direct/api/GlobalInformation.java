package edu.miami.ccs.bao.direct.api;
/**
*Copyright (c) 2011, The University of Miami
*All rights reserved.
 
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the University of Miami nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.
 
*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
*ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
*WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
*DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
*ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
*SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Just a global information storage
 * 
 * @author akoleti, Sam Center for Computational Science
 * University of Miami
 * 
 */

public class GlobalInformation {

	/**
     * This will check the given bioassay has been checked. If it is checked, then a different
     * measure group will be used instead for other stuff
     */
    public Map<String, Boolean> checkedBioAssayMap = new HashMap<String, Boolean>();


    /**
     * annotation file cell name : label:the global id
     */

    public Map<String, List<NameToId>> assayProviderMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> assayCommentsMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> assayTitleMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> assayDescriptionMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> assayDepositorMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> assayProtocolMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> positiveControlMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> negativeControlMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> highControlMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> lowControlMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> backgroundControlMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> bioassayTypeMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> assayMeasurementTypeMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> bioassayMeasurementContentMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> assayFootprintMapping =
            new ConcurrentHashMap<String, List<NameToId>>();


    public Map<String, List<NameToId>> transfectionMapping =
            new ConcurrentHashMap<String, List<NameToId>>();


    public Map<String, List<NameToId>> proteinPurityMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> uniprotMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> signalDirectionMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> organismMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> kitDetailMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> detectionPlatformMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> proteinProteinInteractionMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> detergentMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> substrateIncubationMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> emissionWavelengthMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> proteinModificationMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> excitationWavelengthMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> cellSpecTargetMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> constructTargetMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> mediumTargetMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> serumTargetMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> substrateMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> assaySerumMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> potentiatorMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> absorbanceWavelengthMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> transfectionAgentMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> enzymeReactionMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> coupledSubstrateMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> measuredEntityMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> assayMediumMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> detectionReagentMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> redoxReagentMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> cellModificationMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> proteinPreparationMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> inhibitorReagentMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> dmsoMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> taxMapping = new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> perturbagenIncubationMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> assayPhaseCharacteristicMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> screeningCampaignMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> inducerMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> transcriptionFactorMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> geneMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> artRegulateElementMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> kitNameMapping =
            new ConcurrentHashMap<String, List<NameToId>>();


    public Map<String, List<NameToId>> biologicalProcessMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> detectionMethodMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> signalingpathwayMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> proteinTargetMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> designMapping =
            new ConcurrentHashMap<String, List<NameToId>>();

    public Map<String, List<NameToId>> formatMapping =
            new ConcurrentHashMap<String, List<NameToId>>();


}

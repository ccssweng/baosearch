package edu.miami.ccs.bao.direct.impl.specification;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.*;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * AssayProvider class creates the subject, object and, predicate for an assay provider
 * for a particular assay taking the information from Database
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class AssayProvider extends AbstractConcept {

	private static String id = "027";

	private static String sourceName = "sourceName";

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	public void visit(DirectContext directContext, Individual subject,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int aid) {
		String sql = "select " + sourceName + " from pcassay where aid =" + aid;
		directContext.doResult(sql, this, directContext, subject, predicate,
				currentRow, tBoxModel, aBoxModel);

	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.Concept#doData(java.sql.ResultSet,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {
		while (rset.next()) {
			String sName = rset.getString(sourceName);
			if (sName != null && sName.length() > 0) {

				OntClass clazz = getOntClass(tBoxModel, getId(id),
						directContext);
				GlobalInformation gInfo = directContext.getGlobalInformation();
				List<NameToId> nameToIdList = gInfo.assayProviderMapping
						.get(sName);
				if (nameToIdList == null) {
					ArrayList<NameToId> toIds = new ArrayList<NameToId>();
					NameToId specificObj = new NameToId(sName, _unique());
					toIds.add(specificObj);
					gInfo.assayProviderMapping.put(sName, toIds);
					String id = Util.getIndividualIri(directContext
							.getConfiguration().getNs(NS_BAO),
							Constants.INDI_PREFIX + clazz.getLocalName()
									+ specificObj.getId());
					createIndividual(clazz, id, Constants.EMPTY_NAME,
							aBoxModel, LabelModifier.CLASS_LABEL);
				}

				nameToIdList = gInfo.assayProviderMapping.get(sName);
				NameToId specificObj = nameToIdList.get(nameToIdList
						.indexOf(NameToId.searchObj(sName)));
				String id = Util.getIndividualIri(directContext
						.getConfiguration().getNs(NS_BAO),
						Constants.INDI_PREFIX + clazz.getLocalName()
								+ specificObj.getId());
				Individual specificIndividual = getABoxIndividual(aBoxModel, id);
				subject.addProperty(predicate, specificIndividual);
			}
		}
	}
}

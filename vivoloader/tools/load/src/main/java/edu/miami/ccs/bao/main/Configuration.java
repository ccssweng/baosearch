package edu.miami.ccs.bao.main;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Configuration structure of the static information
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class Configuration {

	public static String jdbcUrl = "jdbcUrl";

	public static String jdbcUserName = "jdbcUserName";

	public static String jdbcPassWord = "jdbcPassWord";

	public static String annonFile = "annonFile";

	public static String owlFile = "owlFile";

	public static String outDirectory = "outDirectory";

	public static String storeType = "storeType";

	public static String outOwlFileName = "outOwlFileName";

	public static String maxDbRead = "maxDbRead";

	public static String outSdb = "outSdb";

	public static String outSdbUserName = "outSdbUserName";

	public static String outSdbPassWord = "outSdbPassWord";

	public static String baoSearchConceptualization = "baoSearchConceptualization";

	public static String namespaces = "namespaces";

	public static String namespace = "namespace";

	private static String prefix = "prefix";

	private static String altEntries = "altEntries";

	private static String altEntry = "altEntry";

	private static String externalImport = "externalImport";

	private static String localImport = "localImport";

	private static String variables = "variables";

	private static String variable = "variable";

	private static String name = "name";

	private Map<String, String> configData = new HashMap<String, String>();

	private Map<String, String> namespaceData = new HashMap<String, String>();

	private Map<String, String> altEntryMap = new HashMap<String, String>();

	private Map<String, String> variablesMap = new HashMap<String, String>();

	protected boolean vERBOSE = true;

	/**
	 * 
	 * @param file
	 * @throws FileNotFoundException
	 * @throws XMLStreamException
	 */
	public void read(File file) throws FileNotFoundException,
			XMLStreamException {
		InputStream in = new FileInputStream(file);
		// create the parser
		XMLStreamReader parser = XMLInputFactory.newInstance()
				.createXMLStreamReader(in);
		// create the builder
		StAXOMBuilder builder = new StAXOMBuilder(parser);
		build(builder.getDocumentElement());
	}

	/**
	 * 
	 * @param doc
	 */
	private void build(OMElement doc) {
		for (Iterator iterator = doc.getChildElements(); iterator.hasNext();) {
			OMElement element = (OMElement) iterator.next();
			String localName = element.getLocalName();

			// namespaces
			if (localName.equals(namespaces)) {
				for (Iterator childIterNamespace = element
						.getChildrenWithLocalName(namespace); childIterNamespace
						.hasNext();) {
					OMElement namespaceEle = (OMElement) childIterNamespace
							.next();
					String prefixValue = namespaceEle.getAttributeValue(
							new QName(prefix)).trim();
					namespaceData.put(prefixValue, namespaceEle.getText()
							.trim());
				}
				continue;
			}

			if (localName.equals(altEntries)) {
				for (Iterator iter2 = element
						.getChildrenWithLocalName(altEntry); iter2.hasNext();) {
					OMElement altEntryEle = (OMElement) iter2.next();
					altEntryMap.put(
							altEntryEle
									.getFirstChildWithName(
											new QName(externalImport))
									.getText().trim(),
							altEntryEle
									.getFirstChildWithName(
											new QName(localImport)).getText()
									.trim());
				}
				continue;
			}

			String localText = element.getText().trim();
			if (localName.equals(jdbcUrl)) {
				configData.put(jdbcUrl, localText);
				continue;
			}
			if (localName.equals(annonFile)) {
				configData.put(annonFile, localText);
				continue;
			}
			if (localName.equals(outDirectory)) {
				configData.put(outDirectory, localText);
				continue;
			}
			if (localName.equals(jdbcUserName)) {
				configData.put(jdbcUserName, localText);
				continue;
			}

			if (localName.equals(owlFile)) {
				configData.put(owlFile, localText);
				continue;
			}

			if (localName.equals(jdbcPassWord)) {
				configData.put(jdbcPassWord, localText);
				continue;
			}

			if (localName.equals(storeType)) {
				configData.put(storeType, localText);
				continue;
			}

			if (localName.equals(outOwlFileName)) {
				configData.put(outOwlFileName, localText);
				continue;
			}

			if (localName.equals(maxDbRead)) {
				configData.put(maxDbRead, localText);
				continue;
			}

			if (localName.equals(outSdb)) {
				configData.put(outSdb, localText);
				continue;
			}

			if (localName.equals(outSdbUserName)) {
				configData.put(outSdbUserName, localText);
				continue;
			}

			if (localName.equals(outSdbPassWord)) {
				configData.put(outSdbPassWord, localText);
				continue;
			}

			if (localName.equals(baoSearchConceptualization)) {
				configData.put(baoSearchConceptualization, localText);
				continue;
			}

			// variables
			if (localName.equals(variables)) {
				for (Iterator childIterNamespace = element
						.getChildrenWithLocalName(variable); childIterNamespace
						.hasNext();) {
					OMElement namespaceEle = (OMElement) childIterNamespace
							.next();
					String prefixValue = namespaceEle.getAttributeValue(
							new QName(name)).trim();
					variablesMap
							.put(prefixValue, namespaceEle.getText().trim());
				}
			}
		}

		postProcessVariables();

		if (vERBOSE) {
			for (String key : configData.keySet()) {

			}

			for (String key : altEntryMap.keySet()) {

			}
		}

	}

	public void put(String key, String value) {
		configData.put(key, value);
	}

	public String get(String key) {
		return configData.get(key);
	}

	public void putNs(String prefix, String ns) {
		namespaceData.put(prefix, ns);
	}

	public String getNs(String prefix) {
		String ns = namespaceData.get(prefix);
		if (ns == null || ns.length() == 0) {

			System.exit(-100);
		}
		return ns;
	}

	public Set<Map.Entry<String, String>> getNsEntrySet() {
		return namespaceData.entrySet();
	}

	public void putAltEntry(String key, String value) {
		altEntryMap.put(key, value);
	}

	public Set<Map.Entry<String, String>> getAltEntrySet() {
		return altEntryMap.entrySet();
	}

	public static String fileExists(String fileName)
			throws FileNotFoundException {
		if (new File(fileName).exists()) {
			return fileName;
		} else {
			throw new FileNotFoundException("File does not exists " + fileName);
		}
	}

	private void postProcessVariables() {
		for (String key : variablesMap.keySet()) {
			// for each key
			String var = "\\$\\{" + key.trim() + "\\}";
			String varValue = variablesMap.get(key);

			Pattern replace = Pattern.compile(var);
			// replace
			Map<String, String> tmp = new HashMap<String, String>();
			tmp.putAll(altEntryMap);
			for (String altKey : tmp.keySet()) {
				String altValue = tmp.get(altKey);
				Matcher matcher = replace.matcher(altValue);
				if (matcher.find()) {
					int startIdx = matcher.start();
					int endIdx = matcher.end();
					StringBuilder buf = new StringBuilder();
					buf.append(altValue.substring(0, startIdx))
							.append(varValue)
							.append(altValue.substring(endIdx));
					altEntryMap.put(altKey, buf.toString());
				}
			}

			Matcher matcher = replace.matcher(configData.get(annonFile));
			if (matcher.find()) {
				String temp = configData.get(annonFile);
				int startIdx = matcher.start();
				int endIdx = matcher.end();
				StringBuilder buf = new StringBuilder();
				buf.append(temp.substring(0, startIdx)).append(varValue)
						.append(temp.substring(endIdx));
				configData.put(annonFile, buf.toString());
			}

			matcher = replace.matcher(configData.get(owlFile));
			if (matcher.find()) {
				String temp = configData.get(owlFile);
				int startIdx = matcher.start();
				int endIdx = matcher.end();
				StringBuilder buf = new StringBuilder();
				buf.append(temp.substring(0, startIdx)).append(varValue)
						.append(temp.substring(endIdx));
				configData.put(owlFile, buf.toString());
			}
		}
	}
}

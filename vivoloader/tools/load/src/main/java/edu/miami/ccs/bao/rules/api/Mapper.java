package edu.miami.ccs.bao.rules.api;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import org.apache.axiom.om.OMElement;

import java.util.List;
import java.util.Map;

/**
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public interface Mapper {

	Namespace getNamespace();

	Map<String, IndividualTypeA> getIndividualsTypeA();

	Map<String, IndividualTypeB> getIndividualsTypeB();

	Map<String, IndividualTypeC> getIndividualsTypeC();

	Map<String, SingleOntologyIndividual> getSingleOntologyIndividual();

	void build(OMElement doc);

	public interface Build {
		void build(OMElement ele);
	}

	public interface Namespace extends Build {
		Map<String, String> getNs();

		String getXmlBaseNs();
	}

	public interface Individual extends Build {

		String getId();

		String getNsPrefix();

		ConceptType getConceptType();

		List<DataType> getDataTypes();

		List<ObjType> getObjTypes();

	}

	public interface SingleOntologyIndividual extends Individual {

	}

	public interface IndividualTypeA extends Individual {

	}

	public interface IndividualTypeB extends Individual {
		List<If> getIfs();

	}

	public interface IndividualTypeC extends Individual {

	}

	public interface If extends Build {
		String getTest();

		ConceptType getConceptType();

		List<ObjType> getObjTypes();

		List<DataType> getDataTypes();

	}

	public interface ConceptType extends Build {
		String getOnt();

		String getXref();
	}

	public interface DataType extends ConceptType {
		String getXsd();

		String getData();

	}

	public interface ObjType extends Build {
		String getOnt();

		String getRefSingleOntologyIndividual();

		String getRefTypeA();

		String getRefTypeB();

		String getRefTypeC();

		String getRefTBoxIndividual();
	}
}

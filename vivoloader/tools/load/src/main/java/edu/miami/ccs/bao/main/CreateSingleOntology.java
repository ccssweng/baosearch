package edu.miami.ccs.bao.main;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * This class creates the bioassay ontology combining all the merged ontologies
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class CreateSingleOntology {

	private Map<String, String> altEntries;

	private String absInFile;

	private String absOutFile;

	/**
	 * 
	 * @param altEntries
	 * @param absInFile
	 * @param absOutFile
	 */
	public CreateSingleOntology(Map<String, String> altEntries,
			String absInFile, String absOutFile) {
		this.altEntries = altEntries;
		this.absInFile = absInFile;
		this.absOutFile = absOutFile;
	}

	/**
	 * 
	 * @throws FileNotFoundException
	 */
	public void serializeOntology() throws FileNotFoundException {
		OntModel model = ModelFactory
				.createOntologyModel(OntModelSpec.OWL_DL_MEM);
		for (Map.Entry<String, String> entry : altEntries.entrySet()) {
			model.getDocumentManager().addAltEntry(entry.getKey(),
					entry.getValue());
		}

		model.read(absInFile);

		model.write(new FileOutputStream(new File(absOutFile)),
				"RDF/XML-ABBREV");
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Map<String, String> altEntries = new HashMap<String, String>();

		altEntries
				.put("http://www.bioassayontology.org/bao",
						"file:/Users/akoleti/svn/baovitro/trunk/ontology/bioassay_new.owl");

		altEntries
				.put("http://bioassayontology.org/bao/external/CLO_import.owl",
						"file:/Users/akoleti/svn/baovitro/trunk/ontology/onto_fox/CLO_import.owl");
		altEntries
				.put("http://bioassayontology.org/bao/external/IAO_import.owl",
						"file:/Users/akoleti/svn/baovitro/trunk/ontology/import/IAO_import.owl");
		altEntries
				.put("http://bioassayontology.org/bao/external/OBI_import.owl",
						"file:/Users/akoleti/svn/baovitro/trunk/ontology/import/OBI_import.owl");
		altEntries
				.put("http://bioassayontology.org/bao/external/UO_import.owl",
						"file:/Users/akoleti/svn/baovitro/trunk/ontology/import/UO_import.owl");
		altEntries
				.put("http://bioassayontology.org/bao/external/organism_import.owl",
						"file:/Users/akoleti/svn/baovitro/trunk/ontology/onto_fox/organism_import.owl");
		altEntries
				.put("http://bioassayontology.org/bao/external/signaling_pathway_import.owl",
						"file:/Users/akoleti/svn/baovitro/trunk/ontology/onto_fox/signaling_pathway_import.owl");
		altEntries
				.put("http://www.bioassayontology.org/bao/external/biological_processes_import.owl",
						"file:/Users/akoleti/svn/baovitro/trunk/ontology/onto_fox/biological_processes_import.owl");

		String in = "file:/Users/akoleti/svn/baovitro/trunk/ontology/bao_search_ont/baoSearch.owl";
		String out = "/Users/akoleti/svn/baovitro/trunk/ontology/bao_uni.owl";
		CreateSingleOntology cso = new CreateSingleOntology(altEntries, in, out);
		try {
			cso.serializeOntology();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
}

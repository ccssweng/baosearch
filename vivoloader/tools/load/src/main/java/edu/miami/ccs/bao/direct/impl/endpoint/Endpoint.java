package edu.miami.ccs.bao.direct.impl.endpoint;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.Concept;
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.direct.impl.Nothing;
import edu.miami.ccs.bao.direct.impl.bioassay.BioAssayComponent;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Endpoint class calls specific endpoint class basing on what is there in the
 * annotation file
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class Endpoint extends BioAssayComponent {

	private static String endpointStandardized = "EndpointStandardized";

	protected final String annonTid = "TID";

	private static Map<String, Concept> concepts = new ConcurrentHashMap<String, Concept>();

	private static String percentActivation = "Percent activation"
			.toLowerCase();
	private static String percentStimulation = "Percent stimulation"
			.toLowerCase();
	private static String percentGrowthInhibition = "Percent growth inhibition"
			.toLowerCase();
	private static String percentInhibition = "Percent inhibition"
			.toLowerCase();
	private static String ac50 = "AC50".toLowerCase();
	private static String percentViability = "Percent viability".toLowerCase();
	private static String activation = "Activation".toLowerCase();
	private static String activityDirection = "Activity direction"
			.toLowerCase();
	private static String cc50 = "CC50".toLowerCase();
	private static String ec30 = "EC30".toLowerCase();
	private static String ec50 = "EC50".toLowerCase();
	private static String ic50 = "IC50".toLowerCase();
	private static String growthInhibition = "Growth inhibition".toLowerCase();
	private static String inhibition = "Inhibition".toLowerCase();
	private static String luminescenceIntensity = "Luminescence intensity"
			.toLowerCase();
	private static String modeOfAction = "Mode of action".toLowerCase();
	private static String maximalResponse = "Maximal response".toLowerCase();
	private static String rawActivity = "Raw activity reported".toLowerCase();

	private static String percentResponse = "Percent response".toLowerCase();
	private static String maximalResponse2 = "maximal response".toLowerCase();
	private static String foldActivation = "Fold activation".toLowerCase();
	private static String foldInhibition = "Fold inhibition".toLowerCase();
	private static String kd = "Kd".toLowerCase();
	private static String tm = "Tm".toLowerCase();
	private static String bmax = "Bmax".toLowerCase();
	private static String pertConcentration = "Perturbagen concentration"
			.toLowerCase();
	private static String ki = "Ki".toLowerCase();
	private static String response = "Response".toLowerCase();

	static {
		concepts.put(percentActivation, new PercentActivation()); // 0
		concepts.put(percentStimulation, new PercentStimulation()); // 1
		concepts.put(percentGrowthInhibition, new PercentGrowthInhibition());
		concepts.put(percentInhibition, new PercentInhibition());// 3
		concepts.put(ac50, new Ac50());
		concepts.put(percentViability, new PercentViability());
		concepts.put(activation, new Nothing()); // 5
		concepts.put(activityDirection, new Nothing());
		concepts.put(cc50, new Cc50());
		concepts.put(ec30, new Nothing());
		concepts.put(ec50, new Ec50());
		concepts.put(ic50, new Ic50());// 11
		concepts.put(growthInhibition, new Nothing());
		concepts.put(inhibition, new Nothing());
		concepts.put(luminescenceIntensity, new Nothing());
		concepts.put(modeOfAction, new Nothing());
		concepts.put(maximalResponse, new MaximalResponse());
		concepts.put(maximalResponse2, new MaximalResponse());
		concepts.put(rawActivity, new RawActivity());
		concepts.put(kd, new Kd());
		concepts.put(percentResponse, new PercentResponse());
		concepts.put(foldActivation, new FoldActivation());
		concepts.put(foldInhibition, new FoldInhibition());
		concepts.put(tm, new Tm());
		concepts.put(bmax, new BMax());
		concepts.put(pertConcentration, new PerturbagenConcentration());
		concepts.put(ki, new Ki());
		concepts.put(response, new Response());
	}

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	public void visit(DirectContext directContext, Individual measureGroup,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int mult) {

		String value = Util.getValue(
				directContext.getCsvDataSource().getHead(), currentRow,
				endpointStandardized);
		if (value != null) {
			String[] valueToks = value.toLowerCase().split(";");
			String tids = Util.getValue(directContext.getCsvDataSource()
					.getHead(), currentRow, annonTid);
			if (tids == null) {
				error("tid are missing for", value);
				return;
			}
			String[] tidToks = tids.split(";");
			if (tidToks.length != valueToks.length) {
				error("tid missmatch", value + " " + tids);
				return;
			}

			for (int i = 0; i < valueToks.length; i++) {
				String valueTok = valueToks[i];
				Concept concept;
				if ((concept = concepts.get(valueTok.trim())) != null
						&& !(concept instanceof Nothing)) {
					// measure group has endpoint endpoint
					concept.visit(directContext, measureGroup, predicate,
							currentRow, tBoxModel, aBoxModel,
							Integer.parseInt(tidToks[i].trim()));
				}
			}
		} else {
			error("value not found ", endpointStandardized);
		}
	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.Concept#doData(java.sql.ResultSet,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {
		// not used

	}

	/**
	 * 
	 * @param individual
	 * @param predicate
	 * @param aBoxModel
	 * @param annonTestedConc
	 * @param dbTestedConc
	 */

	protected void testConcAndDb(Individual individual,
			DatatypeProperty predicate, OntModel aBoxModel,
			String annonTestedConc, String dbTestedConc) {
		if (annonTestedConc != null) {
			if (dbTestedConc != null) {
				if (annonTestedConc.equals(dbTestedConc)) {
					addLiteral(individual, predicate, 'f', aBoxModel,
							annonTestedConc);
					message("consistent entry");
				} else {
					warning("annon conc and database conc values inconsistant annonconc="
							+ annonTestedConc
							+ "dbConc="
							+ dbTestedConc
							+ " we use the db value");
					addLiteral(individual, predicate, 'f', aBoxModel,
							dbTestedConc);
				}
			} else {
				warning("db tested conc is null");
				addLiteral(individual, predicate, 'f', aBoxModel,
						annonTestedConc);
			}
		} else {
			warning("annon conc is null");
			if (dbTestedConc != null) {
				addLiteral(individual, predicate, 'f', aBoxModel, dbTestedConc);
			} else {
				warning("dbTested conc is null");
			}
		}
	}
}

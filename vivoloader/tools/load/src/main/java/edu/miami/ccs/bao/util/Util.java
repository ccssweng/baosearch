package edu.miami.ccs.bao.util;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import edu.miami.ccs.bao.rules.api.Constants;
import edu.miami.ccs.bao.rules.api.SSRow;

/**
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class Util {
	/**
	 * 
	 * @param prefix
	 * @param test
	 * @return
	 */
	public static boolean isPrefix(String prefix, String test) {
		return test.startsWith(prefix);
	}

	/**
	 * 
	 * @param head
	 * @param row
	 * @param colName
	 * @return
	 */
	public static String getValue(SSRow head, SSRow row, String colName) {
		int index = head.getIndex(colName);
		if (index < 0) {
			return null;
		} else {
			return row.getValue(index);
		}
	}

	/**
	 * 
	 * @param test
	 * @return
	 */
	public static String getPrefix(String test) {
		if (isPrefix(Constants.PREFIX_U, test)) {
			return Constants.PREFIX_U;
		} else if (isPrefix(Constants.PREFIX_D, test)) {
			return Constants.PREFIX_D;
		} else {
			return "";
		}
	}

	/**
	 * 
	 * @param test
	 * @return
	 */

	public static String getStrippedEntry(String test) {
		if (isPrefix(getPrefix(test), test)) {
			return test.substring(2, test.length() - 1);
		} else {
			return test;
		}
	}

	/**
	 * 
	 * @param prefix
	 * @param test
	 * @return
	 */
	public static String getStrippedEntry(String prefix, String test) {
		if (isPrefix(prefix, test)) {
			return test.substring(2, test.length() - 1);
		} else {
			return test;
		}

	}

	public static String getValue(SSRow uHead, SSRow uRow, SSRow dHead,
			SSRow dRow, String test) {
		String prefix = getPrefix(test);
		if (prefix.equals(Constants.PREFIX_U)) {
			return getValue(uHead, uRow, getStrippedEntry(prefix, test));
		} else if (prefix.equals(Constants.PREFIX_D)) {
			return getValue(dHead, dRow, getStrippedEntry(prefix, test));
		} else {
			// default
			return getValue(uHead, uRow, getStrippedEntry(prefix, test));
		}
	}

	public static String getIndividualIri(String ns, String id) {
		// Individual IRIs should be dereferenceable. Therefore, when ever we
		// replace the last #
		// with a /.
		int index;
		if (((index = ns.lastIndexOf('#')) >= 0) && index == ns.length() - 1) {
			ns = ns.substring(0, ns.length() - 1) + "/";
		}
		return ns + id;
	}
}

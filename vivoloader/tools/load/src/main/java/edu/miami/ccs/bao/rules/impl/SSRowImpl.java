package edu.miami.ccs.bao.rules.impl;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import edu.miami.ccs.bao.rules.api.SSRow;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SSRowImpl implements SSRow {

	Map<Integer, String> row = new HashMap<Integer, String>();

	public void addRow(String value) {
		row.put(row.size(), value);
	}

	public int getIndex(String value) {
		if (row.containsValue(value)) {
			Set<Map.Entry<Integer, String>> entries = row.entrySet();
			for (Map.Entry<Integer, String> entryObj : entries) {
				if (entryObj.getValue().equals(value)) {
					return entryObj.getKey();
				}
			}
		}
		return -1;
	}

	public String getValue(int index) {
		return row.get(index);
	}

	public Map<Integer, String> getRow() {
		return this.row;
	}

	@Override
	public ResultSet getResultSet() {
		throw new RuntimeException("This method is not supported");
	}
}

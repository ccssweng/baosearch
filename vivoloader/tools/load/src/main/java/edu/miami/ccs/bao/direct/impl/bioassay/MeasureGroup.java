package edu.miami.ccs.bao.direct.impl.bioassay;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.AbstractConcept;
import edu.miami.ccs.bao.direct.api.Concept;
import edu.miami.ccs.bao.direct.api.Constants;
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.direct.impl.specification.AssayStage;
import edu.miami.ccs.bao.direct.impl.specification.DetectionTechnology;
import edu.miami.ccs.bao.direct.impl.target.MetaTarget;
import edu.miami.ccs.bao.direct.impl.endpoint.Endpoint;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * MeasureGroup class creates a link between measure group and its
 * relations ex: measure group has endpoint etc.
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class MeasureGroup extends AbstractConcept {

	protected final String hasDetectionTechnology = "207";

	protected final String hasEndpoint = "208";

	protected final String hasTarget = "211";

	protected final String hasDesign = "212";

	protected final String hasAssayStage = "210";

	protected final String isMeasureGroupOf = "426";

	protected final String id = "040";

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	public void visit(DirectContext directContext, Individual bioassaySubject,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int aid) {

		String shortId = aid + _unique();
		String id = Util.getIndividualIri(directContext.getConfiguration()
				.getNs(NS_BAO), Constants.INDI_PREFIX + getId_(this.id)
				+ shortId);
		Individual mySubject = createIndividual(
				getOntClass(tBoxModel, getId(this.id), directContext), id,
				shortId, aBoxModel, LabelModifier.CLASS_LABEL);
		bioassaySubject.addProperty(predicate, mySubject);

		// //////// has endpoint
		ObjectProperty op;
		op = getObjectProperty(tBoxModel, getId(hasEndpoint), directContext);
		Concept endpoint = new Endpoint();
		endpoint.visit(directContext, mySubject, op, currentRow, tBoxModel,
				aBoxModel, -1);

		// // has meta target
		op = getObjectProperty(tBoxModel, getId(hasTarget), directContext);
		Concept metaTarget = new MetaTarget();
		metaTarget.visit(directContext, mySubject, op, currentRow, tBoxModel,
				aBoxModel, -1);

		// /has detection technology
		op = getObjectProperty(tBoxModel, getId(hasDetectionTechnology),
				directContext);
		Concept detection = new DetectionTechnology();
		detection.visit(directContext, mySubject, op, currentRow, tBoxModel,
				aBoxModel, -1);

		// /has design
		op = getObjectProperty(tBoxModel, getId(hasDesign), directContext);
		Concept designConcept = new Design();
		designConcept.visit(directContext, mySubject, op, currentRow,
				tBoxModel, aBoxModel, -1);

		// is measure group of
		op = getObjectProperty(tBoxModel, getId(isMeasureGroupOf),
				directContext);
		mySubject.addProperty(op, bioassaySubject);

		// has assay stage
		op = getObjectProperty(tBoxModel, getId(hasAssayStage), directContext);
		Concept assayStage = new AssayStage();
		assayStage.visit(directContext, mySubject, op, currentRow, tBoxModel,
				aBoxModel, aid);

	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.Concept#doData(java.sql.ResultSet,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {
		// nothing
	}
}

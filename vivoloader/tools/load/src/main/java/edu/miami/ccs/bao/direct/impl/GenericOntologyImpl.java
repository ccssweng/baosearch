package edu.miami.ccs.bao.direct.impl;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.sdb.SDBFactory;
import com.hp.hpl.jena.sdb.Store;
import com.hp.hpl.jena.sdb.StoreDesc;
import com.hp.hpl.jena.sdb.sql.JDBC;
import com.hp.hpl.jena.sdb.sql.SDBConnection;
import com.hp.hpl.jena.sdb.store.DatabaseType;
import com.hp.hpl.jena.sdb.store.LayoutType;
import com.hp.hpl.jena.sdb.util.StoreUtils;
import com.hp.hpl.jena.tdb.TDB;
import com.hp.hpl.jena.tdb.TDBFactory;
import edu.miami.ccs.bao.direct.api.Concept;
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.direct.api.StoreType;
import edu.miami.ccs.bao.main.Configuration;
import edu.miami.ccs.bao.rules.api.Ontology;
import edu.miami.ccs.bao.rules.api.SSRow;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Set;


/**
 * Generic ontologyimplements the ontology for creates the sdb or tdb
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class GenericOntologyImpl implements Ontology {

	protected OntModel tBoxModel;

	protected OntModel aBoxModel;

	protected DirectContext directContext;

	protected Store store;

	public GenericOntologyImpl(DirectContext directContext) {
		this.directContext = directContext;
	}

	/*
	 * @see edu.miami.ccs.bao.rules.api.Ontology#loadOntModel(java.lang.String,
	 * java.lang.String, edu.miami.ccs.bao.direct.api.StoreType)
	 */
	public void loadOntModel(String _url, String _aBoxStore, StoreType type)
			throws Exception {

		switch (type) {
		case TDB:

		case TDB_JOSEKI:

			TDB.getContext().set(TDB.symLogExec, true);
			Model model = TDBFactory.createModel(directContext
					.getConfiguration().get(Configuration.outDirectory));
			aBoxModel = ModelFactory.createOntologyModel(
					OntModelSpec.OWL_DL_MEM, model);

			// read the ontology into this aBoxModel
			InputStream in = new FileInputStream(new File(directContext
					.getConfiguration().get(Configuration.owlFile)));
			for (Map.Entry<String, String> entry : directContext
					.getConfiguration().getAltEntrySet()) {
				aBoxModel.getDocumentManager().addAltEntry(entry.getKey(),
						entry.getValue());
			}
			aBoxModel.read(in, null);
			break;
		case SDB:

			StoreDesc storeDesc = new StoreDesc(
					LayoutType.LayoutTripleNodesHash, DatabaseType.MySQL);
			JDBC.loadDriverMySQL();
			SDBConnection conn = new SDBConnection(directContext
					.getConfiguration().get(Configuration.outSdb),
					directContext.getConfiguration().get(
							Configuration.outSdbUserName), directContext
							.getConfiguration().get(
									Configuration.outSdbPassWord));
			store = SDBFactory.connectStore(conn, storeDesc);
			if (StoreUtils.isFormatted(store)) {
				store.getTableFormatter().truncate();
			} else {
				store.getTableFormatter().create();
			}
			model = SDBFactory.connectDefaultModel(store);
			aBoxModel = ModelFactory.createOntologyModel(
					OntModelSpec.OWL_DL_MEM, model);

			tBoxModel = ModelFactory.createOntologyModel(
					OntModelSpec.OWL_DL_MEM, null);
			in = new FileInputStream(new File(directContext.getConfiguration()
					.get(Configuration.owlFile)));
			tBoxModel.read(in, null);
			// TODO: we are using the merged ontology to load data
			/*
			 * for (Map.Entry<String, String> entry :
			 * directContext.getConfiguration() .getAltEntrySet()) {
			 * aBoxModel.getDocumentManager().addAltEntry(entry.getKey(),
			 * entry.getValue()); }
			 */

			break;
		default:

		}

	}

	/*
	 * @see edu.miami.ccs.bao.rules.api.Ontology#updateModel()
	 */
	public void updateModel() {
		Concept thing = new Thing();
		StoreType type = StoreType.getType(directContext.getConfiguration()
				.get(Configuration.storeType));
		for (SSRow row : directContext.getCsvDataSource().getRows()) {
			switch (type) {
			case TDB:
			case TDB_JOSEKI:
				thing.visit(directContext, null, null, row, aBoxModel,
						aBoxModel, -1);
				break;
			case SDB:
				thing.visit(directContext, null, null, row, tBoxModel,
						aBoxModel, -1);
				break;
			}

		}
	}

	/*
	 * @see edu.miami.ccs.bao.rules.api.Ontology#write(java.io.OutputStream,
	 * edu.miami.ccs.bao.direct.api.StoreType)
	 */
	public void write(OutputStream out, StoreType type) throws Exception {

		switch (type) {
		case TDB:
		case TDB_JOSEKI:

			aBoxModel.close();
			break;
		case SDB:
			store.getConnection().close();
			store.close();
			break;
		default:
			/*
			 * RDFWriter writer = aBoxModel.getWriter("RDF/XML-ABBREV");
			 * writer.setProperty("xmlbase", directContext.getNs());
			 * aBoxModel.setNsPrefix(directContext.getPrefix(),
			 * directContext.getNs()); writer.write(aBoxModel, out,
			 * directContext.getNs()); out.flush(); out.close();
			 */
		}

	}

	public void message(String message) {

	}

	public void error(String err1, String err2) {

		System.exit(-89);
	}
}

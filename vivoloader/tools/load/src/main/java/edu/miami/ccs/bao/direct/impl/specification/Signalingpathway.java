package edu.miami.ccs.bao.direct.impl.specification;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import edu.miami.ccs.bao.direct.api.AbstractConcept;
import edu.miami.ccs.bao.direct.api.Concept;
import edu.miami.ccs.bao.direct.api.Constants;
import edu.miami.ccs.bao.direct.api.DirectContext;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * SignalingPathway class creates the subject, object and, predicate for Signaling pathway
 * for a particular assay taking the information from Annotation file
 * 
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class Signalingpathway extends AbstractConcept {

	private static String signalingPathwayId = "266";

	private static String signalingPathwayTarget = "SignalingPathwayTarget";

	private static String hasSpecification = "214";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	public void visit(DirectContext directContext, Individual subject,
			ObjectProperty predicate, SSRow currentRow, OntModel tBoxModel,
			OntModel aBoxModel, int mult) {
		// impl
		// let do a simple error handling
		String potentialIndividualName = Util.getValue(directContext
				.getCsvDataSource().getHead(), currentRow,
				signalingPathwayTarget);

		// error checking
		if (potentialIndividualName == null
				|| potentialIndividualName.length() == 0) {
			warning("description name is empty");
		} else {
			potentialIndividualName = potentialIndividualName.toLowerCase();
			// OntClass clazz = getOntClass(tBoxModel,
			// getId(signalingPathwayId), directContext);

			// special individual exists
			/*
			 * if (potentialIndividualName == null ||
			 * potentialIndividualName.length() == 0) { potentialIndividualName
			 * = clazz.getLocalName(); }
			 */

			/*
			 * String id =
			 * Util.getIndividualIri(directContext.getConfiguration()
			 * .getNs(NS_GO), Constants.INDI_PREFIX + clazz.getLocalName() +
			 * _unique()); Individual mySubject = createIndividual(clazz, id,
			 * potentialIndividualName, aBoxModel, LabelModifier.CLASS_LABEL);
			 * subject.addProperty(predicate, mySubject);
			 * 
			 * ObjectProperty op = getObjectProperty(tBoxModel,
			 * getId(hasSpecification), directContext); Concept metaTargetSpec =
			 * new MetaTargetSpec(); metaTargetSpec .visit(directContext,
			 * mySubject, op, currentRow, tBoxModel, aBoxModel, -1);
			 */

			// TODO; This modeling assumption does not reflect BAO ontology

			String[] pintoks = potentialIndividualName.split(";");
			for (String pin : pintoks) {
				Individual mySubject = createGlobalIndividualParam2(
						pin,
						signalingPathwayId,
						LabelModifier.SHORT_ID,
						null,
						directContext.getGlobalInformation().signalingpathwayMapping,
						directContext, subject, predicate, tBoxModel, aBoxModel);

				// baoseach link to bioassay
				// link back to bio assay
				addBaoSearchObjectPropertyToBioAssay(directContext, tBoxModel,
						aBoxModel, currentRow, mySubject, "hasTarget",
						"isTargetOf");
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.miami.ccs.bao.direct.api.Concept#doData(java.sql.ResultSet,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {
		// not used
	}
}

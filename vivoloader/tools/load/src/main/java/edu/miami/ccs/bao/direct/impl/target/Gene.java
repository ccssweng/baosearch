package edu.miami.ccs.bao.direct.impl.target;

/**
 *Copyright (c) 2011, The University of Miami
 *All rights reserved.

 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the University of Miami nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 *THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF MIAMI BE LIABLE FOR ANY
 *DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
import com.hp.hpl.jena.ontology.*;
import edu.miami.ccs.bao.direct.api.*;
import edu.miami.ccs.bao.rules.api.SSRow;
import edu.miami.ccs.bao.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Gene class creates the subject, object and, predicate for gene
 * for a particular assay taking the information from Annotation file
 * 
 * 
 * @author akoleti, Sam Center for Computational Science University of Miami
 * 
 */
public class Gene extends AbstractConcept {

	private static String gene = "Gene";

	private static String geneId = "GeneID";

	protected final String id = "582";

	protected final String hasId = "383";

	protected final String hasIdValue = "203";

	protected final String geneIdSpec = "380";

	/*
	 * @see
	 * edu.miami.ccs.bao.direct.api.Concept#visit(edu.miami.ccs.bao.direct.api
	 * .DirectContext, com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel, int)
	 */
	@Override
	public void visit(DirectContext directContext,
			Individual biologicalProcess, ObjectProperty predicate,
			SSRow currentRow, OntModel tBoxModel, OntModel aBoxModel, int mult) {

		String geneIndividualName = Util.getValue(directContext
				.getCsvDataSource().getHead(), currentRow, gene);

		// error checking
		if (geneIndividualName == null || geneIndividualName.length() == 0) {
			warning("description name is empty");
		} else {
			OntClass clazz = getOntClass(tBoxModel, getId(this.id),
					directContext);

			GlobalInformation gInfo = directContext.getGlobalInformation();
			List<NameToId> nameToIdList = gInfo.geneMapping
					.get(geneIndividualName);
			if (nameToIdList == null) {
				ArrayList<NameToId> toIds = new ArrayList<NameToId>();
				NameToId specificObj = new NameToId(geneIndividualName,
						_unique());
				toIds.add(specificObj);
				gInfo.geneMapping.put(geneIndividualName, toIds);
				String id = Util.getIndividualIri(directContext
						.getConfiguration().getNs(NS_BAO),
						Constants.INDI_PREFIX + clazz.getLocalName()
								+ specificObj.getId());
				Individual ind1 = createIndividual(clazz, id,
						geneIndividualName, aBoxModel,
						LabelModifier.CLASS_LABEL);

				// set the id
				String geneIndividualId = Util.getValue(directContext
						.getCsvDataSource().getHead(), currentRow, geneId);

				if (geneIndividualId != null && geneIndividualId.length() > 0) {
					ObjectProperty op2 = getObjectProperty(tBoxModel,
							getId(hasId), directContext);
					OntClass clazz2 = getOntClass(tBoxModel,
							getId(this.geneIdSpec), directContext);
					String id2 = Util.getIndividualIri(directContext
							.getConfiguration().getNs(NS_BAO),
							Constants.INDI_PREFIX + clazz2.getLocalName()
									+ _unique());
					Individual ind2 = createIndividual(clazz2, id2,
							geneIndividualId, aBoxModel,
							LabelModifier.CLASS_LABEL);

					DatatypeProperty dp = getDatatypeProperty(tBoxModel,
							getId(hasIdValue), directContext);
					addLiteral(ind2, dp, 'i', aBoxModel, geneIndividualId);
					ind1.addProperty(op2, ind2);
				}
			}

			// add the gene data to the biological process

			nameToIdList = gInfo.geneMapping.get(geneIndividualName);
			NameToId specificObj = nameToIdList.get(nameToIdList
					.indexOf(NameToId.searchObj(geneIndividualName)));
			String id = Util.getIndividualIri(
					directContext.getConfiguration().getNs(NS_BAO),
					Constants.INDI_PREFIX + clazz.getLocalName()
							+ specificObj.getId());
			Individual specificIndividual = getABoxIndividual(aBoxModel, id);
			biologicalProcess.addProperty(predicate, specificIndividual);

		}
	}

	/*
	 * @see edu.miami.ccs.bao.direct.api.Concept#doData(java.sql.ResultSet,
	 * edu.miami.ccs.bao.direct.api.DirectContext,
	 * com.hp.hpl.jena.ontology.Individual,
	 * com.hp.hpl.jena.ontology.ObjectProperty,
	 * edu.miami.ccs.bao.rules.api.SSRow, com.hp.hpl.jena.ontology.OntModel,
	 * com.hp.hpl.jena.ontology.OntModel)
	 */
	@Override
	public void doData(ResultSet rset, DirectContext directContext,
			Individual subject, ObjectProperty predicate, SSRow currentRow,
			OntModel tBoxModel, OntModel aBoxModel) throws SQLException {
		// Not implemented yet
	}
}
